### Task

<!-- Add the reference to the task on ClickUp -->

[ClickUp Task](https://app.clickup.com/t/)<!--append url with task id. Note: it does not include 'CU-'-->

### Summary

<!-- Summarize the investigation and the solution -->

### Scope of the fix (optional)

<!-- In case the issue is only partially fixed by this current PR, add the details of what is not fixed -->

### Additional notes for the reviewer

<!-- Any notes for the developer to aid testing/reviewing the PR -->
