
package com.letstransport.android.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.util.SystemPropertyUtils;
import org.testng.Assert;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import com.letstransport.listener.MyExtentListners;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileBrowserType;

public class AddVehicle {
	// android driver instance creation
	// WebDriver wdriver;
	AndroidDriver driver;

	public AddVehicle(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;

		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Home Page **/

	@FindBy(xpath = "//*[contains(text(),'Vehicles')]")
	private WebElement vehTab;

	public WebElement clkVehicles() {
		return vehTab;
	}

	@FindBy(xpath = "//button[contains(text(),'Add')]")
	private WebElement addFirstVeh;

	public WebElement ClkaddFirstVeh() {
		return addFirstVeh;
	}

	@FindBy(xpath = "//input")
	private WebElement enterVehicleNumber;

	public WebElement enterVehicleNum() {
		return enterVehicleNumber;
	}

	/*
	 * @FindBy(xpath="//*[@id='app']/div/div[2]/div[1]/div/div/div[4]") private
	 * WebElement addVeh;
	 */
	@FindBy(xpath = "//div[text()='Add vehicle']")
	private WebElement addVeh;

	public WebElement clkAddveh() {
		return addVeh;
	}

	@FindBy(xpath = "//*[@id='app']/div/div[2]/div[2]/div/div/div/a/div[1]/div/div/div[1]/div")
	private WebElement vehCreated;

	public WebElement verVeh() {
		return vehCreated;
	}

	@FindBy(xpath = "//*[@id='app']/div/div[2]/div[2]/div[1]")

	private WebElement vehCreationPage;

	public WebElement verifyVehNum() {
		return vehCreationPage;
	}

	@FindBy(xpath = "//*[@id='app']/div/div[2]/div[1]/div/div/div/div/div[2]/div[3]")

	private WebElement ref;

	public WebElement clkRef() {
		return ref;
	}

	@FindBy(xpath = "//div[text()='Vehicle RC']")

	private WebElement RC;

	public WebElement clkRC() {
		return RC;
	}

	@FindBy(xpath = "//*[contains(text(),'Take photo')]")

	private WebElement tkPh;

	public WebElement clkTkPh() {
		return tkPh;
	}
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.android.camera:id/shutter_button']")
	private WebElement tapCamera;

	@FindBy(xpath = "//*[@text='Choose From Gallery']")

	private WebElement gallery;

	public WebElement clkGallery() {
		return gallery;
	}

	@FindBy(xpath = "//android.widget.LinearLayout/descendant::android.widget.FrameLayout/descendant::android.widget.ListView/child::android.widget.TextView[@text='Choose From Gallery']")
	private WebElement fromGallery;
	@FindBy(xpath = "//android.widget.LinearLayout/descendant::android.widget.TextView[@text='Photos']")
	private WebElement album;
	@FindBy(xpath = "//android.widget.TextView[@text='Camera']")
	private WebElement camera;
	@FindBy(xpath = "//android.view.ViewGroup[@index='1']")
	private WebElement selectphoto;

	public void _clickOnGallery() throws Exception {
		MobileActionUtil.waitForElement(fromGallery, driver, "FromGallery", 50);
		MobileActionUtil.clickElement(fromGallery, driver, "FromGallery");
		MobileActionUtil.waitForElement(album, driver, "Album", 50);
		MobileActionUtil.clickElement(album, driver, "Album");
		MobileActionUtil.waitForElement(camera, driver, "Button", 50);
		MobileActionUtil.clickElement(camera, driver, "Button");
		MobileActionUtil.clickElement(selectphoto, driver, "Select photo from gallery");
		Thread.sleep(1000);

	}

	/*@FindBy(xpath = "//*[@text='Photos']")

	private WebElement albm;*/
	@FindBy(xpath = "//*[@text='Camera']")

	private WebElement albm;

	public WebElement clkAlbum() {
		return albm;
	}

	/*@FindBy(xpath = "//android.view.ViewGroup[@index='1']")
	private WebElement pic;*/
	@FindBy(xpath = "//android.view.ViewGroup[@index='3']")
	private WebElement pic;

	public WebElement selPic() {
		return pic;
	}

	@FindBy(xpath = "//div[@class='_16d8d5b0']/div/a/following-sibling::div/div/div[2]")
	private WebElement drivername;

	@FindBy(xpath = "//*[contains(text(),'under verification')]")
	private WebElement docsUnderVerification;

	public WebElement linkdocsUnderVerification() {
		return docsUnderVerification;
	}
	@FindBy(xpath="//android.widget.TextView[@text='Photos']")
	private WebElement photos;

	@FindBy(xpath = "//*[@id='app']/div/div[1]/div[1]")
	private WebElement goback;

	public WebElement clkGoback() {
		return goback;
	}

	@FindBy(xpath = "//*[@id='js--sc--container']/canvas")
	private WebElement scrth;

	public WebElement vryScrd() {
		return scrth;
	}

	public void _handleReferral(AndroidDriver driver) throws Exception {
		Thread.sleep(20000);

		try {
			System.out.println("--Method waiting to handle referral pop up---------");
			MobileActionUtil.clickElementWithoutWait(ref, driver, "Referral");
		} catch (Exception e) {
			System.out.println("--Referral pop up not displayed---------");

		}

	}

	public void _addVehicles(AndroidDriver driver, String vehNum ) throws Exception{

		Thread.sleep(20000);
		//Thread.sleep(15000);
		MobileActionUtil.clickElementWithoutWait(vehTab, driver, "Vehicle Tab");
		/*Thread.sleep(15000);*/
		Thread.sleep(8000);
		MobileActionUtil.switchToView(driver);
		Thread.sleep(10000);
		MobileActionUtil.clickElementWithoutWait(addFirstVeh, driver, "AddVehicle");
		MobileActionUtil.clearAndType(enterVehicleNumber, vehNum, "Vehicle Number", driver);
		MobileActionUtil.clickElementWithoutWait(addVeh, driver, "Add Vehicle");			
		
	}


	

	public void _verifyVehicles(AndroidDriver driver, String vehnum) throws Exception{
		

		Thread.sleep(15000);
		MobileActionUtil.verifyElementContainsText(vehCreated, vehnum, driver);
	}


	public WebElement userTypeLnk(String vehnum) {
		return driver.findElement(By.xpath("//div[text()='" + vehnum + "']"));
	}

	public WebElement vehicleDocType(String docType) {
		return driver.findElement(
				By.xpath("//div[@class='_9336c0b3']/descendant::div[@class='bd57a5cb']/div[text()='docType']"));
	}

	public WebElement userType(String docType) {
		return driver.findElement(By.xpath("//*[contains(text(),'"+docType+"')]"));
			}

	@FindBy(xpath = "//div[@class='b3d6dd4a null'][2]/span[text()='Vehicles']")
	private WebElement vehicleLink;
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.android.camera:id/done_button']")
	private WebElement okButton;

	public void _clickOnVehicleLink() throws Exception {
		MobileActionUtil.waitTillElementVisible(vehicleLink, driver, "Driver", 50);
		MobileActionUtil.actionClick(vehicleLink, driver, "Driver");
	}

	@FindBy(xpath = "//div[@class='_71efa8c4']")
	private List<WebElement> vehicleTemplate;

	@FindBy(xpath = "//div[@class='_467a410f']")
	private WebElement backBtn;

	public void _clickOnBackButton() throws Exception {
		MobileActionUtil.waitForElement(backBtn, driver, "Click On Back Button", 50);
		MobileActionUtil.clickElement(backBtn, driver, "Click On Back Button");
	}

	@FindBy(xpath = "//div[text()='Vehicle verified'][1]")
	private WebElement vehicleVerified;
	@FindBy(xpath = "//div[text()='Change Driver'][1]")
	private WebElement changeDriver;
	@FindBy(xpath = "//div[text()='Change Driver']/../../descendant::div[@class='efc324f6'][1]")
	private WebElement vehicleNo;
	@FindBy(xpath = "//div[text()='ADD DRIVER'][1]")
	private WebElement addDriver1st;
	@FindBy(xpath="//android.view.ViewGroup[@content-desc='Photo taken on 30-Jun-2020 6:59:12 PM']")
	private WebElement selectPhoto;

	public void _uploadAllDocument(String docType) throws Exception {
		MobileActionUtil.clickElementWithoutWait(userType(docType), driver, "Vehicle Docs");
		MobileActionUtil.clickElementWithoutWait(tkPh, driver, "Take Photo");
		Thread.sleep(1000);
		driver.context("NATIVE_APP");
		Thread.sleep(1000);
		_clickOnGallery();
		Thread.sleep(1000);
		MobileActionUtil.switchToView(driver);
		Thread.sleep(2000);
		_clickOnBackButton();
	}

	@FindBy(xpath = "//div[@class='c4c69f04']/input[1]")
	private WebElement driverMobileNoTextField;
	@FindBy(xpath = "//div[@class='f2f20e99']/div[1]/div[2]")
	private WebElement mobTextfield;
	@FindBy(xpath = "//div[@class='c4c69f04']/input[@placeholder='Driver Name']")
	private WebElement driverName;
	@FindBy(xpath = "//div[@class='_00d0dffd']")
	private WebElement addDriverinfo;
	@FindBy(xpath = "//div[@class='_55d0e7d8']/img")
	private WebElement closeImg;
	@FindBy(xpath = "//div[@class='cad2ed5b']/div[2]/child::div/following-sibling::span[text()='Driver']")
	private WebElement Driver;
	@FindBy(xpath = "//div[text()='Reached at Hub'][1]")
	private WebElement reachedHub;
	@FindBy(xpath = "//div[text()='Trip completed'][1]")
	private WebElement tripCompleted;

	public void _changeDriverNameTripDisplay() throws Exception {
		MobileActionUtil.clickElementWithoutWait(vehTab, driver, "Vehicle Tab");
		// String nameOfDriver = _getDriverName();
		Thread.sleep(3000);
		if (vehicleVerified.isDisplayed() && changeDriver.isDisplayed()) {
			String drivernam = drivername.getText();
			MobileActionUtil.actionClick(changeDriver, driver, "click on change driver");
			// MobileActionUtil.clickElement(driverMobileNoTextField, driver,
			// "Click On mobile no Text Field");
			Thread.sleep(3000);
			String value = mobTextfield.getText();
			System.out.println(value);
			MobileActionUtil.actionClick(closeImg, driver, "close image");
			Thread.sleep(3000);
			MobileActionUtil.swipeBottomToTop(2, driver, 0.90, 0.50);
			if (vehicleVerified.isDisplayed() && addDriver1st.isDisplayed()) {
				MobileActionUtil.clickElement(addDriver1st, driver, "Click On Add Driver");
				MobileActionUtil.clickElement(driverMobileNoTextField, driver, "Click On mobile no Text Field");
				MobileActionUtil.clearAndType(driverMobileNoTextField, value, "MobileNoTextField", driver);
				Thread.sleep(1000);
				MobileActionUtil.clickElement(driverName, driver, "Driver");
				MobileActionUtil.clearAndType(driverName, drivernam, "Nmae of Driver", driver);
				Thread.sleep(2000);
				MobileActionUtil.clickElement(addDriverinfo, driver, "Add driver info");
				Thread.sleep(7000);
				_clickOnBackButton();
				// Thread.sleep(3000);
				MobileActionUtil.clickElement(Driver, driver, "Driver");
				Thread.sleep(3000);
				if (reachedHub.isDisplayed()) {
					MobileActionUtil.verifyElementIsDisplayed(reachedHub, driver, "reached Hub");
				} else if (tripCompleted.isDisplayed()) {
					MobileActionUtil.verifyElementIsDisplayed(tripCompleted, driver, "Trip Completed");

				}

			}

		}
	}

	public void _UploadDocumentToExistingProfile() throws Exception {
		;
		String string1 = "KA01";

		String string2 = MobileActionUtil.randomString(4);
		String vecno = MobileActionUtil.concat(string1, string2);
		_addVehicles(driver, vecno);
		Thread.sleep(3000);
		for (WebElement we : vehicleTemplate) {
			if (we.getText().equals(vecno)) {
				MobileActionUtil.clickElementWithoutWait(we, driver, "vehicletemplate");
				Thread.sleep(1000);

			}
			break;
		}

		_uploadAllDocument("Vehicle RC *");
		Thread.sleep(1000);
		_uploadAllDocument("Vehicle FC (Optional)");
		Thread.sleep(500);
		_uploadAllDocument("Vehicle Insurance *");
		Thread.sleep(500);
		_uploadAllDocument("Vehicle Back Image *");
		Thread.sleep(500);

	}
	@FindBy(xpath="//div[@class='_2f7c3201']/form/div[@class='b89c1c5d']")
	private WebElement takephoto;

	public void _uploadDocs(AndroidDriver driver, String vehnum, String docType) throws Exception{
		
		//Thread.sleep(15000);
		Thread.sleep(15000);
		MobileActionUtil.clickElement(userTypeLnk(vehnum), driver, "Vehicle document");
		Thread.sleep(5000);
		MobileActionUtil.clickElement(userType(docType), driver, "Vehicle Docs");
		Thread.sleep(5000);
		//MobileActionUtil.clickElement(tkPh, driver, "Take Photo");
		MobileActionUtil.clickElement(takephoto, driver, "Take Photo");
				
		
	
	}


	public void _takephoto( AndroidDriver driver) throws Exception {
		
		
		MobileActionUtil.clickElement(gallery, driver, "Choose from Gallery");
		Thread.sleep(3000);
		MobileActionUtil.clickElement(albm, driver, "Album");
		Thread.sleep(3000);
		MobileActionUtil.clickElement(pic, driver, "Pic");
		Thread.sleep(3000);
		}
	
   public void _takeohotovivo(AndroidDriver driver) throws Exception{
	 
	   MobileActionUtil.actionClick(gallery, driver, "Choose from Gallery");
		Thread.sleep(2000);
	   MobileActionUtil.clickElement(photos, driver, "click on photos");
	   Thread.sleep(2000);
	   MobileActionUtil.actionClick(selectPhotoFolder, driver, "click on photo folder");
	   Thread.sleep(2000);
	   //MobileActionUtil.actionClick(selectPhoto, driver, "selectphoto");
	   MobileActionUtil.clickElement(selectPhoto, driver, "selectphoto");
	   Thread.sleep(10000);
		
   }
   @FindBy(xpath="//android.support.v7.widget.RecyclerView/descendant::android.widget.RelativeLayout[1]")
   private WebElement selectPhotoFolder;
	
	public void goback(AndroidDriver driver) throws Exception{
		
		MobileActionUtil.clickElement(goback, driver, "Goback");
	}
	

	public void verify_ScratchCard(AndroidDriver driver) throws Throwable{
		Thread.sleep(5000);		
		
//		MobileActionUtil.verifyElementIsDisplayed(scrth, driver, "Scratch Card");
		
		boolean bo = driver.findElement(By.xpath("//*[@id='ic_add_circle_24px']")).isDisplayed();
		
}

	public String _getVehicleNo() throws Exception {
		// Thread.sleep(5000);
		String vehiclenum = "";

		MobileActionUtil.clickElementWithoutWait(vehTab, driver, "Vehicle Tab");
		Thread.sleep(5000);
		if (vehicleVerified.isDisplayed() && changeDriver.isDisplayed()) {
			vehiclenum = vehicleNo.getText();
			System.out.println(vehiclenum);

		} else {

			MobileActionUtil.swipeBottomToTop(1, driver, 0.75, 0.65);
			if (vehicleVerified.isDisplayed() && changeDriver.isDisplayed()) {
				vehiclenum = vehicleNo.getText();
				System.out.println(vehiclenum);
			}
		}
		return vehiclenum;

	}

	public String _getDriverName() throws Exception {
		String drivernam = "";
		if (vehicleVerified.isDisplayed() && changeDriver.isDisplayed()) {
			drivernam = drivername.getText();
			System.out.println(drivernam);
		}
		return drivernam;
	}

	public String _getVehicleInfoIn() throws Exception {
		String mobvalue = "";
		// MobileActionUtil.clickElementWithoutWait(vehTab, driver, "Vehicle
		// Tab");
		if (vehicleVerified.isDisplayed() && changeDriver.isDisplayed()) {

			MobileActionUtil.clickElement(changeDriver, driver, "click on change driver");
			mobvalue = mobTextfield.getAttribute("value");
			// MobileActionUtil.clickElement(closeImg, driver, "close image");

		}
		return mobvalue;
	}
	
	
	public void _verifyVehiclesDel(AndroidDriver driver, String vehnum) throws Exception{
		
		Thread.sleep(20000);
		
		MobileActionUtil.notContainsText(vehCreationPage, vehnum, driver);
		
	}
	

}

