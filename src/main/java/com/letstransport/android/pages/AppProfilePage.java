package com.letstransport.android.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.MobileActionUtil;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class AppProfilePage {

	AndroidDriver driver;

	public AppProfilePage(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** upcoming Page **/
	@FindBy(xpath = "//div[text()='Personal Documents']")
	private WebElement personalDocuments;

	public WebElement getPersonalDocuments() {
		return personalDocuments;
	}

	public void clickOnPersonalDocuements() throws Exception {
		MobileActionUtil.clickElement(this.personalDocuments, this.driver, "Personal Documents ");
	}

	@FindBy(xpath = "//div[text()='Bank Account Documents']")
	private WebElement bankAccountDocuments;

	public WebElement getBankAccountDocuments() {
		return bankAccountDocuments;
	}
	
	public void clickOnBankDocuements() throws Exception {
		MobileActionUtil.clickElement(this.bankAccountDocuments, this.driver, "Bank Acccount Documents ");
	}


	@FindBy(xpath = "//div[text()='Driving Licence (Optional)']")
	private WebElement drivingLicence;
	
	@FindBy(xpath = "//div[text()='PAN Card *']")
	private WebElement panCard;

	public WebElement getDrivingLicence() {
		return drivingLicence;
	}

	@FindBy(xpath = "//div[text()='Passbook Photo']")
	private WebElement passBookPhoto;

	public WebElement getPassbookPhoto() {
		return passBookPhoto;
	}

	@FindBy(xpath = "//div[text()='Take photo']")
	private WebElement takePhotoBtn;

	public WebElement getTakePhotoBtn() {
		return takePhotoBtn;
	}

	@FindBy(xpath = "//android.widget.TextView[@text='Choose From Gallery']")
	private WebElement chooseFromGallery;

	public WebElement getChooseFromGallery() {
		return chooseFromGallery;
	}

	@FindBy(xpath = "//android.widget.TextView[@text='Camera']")
	private WebElement camerafolder;

	public WebElement getCamerafolder() {
		return camerafolder;
	}

	@FindBy(xpath = "//android.view.ViewGroup[@index='1']")
	private WebElement galleryImage;

	public WebElement getCameraImage() {
		return galleryImage;
	}

	// we can use for wait after uploading image
	@FindBy(xpath = "//div[text()='Change photo']")
	private WebElement changePhoto;

	public WebElement getChangephoto() {
		return changePhoto;
	}
	
	@FindBy(xpath = "//*[text()='Personal Documents']/../..//*[text()='Document rejected']")
	private WebElement docuementRejected;

	public WebElement getdocuementRejected() {
		return docuementRejected;
	}

	@FindBy(xpath = "//div[text()='Driving Licence (Optional)']/../..//span[text()='Document rejected']")
	private WebElement driverLicenceRejected;

	public WebElement getDriverLicenceRejected() {
		return driverLicenceRejected;
	}
	
	@FindBy(xpath = "//div[text()='Bank Account Documents']/../..//span[text()='Document rejected']")
	private WebElement bankDocuementRejected;
	
	@FindBy(xpath = "//div[text()='Passbook Photo']/../..//span[text()='Document rejected']")
	private WebElement passBookRejected;
	
	@FindBy(xpath = "//div[text()='Change photo']")
	private WebElement changePhotoBtn;
	
	@FindBy(xpath = "//span[text()='Yes']")
	private WebElement confirmBtn;

	@FindBy(xpath = "//*[text()='Log out']")
	private WebElement logOutBtn;
	
	public void uploadDrivingLicence(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		MobileActionUtil.clickElement(this.personalDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.drivingLicence, this.driver, "Driving Licence");
		MobileActionUtil.clickElement(this.takePhotoBtn, this.driver, "Take photo Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Camera Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);
	}

	public void uploadBankAccountDocuments(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		Thread.sleep(3000);
		MobileActionUtil.clickElement(this.bankAccountDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.passBookPhoto, this.driver, "Driving Licence");
		MobileActionUtil.clickElement(this.takePhotoBtn, this.driver, "Take photo Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Gallery Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);

	}
	
	public void uploadPanCard(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		Thread.sleep(3000);
		MobileActionUtil.clickElement(this.personalDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.panCard, this.driver, "Pan Card");
		MobileActionUtil.clickElement(this.takePhotoBtn, this.driver, "Take photo Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Camera Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);
	}
	
	public void verifyPersonalDocuementRejected() throws Exception {
		Thread.sleep(5000);
		MobileActionUtil.verifyEqualsText("verify Personal Docuement rejected", this.docuementRejected.getText(), "Document rejected");
	}
	
	public void getLogOutBtn(AndroidDriver driver) throws Exception {
		MobileActionUtil.clickElement(this.logOutBtn, driver, "Logout of app");
		MobileActionUtil.clickElement(this.confirmBtn, driver, "yes button app");
	}
	
	public void verifyDriverLicencetRejected(AndroidDriver adriver) throws Exception {
		Thread.sleep(5000);
		MobileActionUtil.verifyEqualsText("verify Driver Licence rejected", this.driverLicenceRejected.getText(), "Document rejected");
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
	}
	
	public void verifyBankDocuementRejected() throws Exception {
		Thread.sleep(5000);
		MobileActionUtil.verifyEqualsText("verify Bank Docuement rejected", this.bankDocuementRejected.getText(), "Document rejected");
	}
	
	public void verifyPassbookPhotoRejected(AndroidDriver adriver) throws Exception {
		Thread.sleep(5000);
		MobileActionUtil.verifyEqualsText("verify Passbook rejected", this.passBookRejected.getText(), "Document rejected");
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
	}
	
	public void changeDrivingLicence(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		MobileActionUtil.clickElement(this.personalDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.drivingLicence, this.driver, "Driving Licence");
		MobileActionUtil.clickElement(this.changePhotoBtn, this.driver, "change photo Button");
		MobileActionUtil.clickElement(this.confirmBtn, this.driver, "yes Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Gallery Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);

	}
	
	public void changePanCard(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		MobileActionUtil.clickElement(this.personalDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.panCard, this.driver, "Pan card");
		MobileActionUtil.clickElement(this.changePhotoBtn, this.driver, "change photo Button");
		MobileActionUtil.clickElement(this.confirmBtn, this.driver, "yes Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Gallery Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);
	}
	
	public void changeBankAccountDocuments(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		MobileActionUtil.clickElement(this.bankAccountDocuments, this.driver, "Personal Documents ");
		MobileActionUtil.clickElement(this.passBookPhoto, this.driver, "Driving Licence");
		MobileActionUtil.clickElement(this.changePhotoBtn, this.driver, "change photo Button");
		MobileActionUtil.clickElement(this.confirmBtn, this.driver, "yes Button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		MobileActionUtil.clickElement(this.chooseFromGallery, this.driver, "Choose From Gallery");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getFolder(adriver,"Camera"), this.driver, "Choose From photos folder");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.getCameraImage(adriver,3), this.driver, "Gallery Image");
		Thread.sleep(30000);
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		adriver.pressKey(new KeyEvent(AndroidKey.BACK));
		MobileActionUtil.switchToWebView(adriver);
	}
	
	public WebElement getCameraImage(AndroidDriver driver, int number) throws Exception {
		return driver.findElement(By.xpath("//android.view.ViewGroup[@index='"+number+"']"));
	}
	
	public WebElement getFolder(AndroidDriver driver,String folder) throws Exception {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='"+folder+"']"));
	}
}
