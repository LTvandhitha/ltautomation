
package com.letstransport.init;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.github.javafaker.Faker;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class GlobalVariables implements TestDataCoulmns {

	public AndroidDriver<MobileElement> aDriver;
	public WebDriver wDriver;
	public NgWebDriver ngWebDriver;
	public DesiredCapabilities capabilities;
	public String browser;
	public int iPort;
	public String sUDID;
	public String sVersion;
	public String sDeviceName;
	public String sAutomationName;
	public String sChillingCenter;

	// generate 10 digit mobile number
	public String randomMobileNumber = MobileActionUtil.generateRandomNumber(10);
	public String appTimeStamp = WebActionUtil.fetchSystemDateTime("dd-MMM hh:mmaa");
	public String portalTimeStamp = WebActionUtil.fetchSystemDateTime("dd-MMM hh:mm aa");
	public String openingTimeStamp = WebActionUtil.fetchSystemDateTime("h:mmaa");
	public String closingTimeStamp = WebActionUtil.fetchSystemDateTime("hh:mmaa");
	public String createdTimeStamp = WebActionUtil.fetchSystemDateTime("hh:mm aa");
	public String displayedTimeStamp = WebActionUtil.fetchSystemDateTime("h:mm aa");
	Faker faker = new Faker();
	public String randomName = faker.name().firstName();
	public String randomName1 = faker.name().firstName();
	public String randomName2 = faker.name().firstName();
	public String randomName3 = faker.name().firstName();
	public String randomName4 = faker.name().firstName();
	public String randomName5 = faker.name().firstName();
	// Property Files Values

	public String sPortal_Url = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PORTAL_URL");
	public String sPortal_Username = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PORTAL_USERNAME");
	public String sPortal_Password = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PORTAL_PASSWORD");
	public String spricing_Username = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PRICING_USERNAME");
	public String spricing_Password = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PRICING_PASSWORD");
	public String sMobile_webView = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "MOBILE_WEBVIEW");
	public String sImage = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "PHOTO_PATH");
	public String sREQUESTED_FILE = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "REQUESTED_FILE");
	public String sREJECTED_FILE = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "REJECTED_FILE");
	public String sRAISED_FILE = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "RAISED_FILE");
	public String sCOMMERCIAL_PATH = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "COMMERCIAL_PATH");
	public String sCORRECTED_FILE = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "CORRECTED_FILE");
	public String sREQ_STATUS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "REQ_STATUS");
	public String sREJ_STATUS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "REJ_STATUS");
	public String sRET_STATUS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "RET_STATUS");
	public String sRAI_STATUS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "RAI_STATUS");
	public String sCOR_STATUS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "COR_STATUS");
	public String sVEHICLE_NUMBER = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "VEHICLE_NUMBER");
	public String sINVEHICLE_NUMBER = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "INVEHICLE_NUMBER");
	public String pVEHICLE_NUMBER = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "VEHICLE_NUMBER_DP");
	public String sSALE_USER = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "SALE_USER");
	public String sSALE_PASS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "SALE_PASSWORD");
	public String sFINANCE_USER = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "FINANCE_USER");
	public String sFINANCE_PASS = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "FINANCE_PASSWORD");
	public String sCLIENT_URL = GenericLib.getProprtyValue(GenericLib.sUserCredFile, "CLIENT_ONBOARDING");

	// Listing data
	public int cCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Client Name");
	public int hcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Hub Name");
	public int rdtCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing",
			"Reporting Date&Time");
	public int vtcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Vehicle Tonnage");
	public int vtyCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Vehicle Type");
	public int vbCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Vehicle Body");
	public int smCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Select Material");
	public int swtCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Select Work Type");
	public int rCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Revenue");
	public int atpCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Amount To Be Paid");
	public int spCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "StartPoint");
	public int epCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "EndPoint");
	public int ldtCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "DisplayedTime");
	public int ldcCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "PhoneNumber");
	public int lvnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "VehicleNumber");
	public int ldCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "Date");
	public int aNCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Web_Listing", "AreaName");

	// SignIn data
	public int mCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "Mobile Number");

	// App Listing data
	public int chCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "Client Hub");
	public int baCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "Booking Amount");
	public int rdCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "Reporting date&time");
	public int aCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "Action");
	public int sPCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "StartPoint");
	public int ePCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "App_Listing", "EndPoint");

	// Signup data
	public int pnCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Onboarding", "partnerName");
	public int dCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Onboarding", "date");

	// Personal docuements data
	public int dlCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"driveringLicence");
	public int pnoCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"panCard");
	public int dobCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"dOB");
	public int dnCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"driverName");
	public int doeCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"dOE");

	// Bank Account Docuements data
	public int ifscCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"iFSCCode");
	public int anCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"accountNumber");
	public int bnCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Driver_Licence_Bank_Docuements",
			"benifitiaryName");

	// Booking data
	public int cnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "clientName");
	public int tCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "timings");
	public int dcCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "driverContact");
	public int drCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "driverName");
	public int vnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "vehicleNumber");
	public int dpCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "driverPricing");
	public int arCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "area");
	public int hCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "clientHub");
	public int dtCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "date");
	public int okCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "openingKm");
	public int ckCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "closingKm");
	public int otCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "openingTime");
	public int ctCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "closingTime");
	public int fdCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "fromDate");
	public int ivnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "incorrectVN");
	public int bctCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "createdTime");
	public int bdtCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Add_Booking", "displayedTime");

	// Driver verification Data
	public int dvnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "driverName");
	public int dvvnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "vehicleNumber");
	public int dvanCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify",
			"driverAdharNumber");
	public int dvonCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "ownerNumber");
	public int dvoanCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify",
			"OwnerAdhaarNumber");
	public int dvbnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "ownerBankName");
	public int dvobanCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify",
			"ownerAccountNumber");
	public int bifscCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "ifscCode");
	public int dvpnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "panNumber");
	public int dvlcCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Verify", "licenceNumber");

	// Requested Payment
	public int rpcnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "clientName");
	public int rpvnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment",
			"vehilceNumber");
	public int rpsdCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "startDate");
	public int rpedCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "endDate");
	public int rpdnCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "dateNo");
	public int rpamCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "amount");
	public int rpctCount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Requested_Payment", "cityName");
	
	//Hub data
	public int hubcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "hubarea");
	public int hubupdate = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "hubareaupdate");
	
	//Driver Expense
	public int drvncount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Expense", "vehicleNumber");
	public int drcncount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Expense", "clientName");
	public int dre1count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Expense", "expense1");
	public int dre2count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Driver_Expense", "expense2");
	
	//Client OnBoard
	public int cobitcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Industry Type");
	public int cobpncount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Pan Number");
	public int cobrocount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Registered office address");
	public int cobgstcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "GSTIN Number");
	public int cobhscount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub State");
	public int cobbacount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Billing Address");
	public int cobfn1count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Name 1");
	public int cobfe1count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Email 1");
	public int cobfc1count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Number 1");
	public int cobfn2count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Name 2");
	public int cobfe2count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Email 2");
	public int cobfcn2count = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Number 2");
	public int cobocncount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Operations Cntct Name");
	public int cobocecount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Operations Cntct Email");
	public int cobocount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Operations Cntct Number");
	public int cobro2ount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "LT Region Office");
	public int cobcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "City");
	public int cobhcncount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Contct Name");
	public int cobhcecount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Contct Email");
	public int cobhc1ount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Contct Number");
	public int cobbtcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Business Type");
	public int cobedcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Commercial End Date");
	public int cobmcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Margin");
	public int cobcpcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Credit Period");
	public int cobhbcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Name");
	//Pending hubs
	public int cobhlcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Latitude");
	public int cobhlocount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Longitude");
	public int cobhacount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Area");
	public int cobhfcount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub FO");
	public int cobfocount = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "FO");
	// edit client OnBoard
	public int cobitcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Industry Type");
	public int cobpncount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Pan Number");
	public int cobgstcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "GSTIN Number");
	public int cobfn1count2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Name 1");
	public int cobfe1count2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Email 1");
	public int cobfn2count2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Name 2");
	public int cobfe2count2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Fin Cntct Email 2");
	public int cobocncount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Operations Cntct Name");
	public int cobocecount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Operations Cntct Email");
	public int cobro2ount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "LT Region Office");
	public int cobcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "City");
	public int cobhcncount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Contct Name");
	public int cobhcecount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Hub Contct Email");
	public int cobbtcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Business Type");
	public int cobedcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Commercial End Date");
	public int cobmcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Margin");
	public int cobcpcount2 = GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Client_OnBoarding", "Credit Period");
	
	//client DashBoard data
	public String dCLIENT_URL=GenericLib.getProprtyValue(GenericLib.sUserCredFile, "CLIENT_DASHBOARDL");
	public String dCLIENT_MOB=GenericLib.getProprtyValue(GenericLib.sUserCredFile, "CLIENT_MOBILENO");
	
	
	

}