package com.letstransport.init;

public interface TestDataCoulmns {

	String TEST_CASE_NO="TEST_CASE_NO";
	
	/** PORTAL Sheet **/
	String Listing_Sheet="Web_Listing";
	String Booking_Sheet="Add_Booking";
	String Driver_Verify_Sheet ="Driver_Verify";
	String Request_Payment_Sheet ="Requested_Payment";
	String Add_Driver_Expense ="TC_Driver_Expenses";
	String Client_OnBoarding_sheet = "Client_OnBoarding";
	
	/** APP Sheet **/
	String App_Listing="App_Listing";
	String Onboarding="Onboarding";
	String Personal_And_Bank_Docuements="Driver_Licence_Bank_Docuements";
	
	/** TestCase **/
	String TC_Listing="Apply_listing_01";
	String TC_Signup="TC_Auto_001";
	String TC_Submit_Profile="TC_Auto_003";
	String TC_Booking_01="TC_Auto_0028";
	String TC_Booking_02="TC_Auto_0031";
	String TC_Booking_03="TC_Auto_0032";
	String TC_Driver_Verify="TC_Auto_0033";
	String TC_Request_Payment="TC_Auto_0034";
	String TC_Client_OnBoarding="TC_Client_OnBoarding";
	String TC_Client_OnBoarding_2="TC_Client_OnBoarding_2";	
}
