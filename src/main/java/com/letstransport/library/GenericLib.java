package com.letstransport.library;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.poi.EncryptedDocumentException;
//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class GenericLib {
	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public static String sFile;
	public static String sDirPath = System.getProperty("user.dir");
	public static String sConfigPath = sDirPath + "/src/main/resources/config/config.xlsx";
	public static String sUserCredFile = sDirPath + "/src/test/resources/propertyfiles/user_cred.properties";
	public static String sReportParamFile = sDirPath + "/src/test/resources/propertyfiles/reportsparameters.properties";
	public static String sRateChartsDir = sDirPath + "/src/test/resources/ratecharts";
	public static String sExportFilesDir = sDirPath + "/src/test/resources/exportfiles";
	public static String sPortalTestDataPath = sDirPath + "/src/test/resources/testdata/portal/portal_testdata.xlsx";
	public static String sAppTestDataPath = sDirPath + "/src/test/resources/testdata/app/app_testdata.xlsx";
	public static String sImportFileDir = sDirPath + "/src/test/resources/importfarmers";
	public static String sImportCCFileDir = sDirPath + "/src/test/resources/importcc";
	
	public static String sShiftReportFromApp = sDirPath + "/src/test/resources/shiftReports/shiftReportFromApp.xlsx";
	public static String sShiftReportFromPortal = sDirPath
			+ "/src/test/resources/shiftReports/shiftReportFromPortal.xlsx";
	public static String sTestDataConfig = sDirPath + "/src/test/resources/config/testDataConfig.xls";
	public static String sChromeDriverPath= sDirPath + "/src/main/resources/driver/chromedriver.exe";
	
	public static String[] aData= GenericLib.toReadExcelData(GenericLib.sAppTestDataPath, "App_Listing", "Apply_listing_01");

	public static int chCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"App_Listing","Client Hub");
	public static int baCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"App_Listing","Booking Amount");
	public static int rdCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"App_Listing","Reporting date&time");
	public static int aCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"App_Listing","Action");
	public static int mCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"App_Listing","Mobile Number");

	public static String ClientHub=aData[chCount];
	public static String BookingAmount=aData[baCount];
	public static String Reprtingdatetime=aData[rdCount];
	public static String Action=aData[aCount];
	public static String MobileNumber=aData[mCount];
	//Area Fo Data
		public static String[] areaFO=GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
		public static int areaSelect=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "CustomerRelations", "Area");
		public static int client1=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "CustomerRelations", "clientName");
		public static int vechileType=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath,"CustomerRelations", "vectype");
		public static int clientLocation=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "CustomerRelations", "clientloc");
		public static int typeofBookin=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath,  "CustomerRelations","Booking_Type");
		public static String areaSelection=areaFO[areaSelect];
		public static String clientnameinput=areaFO[client1];
		public static String clientlocinput=areaFO[clientLocation];
		public static String vechiltTypeInput=areaFO[vechileType];
		public static String bookinType=areaFO[ typeofBookin];

	//public static String[] vData= GenericLib.toReadExcelData(GenericLib.sAppTestDataPath, "Add_vehicle", "AddVehicle_03");

	
	//public static int vCount = GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"Add_vehicle","Veh_Num");	
	
	//public static String vehnum= vData[vCount];
	
	
	public static String[] areaData= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "add_Area", "AddDelArea_15_16");
	
	public static int foCount= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_Area", "fo_name");
	public static int alCount= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_Area", "al_name");
	
	public static String foname = areaData[foCount];
	public static String alname = areaData[alCount];
	
	
	public static String[] cntData= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "add_client", "AddEditClient_17_18");

	public static int emailid= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "email");
	public static int hubname= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "hubname");
	public static int hubarea= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "hubarea");
	public static int lat= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "latitude");
	public static int lngt= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "longtitude");
    public static int hubupdate= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "add_client", "hubareaupdate");
	public static String email = cntData[emailid];
	public static String hname = cntData[hubname];
	public static String harea = cntData[hubarea];
	public static String latitude = cntData[lat];
	public static String longtitude = cntData[lngt];
    public static String[] weblogsheetData=GenericLib.toReadExcelData(GenericLib.sAppTestDataPath, "Weblogsheet_client", "TC_POD_001");
	public static int clientname=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Weblogsheet_client","client_Name");
	public static int addnewname=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Weblogsheet_client", "add_client");
	
	//
	public static String client=weblogsheetData[clientname];
	public static String addnewclient=weblogsheetData[addnewname];
	//public static String dropdownname1=weblogsheetData[dropdowntext1];
    public static String[] logsheetData=GenericLib.toReadExcelData(GenericLib.sAppTestDataPath,  "Weblogsheet_client", "TC_POD_002");
    public static int dropdowntext=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"Weblogsheet_client", "dropdownvalue(NM)");
    public static int searchtextfield=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"Weblogsheet_client", "SearchTextField");
    public static int dropdowntext1=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"Weblogsheet_client", "dropdownvalue1(NM)");
   
    //
    public static String dropdownname1=logsheetData[dropdowntext];
    public static String searchfieldtext=logsheetData[searchtextfield];
    public static String dropdownname2=logsheetData[dropdowntext1];
    
    
    public static String[] GalleryData=GenericLib.toReadExcelData(GenericLib.sAppTestDataPath, "Gallery_Page","TC_POD_003");
    public static int infotext=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath, "Gallery_Page", "display");
    public static int path=GenericLib.getColumnIndex(GenericLib.sAppTestDataPath,"Gallery_Page", "filepath");
    
    //
    public static String enterinfo=GalleryData[infotext];
    public static String enterpath=GalleryData[path];
    
    //Excel data for WebTemplate page
    public static String[] WebTemplateData=GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "WebTemplate", "TC_POD_004");
    public static int actioneditbutton=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "WebTemplate","Action Button");
    public static int timedata=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "WebTemplate", "Time");
    public static int vehicleno=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath,"WebTemplate", "Vehicle_No");
    public static int closingKms=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath,"WebTemplate", "closingKms");
    public static int editclosingKms=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath,"WebTemplate", "editclosingKms");
    //
    public static String editinfobutton=WebTemplateData[actioneditbutton];
    public static String entertime=WebTemplateData[timedata];
    public static String entervehicleno=WebTemplateData[vehicleno];
    public static String closingKmss=WebTemplateData[closingKms];
    public static String editclosingKmss=WebTemplateData[editclosingKms];
    
   //excel date AdvancePayment
    public static String[] advancePaymetdata=GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "advancePayment", "TC_POD_009");
    public static int paymentAmount=GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "advancePayment", "Amount");
    public static String amount=advancePaymetdata[paymentAmount];
    

	public static String hupdate = cntData[hubupdate];
	
	
	public static String[] upldData= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "Upload_Submit", "UploadSubmit_11");
	public static int vehnumRC= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Upload_Submit", "VehicleNumberRC");
	public static int ownerName= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Upload_Submit", "OwnerName");
	public static int modelName= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Upload_Submit", "ModelName");
	
	
	public static String vnRC = upldData[vehnumRC];
	public static String ownName = upldData[ownerName];
	public static String modName = upldData[modelName];
	
	public static String[] llData= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "LatLong", "LatLong_010203");
	public static int clntName= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "LatLong", "clientName");
	public static int wrngLat= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "LatLong", "wrngLat");
	public static int wrngLong= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "LatLong", "wrngLong");
	
	public static String clientName = llData[clntName];
	public static String wLat =  llData[wrngLat];
	public static String wLong = llData[wrngLong];
	
	
	//bulkupdate 
	
	public static String[] bulkupdate= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "Bulk_Update", "TC_BulkUpdate");
	public static int fileloc= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "FileLocation");
	public static int dwnloc= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "dwnlocation");
	
	public static int raisedloc= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "raisedLocation");
	public static int bnknam= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "BankName");
	
	public static String filelocation = bulkupdate[fileloc];
	
	public static String dwnlocation = bulkupdate[dwnloc];
	
	public static String raisedlocation = bulkupdate[raisedloc];
	
	public static String bankname = bulkupdate[bnknam];
	
	public static int date= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "Date");
	public static int ticNum= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "TicketNum");
	public static int HQName= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "HQName");
	public static int VehNum= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "VehNum");
	public static int Amount= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "Amount");
	public static int Name= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "Name");
	public static int AccNum= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "AccNum");
	public static int IFSC= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "IFSC");
	public static int VehType= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "VehType");
	
	public static String ValidateDate = bulkupdate[date];
	public static String ValidateticNum = bulkupdate[ticNum];
	public static String ValidateHQName = bulkupdate[HQName];
	public static String ValidateVehNum = bulkupdate[VehNum];
	public static String ValidateAmount = bulkupdate[Amount];
	public static String ValidateAccNum = bulkupdate[AccNum];
	public static String ValidateName = bulkupdate[Name];
	public static String ValidateIFSC = bulkupdate[IFSC];
	public static String ValidateVehType = bulkupdate[VehType];
	
	
	
	public static int dateData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "DateData");
	public static int ticNumData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "TicketNumData");
	public static int HQNameData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "HQNameData");
	public static int VehNumData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "VehNumData");
	public static int AmountData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "AmountData");
	public static int NameData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "NameData");
	public static int AccNumData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "AccNumData");
	public static int IFSCData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "IFSCData");
	public static int VehTypeData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "VehTypeData");
	public static int MobNumData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "MobNumData");
	
	public static int wrngBankData= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "Bulk_Update", "WrngBankData");
	
	public static String ValidateDatedata = bulkupdate[dateData];
	public static String ValidateticNumdata = bulkupdate[ticNumData];
	public static String ValidateHQNamedata = bulkupdate[HQNameData];
	public static String ValidateVehNumdata = bulkupdate[VehNumData];
	public static String ValidateAmountdata = bulkupdate[AmountData];
	public static String ValidateAccNumdata = bulkupdate[AccNumData];
	public static String ValidateNamedata = bulkupdate[NameData];
	public static String ValidateIFSCdata = bulkupdate[IFSCData];
	public static String ValidateVehTypedata = bulkupdate[VehTypeData];
	public static String ValidateMobNumdata = bulkupdate[MobNumData];
	
	public static String ValidatewrngBankData = bulkupdate[wrngBankData];
	
	
	//labour
	
	public static String[] labour= GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "labour", "TC_Labour");
	public static int helpernamevalue= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "labour", "helpername");
	public static int helpernamevalue1= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "labour", "helpername1");
	public static int helpernamevalue2= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "labour", "helpername2");
	public static int hubvalue= GenericLib.getColumnIndex(GenericLib.sPortalTestDataPath, "labour", "hub");
	
	public static String helpername = labour[helpernamevalue];
	public static String helpername1 = labour[helpernamevalue1];
	public static String helpername2= labour[helpernamevalue2];
	public static String lab_hub = labour[hubvalue];
	
	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: To read the basic environment settings data from config file
	 * based on Property file value
	 */
	public static String getProprtyValue(String sFile, String sKey) {
		Properties prop = new Properties();
		String sValue = null;
		try {
			InputStream input = new FileInputStream(sFile);
			prop.load(input);
			sValue = prop.getProperty(sKey);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sValue;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: To read the basic environment settings data from config file
	 */
	public static Properties getPropertyFile(String sFile) {
		Properties prop = new Properties();
		String sValue = null;
		try {
			InputStream input = new FileInputStream(sFile);
			prop.load(input);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description:To set the basic environment settings data from config file
	 */
	public static void setPropertyValue(String sFile, String sKey, String sValue) {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(new File(sFile));
			prop.load(fis);
			fis.close();
			FileOutputStream fos = new FileOutputStream(new File(sFile));
			prop.setProperty(sKey, sValue);
			prop.store(fos, "Updated  file with " + "Key " + sKey + " and Value " + sValue);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description:To read test data from excel sheet based on TestcaseID
	 */
	public static String[] toReadExcelData(String sFilepath, String sSheet, String sTestCaseID) {
		DataFormatter dataFormatter = new DataFormatter();
		String sData[] = null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			for (int i = 0; i <= iRowNum; i++) {
				if (sht.getRow(i).getCell(0).toString().equals(sTestCaseID)) {
					int iCellNum = sht.getRow(i).getPhysicalNumberOfCells();
					sData = new String[iCellNum];
					for (int j = 0; j < iCellNum; j++) {
						Cell cell = sht.getRow(i).getCell(j);
						sData[j] = dataFormatter.formatCellValue(cell);
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description:To read test data from excel sheet based on TestcaseID
	 */
	public static String[] toReadColumnData(String sFilepath, String sSheet, int coloumn) {
		DataFormatter dataFormatter = new DataFormatter();
		String sData[] = null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			for (int i = 1; i <= iRowNum; i++) {
				sData = new String[iRowNum];
				Cell cell = sht.getRow(i).getCell(coloumn);
				sData[i - 1] = dataFormatter.formatCellValue(cell);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description:To read test data from excel sheet based on TestcaseID
	 */
	public static String[] readTestCaseIds(String sFilepath, String sSheet) {
		String sData[] = null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			sData = new String[iRowNum];
			for (int i = 1; i <= iRowNum; i++) {
				sData[i - 1] = sht.getRow(i).getCell(0).getStringCellValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description:To read test data from excel sheet based on MilkAnalyzer
	 */
	public static String[] toReadMilkAnalyzerData(String sFilepath, String sSheet, String milkAnalyzerType,
			String configType) {
		DataFormatter dataFormatter = new DataFormatter();

		String sData[] = null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			for (int i = 0; i <= iRowNum; i++) {
				if (sht.getRow(i).getCell(0).toString().equals(milkAnalyzerType)) {
					if (sht.getRow(i).getCell(1).toString().equals(configType)) {
						int iCellNum = sht.getRow(i).getPhysicalNumberOfCells();
						sData = new String[iCellNum];
						for (int j = 0; j < iCellNum; j++) {
							Cell cell = sht.getRow(i).getCell(j);
							sData[j] = dataFormatter.formatCellValue(cell);

						}
						break;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | TEST_CASE_NO
	 */

	public static int getColumnIndex(String filepath, String sSheet, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, "TEST_CASE_NO");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}
	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | TEST_CASE_NO
	 */

	public static int readColumnIndex(String filepath, String sSheet, String firstCol, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, firstCol);
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | Sample Template
	 * Datas
	 */

	public static Multimap<String, String> readColumnDataMatrixFromExcel(String filepath, String sSheet,
			String firstColumn, String[] columns) throws Exception {

		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, firstColumn);
		Multimap<String, String> multimap = ArrayListMultimap.create();
		Map<String, String> map = new LinkedHashMap<String, String>();

		int index = 0;
		for (int i = 0; i < columns.length; i++) {
			index = GenericLib.readColumnIndex(filepath, sSheet, firstColumn, columns[i]);
			map.put(columns[i], index + "");
		}
		FileInputStream fis = new FileInputStream(filepath);
		Workbook wb = (Workbook) WorkbookFactory.create(fis);
		Sheet sht = wb.getSheet(sSheet);
		int iRowNum = sht.getLastRowNum();
		for (int i = 1; i <= iRowNum; i++) {
			for (int j = 0; j < columns.length; j++) {
				multimap.put(sht.getRow(i).getCell(Integer.parseInt(map.get(columns[0]))).getStringCellValue(),
						sht.getRow(i).getCell(Integer.parseInt(map.get(columns[j]))).getStringCellValue());
			}
		}
		return multimap;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | Farmer Id
	 */

	public static int readFarmerTempHeadings(String filepath, String sSheet, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, "Farmer Id");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | S No
	 */

	public static int getReportColumnIndex(String filepath, String sSheet, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, "S No");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Method to read data based on row header | MILK_ANALYZER
	 */

	public static int getConfigurationIndex(String filepath, String sSheet, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, "MILK_ANALYZER");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}

	/*
	 * @author: Vandhitha Rao Description: Method to read data based on row
	 * header
	 */

	public static int getHeaderColumnIndex(String filepath, String sSheet, String colName) {
		String[] firstRow = GenericLib.toReadExcelData(filepath, sSheet, "SI No");
		int index = 0;
		for (int i = 0; i < firstRow.length; i++) {
			if (firstRow[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}
		return index;
	}

	/*
	 * @author: Vandhitha Rao Description:Method is used to set data in excel
	 * sheet
	 */

	public static void setCellData(String filePath, String sSheet, String sTestCaseID, String columnName, String value)
			throws Exception {
		int columnNumber = getColumnIndex(filePath, sSheet, columnName);
		try {
			FileInputStream fis = new FileInputStream(filePath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			// logger.info("----------Sheet " + sSheet);
			int lastRowNum = sht.getLastRowNum();
			for (int i = 0; i <= lastRowNum; i++) {
				if (sht.getRow(i).getCell(0).toString().equals(sTestCaseID)) {
					Row rowNum = sht.getRow(i);
					Cell cell = rowNum.getCell(columnNumber);
					if (cell == null) {
						cell = rowNum.createCell(columnNumber);
						cell.setCellValue(value);
					} else {
						cell.setCellValue(value);
					}
					break;
				}
			}
			FileOutputStream fileOut = new FileOutputStream(filePath);
			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	}

	public static void executeBatchCommmand(String command) {
		try {
			String line;
			ArrayList<String> deviceUDID = new ArrayList<String>();
			Process p = Runtime.getRuntime().exec("cmd /c " + command);

			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				// logger.info(line);
				deviceUDID.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				logger.info("" + line);
			}

			bre.close();
			p.waitFor();

		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	/*
	 * @author: Description: To split and return the array
	 */
	public static String[] getSplittedArray(String str, String splitChar) {
		return str.split(splitChar);
	}

	/*
	 * @author: Description: Extract the string based on previous and next
	 * strings and occurrences
	 */
	public static String getString(String str, String startStr, int startOccurance, String endStr, int endOccurance) {
		return str.substring(str.indexOf(startStr, startOccurance) + startStr.length(),
				str.indexOf(endStr, endOccurance));
	}

	/*
	 * @author: Description: Extract the string based on previous and next
	 * strings
	 */
	public static String getString(String str, String startStr, String endStr) {
		return str.substring(str.indexOf(startStr) + startStr.length(), str.indexOf(endStr));
	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description:To read test data from excel sheet based on TestcaseID
	 */
	public static String[] toReadExcelDataWithNull(String sFilepath, String sSheet, String sTestCaseID) {
		String sData[] = null;
		try {
			FileInputStream fis = new FileInputStream(sFilepath);
			Workbook wb = (Workbook) WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sSheet);
			int iRowNum = sht.getLastRowNum();
			for (int i = 0; i <= iRowNum; i++) {
				if (sht.getRow(i).getCell(0).toString().equals(sTestCaseID)) {
					int iCellNum = sht.getRow(i).getPhysicalNumberOfCells();
					sData = new String[iCellNum];
					for (int j = 0; j < iCellNum; j++) {
						String value = sht.getRow(i).getCell(j).getStringCellValue();
						if (value.equals("") || value == null || value.equals(" ")) {
							value = "novalue";
						}
						sData[j] = value;
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sData;
	}

	public static String readCellData(String xlPath, String sheetName, int rowNumber, int cellNumber) throws Exception {

		FileInputStream fis = new FileInputStream(xlPath);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		DataFormatter dataFormatter = new DataFormatter();

		Row row = sh.getRow(rowNumber);
		Cell cell = row.getCell(cellNumber);
		String data = dataFormatter.formatCellValue(cell);

		return data;

	}

	public static String[] readInputDataListFromConfig(String path, String sheetName, String testCaseName)
			throws Exception {

		ArrayList<Integer> index = new ArrayList<Integer>();
		index = findCellIndexOfLabelInExcel(path, sheetName, testCaseName);
		System.out.println("Index of given testcasename :: " + index);
		System.out.println(index.get(0));
		System.out.println(index.get(1));

		String[] inputDataRows = (readCellData(path, sheetName, index.get(0), ((index.get(1)) + 1))).split(",");

		return inputDataRows;
	}

	static boolean labelFoundInExcel = false;
	static DataFormatter dataFormatter = new DataFormatter();

	public static ArrayList<String> getAllValuesOfSelectBoxFromExcel(String xlPath, String sheetName, String content) {
		ArrayList<String> valueOfLabelInExcel = new ArrayList<String>();
		ArrayList<Integer> cellIndexOfLabelInExcel = new ArrayList<Integer>();
		try {
			cellIndexOfLabelInExcel = findCellIndexOfLabelInExcel(xlPath, sheetName, content);

			FileInputStream fis = new FileInputStream(xlPath);
			Workbook w1 = WorkbookFactory.create(fis);
			Sheet s1 = w1.getSheet(sheetName);
			if (cellIndexOfLabelInExcel.size() == 0) {
				System.out.println("Label Not Found In Excel");
				// Assert.fail("Label Not Found In Excel");
			} else {
				for (int i = cellIndexOfLabelInExcel.get(0) + 1; i <= s1.getLastRowNum(); i++) {
					Row r1 = s1.getRow(i);
					try {
						Cell c1 = r1.getCell(cellIndexOfLabelInExcel.get(1));
						String data = dataFormatter.formatCellValue(c1);
						if (data != null) {
							valueOfLabelInExcel.add(data);
						}
					} catch (NullPointerException e) {
						break;
					}
				}
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return valueOfLabelInExcel;
	}

	public static ArrayList<Integer> findCellIndexOfLabelInExcel(String xlPath, String sheetName, String content)
			throws Exception {
		ArrayList<Integer> labelIndexInExcel = new ArrayList<Integer>();

		FileInputStream fis = new FileInputStream(xlPath);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);

		for (int i = 0; i <= sh.getLastRowNum(); i++) {
			Row row = sh.getRow(i);
			try {
				int lastCellCount = row.getLastCellNum();
				for (int j = 0; j < lastCellCount; j++) {

					try {
						Cell cellValue = row.getCell(j);
						String value = dataFormatter.formatCellValue(cellValue);
						if (value.equals(content)) {
							labelIndexInExcel.add(i);
							labelIndexInExcel.add(j);
							labelFoundInExcel = true;
							break;
						}
					} catch (Exception e) {

					}
				}

			} catch (NullPointerException e) {

			}
		}

		/*
		 * if(labelFoundInExcel==false) {
		 * 
		 * // Assert.fail("Given Label Is Not Found In the Excel Sheet");
		 * System.out.println(("Given Label Is Not Found In the Excel Sheet"));
		 * }
		 */
		return labelIndexInExcel;

	}

	public static void updateCSVData(String fileToCopy, String fileToUpdate, String value, String replaceValue, int col)
			throws Exception {

		CSVReader reader = new CSVReader(new FileReader(fileToCopy), ',');
		CSVWriter writer = new CSVWriter(new FileWriter(fileToUpdate));

		// Read all rows at once
		List<String[]> allRows = reader.readAll();
		// allRows.get(row)[col]=replace;
		reader.close();
		// Read CSV line by line and use the string array as you want
		for (String[] output : allRows) {
			// get current row
			String[] parsedRow = new String[output.length];
			for (int i = 0; i < output.length; i++) {
				// parse each column
				parsedRow[i] = output[i].replace("\"", "").replace("=", "").replace(value, replaceValue);
			}
			// write line
			writer.writeNext(parsedRow);
		}
		writer.close();
	}

	public static int getCSVRows(String csvFile) throws IOException {

		File inputFile = new File(csvFile);
		// Read existing file
		CSVReader reader = new CSVReader(new FileReader(inputFile), ',', '\"');
		List<String[]> csvBody = reader.readAll();
		int rows = csvBody.size();
		reader.close();
		return rows - 1;
	}

	public static int getCSVRowID(String csvFile, String matchingText, int index) throws Exception {
		String[] sNextLine2;
		CSVReader reader = new CSVReader(new FileReader(csvFile), ',');
		int counter = 0;
		int rowid = 0;
		while ((sNextLine2 = reader.readNext()) != null) {
			counter++;
			if (sNextLine2[index].contains(matchingText)) {
				rowid = counter;
			} else {
			}
		}
		reader.close();
		return rowid - 1;
	}
}


