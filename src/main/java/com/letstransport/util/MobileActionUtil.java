/***********************************************************************
 * @author 			:		Vandhitha Rao
 * @description		: 		This class contains action methods which is used for performing 
 * 							action while executing script such as Click, SendKeys 
 */

package com.letstransport.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
//import io.appium.java_client.touch.WaitOptions;
//import io.appium.java_client.touch.offset.PointOption;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class MobileActionUtil {

	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * Description : This method has fluent wait implementation for element to
	 * load which is polling every 250 miliseconds
	 * 
	 * @param element
	 * @param driver
	 * @param eleName
	 * @throws IOException
	 */

	public static void waitForElement(WebElement element, AndroidDriver driver, String elementName, int seconds)
			throws IOException {
		try {
			logger.info("---------Waiting for visibility of element---------" + element);
			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			logger.info("---------Element is visible---------" + element);
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is not displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			logger.info("<---------Element is not visible--------->" + element);
			throw e;
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is not displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}

	public static void implicit_Wait_ID(WebElement ele) throws Exception{

        for(int i=0;i<60;i++){
            try{
                ele.isDisplayed();
                break;
            }catch(Exception e){Thread.sleep(2000);

            }   
        }
}


	/**
	 * Description : This method has fluent wait implementation for element to
	 * load which is polling every 250 miliseconds
	 * 
	 * @param element
	 * @param driver
	 * @param eleName
	 * @throws IOException
	 */
	public static void waitForToastMsg(WebElement element, AndroidDriver driver, String eleName, int seconds)
			throws IOException {
		try {
			logger.info("---------Waiting for visibility of Toast Message---------" + element);

			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			logger.info("---------Element is visible---------" + element);
		} catch (Exception e) {

		}
	}


	/**
	 * Description: This method helps to verify whether given web Element is
	 * present page or not.
	 * 
	 * @param element
	 * @param driver
	 * @param elementName
	 * @throws IOException
	 */
	public static void isEleDisplayed(WebElement element, AndroidDriver driver, String elementName) throws IOException {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			if (element.isDisplayed()) {
				System.out.println(elementName + "------ is displayed");
				MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed || " + "\'"
						+ elementName + "\'" + " is displayed ");
			}
		} catch (RuntimeException e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		}
	}

	/**
	 * Description: This method helps to verify whether given web Element is
	 * present page or not.
	 * 
	 * @param element
	 * @param driver
	 * @param elementName
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static boolean isEleDisplayed(WebElement element, AndroidDriver driver, int seconds, int loop)
			throws IOException, InterruptedException {

		boolean flag = false;

		int count = loop;
		while (count > 0) {
			try {
				logger.info("---------Verifying element is displayed or not ---------");
				count--;
				element.isDisplayed();
				flag = true;
				break;

			} catch (RuntimeException e) {
				Thread.sleep(seconds * 1000);
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param element
	 * @param driver
	 * @param elementName
	 * @throws IOException
	 */

	public static void verifyElementIsDisplayed(WebElement element, AndroidDriver driver, String elementName)
			throws IOException {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed  || " + "\'" + elementName
					+ "\'" + " is displayed ");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed  || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed  || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		}
	}

	/**
	 * Description: this method will click on element which is provided.
	 * 
	 * @param element
	 * @param driver
	 * @param elementName
	 * @throws Exception
	 */
	public static void clickElement(WebElement element, AndroidDriver driver, String elementName) throws Exception {

		try {
			logger.info("<---------Verifying element is displayed or not --------->");
			waitForElement(element, driver, elementName, 300);
			element.click();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			throw error;
		} catch (Exception error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			throw error;
		}

	}

	/**
	 * Description: this method hides keyboard
	 * 
	 * @param driver
	 */

	public static void hideKeyboard(AndroidDriver driver) {
		try {
			driver.hideKeyboard();
			Thread.sleep(2000);
		} catch (Throwable e) {

		}
	}

	/**
	 * Description: this method clear texts from textbox/edit box and type the
	 * value which is provided
	 * 
	 * @param element
	 * @param value
	 * @param elementName
	 * @param driver
	 * @throws Exception
	 */
	public static void clearAndType(WebElement element, String value, String elementName, AndroidDriver driver)
			throws Exception {
		try {
			logger.info("---------Method clear and type  ---------");
			element.clear();
			logger.info(elementName + " is cleared");
			element.sendKeys(value);
			logger.info(value + " is entered in " + elementName);
			hideKeyboard(driver);
			logger.info(" hide keyboard");
			MyExtentListners.test.pass("Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName
					+ "\'" + " || User is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'");
		} catch (AssertionError error) {

			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("Unable to type on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("Unable to type in " + "\'" + elementName + "\'");
		}

	}

	/**
	 * Description: this method type the value which is provided
	 * 
	 * @param element
	 * @param value
	 * @param elementName
	 * @param driver
	 * @throws Exception
	 */
	public static void type(WebElement element, String value, String elementName, AndroidDriver driver)
			throws Exception {
		try {
			logger.info("---------Method type  ---------");
			// Thread.sleep(100);
			element.sendKeys(value);
			// Thread.sleep(300);
			hideKeyboard(driver);
			logger.info("---------hide keyboard  ---------");
			MyExtentListners.test.pass("Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName
					+ "\'" + " || User is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("Unable to type on " + elementName);
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("Unable to type in " + elementName);
		}

	}

	/**
	 * Description:Explicit wait to check element is clickable
	 * 
	 * @param element
	 * @param driver
	 * @param eleName
	 * @throws IOException
	 */
	public static void isEleClickable(WebElement element, AndroidDriver driver, String eleName) throws IOException {
		try {
			logger.info("---------Method is Element clickable  ---------");

			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(element)) != null);
			System.out.println(" element is clickable ");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + eleName + "\'" + " is clickable || "
					+ "\'" + eleName + "\'" + " is not clickable", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			System.out.println(" element is not clickable ");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + eleName + "\'" + " is clickable || "
					+ "\'" + eleName + "\'" + " is not clickable", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			System.out.println(" element is not clickable ");
			throw e;
		}
	}

	/**
	 * Description: wait till page load until progress bar is invisible
	 * 
	 * @param eleName
	 * @param driver
	 * @param pageName
	 * @throws IOException
	 */

	public static void waitTillPageLoad(String eleName, AndroidDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of Page loader ---------");

			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(
							By.xpath("//*[name()='svg']/*[name()='g']/*[name()='circle']")))),
					"On clicking" + eleName + " Page is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " user is redirected to "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		}
	}

	/**
	 * Description: Fetch text from element and return as string
	 * 
	 * @param elename
	 * @param driver
	 * @param elementName
	 * @return
	 * @throws IOException
	 */

	public static String gettext(WebElement elename, AndroidDriver driver, String elementName) throws IOException {
		logger.info("--------- get text from element  ---------");
		String eleText = null;
		try {
			isEleDisplayed(elename, driver, elementName);
			eleText = elename.getText();
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName)); // exception
			MyExtentListners.test.addScreenCaptureFromPath(MobileActionUtil.capture(driver, elementName));
			Assert.fail("Unable to fetch text from " + "\'" + elename + "\'");

		}
		return eleText;
	}

	/**
	 * Description: This method verify expected result contains in actual result
	 * 
	 * @param actResult
	 * @param expResult
	 * @throws IOException
	 */

	public static void verifyContainsText(String actResult, String expResult, AndroidDriver driver) throws IOException {
		if (actResult.contains(expResult)) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
					+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);

		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
					+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
					+ " does not contains  Actual :  " + actResult, ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expResult));

		}
	}

	/**
	 * 
	 * @param actResult
	 * @param expResult
	 * @param desc
	 */

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result contains in actual result
	 */

	public static void verifyContainsText(String actResult, String expResult, String desc) throws Exception {
		if (actResult.contains(expResult)) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
					+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);

		} else {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
					+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
					+ " does not contains  Actual :  " + actResult, ExtentColor.RED));

			throw new Exception();

		}
	}

	/**
	 * Description: This method verify expected result equals in actual result
	 * 
	 * @param desc
	 * @param actResult
	 * @param expResult
	 */

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result equals in actual result
	 */

	public static void verifyEqualsText(String desc, String actResult, String expResult) throws Exception {

		if (expResult.equalsIgnoreCase(actResult)) {
			MyExtentListners.test.pass("Verify " + desc + " ||  Expected : " + "\'" + expResult + "\''"
					+ " eqauls  to Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + desc + "  || Expected : " + "\'" + expResult
					+ "\''" + " not eqauls to  Actual :  " + "\'" + actResult + "\'", ExtentColor.RED));
			throw new Exception();
		}
	}

	/**
	 * Description: This method verify expected result not equals in actual
	 * result
	 * 
	 * @param desc
	 * @param actResult
	 * @param expResult
	 */

	public static void verifyNotEqualsText(String desc, String actResult, String expResult) {
		if (!(expResult.equalsIgnoreCase(actResult))) {
			MyExtentListners.test.pass("Verify " + desc + " is printed on receipt or not" + " ||  Expected : " + "\'"
					+ expResult + "\''" + " not  to Actual :  " + actResult);
		} else {
			MyExtentListners.test
					.fail(MarkupHelper
							.createLabel(
									"Verify " + desc + " is printed on receipt or not" + "  || Expected : " + "\'"
											+ expResult + "\''" + "  eqauls to  Actual :  " + "\'" + actResult + "\'",
									ExtentColor.RED));
		}
	}

	/**
	 * Description: This method verify actual result is not null
	 * 
	 * @param desc
	 * @param actResult
	 * @param expResult
	 */

	public static void verifyIsNull(String actResult, String desc) {
		if (actResult == null) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Value is null", ExtentColor.RED));
			throw new RuntimeException();
		} else {
			MyExtentListners.test.pass("Verify" + desc + " is Printed on Receipt or not" + " || " + desc + " : "
					+ actResult + " is printed on receipt");
		}
	}

	/**
	 * 
	 * Description: This method to scroll left side based on device height and
	 * width
	 * 
	 * @param value
	 * @param startX
	 * @param endX
	 * @param driver
	 * @throws Exception
	 */

	public static void swipeRightToLeft(int value, double startX, double endX, AndroidDriver driver) throws Exception {
		try {
			Thread.sleep(1000);
			System.out.println("inside swipe");
			for (int i = 1; i <= value; i++) {
				Dimension dSize = driver.manage().window().getSize();
				int startx = (int) (dSize.width * startX);
				int endx = (int) (dSize.width * endX);
				int starty = dSize.height / 2;
				// driver.swipe(startx, starty, endx, starty, 1000);

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(startx, starty))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
						.moveTo(PointOption.point(endx, starty)).release().perform();
				/*
				 * or action.press(startx,endx).moveTo(starty,endy).release().
				 * perform();
				 */
			}
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, "SwipeLeft"));
			throw e;
		}
	}

	/**
	 * Description: This method to scroll Right side based on device height and
	 * width
	 * 
	 * @param value
	 * @param startx
	 * @param endx
	 * @param driver
	 * @throws Exception
	 */
	public static void swipeLefToRight(int value, double startx, double endx, AndroidDriver driver) throws Exception {
		try {
			Thread.sleep(1000);
			System.out.println("inside swipe");
			for (int i = 1; i <= value; i++) {
				Dimension dSize = driver.manage().window().getSize();
				int startX = (int) (dSize.width * startx);
				int endX = (int) (dSize.width * endx);
				int starty = dSize.height / 2;
				// driver.swipe(startX, starty, endX, starty, 1000);

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(startX, starty))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
						.moveTo(PointOption.point(endX, starty)).release().perform();
				/*
				 * or action.press(startx,endx).moveTo(starty,endy).release().
				 * perform();
				 */
			}
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, "SwipeRight"));
			throw e;

		}
	}

	/**
	 * Description: This method to scroll Up side based on device height and
	 * width
	 * 
	 * @param value
	 * @param driver
	 * @param starty1
	 * @param endy1
	 * @throws Exception
	 */

	public static void swipeBottomToTop(int value, AndroidDriver driver, double starty1, double endy1)
			throws Exception {
		try {
			Thread.sleep(1000);
			System.out.println("inside swipe");
			for (int i = 1; i <= value; i++) {
				Dimension dSize = driver.manage().window().getSize();
				System.out.println(dSize);
				int starty = (int) (dSize.height * starty1);
				int endy = ((int) (dSize.height * endy1));
				int startx = dSize.width / 2;
				// driver.swipe(startx, starty, startx, endy, 1000);

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(startx, starty))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
						.moveTo(PointOption.point(startx, endy)).release().perform();
				/*
				 * or action.press(startx,endx).moveTo(starty,endy).release().
				 * perform();
				 */
			}
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, "SwipeUp"));
			throw e;
		}
	}

	/**
	 * Description: This method to scroll Bottom side based on device height and
	 * width
	 * 
	 * @param value
	 * @param driver
	 * @param starty1
	 * @param endy1
	 * @throws Exception
	 */
	public static void swipeTopToBottom(int value, AndroidDriver driver, double starty1, double endy1)
			throws Exception {
		try {
			Thread.sleep(1000);

			System.out.println("inside swipe");
			for (int i = 1; i <= value; i++) {
				Dimension dSize = driver.manage().window().getSize();
				int starty = (int) (dSize.height * starty1);
				int endy = (int) (dSize.height * endy1);
				int startx = dSize.width / 2;
				// driver.swipe(startx, starty, startx, endy, 1000);

				TouchAction action = new TouchAction(driver);
				action.press(PointOption.point(startx, starty))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
						.moveTo(PointOption.point(startx, endy)).release().perform();
				/*
				 * or action.press(startx,endx).moveTo(starty,endy).release().
				 * perform();
				 */
			}
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, "SwipeDown"));
			throw e;
		}
	}

	/**
	 * Description: This method is to long press on element upto 3 seconds then
	 * release
	 * 
	 * @param driver
	 * @param element
	 * @throws IOException
	 */
	public static void performLongPress(AndroidDriver driver, WebElement element) throws IOException {

		try {
			// TouchAction act1 = new TouchAction((MobileDriver) driver);
			// act1.press(element).waitAction(Duration.ofMillis(10000)).release().perform();
			// need to write new method for LongPress here
			AndroidTouchAction touch = new AndroidTouchAction(driver);
			touch.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(element))).perform();
			System.out.println("LongPress Done");
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, "LongPress"));
			throw e;
		}
	}

	/**
	 * Description: This method is to capture screenshot
	 * 
	 * @param driver
	 * @param screenShotName
	 * @return
	 * @throws IOException
	 */

	public static String capture(AndroidDriver driver, String screenShotName) throws IOException {
		File source = driver.getScreenshotAs(OutputType.FILE);
		String dest = MyExtentListners.screenShotPath + screenShotName + ".png";
		System.out.println(dest);
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	public static String capture(AndroidDriver driver) throws IOException {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);

		File source = driver.getScreenshotAs(OutputType.FILE);
		String dest = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		System.out.println(dest);
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	/**
	 * 
	 * Description: Method for Scrolling to particular element based on
	 * direction and device size
	 * 
	 * @param maxScroll
	 * @param start
	 * @param end
	 * @param scrollType
	 * @param element
	 * @param driver
	 * @throws Exception
	 */
	public static void scrollToElement(int maxScroll, double start, double end, String scrollType, WebElement element,
			AndroidDriver driver) throws Exception {

		while (maxScroll != 0) {
			try {
				if (element.isDisplayed()) {
					maxScroll++;
					break;
				}
			} catch (Exception e) {
				switch (scrollType.toUpperCase()) {
				case ("DOWN"):
					swipeTopToBottom(1, driver, start, end);
					break;

				case ("UP"):
					swipeBottomToTop(1, driver, start, end);
					break;

				case ("LEFT"):
					swipeRightToLeft(1, start, end, driver);
					break;

				case ("RIGHT"):
					swipeLefToRight(1, start, end, driver);
					break;

				default:
					MyExtentListners.test.warning(MarkupHelper.createLabel(" Invalid Swipe type", ExtentColor.AMBER));

					break;

				}

			}
			maxScroll--;
		}
	}

	/**
	 * 
	 * @param driver
	 * @return
	 * @throws IOException
	 */

	public static String getToastMessage(AppiumDriver driver) throws IOException {
		String result = null;
		File scfile = driver.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scfile, new File(GenericLib.sDirPath + "/toasts/screen.png"));
		ITesseract instance = new Tesseract();
		try {
			result = instance.doOCR(scfile);
		} catch (TesseractException e) {
		}
		System.out.println("************* Toast message text************** " + result.toString());
		return result;
	}

	/**
	 * Description : Functional Verification
	 * 
	 * @param desc
	 * @param actResult
	 * @param expResult
	 */

	public static void verifyEqualsText_Funct(String desc, String actResult, String expResult) {
		// if (expResult.equalsIgnoreCase(actResult)) {
		if (expResult.equals(actResult)) {
			MyExtentListners.test.pass("Verify " + desc + " is displayed or not " + " ||  Expected : " + "\'"
					+ expResult + "\''" + " eqauls  to Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Verify " + desc + " is diaplayed or not" + "  || Expected : " + "\'" + expResult
							+ "\''" + " not eqauls to  Actual :  " + "\'" + actResult + "\'", ExtentColor.RED));
		}
	}

	/**
	 * 
	 * @param actResult
	 * @param expResult
	 * @param desc
	 */

	public static void verifyContainsText_Funct(String actResult, String expResult, String desc) {
		if (actResult.contains(expResult)) {
			MyExtentListners.test.pass("Verify Text" + desc + " is displayed or not " + " ||  Expected : " + "\'"
					+ expResult + "\''" + " eqauls  to Actual :  " + actResult);

		} else {
			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Verify Text" + desc + " is diaplayed or not" + "  || Expected : " + "\'" + expResult
							+ "\''" + " not eqauls to  Actual :  " + "\'" + actResult + "\'", ExtentColor.RED));

		}
	}

	public static void isEleIsEnabled(WebElement element, AndroidDriver driver, String elementName) throws IOException {
		try {
			logger.info("---------Verifying element is Enabled or not ---------");

			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			if (element.isEnabled()) {
				System.out.println(elementName + "------ is displayed");
				MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is enabled || " + "\'"
						+ elementName + "\'" + " is enabled ");
			}
		} catch (RuntimeException e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is enabled || " + "\'" + elementName + "\'" + " is not enabled ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			System.out.println(elementName + "------ is not Enabled");
			throw e;
		}
	}

	public static void isEleIsSelected_funct(WebElement element, AndroidDriver driver, String elementName)
			throws IOException {
		try {
			logger.info("---------Verifying element is Selected or not ---------");
			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			if (element.isSelected() == false) {
				System.out.println(elementName + "------ is  Not Selected");
				MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is Not Selected || " + "\'"
						+ elementName + "\'" + " is Not Selected ");
			}
		} catch (RuntimeException e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is Selected || " + "\'" + elementName + "\'" + " is  Selected ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			System.out.println(elementName + "------ is Selected");
			throw e;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to move to element and click refresh
	 */

	public static void actionClick(WebElement element, AndroidDriver driver, String elementName) throws IOException {

		try {
			Actions action = new Actions(driver);
			action.moveToElement(element).click().build().perform();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			throw e;
		}

	}

	/*
	 * @author: Vandhitha Rao
	 * 
	 * Description: Tap on particular element based size co-orinates
	 */
	public static void tapOnElement(double x, double y, AndroidDriver driver) throws InterruptedException {
		Thread.sleep(5000);
		Dimension dSize = driver.manage().window().getSize();
		int sx1 = driver.manage().window().getSize().getWidth();
		int sx2 = driver.manage().window().getSize().getHeight();
		int sX = (int) (dSize.width * x);
		int sY = (int) (dSize.height * y);
		// driver.tap(1, sX, sY, 1); need to write new method for tap here
		TouchAction touchAction = new TouchAction(driver);
		touchAction.tap(PointOption.point(sX, sY)).perform();
		System.out.println("Tapped");
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: handles webwiew and native_app mode
	 */
	public static void switchToWebView(AndroidDriver driver) {
		Set<String> contextNames = driver.getContextHandles();
		/*
		 * System.out.println("Total No of Context Found = " +
		 * contextNames.size()); for (String contextName : contextNames) {
		 * System.out.println(contextName);
		 * 
		 * }
		 */

		for (String contextName : contextNames) {
			System.out.println(contextName);
			if (contextName.contains("WEBVIEW_in")) {
				driver.context(contextName);
			}
		}

	}

	public static void switchToAndroidVeiw(AndroidDriver driver) {
		Set<String> contextNames = driver.getContextHandles();
		/*
		 * System.out.println("Total No of Context Found = " +
		 * contextNames.size()); for (String contextName : contextNames) {
		 * System.out.println(contextName);
		 * 
		 * }
		 */

		for (String contextName : contextNames) {
			System.out.println(contextName);
			if (contextName.contains("NATIVE_")) {
				driver.context(contextName);
			}
		}

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to fetch the system date and time in
	 * yyyy-mm-ddThh-mm-ss
	 * 
	 */
	public static String getSystemDate() {
		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		Date finalNewDate = calendar.getTime();
		dateTimeInGMT.setTimeZone(istTimeZone);
		String finalNewDateString = dateTimeInGMT.format(finalNewDate);
		System.out.println(finalNewDateString);
		return finalNewDateString;

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method will extract device id (IMEI)
	 * 
	 */
	public static String getDeviceId() throws Exception {
		Process proc = Runtime.getRuntime().exec("adb shell dumpsys iphonesubinfo");
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		// read the output from the command
		String s = null;
		String deviceId = "";
		while ((s = stdInput.readLine()) != null) {
			if (s.contains("Device ID")) {
				String[] s1 = s.trim().split(" ");
				deviceId = s1[s1.length - 1];
				System.out.println("IMEI-----------" + deviceId);
				break;
			}
		}
		if (deviceId.equals("") || deviceId == null || deviceId.equals(" ")) {
			throw new Exception(" Please connect the device");
		}
		return deviceId.trim();
	}

	/**
	 * Description: wait till page load until progress bar is invisible
	 * 
	 * @param eleName
	 * @param driver
	 * @param pageName
	 * @throws IOException
	 */

	public static void waitTillProgressBarLoad(String eleName, AndroidDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of progress bar  ---------");

			Wait<AndroidDriver> wait = new FluentWait<AndroidDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loader-backgroud-div")))),
					"On clicking" + eleName + " Page is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " user is redirected to "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		}
	}

	/*
	 * public static void scrollToElement(AndroidDriver driver, String
	 * elementName, boolean scrollDown){ String listID = ((RemoteWebElement)
	 * driver.
	 * findElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.ListView\")"
	 * )).getId(); String direction; if (scrollDown) { direction = "down"; }
	 * else { direction = "up"; } HashMap<String, String> scrollObject = new
	 * HashMap<String, String>(); scrollObject.put("direction", direction);
	 * scrollObject.put("element", listID); scrollObject.put("text",
	 * elementName); driver.executeScript("mobile: scrollTo", scrollObject); }
	 */

	public static String decimalRoundingOff(String data, String format) {

		String str = data;

		switch (format) {

		case ".0":
			if (!(str.contains("."))) {
				str = str + ".0";
			}
			break;

		case ".00":
			if (!(str.contains("."))) {
				str = str + ".00";
			} else if ((str.substring(str.indexOf('.'))).length() == 2) {
				str = str + "0";
			}
			break;
		}

		return str;
	}

	public static void scrollToElement(AndroidDriver driver, double starty1, double endy1) {
		{
			boolean found = false;
			while (!found) {
				try {
					driver.findElement(By.xpath(""));
					found = true;
				} catch (Exception e) {
					Dimension dSize = driver.manage().window().getSize();
					int starty = (int) (dSize.height * starty1);
					int endy = ((int) (dSize.height * endy1));
					int startx = dSize.width / 2;
					/*
					 * TouchAction action = new TouchAction(driver);
					 * action.press(PointOption.point(startx,
					 * starty)).waitAction(WaitOptions.waitOptions(Duration.
					 * ofSeconds(3))) .moveTo(PointOption.point(startx,
					 * endy)).release().perform(); or
					 * action.press(startx,endx).moveTo(starty,endy).release().
					 * perform();
					 */
				}
			}
		}
	}

	public static void scrollIntoViewAndClick(WebDriver driver, WebElement ele) throws IOException {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});", ele);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
		} catch (Exception e) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(" Unable to scroll to an element " + ele, ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
		}
	}

	public static String capture(WebDriver driver) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}

	public static void actionClickAndType(WebElement element, AndroidDriver driver, String elementName, String Value)
			throws IOException {

		try {
			isEleClickable(element, driver, elementName);
			Actions action = new Actions(driver);
			action.click(element).sendKeys(Value, Keys.ENTER).build().perform();
			MyExtentListners.test.pass("Verify user is able to click and type " + "\'" + elementName + "\'"
					+ " ||  User is able to click and type " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(
							"Verify user is able to click and type" + "\'" + elementName + "\'"
									+ "  || User is not able to click and type" + "\'" + elementName + "\'",
							ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to click and type " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(
							"Verify user is able to click and type " + "\'" + elementName + "\'"
									+ " || User is not able to click and type " + "\'" + elementName + "\'",
							ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	public static String capture(AndroidDriver driver, WebElement element) throws IOException {
		File source = driver.getScreenshotAs(OutputType.FILE);
		String dest = MyExtentListners.screenShotPath + element + ".png";
		System.out.println(dest);
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	public static void waitTillElementVisible(WebElement element, AndroidDriver driver, String elementName, int seconds)
			throws IOException {
		try {
			logger.info("---------Waiting for visibility of element---------" + element);
			WebDriverWait wait = new WebDriverWait(driver, seconds);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			logger.info("---------Element is visible---------" + element);
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is not displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			logger.info("<---------Element is not visible--------->" + element);
			throw e;
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is not displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}

	public final static String generateRandomNumber(long len) {
		if (len > 18)
			throw new IllegalStateException("To many digits");
		long tLen = (long) Math.pow(10, len - 1) * 9;

		long number = (long) (Math.random() * tLen) + (long) Math.pow(10, len - 1) * 1;

		String tVal = number + "";
		if (tVal.length() != len) {
			throw new IllegalStateException("The random number '" + tVal + "' is not '" + len + "' digits");
		}
		return tVal;
	}

	public static String enterMobileNumber(String genNum, AndroidDriver driver) {
		// String genNum = generateRandomNumber(digit);
		char[] Number = genNum.toCharArray();
		for (int j = 0; j < Number.length; j++) {
			driver.pressKey(new KeyEvent(AndroidKey.valueOf("DIGIT_" + Number[j])));
		}
		return genNum;
	}
	public void generateRandomMobileNo(int n){
		Random objGenerator = new Random();
        for (int iCount = 0; iCount< 10; iCount++){
          int randomNumber = objGenerator.nextInt(100);
          System.out.println("Random No : " + randomNumber); 
		
	}
	}



	public static String getOTP(WebDriver driver) throws Exception {
		driver.get("https://test-user-management-1096.appspot.com/otp/latest");
		Thread.sleep(7000);
		String Otptext = driver.findElement(By.xpath("//pre[@style='word-wrap: break-word; white-space: pre-wrap;']")).getText();
		String OTP = Otptext.substring(10, 14);
		System.out.println(OTP);
		driver.navigate().back();
		return OTP;
	}

	public static void enterOTP(AndroidDriver adriver, WebDriver wdriver) throws Exception {
		char[] Number = getOTP(wdriver).toCharArray();
		for (int j = 0; j < Number.length; j++) {
			adriver.pressKey(new KeyEvent(AndroidKey.valueOf("DIGIT_" + Number[j])));

		}
	}


	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: handles webwiew and native_app mode
	 */

	public static void switchToView(AndroidDriver driver) {
		Set<String> contextNames = driver.getContextHandles();
		System.out.println("Total No of Context Found = " + contextNames.size());
		for (String contextName : contextNames) {
			System.out.println(contextName);

			if(contextName.contains("WEBVIEW_in")){
				driver.context(contextName);
			}

		}
		/*
		 * for (String contextName : contextNames) { if
		 * (contextName.contains("NATIVE_APP")) { driver.context(contextName);
		 * System.out.println(contextName); } else {
		 * driver.context(contextName); System.out.println(contextName); }
		 * 
		 * }
		 */
	}


	public static void clickElementWithoutWait(WebElement element, AndroidDriver driver, String elementName)
			throws Exception {

		try {
			logger.info("<---------Verifying element is displayed or not --------->");
			element.click();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			throw error;
		} catch (Exception error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, elementName));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			throw error;
		}

	}

	/*
	 * @author : Vandhitha Rao
	 * 
<<<<<<< HEAD
	 * Description : To sate a random string to match a vehicle number
	 * 
	 * @return String
	 */
	
	


	public static String generateRandomString(int n,int m) {

//		int n = 2;
//		int m = 4;

		// chose a Character random from this String
		String AlphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String numString = "0123456789";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);
		StringBuilder sc = new StringBuilder(m);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaString.charAt(index));
		}

		for (int i = 0; i < m; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (numString.length() * Math.random());

			// add Character one by one in end of sb
			sc.append(numString.charAt(index));
		}

		return sb.toString() + sc.toString().substring(0, 2) + sb.toString() + sc.toString();
	}

	static final String AB = "0123456789";
	static SecureRandom rnd = new SecureRandom();

	public static String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}


	/**
	 * Author: Vandhitha Description: This method verifies if element doesnt
	 * contain expected text
	 * 
	 * @param actResult
	 * @param expResult
	 * @throws IOException
	 */

	public static void notContainsText(WebElement ele, String expStr, AndroidDriver driver) throws IOException {
		String actStr = ele.getText();
		// System.out.println(actStr);
		if (!actStr.toLowerCase().contains(expStr.toLowerCase())) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
					+ "  || Expected : " + "\'" + expStr + "\''" + "contains  Actual :  " + actStr);

		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
							+ " ||  Expected : " + "\'" + expStr + "\''" + " does not contains  Actual :  " + actStr,
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expStr));

		}
	}

	public static String concat(String a,String b){
		a +=b;
		return a;
	}


	/*
	 * Author: vandhitha description: waits till invisibility of an element
	 */

	public static void waitForinvisiblityofElement(String ele, Integer seconds, AndroidDriver driver) {
		WebDriverWait wait6 = new WebDriverWait(driver, 100);
		wait6.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(ele)));
	}
	
	 public static void verifyElementContainsText(WebElement ele, String expStr, AndroidDriver driver) throws IOException {
	        String actStr= ele.getText();
	        System.out.println(actStr);
	        if (actStr.toLowerCase().contains(expStr.toLowerCase())) {
	            MyExtentListners.test.pass("Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  "
	                    + actStr + "  || Expected : " + "\'" + expStr + "\''" + "contains  Actual :  " + actStr);
	        } else {
	            MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expStr + "\''"
	                    + " contains  Actual :  " + actStr + " ||  Expected : " + "\'" + expStr + "\''"
	                    + " does not contains  Actual :  " + actStr, ExtentColor.RED));
	            MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expStr));
	        }
	    }

	 public static String generateRandomCString(int length) {
		   String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
		   String CHAR_UPPER = CHAR_LOWER.toUpperCase();
		   String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER ;
		     SecureRandom random = new SecureRandom();
		         if (length < 1) throw new IllegalArgumentException();

		         StringBuilder sb = new StringBuilder(length);
		         for (int i = 0; i < length; i++) {

		 			// 0-62 (exclusive), random returns 0-61
		             int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
		             char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

		             // debug
		             System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

		             sb.append(rndChar);

		         }

		         return sb.toString();

		     }
	 public static void scrollIntoView(WebDriver driver, WebElement ele) throws IOException {
			try {
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});", ele);
			} catch (Exception e) {
				MyExtentListners.test
						.fail(MarkupHelper.createLabel(" Unable to scroll to an element " + ele, ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
			}
		}

	 public static void verifyContainsTextIgnoreCase(String actResult, String expResult, AndroidDriver driver) throws IOException {
			if (actResult.equalsIgnoreCase(expResult)) {
				MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
						+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);

			} else {
				MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
						+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
						+ " does not contains  Actual :  " + actResult, ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expResult));

			}
		}


}