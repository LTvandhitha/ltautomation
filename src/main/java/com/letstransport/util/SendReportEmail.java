

package com.letstransport.util;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import com.letstransport.listener.MyExtentListners;

import ch.qos.logback.core.net.SyslogOutputStream;

import javax.mail.internet.MimeMultipart;
	public class SendReportEmail{

		
			
		
	    public static void sendEmail() {

	        final String username = "internal.foapp@letstransport.team";
	        final String password = "d@taplatform";

	        Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.gmail.com");
			prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	        prop.put("mail.smtp.port", "587");
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS

	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });
	        
	        
	        

	        try {

	        	MimeMessage message = new MimeMessage(session);  
	            message.setFrom(new InternetAddress("internal.foapp@letstransport.team"));  
//	            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("vanditha.r@letstransport.team,nishant.jain@letstransport.team,raghul.r@letstransport.team,suresh.p@letstransport.team,priyanka.dev@letstransport.team"));
//	            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("vanditha.r@letstransport.team,nishant.jain@letstransport.team,sagar.ks@letstransport.team,nilay.sahu@letstransport.team,vaibhav.miholia@letstransport.team,lonee.kashyap@letstransport.team"));
	            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("vanditha.r@letstransport.team,nishant.jain@letstransport.team,punam.g@letstransport.team,ajay.shukla@letstransport.team,priyanka.dev@letstransport.team"));
//	            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("vanditha.r@letstransport.team"));
//	            message.setSubject("Automation Report-BulkUpdate/Labour/Pricing "+ getTimeStamp());
	            message.setSubject("Automation Report-Client onboarding "+ getTimeStamp());
//	            message.setSubject("Automation Report-DriverMode/Loadboard "+ getTimeStamp());
	        	
	        	//3) create MimeBodyPart object and set your message text     
	            BodyPart messageBodyPart1 = new MimeBodyPart();  
//	            messageBodyPart1.setText("All ,"
//                    + "\n\n Attached is todays Automation Report- Client invoicing POD"+
//            		"\n\n Please download the attachment for better view of the html file ");  
	            
	            
	            messageBodyPart1.setText("All ,"
	                    + "\n\n Attached is todays Automation Report- Client Onboarding POD"+
	            		"\n\n COB scripts fixed after Area lead mapping"+
	            		"\n\n Please download the attachment for better view of the html file ");
	            
	            
//	            messageBodyPart1.setText("All ,"
//	                    + "\n\n Attached is todays Automation Report- DriverMode/Loadboard"+
//	            		"\n\n Please download the attachment for better view of the html file ");
//	                  
	            
	            MimeBodyPart messageBodyPart2 = new MimeBodyPart(); 
	            
	            
	            String filename =createdFilepath();
	            System.out.println(filename);
  	            DataSource source = new FileDataSource(filename);  
	            messageBodyPart2.setDataHandler(new DataHandler(source));  
//	            messageBodyPart2.setFileName(filename);  
     
	            messageBodyPart2.setFileName(new File(filename).getName());
	            
	            Multipart multipart = new MimeMultipart();  
	            multipart.addBodyPart(messageBodyPart1);  
	            multipart.addBodyPart(messageBodyPart2);  
          
	            
	            message.setContent(multipart); 
       
	            
	            
   	            Transport.send(message);
	            

	            System.out.println("Done");

	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public static String getTimeStamp(){
		    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");   
		  
	 return formatter.format(new Date()); 
	 }
	    
	    
	    
	    public static String createdFilepath(){
	    	
	    	
	    	MyExtentListners obj= new MyExtentListners();
		    
		    String val= obj.sfile;
//		    System.out.println(val);
		    
			return val;
    	
	    	
	    	
	    	
	    }


	}