package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class AddBookingPage {

	WebDriver driver;

	public AddBookingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	@FindBy(xpath = "//*[@id='client_name']//input")
	private WebElement clientName;

	@FindBy(xpath = "//*[@id='reporting_point']")
	private WebElement clientHub;

	@FindBy(xpath = "//*[@id='reporting_point']//div[@class='sc-cMljjf hEWgzY'][1]")
	private WebElement clientHubSuggestion;

	@FindBy(xpath = "//*[@id='pricing_id']")
	private WebElement clientPricing;

	@FindBy(xpath = "//*[@id='pricing_id']//div[@class='sc-cMljjf hEWgzY'][1]")
	private WebElement clientPricingSuggestion;

	@FindBy(xpath = "//*[text()='Driver Name']/../input")
	private WebElement driverName;

	@FindBy(xpath = "//*[text()='Driver Contact']/../input")
	private WebElement driverContact;

	@FindBy(xpath = "//*[text()='Vehicle Number']/../input")
	private WebElement vehicleNumber;

	@FindBy(xpath = "//*[text()='Vehicle Number']/../../..//div[@class='sc-iwsKbI bldFTk']")
	private WebElement vehicleSuggestion;

	@FindBy(xpath = "//*[text()='Driver Pricing']/../..//input[@type='text']/following-sibling::div")
	private WebElement driverPricing;

	@FindBy(xpath = "//*[text()='Driver Pricing']/..")
	private WebElement driverPricingInputField;

	@FindBy(xpath = "//*[text()='Driver Pricing']/..//div[@class='sc-cMljjf hEWgzY']")
	private WebElement driverPricingSuggestion;

	@FindBy(xpath = "//*[@id='vehicle_type']")
	private WebElement vehicleType;
	
	@FindBy(xpath = "//*[@id='vehicle_type_attribute_id']//span")
	private WebElement vehicleTypeAttribute;
	
	@FindBy(xpath = "//*[@id='material_id']")
	private WebElement materials;
	
	@FindBy(xpath = "//*[@id='work_type_id']")
	private WebElement helpers;
	
	@FindBy(xpath = "//*[@id='vehicle_type_attribute_id']//div[@class='sc-cMljjf hEWgzY']")
	private WebElement vehicleTypeAttributeSugestion;

	@FindBy(xpath = "//*[@id='vehicle_type']//div[text()='0.65 Ton-Mahindra Champion']")
	private WebElement vehicleTypeSugestion;

	public WebElement getVehicleType(WebDriver driver, String vehicleType) {
		return driver.findElement(By.xpath("//*[@id='vehicle_type']//div[text()='" + vehicleType + "']"));
	}
	public WebElement getHelpers(WebDriver driver, String helper) {
		return driver.findElement(By.xpath("//*[@id='work_type_id']//div[text()='" + helper + "']"));
	}
	
	public WebElement getVehicleMaterial(WebDriver driver, String material) {
		return driver.findElement(By.xpath("//*[@id='material_id']//div[text()='"+material+"']"));
	}

	public WebElement getVehicleSuggestion(WebDriver driver, String client) throws Exception {
		Thread.sleep(5000);
		return driver.findElement(By.xpath("//*[@id='client_name']//div[text()='" + client + "']"));
	}

	@FindBy(xpath = "//*[text()='Vehicle Body Type']/../input")
	private WebElement vehicleBodyType;

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveBtn;

	@FindBy(xpath = "//*[text()='Vehicle Number']/../..//div[2]")
	private WebElement vehicleSelect;

	@FindBy(xpath = "//*[@class='sc-hSdWYo gEddZk']")
	private WebElement timeIcon;

	public void createBooking(WebDriver driver, String filepath, String sheet, String testcase,String timeStamp,String vehicle) throws Exception {
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		String[] sData = null;
		for(int i=0;i<sTestCaseIds.length;i++){
			if(sTestCaseIds[i].equalsIgnoreCase(testcase)){
				sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			}
		}
		WebActionUtil.type(this.clientName, sData[BaseLib.gv.cnCount], "Client Name", driver);
		WebActionUtil.waitTillPageLoad(" Westside Client", driver, "Booking page", 120);
		WebActionUtil.clickElement(this.getVehicleSuggestion(driver, sData[BaseLib.gv.cnCount]), driver, "Liberty");
		WebActionUtil.waitTillPageLoad("Client", driver, "Booking page", 120);
		WebActionUtil.clickElement(this.timeIcon, driver, "time Icon");
		WebActionUtil.HandleDateTime(driver, timeStamp, "n");
		WebActionUtil.clickElement(this.clientHub, driver, "Client Hub");
		WebActionUtil.clickElement(this.clientHubSuggestion, driver, "westside");
		WebActionUtil.clickElement(this.clientPricing, driver, "Client pricing");
		WebActionUtil.clickElement(this.clientPricingSuggestion, driver, "Client pricing");
		WebActionUtil.type(this.driverName, sData[BaseLib.gv.drCount], "Driver Name", driver);
		WebActionUtil.type(this.driverContact, sData[BaseLib.gv.dcCount], "Driver Number", driver);
		WebActionUtil.type(this.vehicleNumber, vehicle, "Vehicle Number", driver);
		WebActionUtil.clickElement(this.vehicleNumber, driver, "click vehicle number");
		WebActionUtil.clickElement(this.vehicleSelect, driver, " select vehicle number");
		WebActionUtil.clickElement(this.driverPricingInputField, driver, "Driver Price");
		//WebActionUtil.type(this.driverPricing, sData[BaseLib.gv.dpCount], "Driver pricing", driver);
		WebActionUtil.clickElement(this.driverPricing,driver,"select Driver pricing");
		WebActionUtil.clickElement(this.vehicleType, driver, "Vehicle Type");
		WebActionUtil.clickElement(this.getVehicleType(driver, "0.65 Ton-Mahindra Champion"), driver,
				"Vehicle Type suggestion");
		WebActionUtil.type(this.vehicleBodyType, "Sexy body", "Vehicle Body Name", driver);
		WebActionUtil.waitTillPageLoad("Vehicle type attribute", driver, "Add booking page", 120);
		WebActionUtil.clickElement(this.vehicleTypeAttribute, driver, "Vehicle Attributes");
		WebActionUtil.clickElement(this.vehicleTypeAttributeSugestion, driver, "suggest Vehicle Attributes");
		WebActionUtil.clickElement(this.materials, driver, "Vehicle Material");
		WebActionUtil.clickElement(this.getVehicleMaterial(driver, "Grocery & Gourmet Foods"), driver,
				"Vehicle material");
		WebActionUtil.clickElement(this.helpers, driver, "helpers ");
		WebActionUtil.clickElement(this.getHelpers(driver, "Driver"), driver,
				"Vehicle helper");
	    WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPageLoad("Save button", driver, "Add booking page", 120);
	}
}
