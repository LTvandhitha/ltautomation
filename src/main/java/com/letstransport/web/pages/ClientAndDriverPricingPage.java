package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.WebActionUtil;

public class ClientAndDriverPricingPage {
	WebDriver driver;

	public ClientAndDriverPricingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[contains(text(),'Client Pricings')]//i[@class='fa fa-plus']")
	private WebElement clientPricingIcon;

	@FindBy(xpath = "//*[contains(text(),'Driver Pricings')]//i[@class='fa fa-plus']")
	private WebElement driverPricingIcon;

	@FindBy(xpath = "//*[text()='Name:']//following-sibling::input")
	private WebElement nameInputBox;

	@FindBy(xpath = "//*[text()='Client:']/..//input[@placeholder='Client']")
	private WebElement clientNameInputBox;

	@FindBy(xpath = "//*[text()='Client:']/..//input[@placeholder='search client..']")
	private WebElement clientNameInputBox2;

	@FindBy(xpath = "//*[text()='Client:']/..//span[@ng-repeat='prop in propToDisplay']")
	private WebElement clientSuggestion;

	@FindBy(xpath = "//*[text()='Vehicle Type:']/..//input[@placeholder='Vehicle Type']")
	private WebElement vehicleTypeInputBox;

	@FindBy(xpath = "//*[text()='Payment Mode:']/..//select[@name='paymentMode']")
	private WebElement payModeSelectBox;

	@FindBy(xpath = "//*[text()='Vehicle Type:']/..//span[@ng-repeat='prop in propToDisplay']")
	private WebElement clickVehicleTypeSuggestion;

	public void selectPricingType(WebDriver driver, String pricingType) {
		driver.findElement(By.xpath("//*[contains(text(),'Pricing Type')]//option[text()='" + pricingType + "']"))
				.click();
	}

	public void selectDriverType(WebDriver driver, String driverType) {
		driver.findElement(By.xpath("//*[contains(text(),'Driver Type')]//option[text()='" + driverType + "']"))
				.click();
	}

	public void selectPayMode(WebDriver driver, String mode) {
		driver.findElement(By.xpath("//*[contains(text(),'Payment Mode:')]/..//option[text()='" + mode + "']")).click();
	}

	public void selectDay(WebDriver driver, String day) {
		driver.findElement(By.xpath("//*[text()='Weekly Days:']/..//div[text()='" + day + "']")).click();
	}

	public void selectDriverDay(WebDriver driver, String day) {
		driver.findElement(By.xpath("//*[text()='Weekly Working Days:']/..//div[text()='" + day + "']")).click();
	}

	@FindBy(xpath = "//*[contains(text(),'Full Month:')]//label[@ng-click='vm.clientPricing.full_month = !vm.clientPricing.full_month']")
	private WebElement fullMonthToggleBtn;

	@FindBy(xpath = "//*[text()='Hub Level Extra KM(Distance) Calculation:']/..//label[@ng-click='vm.hubLevelCalculationClicked()']")
	private WebElement hubLevelCalculationToggleBtn;

	@FindBy(xpath = "//*[contains(text(),'Select Parameter')]")
	private WebElement parameterDropDown;

	@FindBy(xpath = "//*[text()='Company Commission:']/..//input[@placeholder='commission']")
	private WebElement compCommission;

	public void selectParameter(WebDriver driver, String parameter) {
		driver.findElement(By.xpath("//*[contains(text(),'Select Parameter')]/..//a[text()='" + parameter + "']"))
				.click();
	}

	public void selectHour(WebDriver driver, String hour) {
		driver.findElement(By.xpath(
				"//span[text()='Value :']/..//select[@ng-model='attribute.hour']//option[text()='" + hour + "']"))
				.click();
	}

	public void selectMinute(WebDriver driver, String minute) {
		driver.findElement(By.xpath(
				"//span[text()='Value :']/..//select[@ng-model='attribute.minute']//option[text()='" + minute + "']"))
				.click();
	}

	@FindBy(xpath = "//*[text()='Rate :']//following-sibling::input")
	private WebElement otRate;

	@FindBy(xpath = "//*[contains(text(),'Base Fare Monthly')]/..//*[text()='Value :']//following-sibling::input")
	private WebElement baseFareMonthlyRate;

	@FindBy(xpath = "//*[contains(text(),'Base Salary Monthly')]/..//*[text()='Value :']//following-sibling::input")
	private WebElement baseSalaryMonthly;

	@FindBy(xpath = "//*[text()='Lower Limit :']//following-sibling::input")
	private WebElement distanceChargesMonthlyLimit;

	@FindBy(xpath = "//span[text()='Lower Limit :']/../../..//span[text()='Value :']//following-sibling::input")
	private WebElement distanceChargesMonthlyRate;

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveBtn;

	@FindBy(xpath = "//*[text()='Cancel']")
	private WebElement cancelBtn;

	@FindBy(xpath = "//*[contains(text(),'Client Pricings')]/..//*[text()='Client : ']/..//input")
	private WebElement clientInputBox;

	@FindBy(xpath = "//*[contains(text(),'Driver Pricings')]/..//*[text()='Client : ']/..//input")
	private WebElement driverInputBox;

	@FindBy(xpath = "//*[contains(text(),'Client Pricings')]/..//*[text()='Client : ']/..//span[@ng-repeat='prop in propToDisplay']")
	private WebElement suggestClient;

	@FindBy(xpath = "//*[contains(text(),'Driver Pricings')]/..//*[text()='Client : ']/..//span[@ng-repeat='prop in propToDisplay']")
	private WebElement suggestClient2;

	@FindBy(xpath = "//*[contains(text(),'Active:')]//label[@ng-click='vm.toggleActive(!vm.clientPricing.active)']")
	private WebElement activeClientToggleBtn;

	@FindBy(xpath = "//*[contains(text(),'Active:')]//label[@ng-click='vm.toggleActive(!vm.driverPricing.active)']")
	private WebElement activeDriverToggleBtn;

	public WebElement getClientPricing(WebDriver driver, String name) {
		return driver.findElement(By
				.xpath("//*[contains(text(),'Client Pricings')]/..//*[text()='Client : ']/../../..//*[contains(text(),'"
						+ name + "')]"));
	}

	// get list of Webelements
	public List<WebElement> getClientPricings(WebDriver driver, String name) {
		return driver.findElements(By
				.xpath("//*[contains(text(),'Client Pricings')]/..//*[text()='Client : ']/../../..//*[contains(text(),'"
						+ name + "')]"));
	}

	public List<WebElement> getDriverPricings(WebDriver driver, String name) {
		return driver.findElements(By
				.xpath("//*[contains(text(),'Driver Pricings')]/..//*[text()='Client : ']/../../..//*[contains(text(),'"
						+ name + "')]"));
	}

	public WebElement getDriverPricing(WebDriver driver, String name) {
		return driver.findElement(By
				.xpath("//*[contains(text(),'Driver Pricings')]/..//*[text()='Client : ']/../../..//*[contains(text(),'"
						+ name + "')]"));
	}

	@FindBy(xpath = "//*[text()='Vehicle Type:']/..//i[@class='fa fa-pencil c-cursor-pointer c-margin-left-10']")
	private WebElement vehicleTypePencilIcon;

	@FindBy(xpath = "//*[text()='Vehicle Type:']/..//i[@class='fa fa-pencil cursor-pointer c-margin-left-10']")
	private WebElement vehicleTypePencilIcon2;

	public String getVehicleType(WebDriver driver) {
		return driver
				.findElement(By.xpath("//*[text()='Vehicle Type:']/..//span[@class='c-margin-left-10 ng-binding']"))
				.getText();
	}

	public String getVehicleType2(WebDriver driver) {
		return driver
				.findElement(By.xpath("//*[text()='Vehicle Type:']/..//div[@ng-show='vm.driverPricing.vehicle_type']"))
				.getText();
	}

	public String getPaymentMode(WebDriver driver) {
		return driver.findElement(By.xpath("// *[text()='Payment Mode:']/..//option[@selected='selected'][2]"))
				.getText();
	}

	public WebElement getWeeklyDays(WebDriver driver) {
		return driver.findElement(
				By.xpath("//*[text()='Weekly Days:']/..//div[@class='week-day c-noselect ng-binding ng-scope']"));
	}

	public WebElement getWeeklyWorkingDays(WebDriver driver) {
		return driver.findElement(By
				.xpath("//*[text()='Weekly Working Days:']/..//div[@class='week-day c-noselect ng-binding ng-scope']"));
	}

	public String getWorkingHours(WebDriver driver) {
		return driver
				.findElement(By
						.xpath("//*[contains(text(),'Working Hours')]/..//select[@ng-model='attribute.hour']//option[@selected='selected'][2]"))
				.getText();
	}

	public String getWorkingMinutes(WebDriver driver) {
		return driver
				.findElement(By
						.xpath("//*[contains(text(),'Working Hours')]/..//select[@ng-model='attribute.minute']//option[@selected='selected'][2]"))
				.getText();
	}

	public void updateHour(WebDriver driver, String hour) {
		driver.findElement(
				By.xpath("//*[contains(text(),'Working Hours')]/..//select[@ng-model='attribute.hour']//option[text()='"
						+ hour + "']"))
				.click();
	}

	public void updateMinute(WebDriver driver, String hour) {
		driver.findElement(By
				.xpath("//*[contains(text(),'Working Hours')]/..//select[@ng-model='attribute.minute']//option[text()='"
						+ hour + "']"))
				.click();
	}

	public void addClientPricing(WebDriver driver, String name) throws Exception {
		WebActionUtil.clickElement(this.clientPricingIcon, driver, "Client pricing icon");
		WebActionUtil.type(this.nameInputBox, name, "Client Name", driver);
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		this.selectPricingType(driver, "Monthly");
		WebActionUtil.type(this.clientNameInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.clientSuggestion, driver, "Client select");
		WebActionUtil.type(this.vehicleTypeInputBox, "2.1 Ton-Tata 207", "Vehicle Type", driver);
		WebActionUtil.clickElement(this.clickVehicleTypeSuggestion, driver, "Vehicle Type");
		this.selectPayMode(driver, "cash");
		this.selectDay(driver, "Mo");
		this.selectDay(driver, "Tu");
		this.selectDay(driver, "We");
		this.selectDay(driver, "Th");
		this.selectDay(driver, "Fr");
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Working Hours");
		this.selectHour(driver, "9");
		this.selectMinute(driver, "0");
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Overtime Rate");
		WebActionUtil.type(this.otRate, "4", "OT rate", driver);
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Base Fare Monthly");
		WebActionUtil.type(this.baseFareMonthlyRate, "5", "OT rate", driver);
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Fixed Distance Charges Monthly");
		WebActionUtil.type(this.distanceChargesMonthlyLimit, "2000", "Lower limit", driver);
		WebActionUtil.type(this.distanceChargesMonthlyRate, "10", "rate", driver);
		WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
	}

	public void verifyAddedClientPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.clientInputBox, "cko", "Client Name", driver);
		Thread.sleep(8000);
		WebActionUtil.clickElement(this.suggestClient, driver, "click client");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.verifyEqualsText("Client pricing ", this.getClientPricing(driver, name).getText(), name);
	}

	public void updateClientPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.clientInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.clickElement(this.getClientPricing(driver, name), driver, "client pricing");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		Thread.sleep(8000);
		WebActionUtil.clickElement(this.vehicleTypePencilIcon, driver, "pencil icon");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.type(this.vehicleTypeInputBox, "13 Ton-24 Feet", "Vehicle Type", driver);
		WebActionUtil.clickElement(this.clickVehicleTypeSuggestion, driver, "Vehicle Type");
		this.selectPayMode(driver, "cheque");
		this.selectDay(driver, "Sa");
		this.updateHour(driver, "8");
		this.updateMinute(driver, "0");
		WebActionUtil.clearAndType(this.otRate, "3", "OT rate", driver);
		WebActionUtil.clearAndType(this.baseFareMonthlyRate, "4", "OT rate", driver);
		WebActionUtil.clearAndType(this.distanceChargesMonthlyLimit, "3000", "Lower limit", driver);
		WebActionUtil.clearAndType(this.distanceChargesMonthlyRate, "11", "rate", driver);
		WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		Thread.sleep(7000);
	}

	public void verifyUpdatedClientPricing(WebDriver driver, String name) throws Exception {
		WebActionUtil.clickElement(this.getClientPricing(driver, name), driver, "client pricing");
		Thread.sleep(7000);
		WebActionUtil.verifyEqualsText("Vehicle type ", this.getVehicleType(driver), "13 Ton-24 Feet");
		WebActionUtil.verifyEqualsText("Payment Mode ", this.getPaymentMode(driver), "cheque");
		WebActionUtil.assertElementDisplayed(this.getWeeklyDays(driver), driver, "Su");
		WebActionUtil.verifyEqualsText("Working hrs ", this.getWorkingHours(driver), "8");
		WebActionUtil.verifyEqualsText("Working minutes ", this.getWorkingMinutes(driver), "0");
		WebActionUtil.verifyEqualsText("updated OT ", this.otRate.getAttribute("value"), "3");
		WebActionUtil.verifyEqualsText("Base monthly fare ", this.baseFareMonthlyRate.getAttribute("value"), "4");
		WebActionUtil.verifyEqualsText("distance charges monthly limit ",
				this.distanceChargesMonthlyLimit.getAttribute("value"), "3000");
		WebActionUtil.verifyEqualsText("distance charges monthly rate ",
				this.distanceChargesMonthlyRate.getAttribute("value"), "11");
		WebActionUtil.clickElement(this.cancelBtn, driver, "Cancel button");
	}

	public void deleteClientPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.clientInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.clickElement(this.getClientPricing(driver, name), driver, "client pricing");
		Thread.sleep(7000);
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.clickElement(this.activeClientToggleBtn, driver, "client pricing");
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.cancelBtn, driver, "cancel button");
		Thread.sleep(7000);
	}

	public void verifyDeletedClientPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.waitTillPaymentPageLoad("client", driver, "Client pricing Page", 120);
		driver.navigate().refresh();
		WebActionUtil.type(this.clientInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("client", driver, "Client pricing Page", 120);
		WebActionUtil.eleIsNotDisplayed(this.getClientPricings(driver, name), driver, "client pricing", 120);
	}

	public void verifyAddedDriverPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.driverInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient2, driver, "click client");
		WebActionUtil.waitTillPaymentPageLoad("driver ", driver, "Driver pricing Page", 120);
		WebActionUtil.verifyEqualsText("Driver pricing ", this.getDriverPricing(driver, name).getText(), name);
	}

	public void addDriverPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.clickElement(this.driverPricingIcon, driver, "driver pricing icon");
		WebActionUtil.type(this.nameInputBox, name, "Client Name", driver);
		WebActionUtil.waitTillPaymentPageLoad("Payment modes", driver, "Driver pricing Page", 120);
		this.selectDriverType(driver, "Scheduled");
		WebActionUtil.type(this.vehicleTypeInputBox, "2.1 Ton-Tata 207", "Vehicle Type", driver);
		WebActionUtil.clickElement(this.clickVehicleTypeSuggestion, driver, "Vehicle Type");
		WebActionUtil.type(this.clientNameInputBox2, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.clientSuggestion, driver, "Client select");
		this.selectDriverDay(driver, "Mo");
		this.selectDriverDay(driver, "Tu");
		this.selectDriverDay(driver, "We");
		this.selectDriverDay(driver, "Th");
		this.selectDriverDay(driver, "Fr");
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Working Hours");
		this.selectHour(driver, "9");
		this.selectMinute(driver, "0");
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Overtime Rate");
		WebActionUtil.type(this.otRate, "4", "OT rate", driver);
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Base Salary Monthly");
		WebActionUtil.type(this.baseSalaryMonthly, "5000", "OT rate", driver);
		WebActionUtil.clickElement(this.parameterDropDown, driver, "Parameter dropDown");
		this.selectParameter(driver, "Fixed Distance Charges Monthly");
		WebActionUtil.type(this.distanceChargesMonthlyLimit, "2000", "Lower limit", driver);
		WebActionUtil.type(this.distanceChargesMonthlyRate, "10", "rate", driver);
		WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPaymentPageLoad("driver price created", driver, "driver pricing Page", 120);
		Thread.sleep(5000);
	}

	public void updateDriverPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.driverInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient2, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("Driver", driver, "Driver pricing Page", 120);
		WebActionUtil.clickElement(this.getDriverPricing(driver, name), driver, "driver pricing");
		WebActionUtil.waitTillPaymentPageLoad("edit driver pricing", driver, "driver pricing Page", 120);
		Thread.sleep(8000);
		WebActionUtil.clickElement(this.vehicleTypePencilIcon2, driver, "pencil icon");
		WebActionUtil.waitTillPaymentPageLoad("cliked on pencil icon", driver, "Driver pricing Page", 120);
		WebActionUtil.type(this.vehicleTypeInputBox, "13 Ton-24 Feet", "Vehicle Type", driver);
		WebActionUtil.clickElement(this.clickVehicleTypeSuggestion, driver, "Vehicle Type");
		this.selectDriverDay(driver, "Sa");
		this.updateHour(driver, "8");
		this.updateMinute(driver, "0");
		WebActionUtil.clearAndType(this.otRate, "3", "OT rate", driver);
		WebActionUtil.clearAndType(this.baseSalaryMonthly, "4000", "OT rate", driver);
		WebActionUtil.clearAndType(this.distanceChargesMonthlyLimit, "3000", "Lower limit", driver);
		WebActionUtil.clearAndType(this.distanceChargesMonthlyRate, "11", "rate", driver);
		WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPaymentPageLoad("Driver price updated", driver, "driver pricing Page", 120);
		Thread.sleep(8000);
	}

	public void verifyUpdatedDriverPricing(WebDriver driver, String name) throws Exception {
		WebActionUtil.clickElement(this.getDriverPricing(driver, name), driver, "client pricing");
		Thread.sleep(7000);
		WebActionUtil.verifyEqualsText("Vehicle type ", this.getVehicleType2(driver), "13 Ton-24 Feet");
		WebActionUtil.assertElementDisplayed(this.getWeeklyWorkingDays(driver), driver, "Su");
		WebActionUtil.verifyEqualsText("Working hrs ", this.getWorkingHours(driver), "8");
		WebActionUtil.verifyEqualsText("Working minutes ", this.getWorkingMinutes(driver), "0");
		WebActionUtil.verifyEqualsText("updated OT ", this.otRate.getAttribute("value"), "3.0");
		WebActionUtil.verifyEqualsText("Base monthly fare ", this.baseSalaryMonthly.getAttribute("value"), "4000");
		WebActionUtil.verifyEqualsText("distance charges monthly limit ",
				this.distanceChargesMonthlyLimit.getAttribute("value"), "3000");
		WebActionUtil.verifyEqualsText("distance charges monthly rate ",
				this.distanceChargesMonthlyRate.getAttribute("value"), "11");
		WebActionUtil.clickElement(this.cancelBtn, driver, "Cancel button");
	}

	public void deleteDriverPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.type(this.driverInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient2, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("Driver", driver, "Driver pricing Page", 120);
		WebActionUtil.clickElement(this.getDriverPricing(driver, name), driver, "Driver pricing");
		Thread.sleep(7000);
		WebActionUtil.waitTillPaymentPageLoad("driver pricing", driver, "Driver pricing Page", 120);
		WebActionUtil.clickElement(this.activeDriverToggleBtn, driver, "driver pricing");
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.cancelBtn, driver, "cancel button");
		Thread.sleep(7000);
	}

	public void verifyDeletedDriverPricing(WebDriver driver, String name) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebActionUtil.waitTillPaymentPageLoad("Driver", driver, "Driver pricing Page", 120);
		driver.navigate().refresh();
		WebActionUtil.type(this.driverInputBox, "cko", "Client Name", driver);
		Thread.sleep(7000);
		WebActionUtil.clickElement(this.suggestClient2, driver, "suggested client");
		WebActionUtil.waitTillPaymentPageLoad("client", driver, "Driver pricing Page", 120);
		WebActionUtil.eleIsNotDisplayed(this.getDriverPricings(driver, name), driver, "Driver pricing", 120);
	}
}
