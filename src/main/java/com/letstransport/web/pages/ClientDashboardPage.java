package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.android.AndroidDriver;

public class ClientDashboardPage {
	WebDriver driver;

	public ClientDashboardPage(WebDriver driver) {
		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@type='number']")
	private WebElement clientPhoneNoTextField;

	@FindBy(xpath = "//span[text()='Send OTP']")
	private WebElement sentOtpButton;

	@FindBy(xpath = "//div[@class='_3061b046']/input")
	private WebElement sendOtpTextField;

	@FindBy(xpath = "//div[@class='_08aa5776']")
	private WebElement closeBtn;

	@FindBy(xpath = "//div[text()='No Listings To Show']")
	private WebElement noListingToShow;

	@FindBy(xpath = "//div[text()='Profile']")
	private WebElement profileIcon;

	@FindBy(xpath = "//div[text()='_260abf8']")
	private WebElement profileInner;

	@FindBy(xpath = "//div[text()='Switch account']")
	private WebElement switchAccount;

	@FindBy(xpath = "//div[text()='Choose an account']")
	private WebElement chooseAccount;

	@FindBy(xpath = "//div[text()='megha.CROMAVERTICAL1']")
	private WebElement switchProfile;

	@FindBy(xpath = "//div[@class='_9d3b1904']/div[3]/div[@class='d0c6cb57']")
	private WebElement adminDetails;

	public WebElement getAdminDetails(WebDriver driver, int n) {
		return driver
				.findElement(By.xpath("//div[@class='_9d3b1904']/div[3]/div[@class='da5cb5ba']/div[\'" + n + "\']"));

	}

	@FindBy(xpath = "//div[@class='_9d3b1904']/div[3]/div[@class='da5cb5ba']/div[1]/following-sibling::div[1]")
	private WebElement userList;

	@FindBy(xpath = "//div[@class='_9d3b1904']/div[3]/div[@class='da5cb5ba']/div[1]/following-sibling::div[2]")
	private WebElement hubStatus;

	@FindBy(xpath = "//div[@class='f99226e1']/descendant::section/descendant::div[2]/child::div/div[text()='megha.CROMAVERTICAL1']")
	private WebElement userlist;
	/*
	 * @FindBy(
	 * xpath="//section[@class='a35471a8 e7b196b0 _17798a37']/descendant::div[2]/figure"
	 * ) private WebElement userlist;
	 */

	@FindBy(xpath = "//div[@class='bc2bec7e']/descendant::div/following-sibling::div[@class='_5b306715']/descendant::div[2]/figure/following-sibling::div/div[@class='_107321e6']")
	private WebElement userListName;

	@FindBy(xpath = "//div[@class='bc2bec7e']/descendant::div/following-sibling::div[@class='_5b306715']/descendant::div[2]/figure/following-sibling::div/div[@class='da4073a0']")
	private WebElement userPhoneNum;

	@FindBy(xpath = "//h3[@class='ea024b22 b23be590']/following-sibling::address[1]/h4")
	private WebElement hubadress1;
	@FindBy(xpath = "//h3[@class='ea024b22 b23be590']/following-sibling::address[2]/h4")
	private WebElement hubadress2;
	@FindBy(xpath="//div[@class='_5b306715']/descendant::div[2]/following-sibling::ul/child::li[1]")
	private WebElement editUser;
	@FindBy(xpath="//div[@class='_5b306715']/descendant::div[2]/following-sibling::ul/child::li[2]")
	private WebElement deleteUser;

	public void ClientDashBoard_Admin_login(String URL, String PhoneNo, WebDriver driver) throws Exception {
		driver.get(URL);
		Thread.sleep(200);
		WebActionUtil.waitForElement(clientPhoneNoTextField, driver, "client registered phone number", 500);
		WebActionUtil.clickElement(clientPhoneNoTextField, driver, "client registered phone number");
		WebActionUtil.clearAndType(clientPhoneNoTextField, PhoneNo, "client registered phone number", driver);
		WebActionUtil.waitForElement(sentOtpButton, driver, "Sent Otp Button", 500);
		WebActionUtil.clickElement(sentOtpButton, driver, "Sent Otp");
		Thread.sleep(500);
		String otp = WebActionUtil.openNewWindow(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(sendOtpTextField, driver, "Send Otp");
		WebActionUtil.clearAndTypeEnter(sendOtpTextField, otp, "OtpTextField", driver);
		WebActionUtil.waitTillCDBPageLoad("clientdashboard home", driver, "ClientDashboardPage", 1000);
		Thread.sleep(3000);
		WebActionUtil.clickByJavaSriptExecutor(closeBtn, driver, "click on close btn");
		Thread.sleep(500);
		WebActionUtil.clickByJavaSriptExecutor(profileIcon, driver, "profile");
		WebActionUtil.clickElement(switchAccount, driver, "Switch Account");
		WebActionUtil.clickElement(switchProfile, driver, "Switch profile");
		WebActionUtil.waitTillCDBPageLoad("clientdashboard Admin", driver, "ClientDashboardPage", 6000);
		WebActionUtil.clickByJavaSriptExecutor(closeBtn, driver, "click on close btn");
		Thread.sleep(3000);
		String admin = adminDetails.getText();
		WebActionUtil.verifyContainsEqualText(admin, "Admin Details");
		String addNewUser = getAdminDetails(driver, 1).getText();
		String UserList = userList.getText();
        String HubStatus = hubStatus.getText();
		WebActionUtil.verifyContainsEqualText(addNewUser, "Add New User");
		Thread.sleep(500);
		WebActionUtil.verifyContainsEqualText(UserList, "User List");
		Thread.sleep(500);
		WebActionUtil.verifyContainsEqualText(HubStatus, "Hub Stats");

	}

	public void ClientDashBoard_Admin_ViewUserList(String URL, String PhoneNo, WebDriver driver,String clientName,String hub1,String hub2,String edit_client,String del_client) throws Exception {
		driver.get(URL);
		Thread.sleep(200);
		WebActionUtil.waitForElement(clientPhoneNoTextField, driver, "client registered phone number", 500);
		WebActionUtil.clickElement(clientPhoneNoTextField, driver, "client registered phone number");
		WebActionUtil.clearAndType(clientPhoneNoTextField, PhoneNo, "client registered phone number", driver);
		WebActionUtil.waitForElement(sentOtpButton, driver, "Sent Otp Button", 500);
		WebActionUtil.clickElement(sentOtpButton, driver, "Sent Otp");
		Thread.sleep(500);
		String otp = WebActionUtil.openNewWindow(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(sendOtpTextField, driver, "Send Otp");
		WebActionUtil.clearAndTypeEnter(sendOtpTextField, otp, "OtpTextField", driver);
		WebActionUtil.waitTillCDBPageLoad("clientdashboard home", driver, "ClientDashboardPage", 1000);
		Thread.sleep(3000);
		WebActionUtil.clickByJavaSriptExecutor(closeBtn, driver, "click on close btn");
		Thread.sleep(500);
		WebActionUtil.clickByJavaSriptExecutor(profileIcon, driver, "profile");
		WebActionUtil.clickElement(switchAccount, driver, "Switch Account");
		WebActionUtil.clickElement(switchProfile, driver, "Switch profile");
		WebActionUtil.waitTillCDBPageLoad("clientdashboard Admin", driver, "ClientDashboardPage", 6000);
		WebActionUtil.clickByJavaSriptExecutor(closeBtn, driver, "click on close btn");
		Thread.sleep(3000);
		WebActionUtil.clickElement(userList, driver, "userList");
		WebActionUtil.waitTillCDBPageLoad("user list", driver, "ClientDashboardPage", 4000);
		Thread.sleep(3000);
		WebActionUtil.clickElement(userlist, driver, "name of user");
		Thread.sleep(1000);
		String userName = userListName.getText();
		WebActionUtil.verifyContainsEqualText(userName, clientName);
		String userPhone = userPhoneNum.getText();
		WebActionUtil.verifyContainsEqualText(userPhone, PhoneNo);
		String firstHub = hubadress1.getText();
		WebActionUtil.verifyContainsEqualText(firstHub, hub1);
		String secondHub = hubadress2.getText();
		WebActionUtil.verifyContainsEqualText(secondHub, hub2);
	    String editusr = editUser.getText();
	    WebActionUtil.verifyContainsEqualText(editusr, edit_client);
		String delusr = deleteUser.getText();
		WebActionUtil.verifyContainsEqualText(delusr,del_client);
	}

}
