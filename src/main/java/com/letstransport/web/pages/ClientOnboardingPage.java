package com.letstransport.web.pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class ClientOnboardingPage {

	WebDriver driver;

	public ClientOnboardingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Select Group']/..//div[text()='Add New ']")
	private WebElement addGroup;

	@FindBy(xpath = "//label[text()='Group Name']/../..//input[@id='name']")
	private WebElement groupName;

	@FindBy(xpath = "//label[text()='Industry Type']/../..//option[text()='Select Industry']")
	private WebElement clickIndustry;

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveButton;
	
	@FindBy(xpath = "//div[text()='*Upload upto 5 documents(maximum size is 5 MB)']/..//button[text()='Save']")
	private WebElement sveButton;
	
//	@FindBy(xpath = "(//div[text()='View']/../..//div)[2]")
	@FindBy(xpath ="//*[@class='ba4f191b _25b9dce9']")
	private WebElement commerceCheckBox;		
	
	@FindBy(xpath = "//div[text()=' Select from other commercial ']")
	private WebElement selectOtherCommercial;

	@FindBy(xpath = "//button[text()='Next']")
	private WebElement nextButton;

	@FindBy(xpath = "//button[text()='Submit']")
	private WebElement submitButton;

	@FindBy(xpath = "//span[text()='Close']")
	private WebElement closeButton;
	
	@FindBy(xpath = "//div[text()='Group']/..")
	private WebElement groupTab;

	@FindBy(xpath = "//label[text()='Legal Entity Name']/../..//input[@id='name']")
	private WebElement entityName;

	@FindBy(xpath = "//label[text()='PAN Number']/../..//input[@id='pan_number']")
	private WebElement panNumber;

	@FindBy(xpath = "//label[text()='Registered office address']/../..//textarea[@id='registered_office_address']")
	private WebElement officeAddress;

	@FindBy(xpath = "//label[text()='Vertical Name']/../..//input[@id='name']")
	private WebElement verticalName;

	@FindBy(xpath = "//label[text()='GSTIN Number']/../..//input[@id='gstin_number']")
	private WebElement gstNumber;

	@FindBy(xpath = "//input[@id='is_dummy']")
	private WebElement checkBoxDummy;

	@FindBy(xpath = "//label[text()='Billing Address']/../..//textarea[@id='billing_address']")
	private WebElement billingAddress;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='FcNameOne']")
	private WebElement FcNameOne;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='FcEmailOne']")
	private WebElement FcEmailOne;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='FcContactOne']")
	private WebElement FcContactOne;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='FcNameTwo']")
	private WebElement FcNameTwo;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='FcEmailTwo']")
	private WebElement FcEmailTwo;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='FcContactTwo']")
	private WebElement FcContactTwo;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='OcName']")
	private WebElement OcName;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='OcEmail']")
	private WebElement OcEmail;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='OcContact']")
	private WebElement OcContact;

//	@FindBy(xpath = "//label[text()='Hub Name']/../..//input[@id='hubName']")
	@FindBy(xpath = "//input[@id='hubName']")
	
	private WebElement hubName;
	
	@FindBy(xpath = "//div[text()='View']")
	private WebElement veiwCommercials;

	@FindBy(xpath = "//label[text()='Hub address']/../..//textarea[@id='address']")
	private WebElement hubAddress;

	@FindBy(xpath = "//label[text()='City (LT Regional City Office)']/../..//option[text()='Select LT Regional office for the hub']")
	private WebElement clickHub;

	@FindBy(xpath = "//label[text()='City']/../..//option[text()='Select Hub City']")
	private WebElement clickCity;

//	@FindBy(xpath = "//label[text()='Hub State']/../..//option[text()='Select State']")
	@FindBy(xpath ="//select[@name='state']")
	private WebElement clickState;
	
	@FindBy(xpath ="//select[@name='city_code']")
	private WebElement clickRegionalCity;

//	public WebElement selectState(WebDriver driver, String state) {
//		return driver.findElement(By.xpath("//label[text()='Hub State']/../..//option[text()='" + state + "']"));
//	}

	
	public WebElement selectState(WebDriver driver, String state) {
		return driver.findElement(By.xpath("//*[text()='" + state + "']"));
	}
	
	public WebElement SelectRegionCity(WebDriver driver, String city) {
		return driver.findElement(By.xpath("//*[text='" + city + "']"));
	}
	
	//
	
	public WebElement selectIndustry(WebDriver driver, String industry) {
		return driver.findElement(By.xpath("//label[text()='Industry Type']/../..//option[text()='" + industry + "']"));
	}

	public WebElement selectRegOffice(WebDriver driver, String state) {
		return driver.findElement(
				By.xpath("//label[text()='City (LT Regional City Office)']/../..//option[text()='" + state + "']"));
	}

	public WebElement selectCity(WebDriver driver, String city) {
		return driver.findElement(By.xpath("//label[text()='City']/../..//option[text()='" + city + "']"));
	}

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='name']")
	private WebElement hubContact;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='email']")
	private WebElement hubEmail;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='contact_number']")
	private WebElement hubCntctNumber;

	@FindBy(xpath = "//div[text()='Add New ']")
	private WebElement addNewCommercial;

	@FindBy(xpath = "//label[text()='Commercial Name']/../..//input[@id='name']")
	private WebElement commercialName;

	@FindBy(xpath = "//label[text()='Business Type']/../..//option[text()='Select Industry']")
	private WebElement businessType;

	@FindBy(xpath = "//label[text()='Contract End Date']/../..//input[@id='end']")
	private WebElement enddate;

	@FindBy(xpath = "//label[text()='Margin %']/../..//input[@id='margin']")
	private WebElement margin;
	
	@FindBy(xpath = "//label[text()='Credit Period']/../..//input[@id='credit_period']")
	private WebElement creditPeriod;
	
	@FindBy(xpath = "//label[text()='Remarks']/../..//textarea[@id='remarks']")
	private WebElement remarks;	

	@FindBy(xpath = "//label[text()='Attach Commercial']")
	private WebElement attachCommercial;

	@FindBy(xpath = "//label[text()='Attach Client Approval files']")
	private WebElement attachApproval;

	@FindBy(xpath = "//div[text()='Lock']")
	private WebElement lockButton;

	@FindBy(xpath = "//*[local-name()='svg']//*[name()='g']")
	private WebElement logOutButton;
	
	@FindBy(xpath = "//button[text()='Submit to OPM']")
	private WebElement submitToOPM;

	@FindBy(xpath = "//button[text()='Submit to finance']")
	private WebElement submitToFinance;
	
	@FindBy(xpath = "//div[text()='Pending List']")
	private WebElement pendingList;
	
	@FindBy(xpath = "//span[text()='Group']/../..//button[text()='Next']")
	private WebElement groupNext;
	
	@FindBy(xpath = "//span[text()='Legal Entity']/../..//button[text()='Next']")
	private WebElement legalEntityNext;
	
	@FindBy(xpath = "//span[text()='Vertical']/../..//button[text()='Next']")
	private WebElement verticalNext;
	
	@FindBy(xpath = "//button[text()='Next']")
	private WebElement Next;
	
	@FindBy(xpath = "//*[text()='POC Details']")
	private WebElement POCdetails;
	
	
	
	@FindBy(xpath = "//span[text()='GSTIN Details']/../..//button[text()='Next']")
	private WebElement GSTNDetailsNext;
	
	@FindBy(xpath = "//span[text()='City']/../..//button[text()='Next']")
	private WebElement hubNext;
	
	@FindBy(xpath = "//div[text()='Commercial']/../..//button[text()='Submit to OPM']")
	private WebElement commercialToOPM;
	
	@FindBy(xpath = "//div[text()='Approval required']/../../..//div[text()='View']")
	private WebElement approvalReqView;
	
	@FindBy(xpath = "//div[text()='Is this commercial approved?']/..//div[text()='Approved']")
	private WebElement approvedButton;
	
	

	public WebElement selectBusinessType(WebDriver driver, String type) {
		return driver.findElement(By.xpath("//label[text()='Business Type']/../..//option[text()='" + type + "']"));
	}

	public WebElement getLockedClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='" + client+ "']"));
	}

	public WebElement getApproveClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='" + client+ "']"));
	}
	
	public WebElement getDeleteClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='"+client+"']/../../../..//div[text()='Delete']"));
	}
	
	@FindBy(xpath = "//input[@placeholder='Search by Card Id']")
	private WebElement clientSearchBar;
	
	@FindBy(xpath = "//div[text()=' No Data Found !! ']")
	private WebElement noDataText;
	
	@FindBy(xpath = "//div[text()='Group']//img[@src='src/assets/images/unlocked.svg']")
	private WebElement clickGroup;
	
	@FindBy(xpath = "//span[text()='Login']")
	private WebElement googgleLoginBtn;
	
	@FindBy(xpath = "//div[(text()='Use another account')]")
	private WebElement anotherAccount;

	@FindBy(xpath = "//input[@type='email']")
	private WebElement usrName;

	@FindBy(xpath = "//input[@type='password']")
	private WebElement pswrd;

	@FindBy(xpath = "//span[text()='Next']")
	private WebElement nxtBtn;
	
	@FindBy(xpath = "//div[text()='karthik.kr@letstransport.team']")
	private WebElement krBtn;
	
	@FindBy(xpath = "//div[text()='priya']")
	private WebElement priyaBtn;
	
	@FindBy(xpath = "//button[text()='Next']")
	private WebElement nextBtn;
	
	@FindBy(xpath = "//*[text()='Submit to OPM']")
	private WebElement SubmitOPM;

	
	
	@FindBy(xpath = "//div[text()='OPM Approval']")
	private WebElement opmApproval;
	
	@FindBy(xpath = "//input[@placeholder='Search by Group Name']")
	private WebElement searchByGroupName;
	
	
	@FindBy(xpath ="//*[contains(text(),'Download')]")
	private WebElement dwnloadFile;
	
	@FindBy(xpath ="//*[@class='a0628723']")
	private WebElement createdGroup;
	
	//
	
	//
	//div[text()='PLASMA']

	public void COB_Sales_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		driver.get(URL);
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(8000);
	}
	
	public void quick_COB_Sales_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		driver.get(URL);
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clickByJavaSriptExecutor(this.anotherAccount, driver, "another Account");
		WebActionUtil.waitForVisiblityofElement("//input[@type='email']", 90, driver);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(8000);
	}

	public void addNewGroup(WebDriver driver,String filepath,String sheet, String testcase, String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickByJavaSriptExecutor(this.addGroup, driver, "Add group Button");
		WebActionUtil.waitTillCOBPageLoad("Add group button", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.groupName, client, "Group name", driver);
		WebActionUtil.clickElement(this.clickIndustry, driver, "Click industry");
		WebActionUtil.waitTillCOBPageLoad("Industry dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectIndustry(driver, sData[BaseLib.gv.cobitcount]), driver, "IT industry");
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewEntity(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.entityName,  client+" entity", "Entity name", driver);
		WebActionUtil.type(this.panNumber, sData[BaseLib.gv.cobpncount], "Pan number", driver);
		WebActionUtil.type(this.officeAddress, sData[BaseLib.gv.cobrocount], "Office Address", driver);
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewVertical(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.verticalName, client+" Vertical 1", "vertica name", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewGSTNo(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("State dropdown", driver, "ClientOnboarding Page", 120);
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		
	
		WebActionUtil.actionClick(this.clickState, driver, "Click State");

		WebActionUtil.clickElement(this.selectState(driver,sData[BaseLib.gv.cobhscount]),driver,"");
	
		WebActionUtil.type(this.gstNumber, sData[BaseLib.gv.cobgstcount], "GST No", driver);
		WebActionUtil.waitTillCOBPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		 WebActionUtil.clickElement(this.checkBoxDummy,driver, "Dummy GST No");
		 WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.submitButton, driver, "Submit button");
		WebActionUtil.waitTillCOBPageLoad("Submit button", driver, "Client Onboarding Page", 120);
	}
	
	public void addNewGSTNoSubmit(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.type(this.gstNumber, sData[BaseLib.gv.cobgstcount], "GST No", driver);
		WebActionUtil.waitTillCOBPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		 WebActionUtil.clickElement(this.checkBoxDummy,driver, "Dummy GST No");
		 WebActionUtil.waitTillPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.clickState, driver, "Click State");
		WebActionUtil.waitTillCOBPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectState(driver, sData[BaseLib.gv.cobhscount]), driver, "IT industry");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");		
		WebActionUtil.waitTillCOBPageLoad("pending list", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list");
		
	}

	public void addNewGSTDetails(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
//		WebActionUtil.waitTillCOBPageLoad("Submit button", driver, "Client Onboarding Page", 120);
//		WebActionUtil.type(this.billingAddress, sData[BaseLib.gv.cobbacount], "Billing Address", driver);
		WebActionUtil.type(this.FcNameOne, sData[BaseLib.gv.cobfn1count], "Finance contact name", driver);
		WebActionUtil.type(this.FcEmailOne, sData[BaseLib.gv.cobfe1count], "Finance contact email", driver);
		WebActionUtil.type(this.FcContactOne, sData[BaseLib.gv.cobfc1count], "Finance contact No", driver);
		WebActionUtil.scrollIntoView(driver, this.OcName);
		WebActionUtil.type(this.FcNameTwo, sData[BaseLib.gv.cobfn2count], "Finance contact name", driver);
		WebActionUtil.type(this.FcEmailTwo, sData[BaseLib.gv.cobfe2count], "Finance contact email", driver);
		WebActionUtil.type(this.FcContactTwo, sData[BaseLib.gv.cobfcn2count], "Finance contact No", driver);
		
		WebActionUtil.type(this.OcName, sData[BaseLib.gv.cobocncount], "Operation contact name", driver);
		WebActionUtil.type(this.OcEmail, sData[BaseLib.gv.cobocecount], "Operation contact email", driver);
		WebActionUtil.type(this.OcContact, sData[BaseLib.gv.cobocount], "Operation contact No", driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewHUB(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		
		System.out.println(selectState(driver,sData[BaseLib.gv.cobro2ount]));
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		
		
//		WebActionUtil.actionClick(this.clickRegionalCity, driver, "Click Regional city");
		Thread.sleep(2000);
		WebActionUtil.selectbyIndex(driver, this.clickRegionalCity,1);
		
//		WebActionUtil.selectbyValue(driver, this.clickRegionalCity, "Bangalore");
		
		Thread.sleep(2000);
//		WebActionUtil.actionClick(this.SelectRegionCity(driver,sData[BaseLib.gv.cobro2ount]),driver,"Regional city");

		
		WebActionUtil.type(this.hubName, client+" HUB", "HUB NAME", driver);
		WebActionUtil.type(this.hubAddress, client+" HUB Address goes here", "HUB Address", driver);
//		WebActionUtil.clickElement(this.clickHub, driver, "click HUB");
	
		WebActionUtil.type(this.hubContact, sData[BaseLib.gv.cobhcncount], "HUB contact name", driver);
		WebActionUtil.type(this.hubEmail, sData[BaseLib.gv.cobhcecount], "HUB email name", driver);
		WebActionUtil.type(this.hubCntctNumber, sData[BaseLib.gv.cobhc1ount], "HUB contact number", driver);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
		
	}

	public String addNewCommercial(WebDriver driver,String filepath,String sheet, String testcase,String client,String sheet1,String cpath) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);				
		WebActionUtil.waitTillCOBPageLoad("Commercial", driver, "Client Onboarding Page", 120);
		
		
		WebActionUtil.scrollIntoView(driver, this.addNewCommercial);
		WebActionUtil.waitTillCOBPageLoad("add new Commercial", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.addNewCommercial, driver, "Commercial");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(dwnloadFile, driver, "Download File");
		WebActionUtil.downloadOverWrite(cpath);
		
		
		
		WebActionUtil.type(this.commercialName, client+" commercial", "Commercial "+client, driver);
		
		
		WebActionUtil.clickElement(this.businessType, driver, "Business type");
		WebActionUtil.clickElement(this.selectBusinessType(driver, sData[BaseLib.gv.cobbtcount]), driver, "Business type");
		WebActionUtil.waitTillCOBPageLoad("Business type Ent OD / Adhoc ", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.enddate, sData[BaseLib.gv.cobedcount], "Commercial Name", driver);
		WebActionUtil.type(this.margin, sData[BaseLib.gv.cobmcount], "Margin", driver);
		
		WebActionUtil.type(this.creditPeriod, sData[BaseLib.gv.cobcpcount], "Credit period", driver);
		WebActionUtil.type(this.remarks, "could have been better", "Remarks", driver);
		
		WebActionUtil.scrollIntoView(driver, this.attachCommercial);
		WebActionUtil.clickElement(this.attachCommercial, driver, "attach commercial");
		WebActionUtil.uploadFile(cpath);
		WebActionUtil.scrollIntoView(driver, this.sveButton);
		WebActionUtil.clickElement(this.sveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("commerce Check Box", driver, "Client Onboarding Page", 120);

		WebActionUtil.waitTillCOBPageLoad("select Other Commercial", driver, "select Other Commercial", 300);
		WebActionUtil.clickElement(this.commerceCheckBox, driver, "Commercial check box");
		
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.waitTillCOBPageLoad("save button", driver, "Client Onboarding Page", 300);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		
		WebActionUtil.waitTillCOBPageLoad("submit to finance", driver, "Client Onboarding Page", 300);
		WebActionUtil.clickElement(this.submitToFinance, driver, "submit to finance");
		
		Thread.sleep(5000);
		WebActionUtil.waitTillCOBPageLoad("pending List page", driver, "Client Onboarding Page", 300);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list section");
		
		WebActionUtil.waitTillCOBPageLoad("pending List page", driver, "Client Onboarding Page", 300);
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client));
		
		WebActionUtil.assertElementDisplayed(this.getLockedClient(driver, client), driver, "client after submitting to finance");
		return client;
	}
	
	public void COB_Finance_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clickByJavaSriptExecutor(this.anotherAccount, driver, "new account Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='email']", 90, driver);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
//		WebActionUtil.waitForVisiblityofElement("//div[text()='priya']", 90, driver);
//		WebActionUtil.clickByJavaSriptExecutor(this.priyaBtn, driver, "priya Button");
		Thread.sleep(10000);
	}
	
	public void Approve_Client(WebDriver driver,String client) throws Exception{
		
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 50000, driver);
		WebActionUtil.waitTillCOBPageLoad("Group Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list section");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 50000, driver);
		WebActionUtil.waitTillCOBPageLoad("Group Next", driver, "Client Onboarding Page", 120);
		
		
//		System.out.println(getLockedClient(driver, client));
		
		
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client));
		WebActionUtil.clickElement(this.getLockedClient(driver, client), driver, "select the client");
		WebActionUtil.waitTillCOBPageLoad("Group Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.groupNext, driver, "group next button");
		WebActionUtil.waitTillCOBPageLoad("legal entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.legalEntityNext, driver, "legal entity next button");
		WebActionUtil.waitTillCOBPageLoad("Vertical Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.verticalNext, driver, "Vertical next button");
		WebActionUtil.waitTillCOBPageLoad("Lock Button", driver, "Client Onboarding Page", 200);
		WebActionUtil.scrollIntoView(driver, this.lockButton);
		WebActionUtil.clickElement(this.lockButton, driver, "lock button");
		
		// new changes POC details
			
		
		
		
		WebActionUtil.waitTillCOBPageLoad("POC detail", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.POCdetails, driver, "POC details");
		WebActionUtil.scrollIntoView(driver, this.Next);
		WebActionUtil.clickElement(this.Next, driver, "next button");
		
		WebActionUtil.waitTillCOBPageLoad("NExt", driver, "Client Onboarding Page", 120);
		
		WebActionUtil.scrollIntoView(driver, this.Next);
		WebActionUtil.clickElement(this.Next, driver, "next button");
		
		
		
		
		
		
////		WebActionUtil.waitTillCOBPageLoad("GSTN Details", driver, "Client Onboarding Page", 120);
////		WebActionUtil.scrollIntoView(driver, this.GSTNDetailsNext);
////		WebActionUtil.clickElement(this.GSTNDetailsNext, driver, "GSTNo next button");
////		WebActionUtil.waitTillCOBPageLoad("Hub Next", driver, "Client Onboarding Page", 120);
////		WebActionUtil.scrollIntoView(driver, this.hubNext);
////		WebActionUtil.clickElement(this.hubNext, driver, "HUb next button");
////		WebActionUtil.waitTillCOBPageLoad("Commercial next", driver, "Client Onboarding Page", 120);
		
		WebActionUtil.waitTillCOBPageLoad("Lock Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.lockButton, driver, "lock button");
		
		WebActionUtil.waitTillCOBPageLoad("View Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.approvalReqView, driver, "view button");
		
		
		WebActionUtil.waitTillCOBPageLoad("Approvals", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.approvedButton);
	
		driver.findElement(By.xpath("//div[text()='Is this commercial approved?']/..//div[text()='Approved']")).click();
		
		
		
//		try {
//			Thread.sleep(5000);
//			WebActionUtil.clickElement(this.approvedButton, driver, "Approve button");
//		} catch (Exception f) {
//			
//		}
//		
//		    try {
		        Alert alert = driver.switchTo().alert();
		        String alertText = alert.getText();
		        System.out.println("Alert data: " + alertText);
		        Thread.sleep(5000);
		        alert.accept();
//		    } catch (NoAlertPresentException e) {
//		    	 
//		    }
		
		    
		WebActionUtil.waitTillCOBPageLoad("approve Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.closeButton);
		WebActionUtil.clickElement(this.closeButton, driver, "close button");
//		WebActionUtil.waitTillCOBPageLoad("close Button", driver, "Client Onboarding Page", 120);
//		WebActionUtil.clickElement(this.commercialToOPM, driver, "commercial OPM");
//		WebActionUtil.waitTillCOBPageLoad("Commercial OPM", driver, "Client Onboarding Page", 230);
//		Thread.sleep(5000);
		WebActionUtil.waitTillCOBPageLoad("submit to opm", driver, "Client Onboarding Page", 180);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100000, driver);
		WebActionUtil.scrollIntoView(driver, this.SubmitOPM);
		WebActionUtil.clickElement(this.SubmitOPM, driver, "OPM Approval");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 30000, driver);	
		
		
		
		Thread.sleep(10000);
		WebActionUtil.clickElement(this.opmApproval, driver, "OPM Approval");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100000, driver);		
		WebActionUtil.waitTillCOBPageLoad("opm approval", driver, "Client Onboarding Page", 180);
		WebActionUtil.scrollIntoView(driver, this.getApproveClient(driver, client));
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100000, driver);
		WebActionUtil.waitTillCOBPageLoad("opm approval", driver, "Client Onboarding Page", 180);
		WebActionUtil.assertElementDisplayed(this.getApproveClient(driver, client), driver, "client after submitting to OPM");
		}

	
	public WebElement getClient(WebDriver driver, String client) {
		
		client.equalsIgnoreCase(client);
		return driver.findElement(By.xpath("//div[text()='"+client+"']"));
		
		
		

	}
	public void verfiyAfterAddingHubs(WebDriver driver,String client) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.searchByGroupName, client, "Client Name", driver);
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100000, driver);
		
		WebActionUtil.verifyElementContainsText(this.createdGroup,client, driver);
//		WebActionUtil.assertElementDisplayed(this.getClient(driver, client), driver, "client name");
	}
	
	public void clickLogOut(WebDriver driver) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.logOutButton, driver, "Logout COB");
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
	}
	
	public void deleteCOBCard(WebDriver driver,String client) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Pending client page", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.getDeleteClient(driver, client));
		WebActionUtil.assertElementDisplayed(this.getDeleteClient(driver, client), driver, "client after submitting to finance");
		WebActionUtil.waitTillCOBPageLoad("Pending client page", driver, "Client Onboarding Page", 120);
		WebActionUtil.actionClick(this.getDeleteClient(driver, client), driver, "Delete client");
		WebActionUtil.waitTillCOBPageLoad("Pending client page", driver, "Client Onboarding Page", 120);
	}
	
	public void verifyDeletedClient(WebDriver driver,String client) throws Exception{
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 50000, driver);
		WebActionUtil.type(this.clientSearchBar, client, client, driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.assertElementDisplayed(this.noDataText, driver, "No data found");
	}
	
	public void editClient(WebDriver driver,String client) throws Exception{	
/*		WebActionUtil.waitTillCOBPageLoad("Pending client page", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list page");
		WebActionUtil.waitTillCOBPageLoad("Pending client page", driver, "Client Onboarding Page", 120);
		WebActionUtil.waitForVisiblityofElement("//input[@placeholder='Search by Card Id']", 120, driver);
		WebActionUtil.assertElementDisplayed(this.clientSearchBar, driver, "pending page search bar");
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client));*/
		WebActionUtil.actionClick(this.getLockedClient(driver, client), driver, "Select "+client);
		WebActionUtil.waitTillCOBPageLoad("client edit page", driver, "Client Onboarding Page", 120);
		WebActionUtil.actionClick(this.clickGroup, driver, "clicked on Group ");
		WebActionUtil.waitTillCOBPageLoad("group section", driver, "Client Onboarding Page", 120);
	}
	
	public void editGroup(WebDriver driver,String filepath,String sheet, String testcase, String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.deleteAndType(this.groupName, client+" 2", "Group name", driver);
		WebActionUtil.waitTillCOBPageLoad("Industry dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectIndustry(driver, sData[BaseLib.gv.cobitcount]), driver, "IT industry");
		WebActionUtil.waitTillCOBPageLoad("Industry dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectIndustry(driver, "Electronics"), driver, "Electronics industry");
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void editEntity(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.deleteAndType(this.entityName, client+" 2 entity", client+"2 entity", driver);
		WebActionUtil.deleteAndType(this.panNumber,"JIMCM6541L", "Pan number", driver);
		WebActionUtil.deleteAndType(this.officeAddress, sData[BaseLib.gv.cobrocount]+" i mean really here", "Office Address", driver);
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}
	
	public void editVertical(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.deleteAndType(this.verticalName, client+" 2 Vertical", "vertical name", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}
	
	public void editGSTNo(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.deleteAndType(this.gstNumber, "29JIMCM6541L", "GST No", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}
	
	public void editGSTDetails(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
		WebActionUtil.deleteAndType(this.billingAddress, sData[BaseLib.gv.cobbacount], "Billing Address", driver);
		
		WebActionUtil.clickElement(this.saveButton, driver, "save");
		
		WebActionUtil.clickElement(this.nextBtn, driver, "next");
		
		WebActionUtil.scrollIntoView(driver, this.FcNameOne);
		
		WebActionUtil.deleteAndType(this.FcNameOne, sData[BaseLib.gv.cobfn1count], "Finance contact name", driver);
		WebActionUtil.deleteAndType(this.FcEmailOne, sData[BaseLib.gv.cobfe1count], "Finance contact email", driver);
		WebActionUtil.deleteAndType(this.FcContactOne, sData[BaseLib.gv.cobfc1count], "Finance contact No", driver);
		WebActionUtil.scrollIntoView(driver, this.OcName);
		WebActionUtil.deleteAndType(this.FcNameTwo, sData[BaseLib.gv.cobfn2count], "Finance contact name", driver);
		WebActionUtil.deleteAndType(this.FcEmailTwo, sData[BaseLib.gv.cobfe2count], "Finance contact email", driver);
		WebActionUtil.deleteAndType(this.FcContactTwo, sData[BaseLib.gv.cobfcn2count], "Finance contact No", driver);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.deleteAndType(this.OcName, sData[BaseLib.gv.cobocncount], "Operation contact name", driver);
		WebActionUtil.deleteAndType(this.OcEmail, sData[BaseLib.gv.cobocecount], "Operation contact email", driver);
		WebActionUtil.deleteAndType(this.OcContact, sData[BaseLib.gv.cobocount], "Operation contact No", driver);
		WebActionUtil.waitTillCOBPageLoad("GST details", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}
	
	public void editHUB(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.deleteAndType(this.hubName, client+" 2 HUB", "HUB NAME", driver);
		WebActionUtil.deleteAndType(this.hubAddress, client+" HUB Address goes here", "HUB Address", driver);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.deleteAndType(this.hubContact,sData[BaseLib.gv.cobhcncount], "HUB contact name", driver);
		WebActionUtil.deleteAndType(this.hubEmail, sData[BaseLib.gv.cobhcecount], "HUB email name", driver);
		WebActionUtil.deleteAndType(this.hubCntctNumber, sData[BaseLib.gv.cobhc1ount], "HUB contact number", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}
	
	public void editCommercials(WebDriver driver,String filepath,String sheet, String testcase,String client,String sheet2) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.veiwCommercials, driver, "View button");
		WebActionUtil.waitTillCOBPageLoad("view button", driver, "Client Onboarding Page", 120);
		WebActionUtil.deleteAndType(this.commercialName, client+" 2 commercial", "Commercial "+client, driver);
		WebActionUtil.clickElement(this.selectBusinessType(driver, sData[BaseLib.gv.cobbtcount]), driver, "Business type");
		WebActionUtil.clickElement(this.selectBusinessType(driver, "Scheduled"), driver, "Business type");
		WebActionUtil.waitTillCOBPageLoad("Business type Ent OD / Adhoc ", driver, "Client Onboarding Page", 120);
		WebActionUtil.deleteAndType(this.enddate, sData[BaseLib.gv.cobedcount], "Commercial Name", driver);
		WebActionUtil.deleteAndType(this.margin, "5", "Margin", driver);
		WebActionUtil.scrollIntoView(driver, this.attachApproval);
		WebActionUtil.deleteAndType(this.creditPeriod, "15", "Credit period", driver);
		WebActionUtil.deleteAndType(this.remarks, "could have been better", "Remarks", driver);
		WebActionUtil.clickElement(this.attachCommercial, driver, "attach commercial");
		WebActionUtil.uploadFile(sheet2);
		WebActionUtil.waitTillCOBPageLoad("attach commercial", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.sveButton);
		WebActionUtil.clickElement(this.sveButton, driver, "save button");
		Thread.sleep(5000);
		WebActionUtil.clickElement(this.opmApproval, driver, "opm approval section");
		WebActionUtil.waitTillCOBPageLoad("attach commercial", driver, "Client Onboarding Page", 300);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list section");
		WebActionUtil.waitTillCOBPageLoad("pending List page", driver, "Client Onboarding Page", 300);
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client+" 2"));
		WebActionUtil.assertElementDisplayed(this.getLockedClient(driver, client+" 2"), driver, "client after submitting to finance");
	}
	
	public void navigatePendingHub(WebDriver driver) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.logOutButton, driver, "Logout COB");
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
	}
}
