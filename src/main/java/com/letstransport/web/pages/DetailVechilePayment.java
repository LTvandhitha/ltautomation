package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.init.InitializePages;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class DetailVechilePayment {
	WebDriver driver;

	public DetailVechilePayment(WebDriver driver) {
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='options-footer']/div[text()='Fetch Logsheets']")
	private WebElement fetchLogSheet;
	@FindBy(xpath = "//div[@class='count-parent unmapped']")
	private WebElement unmapped;
	@FindBy(xpath = "//div[@class='vehicle-icon vehicle-info scheduled']")
	private WebElement unmappedVechileNo;
	@FindBy(xpath = "//div[@class='close-button']")
	private WebElement closeBtn;
	@FindBy(xpath = "//div[@class='back-home-button']/a[text()='Back To Home']")
	private WebElement backToHome;
	@FindBy(xpath = "//div[@class='link-with-submenu  ']/li[text()='Payments']")
	private WebElement payments;
	@FindBy(xpath = "//li[text()='Detailed Vehicle Payments']")
	private WebElement detailPayment;
	@FindBy(xpath = "//div[text()='Search Client...']/..//input")
	private WebElement searchClient;
	@FindBy(xpath = "//div[text()='LONEE']")
	private WebElement clientName;
	@FindBy(xpath = "//div[text()='GO']")
	private WebElement GoBtn;
	@FindBy(xpath = "//div[text()='View Payments']")
	private WebElement viewPayments;

	public WebElement searchvecNo(String vecnumber) {
		return this.driver.findElement(By.xpath("//div[text()=\'" + vecnumber + "\']/preceding-sibling::div/div/div"));

	}

	@FindBy(xpath = "//th[text()='Paid Amount']/../../following-sibling::tbody/tr/td[5]")
	private WebElement amountbeforegoing;

	public String _getPaidAmounForVehicle(String vecnum) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.clickElement(payments, driver, "ClickOnPayment");
		WebActionUtil.clickElement(detailPayment, driver, "click on detail payment");
		Thread.sleep(3000);
		WebActionUtil.waitForElement(searchClient, driver, "search client", 500);
		WebActionUtil.actionClick(searchClient, driver, "click on search client");
		WebActionUtil.clearAndType(searchClient, "lonee", "name of client", driver);
		WebActionUtil.clickElement(clientName, driver, "name of client");
		WebActionUtil.clickElement(GoBtn, driver, "clicl on Go");
		Thread.sleep(7000);
		WebActionUtil.actionClick(this.searchvecNo(vecnum), driver, "click on checkbox");
		WebActionUtil.actionClick(viewPayments, driver, "click on view payment");
		Thread.sleep(15000);
		String amountpaid = amountbeforegoing.getText();
		return amountpaid;

	}

	public void _verifyunmappedLogsheetPayment(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				" wait till searchtextfield will be visible", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				"click on searchTextfield", GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "select and click on NM");
		wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet link");
		WebActionUtil.clickElement(unmapped, driver, "click on unMapped");
		String unmappedVecNo = unmappedVechileNo.getText();
		WebActionUtil.clickElement(closeBtn, driver, "click on close");
		WebActionUtil.clickElement(backToHome, driver, "click on Back To Home");
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, " click on hamberger");
		WebActionUtil.clickElement(payments, driver, "Click On Payment");
		WebActionUtil.clickElement(detailPayment, driver, "click on detail payment");
		Thread.sleep(3000);
		WebActionUtil.waitForElement(searchClient, driver, "wait for search client textfield", 500);
		WebActionUtil.actionClick(searchClient, driver, "click on search client textfield");
		WebActionUtil.clearAndType(searchClient, "lonee", "name of client", driver);
		WebActionUtil.clickElement(clientName, driver, "name of client");
		WebActionUtil.clickElement(GoBtn, driver, "click on Go Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(5000);
		WebActionUtil.actionClick(this.searchvecNo(unmappedVecNo), driver, "click on checkbox");
		WebActionUtil.actionClick(viewPayments, driver, "click on view payment");
		Thread.sleep(50000);
		String amountpaidaftergoing = amountbeforegoing.getText();
		WebActionUtil.verifyContainsText("0", amountpaidaftergoing);

	}
	@FindBy(xpath="//div[text()='EntOdArranged']")
	private WebElement EntodArranged;
	@FindBy(xpath="//div[text()='Search']")
	private WebElement searchBtn;
	public WebElement getDriverPayoutChkBox(WebDriver driver,String vecnumber){
		return driver.findElement(By.xpath("//td[text()=\'" + vecnumber + "\']/../td[26]/div/div/div[@class='checkbox-div ']"));
	}
	@FindBy(xpath="//div[text()='REQUEST PAYMENT']")
	private WebElement requestPayment;
	@FindBy(xpath="//div[@class='sc-dNLxif fFQkov']")
	private WebElement backBtn;
	//td[text()='YW95YW9569']/following-sibling::td[3]
	public WebElement getdriver_Client(WebDriver driver,String vecnumber){
		return driver.findElement(By.xpath("//td[text()=\'" + vecnumber + "\']/following-sibling::td[3]"));
	}
	

	public void _EntodDetailVechilePayment(WebDriver driver,String createdIntoVerified) throws Exception{
		InitializePages wdInit = new InitializePages(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		WebActionUtil.clickElement(EntodArranged, driver, "Entod Arranged");
		WebActionUtil.clickElement(searchBtn, driver, "Search Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getDriverPayoutChkBox(driver, createdIntoVerified));
		Thread.sleep(1000);
		String clientname = getdriver_Client(driver, createdIntoVerified).getText();
		WebActionUtil.clickElement(requestPayment, driver, "Request Payment");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(backBtn, driver, "Back Button");
	}
}
