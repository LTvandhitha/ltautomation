package com.letstransport.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class DetailedVehiclePaymentsPage {

	WebDriver driver;

	public DetailedVehiclePaymentsPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Search Client...']/..//input")
	private WebElement searchClientInput;

	@FindBy(xpath = "//*[text()='Search Client...']")
	private WebElement searchClient;

	@FindBy(xpath = "//*[text()='GO']")
	private WebElement goBtn;

	@FindBy(xpath = "//*[text()='Freeze Payments']")
	private WebElement freezePayments;
	
	@FindBy(xpath = "//*[text()='View Payments']")
	private WebElement viewPayments;

	@FindBy(xpath = "//*[text()='Freeze Payment Till This Date']")
	private WebElement freezePaymentsTillDate;

	@FindBy(xpath = "//*[text()='Request Payment']")
	private WebElement requestPayment;
	
	@FindBy(xpath = "//*[@class='back-button']")
	private WebElement backBtn;
	
	@FindBy(xpath = "//*[@class='Select-menu-outer']")
	private WebElement xyz;
	
	@FindBy(xpath = "//*[text()='Total Amount']//input[@type='checkbox']")
	private List<WebElement> checkBoxesTotalAmount;
	
	@FindBy(xpath = "//*[text()='Total Amount']//input[@type='checkbox']")
	private WebElement checkBoxTotalAmount;
	
	@FindBy(xpath = "//*[text()='1' and text()=' Failed']")
	private List<WebElement> requestFailed;

	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(
				By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"
						+ date + "']"));
	}

	public WebElement getCheckBox(WebDriver driver, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + vehicle + "']/..//*[@class='checkbox-div ']"));
	}

	public WebElement getPaymentCheckBox(WebDriver driver, String vehicle) {
		return driver.findElement(By.xpath(
				"//*[text()='Vehicle Number']/../../..//*[text()='" + vehicle + "']/..//*[@class='checkbox-div ']"));
	}

	public WebElement verifyReqSuccess(WebDriver driver, String vehicle) {
		return driver.findElement(
				By.xpath("//*[text()='Vehicle Number']/../../..//*[text()='" + vehicle + "']/..//*[text()='Success']"));
	}
	
	public WebElement getClient(WebDriver driver, String client) {
		return driver.findElement(
				By.xpath("//*[@class='Select-menu-outer']//*[text()='"+client+"']"));
	}

	public void requestPayment(WebDriver driver,String filepath, String sheet,String testcase,String vehnum) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.searchClient, driver, "client search");
		WebActionUtil.type(this.searchClientInput, sData[BaseLib.gv.rpcnCount], "Search Client", driver);
		Thread.sleep(10000);		
		WebActionUtil.clickElement(this.getClient(driver,sData[BaseLib.gv.rpcnCount]), driver, "client select");
		WebActionUtil.clickElement(this.goBtn, driver, "Go button");
		WebActionUtil.waitTillPageLoad("Go button", driver, "Detailed Vehicle Payments Page", 90);
		WebActionUtil.assertElementDisplayed(this.getCheckBox(driver, vehnum), driver, "vehicle details");
		WebActionUtil.clickElement(this.getCheckBox(driver, vehnum), driver, "select vehicle");
		//WebActionUtil.clickElement(this.viewPayments, driver, "Freeze Payments");
		WebActionUtil.clickElement(this.freezePayments, driver, "Freeze Payments");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select date");
		WebActionUtil.clickElement(this.freezePaymentsTillDate, driver, "payments till date");
		WebActionUtil.waitTillPageLoad("Freeze Payments", driver, "Detailed Vehicle Payments Page", 90);
		//WebActionUtil.waitForElement(this.getPaymentCheckBox(driver, "KA021111"), driver, "Detailed Vehicle Payments Page", 60);
		Thread.sleep(100000);
		if(!this.requestFailed.isEmpty()){
			this.driver.navigate().back();
			WebActionUtil.assertElementDisplayed(this.getCheckBox(driver, sData[BaseLib.gv.rpvnCount]), driver, "vehicle details");
			WebActionUtil.clickElement(this.getCheckBox(driver, sData[BaseLib.gv.rpvnCount]), driver, "select vehicle");
			//WebActionUtil.clickElement(this.viewPayments, driver, "Freeze Payments");
			WebActionUtil.clickElement(this.freezePayments, driver, "Freeze Payments");
			
			
			
			WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select date");
			WebActionUtil.clickElement(this.freezePaymentsTillDate, driver, "payments till date");
			WebActionUtil.waitTillPageLoad("Freeze Payments", driver, "Detailed Vehicle Payments Page", 90);
			Thread.sleep(75000);
		}
		if(checkBoxTotalAmount.isDisplayed()){
			for(int i=0;i<checkBoxesTotalAmount.size();i++){
					checkBoxesTotalAmount.get(i).click();
			}
		}
		else System.out.println("No Total amount to add to the payment");
		WebActionUtil.clickElement(this.getPaymentCheckBox(driver, vehnum), driver, "select payment");
		WebActionUtil.assertElementDisplayed(this.requestPayment, driver, "request payment button");
		WebActionUtil.clickElement(this.requestPayment, driver, "request payment");
		
		
		
		WebActionUtil.waitTillPageLoad("success Payments", driver, "Detailed Vehicle Payments Page", 90);
		WebActionUtil.assertElementDisplayed(this.verifyReqSuccess(driver, vehnum), driver, "payment request success");
		Thread.sleep(5000);
		//driver.navigate().back();
		//driver.switchTo().alert().accept();
		//Thread.sleep(5000);	
	}
	}

