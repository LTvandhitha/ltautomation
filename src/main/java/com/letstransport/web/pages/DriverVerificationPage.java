package com.letstransport.web.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class DriverVerificationPage {

	WebDriver driver;

	public DriverVerificationPage(WebDriver driver) {
        // Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//*[text()='Scheduled Stats']")
	private WebElement scheduledStats;
	
	@FindBy(xpath="//span[text()='Verified']")
	private WebElement verifiedLink;

	@FindBy(xpath = "//*[text()='Created']")
	private WebElement createdBtn;

	@FindBy(xpath = "//*[text()='RC Photo']")
	private WebElement rcBtn;

	@FindBy(xpath = "//*[text()='Aadhar Photo']")
	private WebElement adharBtn;

	@FindBy(xpath = "//*[text()='Owner Aadhar Photo']")
	private WebElement OwnerAdharBtn;

	@FindBy(xpath = "//*[text()='Driver Aadhar Number']/..//input")
	private WebElement inputAdharNum;

	@FindBy(xpath = "//*[text()='Owner Name']/..//input")
	private WebElement inputOwnerName;

	@FindBy(xpath = "//*[text()='Owner Number']/..//input")
	private WebElement inputOwnerNum;

	@FindBy(xpath = "//*[text()='Owner Aadhar Number']/..//input")
	private WebElement inputOwnerAdharNum;

	@FindBy(xpath = "//*[text()='Owner Bank Name']/..//input")
	private WebElement inputOwnerBankName;

	@FindBy(xpath = "//*[text()='Owner Account Number']/..//input")
	private WebElement inputOwnerAccountNum;

	@FindBy(xpath = "//*[text()='IFSC Code']/..//input")
	private WebElement inputIfscCode;

	@FindBy(xpath = "//*[text()='Bank Pass book Photo']")
	private WebElement bankPassBookBtn;

	@FindBy(xpath = "//*[text()='Consent Form']")
	private WebElement consentForm;

	@FindBy(xpath = "//*[text()='PAN Photo']")
	private WebElement panPhoto;

	@FindBy(xpath = "//*[text()='Licence Photo']")
	private WebElement licencePhoto;

	@FindBy(xpath = "//*[text()='Submit']")
	private WebElement submitBtn;

	@FindBy(xpath = "//*[text()='Verify']")
	private WebElement verifyBtn;

	@FindBy(xpath = "//*[text()='Close']")
	private WebElement closeBtn;

	@FindBy(xpath = "//*[text()='Verified']")
	private WebElement verifiedBtn;

	@FindBy(xpath = "//*[text()='PAN Number']/..//input")
	private WebElement inputPanNumber;

	@FindBy(xpath = "//*[text()='Licence Number']/..//input")
	private WebElement inputLicenceNumber;
	
	@FindBy(xpath = "//input[@placeholder='Vehicle Number']/..//input")
	private WebElement inputVehicleNumber;
	
	@FindBy(xpath = "//div[@class='sc-bbmXgH ethyYv']")
	private WebElement searchIcon;
	
	
	public void verifiedLink(WebDriver driver) throws Exception{
		Thread.sleep(3000);
		WebActionUtil.clickElement(verifiedLink, driver, "click on verified");
		Thread.sleep(3000);
	}
	/*@FindBy(xpath="//div[@class='list-item status verified'][1]/../div[4]")
	private WebElement verifiedVecNum;*/
	
	@FindBy(xpath="//div[@class='list-item status verified'][1]/../div[4]")
	private List<WebElement> verifiedVecNum;

	public WebElement getShowBtn(WebDriver driver, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + vehicle + "']/..//*[text()='Show']"));
	}

	public WebElement getVerifiedStatus(WebDriver driver, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + vehicle + "']/..//*[text()='verified']"));
	}

	public void driverVerification(WebDriver driver, String filepath, String sheet,String testcase,String imagePath) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.scheduledStats, driver, "scheduled Stats");
		WebActionUtil.clickElement(this.createdBtn, driver, "scheduled Stats");
		WebActionUtil.waitTillPageLoad("created button", driver, "created button", 20);
		WebActionUtil.clickElement(this.getShowBtn(driver, sData[BaseLib.gv.dvvnCount]), driver, "Show button");
		WebActionUtil.clickElement(this.rcBtn, driver, "RC photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.type(this.inputAdharNum, sData[BaseLib.gv.dvanCount], "Date input field", driver);
		WebActionUtil.clickElement(this.adharBtn, driver, "Adhar photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.type(this.inputOwnerName, sData[BaseLib.gv.dvnCount], "Owner Name", driver);
		WebActionUtil.type(this.inputOwnerNum, sData[BaseLib.gv.dvonCount], "Phone number input field", driver);
		WebActionUtil.type(this.inputOwnerAdharNum, sData[BaseLib.gv.dvoanCount], "Adhaar num input field", driver);
		WebActionUtil.clickElement(this.OwnerAdharBtn, driver, "Owner Adhar photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.type(this.inputOwnerBankName, sData[BaseLib.gv.dvbnCount], "Bank Name", driver);
		WebActionUtil.type(this.inputOwnerAccountNum, sData[BaseLib.gv.dvobanCount], "Owner Account number input field", driver);
		WebActionUtil.type(this.inputIfscCode, sData[BaseLib.gv.bifscCount], "Bank iFSC code input field", driver);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, this.licencePhoto);
		WebActionUtil.clickElement(this.bankPassBookBtn, driver, "Bank Pass book Photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.clickElement(this.consentForm, driver, "Consent Form");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.type(this.inputPanNumber, sData[BaseLib.gv.dvpnCount], "Pan Input Number", driver);
		WebActionUtil.clickElement(this.panPhoto, driver, "pan Photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(2000);
		WebActionUtil.type(this.inputLicenceNumber, sData[BaseLib.gv.dvlcCount], "input Licence Number", driver);
		WebActionUtil.scrollIntoView(driver, this.licencePhoto);
		WebActionUtil.clickElement(this.licencePhoto, driver, "licence Photo");
		WebActionUtil.uploadFile(imagePath);
		Thread.sleep(10000);
		WebActionUtil.clickElement(this.submitBtn, driver, "submit Button");
		WebActionUtil.waitTillPageLoad("Submit button", driver, "Submit button", 60);
		WebActionUtil.clickElement(this.verifyBtn, driver, "Verify Button");
		WebActionUtil.waitTillPageLoad("Verify button", driver, "Verify button", 60);
		WebActionUtil.clickElement(this.closeBtn, driver, "Close Button");

	}

	public void verifyDriverVerified(WebDriver driver, String filepath, String sheet,String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.verifiedBtn, driver, "verified Button");
		WebActionUtil.waitTillPageLoad("Verified button", driver, "Verified page", 60);
		WebActionUtil.type(this.inputVehicleNumber, sData[BaseLib.gv.dvvnCount], "Vehicle number", driver);;
		WebActionUtil.clickElement(this.searchIcon, driver, "Search button");
		WebActionUtil.waitTillPageLoad("search button", driver, "Verified page", 60);
		WebActionUtil.assertElementDisplayed(this.getVerifiedStatus(driver, sData[BaseLib.gv.dvvnCount]), driver, "Verified status");
	}
	@FindBy(xpath="//span[text()='Created']")
	private WebElement createdButton;
	public void createdStatus(WebDriver driver) throws Exception{
		Thread.sleep(3000);
		WebActionUtil.clickElement(createdButton, driver, "click on created");
		Thread.sleep(3000);
	}
	@FindBy(xpath="//li[text()='Adhoc Stats']")
	private WebElement adhocStatus;
	public void adhocStatus(WebDriver driver) throws Exception{
		Thread.sleep(3000);
		WebActionUtil.clickElement(adhocStatus, driver, "Adhoc Status");
		Thread.sleep(3000);
	}
	@FindBy(xpath="//div[@class='verification-list']/div/child::div[2]/div[7][text()='created']/../div[10]/div[text()='Show']")
	private WebElement showBtn;
	@FindBy(xpath="//div[@class='verification-list']/child::div[1]/descendant::div[1]/child::div[4]")
	private WebElement createdVechileNo;
	@FindBy(xpath="//label[text()='Driver Name']/../child::div/input")
	private WebElement driverName;
	@FindBy(xpath="//label[text()='Driver Phone Number']/../child::div/input")
	private WebElement driverPhoneNumber;
	@FindBy(xpath="//label[text()='Owner Bank Name']/../child::div/input")
	private WebElement ownerBankNmae;
	@FindBy(xpath="//label[text()='Owner Account Number']/../child::div/input")
	private WebElement accountNo;
	@FindBy(xpath="//label[text()='IFSC Code']/../child::div/input")
	private WebElement IFSCtextBox;
	@FindBy(xpath="//button[text()='Submit']")
	private WebElement submitButn;
	@FindBy(xpath="//button[text()='Verify']")
	private WebElement verifyButtn;
	@FindBy(xpath="//button[text()='Close']")
	private WebElement closeButn;
	@FindBy(xpath="//div[text()='verified']/preceding-sibling::div[3]")
	private WebElement verifiedVechile;
	public String getcreatedVecDetails(WebDriver driver) throws Exception{
		String vecNo="";
		//vecNo=createdVechileNo.getText();
		WebActionUtil.clickElement(showBtn, driver, "Show Button");
		Thread.sleep(2000);
		WebActionUtil.clickElement(driverName, driver, "driver name");
		String nameOfDriver = WebActionUtil.generateRandomString(4);
		WebActionUtil.clearAndType(driverName,nameOfDriver, "driver name", driver);
		String phNo = WebActionUtil.generateRandomNumberfornDigits1(10);
		WebActionUtil.clearAndTypeEnter(driverPhoneNumber, phNo, "driver phone number", driver);
		WebActionUtil.scrollIntoViewAndClick(driver, ownerBankNmae);
		WebActionUtil.clearAndType(ownerBankNmae, "Megha", "owner bank name", driver);
		WebActionUtil.scrollIntoViewAndClick(driver, accountNo);
		WebActionUtil.clearAndTypeEnter(accountNo, "230101503865", "Account No", driver);
		WebActionUtil.scrollIntoViewAndClick(driver, IFSCtextBox);
		WebActionUtil.clearAndTypeEnter(IFSCtextBox, "ICIC0002301", "Ifsc", driver);
		WebActionUtil.clickElement(submitButn, driver, "click on submit button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(verifyButtn, driver, "click on verify button");
		Thread.sleep(500);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(closeButn, driver, "close Btn");
		Thread.sleep(500);
		String verifiedVec = verifiedVechile.getText();
		return verifiedVec;
		
		
		
	}
	public String getVerifiedVecNo(WebDriver driver) throws Exception{
		String vecno="";
		vecno=verifiedVecNum.get(Integer.parseInt(WebActionUtil.generateRandomNumberfornDigits(1))).getText();
		return vecno; 
		}
}

