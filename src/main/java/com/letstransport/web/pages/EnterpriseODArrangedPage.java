package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class EnterpriseODArrangedPage  {

	WebDriver driver;

	public EnterpriseODArrangedPage(WebDriver driver) {
		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//*[text()='Enterprise OD Arranged']/..//*[@class='filter-input date-input  custom-filter-input-style1 undefined'])[1]")
	private WebElement fromDate;

	@FindBy(xpath = "//*[text()='Search']")
	private WebElement searchBtn;

	@FindBy(xpath = "//*[@class='number-span   today ']")
	private WebElement todayDate;

	@FindBy(xpath = "//*[text()='REQUEST PAYMENT']")
	private WebElement requestPayment;
	
	@FindBy(xpath = "//*[@class='close-btn-popUp']")
	private WebElement closePopup;

	public WebElement getEntodDriverPayment(WebDriver driver, String date, String time, String vehicleNo) {
		return driver.findElement(By.xpath("//*[text()='EntOD']/..//*[text()='" + date + "']/..//*[text()='" + time
				+ "']/..//*[text()='" + vehicleNo + "']/..//*[@class='checkbox-div ']"));
	}

	public void requestEntodPayment(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Entod Arranged", driver, "Entod page", 90);
		WebActionUtil.clickElement(this.fromDate, driver, "select from date");
		WebActionUtil.clickElement(this.todayDate, driver, "today date");
		WebActionUtil.clickElement(this.searchBtn, driver, "search button");
		WebActionUtil.waitTillPageLoad("Search Entod payment request", driver, "Entod page", 90);
		
		if(driver.findElements(By.xpath("//*[@class='close-btn-popUp']")).size()>0)
			{
			WebActionUtil.clickElement(this.closePopup, driver, "close popup");
			WebActionUtil.scrollIntoView(driver,this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum));
			WebActionUtil.assertElementDisplayed(this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum), driver, "Entod Driver Payment");
			WebActionUtil.clickElement(this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum), driver, "Entod Driver Payment");
			WebActionUtil.clickElement(this.requestPayment, driver, "request payment");
			WebActionUtil.waitTillPageLoad("payment requested", driver, "Entod page", 90);
			}
	else{
		   Thread.sleep(2000);
			WebActionUtil.scrollIntoView(driver,this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum));
			WebActionUtil.assertElementDisplayed(this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum), driver, "Entod Driver Payment");
			WebActionUtil.clickElement(this.getEntodDriverPayment(driver, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),
					timeStamp, WebHomePage.vehicleNum), driver, "Entod Driver Payment");
			WebActionUtil.clickElement(this.requestPayment, driver, "request payment");
			WebActionUtil.waitTillPageLoad("payment requested", driver, "Entod page", 90);
		}
	
	}
	
	public void navigateBack(WebDriver driver) throws Exception {
		driver.navigate().back();
		WebActionUtil.waitTillPageLoad("navigate back", driver, "Entod page", 90);
	}
}
