package com.letstransport.web.pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class FinancePage {

	WebDriver driver;

	public FinancePage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='From']/..//*[text()='Select Date']")
	private WebElement fromDate;

	@FindBy(xpath = "//*[text()='To']/..//*[text()='Select Date']")
	private WebElement toDate;

	@FindBy(xpath = "//*[text()='Download .csv']")
	private WebElement downLoadCSV;

	@FindBy(xpath = "//*[text()='Download Selected']")
	private WebElement downLoadSelectedBtn;

	@FindBy(xpath = "//*[text()='Upload .csv']")
	private WebElement upLoadCSV;

	@FindBy(xpath = "//*[text()='Requested']")
	private WebElement requestedTab;

	@FindBy(xpath = "//*[text()='Raised/Paid']")
	private WebElement raisedPaidTab;

	@FindBy(xpath = "//*[text()='Corrected/Returned']")
	private WebElement correctedReturnedTab;

	@FindBy(xpath = "//*[text()='Browse to choose a file']")
	private WebElement browseFile;

	@FindBy(xpath = "//*[@placeholder='Enter Bank File Name']")
	private WebElement bankName;

	@FindBy(xpath = "//*[@value='Done']")
	private WebElement doneBtn;

	@FindBy(xpath = "//*[text()='Done']")
	private WebElement donetxtBtn;

	@FindBy(xpath = "//*[text()='DONE']")
	private WebElement doneBTN;

	@FindBy(xpath = "//*[text()='CONFIRM']")
	private WebElement confirmBtn;

	@FindBy(xpath = "//*[text()='Close']")
	private WebElement closeBtn;

	@FindBy(xpath = "//*[text()='OK']")
	private WebElement okBtn;

	@FindBy(xpath = "//*[text()='Change status of entries to']/..//*[text()='Returned']")
	private WebElement returnBtn;

	@FindBy(xpath = "//*[text()='Change status of entries to']/..//*[text()='Paid']")
	private WebElement paidBtn;

	@FindBy(xpath = "//*[text()='Select Date']")
	private WebElement selectDate;

	@FindBy(xpath = "//*[text()='Selected transaction Status']/..//*[text()='1']/..//*[text()='Successful']")
	private WebElement transactionStatus;
		
	@FindBy(xpath = "//*[@class='number-span  selected-block  ']")
	private WebElement toDateDefault;

	public WebElement selectDate(WebDriver driver, String date) {
		return driver
				.findElement(By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"+date+"']"));
	}

	public WebElement verifyBankDetails(WebDriver driver, String status, int count) {
		return driver
				.findElement(By.xpath("//*[text()='Upload .csv file']/../..//*[text()='Remove']/../../../..//*[text()='"
						+ count + "']/..//div[text()='" + status + "']"));
	}

	public WebElement verifyCorrectedDetails(WebDriver driver, String vehicle, String returnDate, String amount,
			String status) {
		return driver.findElement(By.xpath("//*[text()='File']/../..//*[text()='" + vehicle
				+ "' and @class='finance-table-row-data file-name']/..//*[text()='" + returnDate + "']/..//*[text()='"
				+ amount + "']/..//*[text()='" + status + "']"));
	}

	public WebElement getCorrectedDetailsChkBx(WebDriver driver, String vehicle, String returnDate, String amount,
			String status) {
		return driver.findElement(By.xpath("//*[text()='File']/../..//*[text()='" + vehicle
				+ "' and @class='finance-table-row-data file-name']/..//*[text()='" + returnDate + "']/..//*[text()='"
				+ amount + "']/..//*[text()='" + status + "']/..//*[@class='checkbox-div ']"));
	}

	public WebElement selectVehicle(WebDriver driver, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='File Name']/../..//*[text()='" + vehicle + "']"));
	}

	public void downloadCSV(WebDriver driver, String filepath, String sheet, String testcase, String file)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("finance", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select from date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.waitTillPageLoad("to date", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.toDateDefault, driver, "select default to date");
		WebActionUtil.clickElement(this.downLoadCSV, driver, "download csv");
		WebActionUtil.waitTillPageLoad("finance", driver, "finance Page", 90);
		//WebActionUtil.downloadFile(file);
		WebActionUtil.downloadOverWrite(file);
	}

	public void raiseRequest(WebDriver driver, String oldFile, String newFile, String oldStatus, String newStatus)
			throws Exception {
		GenericLib.updateCSVData(oldFile, newFile, oldStatus, newStatus.toUpperCase(), 12);
	}

	public void raiseCorrectedRequest(WebDriver driver, String oldFile, String newFile, String oldStatus,
			String newStatus) throws Exception {
		GenericLib.updateCSVData(oldFile, newFile, oldStatus, newStatus.toUpperCase(), 13);
	}

	public void raiseRejected(WebDriver driver, String oldFile, String newFile, String oldStatus, String newStatus)
			throws Exception {
		GenericLib.updateCSVData(oldFile, newFile, oldStatus, newStatus, 12);
	}

	public void verifyData(WebDriver driver, String filepath, String sheet, String testcase, String file, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 1), 1,
				sData[BaseLib.gv.rpvnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 1), 3,
				sData[BaseLib.gv.rpcnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 1), 12,
				status.toLowerCase());

	}
	
	public void verifyLData(WebDriver driver, String filepath, String sheet, String testcase, String file, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 1), 1,
				WebHomePage.vehicleNum);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 1), 3,
				sData[BaseLib.gv.rpcnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 1), 12,
				status.toLowerCase());

	}

	public void uploadCSV(WebDriver driver, String filepath, String sheet, String testcase, String file, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.upLoadCSV, driver, "upload csv");
		WebActionUtil.waitTillPageLoad("finance", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.browseFile, driver, "browse file");
		WebActionUtil.uploadFile(file);
		Thread.sleep(10000);
		WebActionUtil.waitTillPageLoad("upload file successfull", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(this.verifyBankDetails(driver, status, GenericLib.getCSVRows(file)),
				driver, "Bank details");
		WebActionUtil.type(this.bankName, sData[BaseLib.gv.rpvnCount], "Bank Name", driver);
		WebActionUtil.clickElement(this.doneBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("uploaded the CSV file", driver, "finance Page", 120);
	}
	
	public void uploadLCSV(WebDriver driver, String filepath, String sheet, String testcase, String file, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.upLoadCSV, driver, "upload csv");
		WebActionUtil.waitTillPageLoad("finance", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.browseFile, driver, "browse file");
		WebActionUtil.uploadFile(file);
		Thread.sleep(10000);
		WebActionUtil.waitTillPageLoad("upload file successfull", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(this.verifyBankDetails(driver, status, GenericLib.getCSVRows(file)),
				driver, "Bank details");
		WebActionUtil.type(this.bankName, WebHomePage.vehicleNum, "Bank Name", driver);
		WebActionUtil.clickElement(this.doneBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("uploaded the CSV file", driver, "finance Page", 120);
	}

	public void deleteFile(WebDriver driver, String file) throws Exception {
		WebActionUtil.deleteCsvFile(file);
	}

	public WebElement getVehicleDetails(WebDriver driver, String vehicle, String date) {
		return driver.findElement(By.xpath(
				"//*[text()='File Name']/../..//*[text()='" + vehicle + "' and @class='file-name']/..//*[text()='"
						+ date + "' and @class='file-date']/..//*[text()='pending']"));
	}

	public WebElement getCheckBox(WebDriver driver, String vehicle, String city) {
		return driver.findElement(By.xpath(
				"//*[text()='Status']/../..//*[text()='raised']/../..//*[text()='Vehicle']/../..//*[text()='" + vehicle
						+ "']/../..//*[text()='City']/../..//*[text()='" + city + "']/..//*[@class='checkbox-div ']"));
	}

	public WebElement getRaisedVehicle(WebDriver driver, String vehicle, String city) {
		return driver.findElement(By
				.xpath("//*[text()='City']/../..//*[text()='" + city + "']/../..//*[text()='Vehicle']/../..//*[text()='"
						+ vehicle + "']/../..//*[text()='Status']/../..//*[text()='raised']"));
	}

	public void verifyVehicleDetails(WebDriver driver, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"vehicle details");
		WebActionUtil.clickElement(this.selectVehicle(driver, sData[BaseLib.gv.rpvnCount]), driver, "raised vehicle");
		WebActionUtil.waitTillPageLoad("raised vehicle details", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(
				this.getRaisedVehicle(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpctCount]), driver,
				"get raised vehicle details");
	}
	
	public void verifyLVehicleDetails(WebDriver driver, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"vehicle details");
		WebActionUtil.clickElement(this.selectVehicle(driver, WebHomePage.vehicleNum), driver, "raised vehicle");
		WebActionUtil.waitTillPageLoad("raised vehicle details", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(
				this.getRaisedVehicle(driver, WebHomePage.vehicleNum, sData[BaseLib.gv.rpctCount]), driver,
				"get raised vehicle details");
	}

	public void closePopUp(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(this.closeBtn, driver, "close button");
	}

	public void returnRaisedRequest(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("raised requests", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle Number");
		WebActionUtil.waitTillPageLoad("Open raised request", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.getCheckBox(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpctCount]),
				driver, "Raised vehicle Number");
		WebActionUtil.clickElement(this.returnBtn, driver, "return Raised vehicle Number");
		WebActionUtil.clickElement(this.selectDate, driver, "click select date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select a date");
		WebActionUtil.clickElement(this.doneBTN, driver, "done Button");
		WebActionUtil.clickElement(this.confirmBtn, driver, "confirm Button");
		WebActionUtil.waitTillPageLoad("confirm button", driver, "finance Page", 150);
		WebActionUtil.assertElementDisplayed(this.transactionStatus, driver, "Transaction Status successfull");
		WebActionUtil.clickElement(this.okBtn, driver, "ok Button");
		WebActionUtil.clickElement(this.closeBtn, driver, "close Button");

	}
	
	public void returnLRaisedRequest(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("raised requests", driver, "finance Page", 90);
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle Number");
		WebActionUtil.waitTillPageLoad("Open raised request", driver, "finance Page", 90);
		WebActionUtil.clickElement(this.getCheckBox(driver, WebHomePage.vehicleNum, sData[BaseLib.gv.rpctCount]),
				driver, "Raised vehicle Number");
		WebActionUtil.clickElement(this.returnBtn, driver, "return Raised vehicle Number");
		WebActionUtil.clickElement(this.selectDate, driver, "click select date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select a date");
		WebActionUtil.clickElement(this.doneBTN, driver, "done Button");
		WebActionUtil.clickElement(this.confirmBtn, driver, "confirm Button");
		WebActionUtil.waitTillPageLoad("confirm button", driver, "finance Page", 150);
		WebActionUtil.assertElementDisplayed(this.transactionStatus, driver, "Transaction Status successfull");
		WebActionUtil.clickElement(this.okBtn, driver, "ok Button");
		WebActionUtil.clickElement(this.closeBtn, driver, "close Button");

	}


	public void verifyCorrectedRequest(WebDriver driver, String filepath, String sheet, String testcase, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.correctedReturnedTab, driver, "corrected Returned Tab");
		WebActionUtil.waitTillPageLoad("corrected Returned Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("Corrected/Returned requests", driver, "finance Page", 120);
		WebActionUtil
				.assertElementDisplayed(
						this.verifyCorrectedDetails(driver, sData[BaseLib.gv.rpvnCount],
								WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
						driver, "Raised vehicle under raised tab");
	}
	
	public void verifyLCorrectedRequest(WebDriver driver, String filepath, String sheet, String testcase, String status)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.correctedReturnedTab, driver, "corrected Returned Tab");
		WebActionUtil.waitTillPageLoad("corrected Returned Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("Corrected/Returned requests", driver, "finance Page", 120);
		WebActionUtil
				.assertElementDisplayed(
						this.verifyCorrectedDetails(driver, WebHomePage.vehicleNum,
								WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
						driver, "Raised vehicle under raised tab");
	}

	public void payRaisedRequest(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("raised requests", driver, "finance Page", 150);
		WebActionUtil.scrollIntoView(driver,
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")));
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getVehicleDetails(driver, sData[BaseLib.gv.rpvnCount], WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle Number");
		WebActionUtil.waitTillPageLoad("Open raised request", driver, "finance Page", 150);
		WebActionUtil.clickElement(this.getCheckBox(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpctCount]),
				driver, "Raised vehicle Number");
		WebActionUtil.clickElement(this.paidBtn, driver, "Pay Raised vehicle Number");
		WebActionUtil.clickElement(this.selectDate, driver, "click select date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select a date");
		WebActionUtil.clickElement(this.doneBTN, driver, "done Button");
		WebActionUtil.clickElement(this.confirmBtn, driver, "confirm Button");
		WebActionUtil.waitTillPageLoad("confirm button", driver, "finance Page", 150);
		WebActionUtil.assertElementDisplayed(this.transactionStatus, driver, "Transaction Status successfull");
		WebActionUtil.clickElement(this.okBtn, driver, "ok Button");
		WebActionUtil.clickElement(this.closeBtn, driver, "close Button");

	}
	
	public void payLRaisedRequest(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance  Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.raisedPaidTab, driver, "raised Paid Tab");
		WebActionUtil.waitTillPageLoad("raised Paid Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("raised requests", driver, "finance Page", 150);
		WebActionUtil.scrollIntoView(driver,
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")));
		WebActionUtil.assertElementDisplayed(
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getVehicleDetails(driver, WebHomePage.vehicleNum, WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), driver,
				"Raised vehicle Number");
		WebActionUtil.waitTillPageLoad("Open raised request", driver, "finance Page", 150);
		WebActionUtil.clickElement(this.getCheckBox(driver, WebHomePage.vehicleNum, sData[BaseLib.gv.rpctCount]),
				driver, "Raised vehicle Number");
		WebActionUtil.clickElement(this.paidBtn, driver, "Pay Raised vehicle Number");
		WebActionUtil.clickElement(this.selectDate, driver, "click select date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select a date");
		WebActionUtil.clickElement(this.doneBTN, driver, "done Button");
		WebActionUtil.clickElement(this.confirmBtn, driver, "confirm Button");
		WebActionUtil.waitTillPageLoad("confirm button", driver, "finance Page", 150);
		WebActionUtil.assertElementDisplayed(this.transactionStatus, driver, "Transaction Status successfull");
		WebActionUtil.clickElement(this.okBtn, driver, "ok Button");
		WebActionUtil.clickElement(this.closeBtn, driver, "close Button");

	}

	public void downloadCorrectedRequest(WebDriver driver, String filepath, String sheet, String testcase, String file,
			String status) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.correctedReturnedTab, driver, "corrected Returned Tab");
		WebActionUtil.waitTillPageLoad("corrected Returned Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("Corrected/Returned requests", driver, "finance Page", 120);
		WebActionUtil.assertElementDisplayed(
				this.verifyCorrectedDetails(driver, sData[BaseLib.gv.rpvnCount],
						WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
				driver, "Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getCorrectedDetailsChkBx(driver, sData[BaseLib.gv.rpvnCount],
						WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
				driver, "Raised vehicle under raised tab");
		WebActionUtil.clickElement(this.downLoadSelectedBtn, driver, "downLoad Selected Button");
		WebActionUtil.waitTillPageLoad("download", driver, "finance Page", 120);
		//WebActionUtil.downloadFile(file);
		WebActionUtil.downloadOverWrite(file);
	}
	
	public void downloadLCorrectedRequest(WebDriver driver, String filepath, String sheet, String testcase, String file,
			String status) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Finance Role", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.correctedReturnedTab, driver, "corrected Returned Tab");
		WebActionUtil.waitTillPageLoad("corrected Returned Tab", driver, "finance Page", 120);
		WebActionUtil.clickElement(this.fromDate, driver, "from date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd"))-2)), driver, "select date");
		WebActionUtil.clickElement(this.toDate, driver, "from date");
		WebActionUtil.clickElement(this.toDateDefault, driver, "select date");
		WebActionUtil.clickElement(this.donetxtBtn, driver, "done Button");
		WebActionUtil.waitTillPageLoad("Corrected/Returned requests", driver, "finance Page", 120);
		WebActionUtil.assertElementDisplayed(
				this.verifyCorrectedDetails(driver, WebHomePage.vehicleNum,
						WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
				driver, "Raised vehicle under raised tab");
		WebActionUtil.clickElement(
				this.getCorrectedDetailsChkBx(driver, WebHomePage.vehicleNum,
						WebActionUtil.prependZero(WebActionUtil.fetchSystemDateTime("d-MMM-yyyy")), sData[BaseLib.gv.rpctCount], status),
				driver, "Raised vehicle under raised tab");
		WebActionUtil.clickElement(this.downLoadSelectedBtn, driver, "downLoad Selected Button");
		WebActionUtil.waitTillPageLoad("download", driver, "finance Page", 120);
		//WebActionUtil.downloadFile(file);
		WebActionUtil.downloadOverWrite(file);
	}

	public void verifyCSVData(WebDriver driver, String filepath, String sheet, String testcase, String file,
			String status) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 2), 2,
				sData[BaseLib.gv.rpvnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 2), 4,
				sData[BaseLib.gv.rpcnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, sData[BaseLib.gv.rpvnCount], 2), 13, status);

	}
	
	public void verifyLCSVData(WebDriver driver, String filepath, String sheet, String testcase, String file,
			String status) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 2), 2,
				WebHomePage.vehicleNum);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 2), 4,
				sData[BaseLib.gv.rpcnCount]);
		WebActionUtil.verifyCsvData(file, GenericLib.getCSVRowID(file, WebHomePage.vehicleNum, 2), 13, status);

	}

}
