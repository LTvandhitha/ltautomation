package com.letstransport.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class ProcessVehiclePaymentsPage {

	WebDriver driver;

	public ProcessVehiclePaymentsPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Search Client...']/..//input")
	private WebElement searchClientInput;

	@FindBy(xpath = "//*[text()='Search Client...']")
	private WebElement searchClient;

	@FindBy(xpath = "//*[@class='Select-menu-outer']")
	private WebElement clientSelect;

	@FindBy(xpath = "//*[text()='Go']")
	private WebElement vpGoBtn;

	@FindBy(xpath = "//*[text()='GO']")
	private List<WebElement> dpGoBtn;

	@FindBy(xpath = "//*[text()='Verify Payments']")
	private WebElement verifyPayments;

	@FindBy(xpath = "//*[text()='Discard Payments']")
	private WebElement discardPayments;

	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement yesBtn;

	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(
				By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"
						+ date + "']"));
	}

	public List<WebElement> selectVehicles(WebDriver driver, String Vehicle, String client, String status) {
		return driver.findElements(By.xpath("//*[text()='" + Vehicle + "']/..//*[text()='" + client + "']/..//*[text()='"
				+ status + "']/..//*[@class='checkbox-div ']"));
	}
	
	public WebElement selectVehicle(WebDriver driver, String Vehicle, String client, String status) {
		return driver.findElement(By.xpath("//*[text()='" + Vehicle + "']/..//*[text()='" + client + "']/..//*[text()='"
				+ status + "']/..//*[@class='checkbox-div ']"));
	}

	public WebElement verifyVehicle(WebDriver driver, String Vehicle, String client) {
		return driver.findElement(
				By.xpath("//*[text()='" + Vehicle + "']/..//*[text()='" + client + "']/..//*[text()='verified']"));
	}

	public void verifyPayment(WebDriver driver,String filepath, String sheet,String testcase,String status,String vehnum) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		WebActionUtil.clickElement(this.searchClient, driver, "client search");
		WebActionUtil.type(this.searchClientInput, sData[BaseLib.gv.rpcnCount], "Search Client", driver);
		Thread.sleep(10000);
		WebActionUtil.clickElement(this.clientSelect, driver, "client select");
		if(!this.dpGoBtn.isEmpty()){
			WebActionUtil.clickElement(this.dpGoBtn.get(0), driver, "Go button");
		}else{
			WebActionUtil.clickElement(this.vpGoBtn, driver, "Go button");
		}
		WebActionUtil.waitTillPageLoad("Go button", driver, "Process Vehicle Payments Page", 60);
//		if(this.selectVehicles(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount], status.toLowerCase()).isEmpty()){
//			this.driver.navigate().refresh();
//			WebActionUtil.waitTillPageLoad("refresh page", driver, "Process Vehicle Payments Page", 60);
//			WebActionUtil.clickElement(this.searchClient, driver, "client search");
//			WebActionUtil.type(this.searchClientInput, sData[BaseLib.gv.rpcnCount], "Search Client", driver);
//			Thread.sleep(10000);
//			WebActionUtil.clickElement(this.clientSelect, driver, "client select");
//			if(!this.dpGoBtn.isEmpty()){
//				WebActionUtil.clickElement(this.dpGoBtn.get(0), driver, "Go button");
//			}else{
//				WebActionUtil.clickElement(this.vpGoBtn, driver, "Go button");
//			}
//			WebActionUtil.waitTillPageLoad("Go button", driver, "Process Vehicle Payments Page", 60);
//		}
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
//		WebActionUtil.assertElementDisplayed(this.selectVehicle(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount], status.toLowerCase()), driver,
		
//				"vehicle details");
		
		WebActionUtil.assertElementDisplayed(this.selectVehicle(driver, vehnum, sData[BaseLib.gv.rpcnCount], status.toLowerCase()), driver,
				
				"vehicle details");
		WebActionUtil.clickElement(this.selectVehicle(driver, vehnum, sData[BaseLib.gv.rpcnCount], status.toLowerCase()), driver,
				"select vehicle");
		WebActionUtil.clickElement(this.verifyPayments, driver, "verfiy Payments");
		WebActionUtil.clickElement(this.yesBtn, driver, "yes button");
		WebActionUtil.waitTillPageLoad("yes button", driver, "Process Vehicle Payments Page", 60);
		WebActionUtil.assertElementDisplayed(this.verifyVehicle(driver, vehnum, sData[BaseLib.gv.rpcnCount]), driver,
				"vehicle details");
	}
	
	public void raiseReturnedPayment(WebDriver driver,String filepath, String sheet,String testcase,String status) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.searchClient, driver, "client search");
		WebActionUtil.type(this.searchClientInput, sData[BaseLib.gv.rpcnCount], "Search Client", driver);
		Thread.sleep(10000);
		WebActionUtil.clickElement(this.clientSelect, driver, "client select");
		if(!this.dpGoBtn.isEmpty()){
			WebActionUtil.clickElement(this.dpGoBtn.get(0), driver, "Go button");
		}else{
			WebActionUtil.clickElement(this.vpGoBtn, driver, "Go button");
		}
		WebActionUtil.waitTillPageLoad("Go button", driver, "Process Vehicle Payments Page", 60);
		if(this.selectVehicles(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount], "returned").isEmpty()){
			this.driver.navigate().refresh();
			WebActionUtil.clickElement(this.searchClient, driver, "client search");
			WebActionUtil.type(this.searchClientInput, sData[BaseLib.gv.rpcnCount], "Search Client", driver);
			Thread.sleep(10000);
			WebActionUtil.clickElement(this.clientSelect, driver, "client select");
			if(!this.dpGoBtn.isEmpty()){
				WebActionUtil.clickElement(this.dpGoBtn.get(0), driver, "Go button");
			}else{
				WebActionUtil.clickElement(this.vpGoBtn, driver, "Go button");
			}
			WebActionUtil.waitTillPageLoad("Go button", driver, "Process Vehicle Payments Page", 60);
		}
		WebActionUtil.assertElementDisplayed(this.selectVehicle(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount], "returned"), driver,
				"vehicle details");
		WebActionUtil.clickElement(this.selectVehicle(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount], "returned"), driver,
				"select vehicle");
		WebActionUtil.clickElement(this.verifyPayments, driver, "verfiy Payments");
		WebActionUtil.clickElement(this.yesBtn, driver, "yes button");
		WebActionUtil.waitTillPageLoad("yes button", driver, "Process Vehicle Payments Page", 60);
		WebActionUtil.assertElementDisplayed(this.verifyVehicle(driver, sData[BaseLib.gv.rpvnCount], sData[BaseLib.gv.rpcnCount]), driver,
				"vehicle details");
	}
}
