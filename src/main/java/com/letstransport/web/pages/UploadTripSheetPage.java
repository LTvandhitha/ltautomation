package com.letstransport.web.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class UploadTripSheetPage {

	WebDriver driver;

	public UploadTripSheetPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	public WebElement getBooking(String client, String hub, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/../../..//*[text()='" + hub
				+ "']/../../..//*[text()='" + vehicle + "']"));
	}

	@FindBy(xpath = "//*[@class='floating-button-icon-div']")
	private WebElement floatingButton;

	@FindBy(xpath = "//*[text()='Upload Tripsheets']")
	private WebElement uploadTripSheet;

	@FindBy(xpath = "//*[text()='Verify Tripsheet']")
	private WebElement verifyTripSheet;

	@FindBy(xpath = "//*[text()='Gallery']/..")
	private WebElement galleryBtn;

	@FindBy(xpath = "//*[text()='Gallery']/../..//img")
	private WebElement imageLink;

	@FindBy(xpath = "//*[text()='Move to Tripsheet']")
	private WebElement moveToTripsheetBtn;

	@FindBy(xpath = "//*[text()='Tripsheet']")
	private WebElement tripsheetBtn;

	@FindBy(xpath = "//*[text()='CLOSE']")
	private WebElement closeBtn;

	@FindBy(xpath = "//*[text()='Vaild date is required']/../input")
	private WebElement dateInput;

	@FindBy(xpath = "//*[@class='selected-sheet-style']")
	private WebElement tripsheetLink;

	@FindBy(xpath = "//*[@placeholder='Search Client...']")
	private WebElement searchClientLink;

	@FindBy(xpath = "//*[@placeholder='Search Hub...']")
	private WebElement searchHubLink;

	public WebElement selectClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']"));
	}

	public WebElement selectHub(WebDriver driver, String hub) {
		return driver.findElement(By.xpath("//*[text()='" + hub + "']"));
	}

	@FindBy(xpath = "//*[@placeholder='Vehicle No.']")
	private WebElement vehicleNoLink;

	@FindBy(xpath = "//*[@src='https://storage.googleapis.com/lt-test-media/1cfd0180-754f-11e9-96c2-1b1ff33f68ac-thumbnail']")
	private WebElement tripsheetLink1;

	@FindBy(xpath = "//*[text()='Opening Kms']/../input")
	private WebElement openingKms;

	@FindBy(xpath = "//*[text()='Closing Kms']/../input")
	private WebElement closingKms;

	@FindBy(xpath = "//*[text()='Opening Time']/../input")
	private WebElement openingTime;

	@FindBy(xpath = "//*[text()='Closing Time']/..//input")
	private WebElement closingTime;

	@FindBy(xpath = "//*[text()='Move to Mapped']")
	private WebElement moveToMapped;

	@FindBy(xpath = "//*[contains(text(),'Mapped')]")
	private WebElement mappedBtn;

	@FindBy(xpath = "//*[contains(text(),'Unmapped')]")
	private WebElement unMappedBtn;

	@FindBy(xpath = "//*[text()='From Date']")
	private WebElement fromDate;

	@FindBy(xpath = "//*[text()='To Date']")
	private WebElement toDate;

	@FindBy(xpath = "//*[@class='number-span  selected-block  ']")
	private WebElement selectToDate;

	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveBtn;

	@FindBy(xpath = "//*[text()='Typing mistake in attendance']")
	private WebElement reasonToMap;

	@FindBy(xpath = "//*[text()='Digital Tripsheets are?']/..//*[text()='Verified']")
	private WebElement verifiedBtn;

	@FindBy(xpath = "//*[text()='Old Remarks']/..//div[@class='close-icon']")
	private WebElement xBtn;

	@FindBy(xpath = "//*[text()='Verification']")
	private WebElement verificationText;

	@FindBy(xpath = "//*[text()='Select Client']")
	private WebElement clientDropDown;

	@FindBy(xpath = "//*[text()='Select Hub']")
	private WebElement hubDropDown;

	@FindBy(xpath = "//*[text()='Select Status']")
	private WebElement statusDropDown;

	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(
//				By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"
//						+ date + "']"));
		
		By.xpath("//*[@class='block numbers  ']//*[@class='number-span   ']/..//*[text()='"+ date + "']"));
	}

	
	
	public WebElement selectDate(WebDriver driver,int date) throws InterruptedException {
		
//		Thread.sleep(5000);
		
		return driver.findElement(
//				By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"
//						+ date + "']"));
//		
				By.xpath("//*[@class='block numbers  ']//*[@class='number-span   ']/..//*[text()='"+ date + "']"));
	
	}
	public WebElement getClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//*[text()='Select Client']//*[text()='" + client.toUpperCase() + "']"));
	}

	public WebElement getClientHub(WebDriver driver, String hub) {
		return driver.findElement(By.xpath("//*[text()='Select Hub']//*[text()='" + hub.toUpperCase() + "']"));
	}

	public WebElement getStatus(WebDriver driver, String status) {
		return driver.findElement(By.xpath("//*[text()='Select Status']//*[text()='" + status + "']"));
	}

	public WebElement getUnmapTripSheet(WebDriver driver, String client, String hub, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='Tripsheet Details']/..//*[@value='" + client
				+ "']/../../../..//*[@value='" + hub + "']/../../../..//*[@value='" + vehicle + "']"));
	}

	public WebElement getMapTripSheet(WebDriver driver, String client, String hub, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/..//*[text()='" + hub + "']/../..//*[text()='"
				+ vehicle + "']/../..//*[text()='Tripsheet']/../..//*[text()='Not Verified']"));
	}

	public WebElement getUnMapTripSheet(WebDriver driver, String client, String hub, String vehicle,
			String wrongVehicle) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/..//*[text()='" + hub + "']/../..//*[text()='"
				+ vehicle + "']/../..//*[text()='Tripsheet']/../..//*[text()='" + wrongVehicle
				+ "']/../..//*[text()='Not Verified']"));
	}

	public WebElement getNumber(WebDriver driver, String number) {
		return driver.findElement(By.xpath("//*[text()='Booking Details']/..//*[text()='" + number + "']"));
	}

	public WebElement MapTripSheet(WebDriver driver, String client, String vehicle) {
		return driver.findElement(By
				.xpath("//*[text()='Not Verified']/..//*[text()='" + vehicle + "']/../..//*[text()='" + client + "']"));
	}

	public WebElement getTripSheet(WebDriver driver, String client, String vehicle) {
		return driver.findElement(By
				.xpath("//*[text()='" + client + "']/../..//*[text()='" + vehicle + "']/../..//*[text()='Verified']"));
	}

	/**
	 * Author: Karthik KR Description: Method to upload the tripsheet and verify
	 * it
	 * 
	 * @param driver
	 * @throws Exception
	 */

	public void uploadTripSheet(WebDriver driver, String filePath) throws Exception {
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		
		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
		WebActionUtil.clickElement(this.uploadTripSheet, driver, "Upload Trip Sheet button");
		WebActionUtil.clickElement(this.galleryBtn, driver, "Gallery button");
        try{WebActionUtil.uploadFile(filePath);}
        catch(Exception e){WebActionUtil.uploadFile(filePath);}
		Thread.sleep(2000);
		//WebActionUtil.uploadFile(filePath);
		WebActionUtil.longPress(this.imageLink, driver, "Image ");
		WebActionUtil.clickElement(this.closeBtn, driver, "Close button");
		WebActionUtil.clickElement(this.moveToTripsheetBtn, driver, "Move to tripsheet button");
		WebActionUtil.waitTillPageLoad("move to tripsheet", driver, "Gallery", 120);
		WebActionUtil.clickElement(this.tripsheetBtn, driver, "tripsheet button");
		// WebActionUtil.scrollIntoView(driver, this.tripsheetLink);
		WebActionUtil.isEleDisplayed(this.tripsheetLink, driver, "tripsheet", 120);
	}
	
	
	public String  fetch_yday(String vm,String vd,String vy){
		String yday=null;
		if (Integer.parseInt(vd)==1)
		{
			int m = Integer.parseInt(vm)-1;
			int d=30;
			
		yday= Integer.toString(m) + Integer.toString(d) +vy;
		
		System.out.println(yday);
			
		}
		return yday;
	}
		
		

	/**
	 * @author: Karthik KR description: method to map tripsheet to Scheduled
	 *          booking
	 * @param driver
	 * @throws Exception
	 */

	public void mapTripSheet(WebDriver driver, String filepath, String sheet, String testcase, String vehicleNumber,String timeStamp1,String timeStamp2)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
//		WebActionUtil.waitTillPageLoad("Select Area", driver, "Homepage", 120);
//		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
//		WebActionUtil.clickElement(this.uploadTripSheet, driver, " upload tripsheet button");
//		WebActionUtil.waitTillPageLoad("loading tripsheet", driver, "Gallery", 120);
//		WebActionUtil.clickElement(this.tripsheetBtn, driver, "tripsheet button");
		// WebActionUtil.scrollIntoViewAndClick(driver, this.tripsheetLink);
		WebActionUtil.waitTillPageLoad("loading tripsheet page", driver, "Tripsheet page", 120);
		// WebActionUtil.clickElement(this.tripsheetLink, driver, "Other
		// tripsheet Link");
		WebActionUtil.scrollIntoViewAndClick(driver, this.dateInput);
		
		String mm=WebActionUtil.fetchSystemDateTime("MM");
		String yy=WebActionUtil.fetchSystemDateTime("yyyy");
		String dd=WebActionUtil.fetchSystemDateTime("dd");
		
		System.out.println(dd);
				
		if (Integer.parseInt(dd)==01)
				{
			String yday_date = fetch_yday(mm,dd,yy);
			
			System.out.println(yday_date);
			
			
//			WebActionUtil.type(this.dateInput, WebActionUtil.fetchSystemDateTime("MMddyyyy"), "Date input field", driver);
			WebActionUtil.type(this.dateInput,yday_date , "Date input field", driver);
			WebActionUtil.scrollUp(driver);
			// WebActionUtil.scrollIntoViewAndClick(driver, this.searchClientLink);
			WebActionUtil.clickElement(this.searchClientLink, driver, "Search Client");
			//Thread.sleep(4000);
			WebActionUtil.waitForVisiblityofElement("//*[text()='" + sData[BaseLib.gv.cnCount] + "']", 300, driver);
			WebActionUtil.scrollIntoViewAndClick(driver, this.selectClient(driver, sData[BaseLib.gv.cnCount]));
			WebActionUtil.waitTillPageLoad("Client seleted", driver, "Homepage", 120);
			WebActionUtil.scrollUp(driver);
			WebActionUtil.clickElement(this.searchHubLink, driver, "Search Hub");
			WebActionUtil.scrollIntoViewAndClick(driver, this.selectHub(driver, sData[BaseLib.gv.hCount]));
			WebActionUtil.waitTillPageLoad("Hub seleted", driver, "Homepage", 120);
			WebActionUtil.scrollUp(driver);
			WebActionUtil.type(this.vehicleNoLink, vehicleNumber, "vehicle No field", driver);
			WebActionUtil.scrollIntoView(driver, this.openingKms);
			WebActionUtil.type(this.openingKms, sData[BaseLib.gv.okCount], "opening KMS", driver);
			WebActionUtil.type(this.closingKms, sData[BaseLib.gv.ckCount], "closing KMS", driver);
			WebActionUtil.clickElement(this.openingTime, driver, "Opening Time");
			WebActionUtil.type(this.openingTime, sData[BaseLib.gv.otCount], "time input field", driver);
			WebActionUtil.clickElement(this.closingTime, driver, "Closing Time");
			WebActionUtil.type(this.closingTime, sData[BaseLib.gv.ctCount], "time input field", driver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
			WebActionUtil.scrollDown(driver);
			Thread.sleep(5000);
			/*
			try {
				WebActionUtil.clickElement(this.moveToMapped, driver, "Move to Mapped Button");
			} catch (Exception e) {
			
			
			
			}*/
			
//			WebActionUtil.scrollIntoView(driver,this.saveBtn);
//			WebActionUtil.clickElement(this.saveBtn, driver, "Save Button");
			
			if (driver.findElements(By.xpath("//*[text()='Move to Mapped']")).size()>0)
			{
				WebActionUtil.clickElement(this.moveToMapped, driver, "Move to Mapped Button");
			}
			else
			WebActionUtil.clickElement(this.saveBtn, driver, "Save Button");
			WebActionUtil.waitTillPageLoad("Moved to Map", driver, "Tripsheet Page", 120);
			Thread.sleep(3000);
			
				}
		else
		{
		
		String yday_date= mm+ (Integer.parseInt(dd)-1) + yy;
		
//		WebActionUtil.type(this.dateInput, WebActionUtil.fetchSystemDateTime("MMddyyyy"), "Date input field", driver);
		WebActionUtil.type(this.dateInput,yday_date , "Date input field", driver);
		WebActionUtil.scrollUp(driver);
		// WebActionUtil.scrollIntoViewAndClick(driver, this.searchClientLink);
		WebActionUtil.clickElement(this.searchClientLink, driver, "Search Client");
		//Thread.sleep(4000);
		WebActionUtil.waitForVisiblityofElement("//*[text()='" + sData[BaseLib.gv.cnCount] + "']", 300, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, this.selectClient(driver, sData[BaseLib.gv.cnCount]));
		WebActionUtil.waitTillPageLoad("Client seleted", driver, "Homepage", 120);
		WebActionUtil.scrollUp(driver);
		WebActionUtil.clickElement(this.searchHubLink, driver, "Search Hub");
		WebActionUtil.scrollIntoViewAndClick(driver, this.selectHub(driver, sData[BaseLib.gv.hCount]));
		WebActionUtil.waitTillPageLoad("Hub seleted", driver, "Homepage", 120);
		WebActionUtil.scrollUp(driver);
		WebActionUtil.type(this.vehicleNoLink, vehicleNumber, "vehicle No field", driver);
		WebActionUtil.scrollIntoView(driver, this.openingKms);
		WebActionUtil.type(this.openingKms, sData[BaseLib.gv.okCount], "opening KMS", driver);
		WebActionUtil.type(this.closingKms, sData[BaseLib.gv.ckCount], "closing KMS", driver);
		WebActionUtil.clickElement(this.openingTime, driver, "Opening Time");
		WebActionUtil.type(this.openingTime, sData[BaseLib.gv.otCount], "time input field", driver);
		WebActionUtil.clickElement(this.closingTime, driver, "Closing Time");
		WebActionUtil.type(this.closingTime, sData[BaseLib.gv.ctCount], "time input field", driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollDown(driver);
		Thread.sleep(5000);
		/*
		try {
			WebActionUtil.clickElement(this.moveToMapped, driver, "Move to Mapped Button");
		} catch (Exception e) {
		
		
		
		}*/
		
//		WebActionUtil.scrollIntoView(driver,this.saveBtn);
//		WebActionUtil.clickElement(this.saveBtn, driver, "Save Button");
		
		if (driver.findElements(By.xpath("//*[text()='Move to Mapped']")).size()>0)
		{
			WebActionUtil.clickElement(this.moveToMapped, driver, "Move to Mapped Button");
		}
		else
		WebActionUtil.clickElement(this.saveBtn, driver, "Save Button");
		WebActionUtil.waitTillPageLoad("Moved to Map", driver, "Tripsheet Page", 120);
		Thread.sleep(3000);
		}
		
		
//		
	}

	public void verifyMappedTripSheet(WebDriver driver, String filepath, String sheet, String testcase,
			String vehicleNumber) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Select Area", driver, "Homepage", 120);
		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
		WebActionUtil.clickElement(this.uploadTripSheet, driver, " upload tripsheet button");
		WebActionUtil.waitTillPageLoad("loading tripsheet", driver, "Gallery", 120);
		WebActionUtil.clickElement(this.mappedBtn, driver, "Mapped Page");
		WebActionUtil.clickElement(this.fromDate, driver, "From date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select From date");
		WebActionUtil.clickElement(this.toDate, driver, "To date");
		WebActionUtil.clickElement(this.selectToDate, driver, "select To date");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		WebActionUtil.waitTillPageLoad("Done", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.clientDropDown, driver, "client DropDown");
		WebActionUtil.waitTillPageLoad("clientDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClient(driver, sData[BaseLib.gv.cnCount]), driver, "client selected");
		WebActionUtil.clickElement(this.hubDropDown, driver, "hub DropDown");
		WebActionUtil.waitTillPageLoad("hubDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClientHub(driver, sData[BaseLib.gv.hCount]), driver, "hub selected");
		WebActionUtil.clickElement(this.statusDropDown, driver, "Status DropDown");
		WebActionUtil.waitTillPageLoad("Status DropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getStatus(driver, "Not Verified"), driver, " Status selected");
		WebActionUtil.waitTillPageLoad("Status selected", driver, "Tripsheet page", 120);
		WebActionUtil.scrollIntoView(driver,
				this.getMapTripSheet(driver, sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount], vehicleNumber));
		WebActionUtil.isEleDisplayed(
				this.getMapTripSheet(driver, sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount], vehicleNumber),
				driver, "Tripsheet", 120);
	}
	
	
	@FindBy(xpath = "//*[@class='left-arrow arrow']")
	private WebElement leftArrow;

	
		public void selectrequireddate(int yday) throws NumberFormatException, IOException, InterruptedException{
			
			if (yday ==31)
			{
		WebActionUtil.clickElement(leftArrow ,driver,"Click left");
						
		yday=30;
			}
		
			
		
			WebActionUtil.clickElement(this.selectDate(driver, yday), driver, "select From date");
		}

	public void verifyUnMappedTripSheet(WebDriver driver, String filepath, String sheet, String testcase,
			String vehicleNumber, String invalidVehicle) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Select Area", driver, "Homepage", 120);
		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
		WebActionUtil.clickElement(this.uploadTripSheet, driver, " upload tripsheet button");
		WebActionUtil.waitTillPageLoad("loading tripsheet", driver, "Gallery", 120);
		WebActionUtil.clickElement(this.mappedBtn, driver, "Mapped Page");
		WebActionUtil.clickElement(this.fromDate, driver, "From date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select From date");
		WebActionUtil.clickElement(this.toDate, driver, "To date");
		WebActionUtil.clickElement(this.selectToDate, driver, "select To date");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		WebActionUtil.waitTillPageLoad("Done", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.clientDropDown, driver, "client DropDown");
		WebActionUtil.waitTillPageLoad("clientDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClient(driver, sData[BaseLib.gv.cnCount]), driver, "client selected");
		WebActionUtil.clickElement(this.hubDropDown, driver, "hub DropDown");
		WebActionUtil.waitTillPageLoad("hubDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClientHub(driver, sData[BaseLib.gv.hCount]), driver, "hub selected");
		WebActionUtil.clickElement(this.statusDropDown, driver, "Status DropDown");
		WebActionUtil.waitTillPageLoad("Status DropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getStatus(driver, "Not Verified"), driver, " Status selected");
		WebActionUtil.waitTillPageLoad("Status selected", driver, "Tripsheet page", 120);
		WebActionUtil.scrollIntoView(driver, this.getUnMapTripSheet(driver, sData[BaseLib.gv.cnCount],
				sData[BaseLib.gv.hCount], vehicleNumber, invalidVehicle));
		WebActionUtil.isEleDisplayed(this.getUnMapTripSheet(driver, sData[BaseLib.gv.cnCount],
				sData[BaseLib.gv.hCount], vehicleNumber, invalidVehicle), driver, "Tripsheet", 120);
	}

	public void verifyUnMappedTripSheet(WebDriver driver, String filepath, String sheet, String testcase, String vehicle)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.unMappedBtn, driver, "Mapped Page");
		WebActionUtil.clickElement(this.fromDate, driver, "From date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select From date");
		WebActionUtil.clickElement(this.toDate, driver, "To date");
		WebActionUtil.clickElement(this.selectToDate, driver, "select To date");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		WebActionUtil.waitTillPageLoad("Done", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.clientDropDown, driver, "client DropDown");
		WebActionUtil.waitTillPageLoad("clientDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClient(driver, sData[BaseLib.gv.cnCount]), driver, "client selected");
		WebActionUtil.clickElement(this.hubDropDown, driver, "hub DropDown");
		WebActionUtil.waitTillPageLoad("hubDropDown", driver, "Tripsheet page", 120);
		WebActionUtil.clickElement(this.getClientHub(driver, sData[BaseLib.gv.hCount]), driver, "hub selected");
		WebActionUtil.waitTillPageLoad("Hub selected", driver, "Tripsheet page", 120);
		WebActionUtil.isEleDisplayed(this.getUnmapTripSheet(driver, sData[BaseLib.gv.cnCount],
				sData[BaseLib.gv.hCount], vehicle), driver, "Tripsheet", 120);
	}

	public void maptheUnmappedTripSheet() throws Exception {
		WebActionUtil.scrollUp(driver);
		WebActionUtil.clickElement(this.saveBtn, driver, "save button");
		WebActionUtil.clickElement(this.reasonToMap, driver, "Reason to Map");
		WebActionUtil.clickElement(this.doneBtn, driver, "done button");
	}

	public void navigateBack(WebDriver driver) {
		driver.navigate().back();
	}

	public void refreshPage(WebDriver driver) throws Exception {
		driver.navigate().refresh();
		Thread.sleep(6000);
	}

	public void floatingIcon(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Agara area", driver, "Home Page", 120);
		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
		WebActionUtil.clickElement(this.uploadTripSheet, driver, " upload tripsheet button");
		WebActionUtil.waitTillPageLoad("loading tripsheet", driver, "Gallery", 120);
		WebActionUtil.clickElement(this.unMappedBtn, driver, "Mapped Page");
		WebActionUtil.waitTillPageLoad("Done", driver, "Tripsheet page", 120);

	}

}
