
package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.paulhammant.ngwebdriver.ByAngular;

//import ch.qos.logback.core.joran.action.ActionUtil;
import io.appium.java_client.android.AndroidDriver;

public class WebAddArea {

	// Webdriver driver instance creation
	WebDriver driver;

	public WebAddArea(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Home Page **/

	@FindBy(xpath = "//*[id='mainContainer' and class='hamburger-menu']")
	private WebElement hamMenu;

	public WebElement clickHamMenu() {
		return hamMenu;
	}

	@FindBy(xpath = "//*[@id='mainContainer']/div/div[3]/div/div[1]/div[1]/div/ul/div/div")
	private WebElement dwnArrow;

	public WebElement clickdwnArrow() {
		return dwnArrow;
	}

	public void clickdownarrow(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(dwnArrow, driver, "Down Arrow");
	}

	@FindBy(xpath = "//*[@id='mainContainer']/div/div[3]/div/div[1]/div[1]/div/ul/div")
	private WebElement dwnArrow1;

	public WebElement clickdwnArrow1() {
		return dwnArrow1;
	}

	@FindBy(xpath = "//*[text()='FQLC']")
	private WebElement fqlclink;

	public WebElement clickFQLC() {
		return fqlclink;
	}

	
	
	@FindBy(xpath = "//*[text()='YESTERDAY']")
	private WebElement yday;
	
	
	@FindBy(xpath = "//*[text()='FQLC Supervisor']")
	private WebElement fqlsupclink;

	public WebElement clickSuperFQLC() {
		return fqlsupclink;
	}

	@FindBy(xpath = "//*[text()='Ops']")
	private WebElement Opslink;

	public WebElement clickOpslink() {
		return Opslink;
	}

	@FindBy(xpath = "//*[text()='View Areas']")
	private WebElement viewArea;

	public WebElement clickviewArea() {
		return viewArea;
	}

	@FindBy(xpath = "//input")
	private WebElement inputtext;

	public WebElement enterText() {
		return inputtext;
	}

	@FindBy(xpath = "//*[contains(@class,'new')]")
	private WebElement addnew;

	public WebElement clickaddnewArea() {
		return addnew;
	}

	@FindBy(xpath = "//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div/div/div/div/div[1]/div[2]/div/input")
	private WebElement areaName;

	public WebElement typeAreaName() {
		return areaName;
	}

	@FindBy(xpath = "//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div/div/div/div/div[1]/div[3]/div/div/select")
	private WebElement foName;

	public WebElement selectFOName() {
		return foName;
	}

	@FindBy(xpath = "//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div/select")
	private WebElement alName;

	public WebElement selectAlName() {
		return alName;
	}

	@FindBy(xpath = "//*[contains(text(),'Save')]")
	private WebElement savebtn;

	public WebElement clickSavebtn() {
		return savebtn;
	}

	@FindBy(xpath = "//*[@value='Save']")
	private WebElement save;

	public WebElement clickSave() {
		return save;
	}

	@FindBy(xpath = "//*[@class='hub-save-button']")
	private WebElement savehub;

	public WebElement clickSavehub() {
		return savehub;
	}

	@FindBy(xpath = "//div[@class='area-card'][last()]")
	private WebElement areaCreated;

	public WebElement verifyArea() {
		return areaCreated;
	}

	@FindBy(xpath = "	//*[@class='add-hub-form']")
	private WebElement addHubcard;

	public WebElement verifyCardtext() {
		return addHubcard;
	}

	@FindBy(xpath = "//div[@class='area-card'][last()]//*[text()='DELETE']")
	private WebElement delArea;

	public WebElement ClkdelArea() {
		return delArea;
	}

	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement yes;

	public WebElement Clkyes() {
		return yes;
	}

	@FindBy(xpath = "//*[contains(text(),'Clients')]")
	private WebElement clients;

	public WebElement clickClients() {
		return clients;
	}

	// @FindBy(xpath="//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/input")
	@FindBy(xpath = "//*[text()='Client Name']/../input[@type='text']")
	private WebElement clientName;

	public WebElement enterClientName() {
		return clientName;
	}

	@FindBy(xpath = "//*[text()='Client Supervisor']/../input[@type='text']")
	private WebElement clientSupervisor;

	public WebElement enterclientSupervisor() {
		return clientSupervisor;
	}

	@FindBy(xpath = "//*[text()='Supervisor Contact']/../input[@type='number']")
	private WebElement supervisorContact;

	public WebElement entersupervisorContact() {
		return supervisorContact;
	}

	@FindBy(xpath = "//div[@class='client-card height-200'][last()]")
	private WebElement createdClient;

	public WebElement verifycreatedClient() {
		return createdClient;
	}

	@FindBy(xpath = "//div[@class='client-card height-200'][last()]")
	private WebElement createdHub;

	public WebElement verifycreatedhub() {
		return createdHub;
	}

	@FindBy(xpath = "//div[@class='client-card height-200'][last()]/.//.//*[text()='EDIT']")
	private WebElement clientEdit;

	public WebElement clkclientEdit() {
		return clientEdit;
	}

	@FindBy(xpath = "//input[@type='email']")
	private WebElement email;

	public WebElement enterEmail() {
		return email;
	}

	// @FindBy(xpath="//div[@class='client-card
	// height-200']/..//*[text()='ADH']/../..//*[text()='ADD HUB']")
	// private WebElement addHub;
	//
	// public WebElement clkaddHub() {
	// return addHub;
	// }

	public WebElement clkclnt(WebDriver driver, String cName) {
		return driver.findElement(By.xpath("//div[@class='client-card height-200']/..//*[text()='" + cName + "']"));
	}

	public WebElement clkclientHub(WebDriver driver, String client) {
		return driver.findElement(By.xpath(
				"//div[@class='client-card height-200']/..//*[text()='" + client + "']/../..//*[text()='ADD HUB']"));
	}

	@FindBy(xpath = "//*[text()='Hub Name']//..//../input[@type='text']")
	private WebElement hubname;

	public WebElement enterhubname() {
		return hubname;
	}

	// @FindBy(xpath="//*[text()='Hub Area']/../..//*[@class='dropdown-text-div
	// down ']")
	// @FindBy(xpath="//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div/div/div[1]/div[2]/div/div")

	@FindBy(xpath = "//*[text()='Hub Area']/../..//*[@class='dropdown-text-div  down ']")

	private WebElement hubarea;

	public WebElement selhubarea() {
		return hubarea;
	}

	@FindBy(xpath = "//*[@class='dropdown-popup-div']")
	private WebElement popup;

	public WebElement clickpopup() {
		return popup;
	}

	@FindBy(xpath = "//*[text()='Hub Latitude']//..//../input[@type='text']")
	private WebElement lat;

	public WebElement enterlat() {
		return lat;
	}

	@FindBy(xpath = "//*[text()='Hub Longitude']//..//../input[@type='text']")
	private WebElement longt;

	public WebElement enterlongt() {
		return longt;
	}

	@FindBy(xpath = "//*[@class='hub-card']")
	private WebElement newhub;

	public WebElement verifyNewhub() {
		return newhub;
	}

	public WebElement verifyhub(WebDriver driver, String newhub) {
		return driver.findElement(By.xpath("//*[@class='hub-card']/..//*[text()=\'" + newhub + "\']"));
	}

	public WebElement clickArea(WebDriver driver, String areaname) {
		return driver.findElement(By.xpath("//*[@class='area-card']/..//*[text()=\'" + areaname + "\']"));
	}

	@FindBy(xpath = "//input[@value='Update']")
	private WebElement update;

	public WebElement clkupdate() {
		return update;
	}

	@FindBy(xpath = "//*[text()='EDIT']")

	private WebElement editbtn;

	public WebElement clkEdit() {
		return editbtn;
	}

	public WebElement selecttestHub(WebDriver driver, String testhub) {

		return driver.findElement(By.xpath("//*[text()=\'" + testhub + "\']/../..//*[text()='EDIT']"));
	}

	public WebElement deleteHub(WebDriver driver, String testhub) {

		return driver.findElement(By.xpath("//*[text()=\'" + testhub + "\']/../..//*[text()='DELETE']"));
	}

	@FindBy(xpath = "//*[text()='DELETE']")
	private WebElement delbtn;

	public WebElement clkDel() {
		return delbtn;
	}

	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement yesbtn;

	public WebElement clkYes() {
		return yesbtn;
	}

	@FindBy(xpath = "//*[contains(text(),'New Book')]")
	private WebElement newBook;

	public WebElement clknewBook() {
		return newBook;
	}

	@FindBy(xpath = "//*[@id='client_name']/div[2]")
	private WebElement clientDropDown;

	public WebElement clkclientDropDown() {
		return clientDropDown;
	}

	@FindBy(xpath = "//*[label='Client Hub']")
	private WebElement clienthubDropdwn;

	public WebElement clkcclienthubDropdwn() {
		return clienthubDropdwn;
	}

	@FindBy(xpath = "//*[@class='mytrash']")
	private WebElement trashBtn;

	public WebElement clientHub(WebDriver driver, String clienthub) {

		return driver.findElement(By.xpath("//*[@type='text']/..//*[text()=\'" + clienthub + "\']"));
	}

	public WebElement clkHubDropdown(WebDriver driver, String hubname) {

		return driver.findElement(By.xpath("//*[@class='sc-cMljjf hEWgzY']/../..//*[text()=\'" + hubname + "\']"));
	}

	//

	//

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Select FQLC
	 * 
	 * @throws Exception
	 * 
	 * 
	 */

	public void _clickOPMFQLC(WebDriver driver) throws Exception {

		WebActionUtil.clickElement(dwnArrow, driver, "Down Arrow-OPM");
		WebActionUtil.clickElement(fqlclink, driver, "FQLC");

	}

	public void _clickOPMFQLC1(WebDriver driver) throws Exception {

		WebActionUtil.scrollIntoViewAndClick(driver, dwnArrow1);
		WebActionUtil.scrollIntoViewAndClick(driver, fqlclink);

	}

	public void _clickOPMFQLCSupervisor(WebDriver driver) throws Exception {

		WebActionUtil.clickElement(dwnArrow, driver, "Down Arrow-OPM");
		Thread.sleep(5000);
		WebActionUtil.scrollIntoViewAndClick(driver, fqlsupclink);

	}

	public void _clickOnDownArrow(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(dwnArrow, driver, "Down Arrow-OPM");
	}

	@FindBy(xpath = "//div[@class='client-card height-200'][last()]/.//.//*[text()='ADD HUB']")
	private WebElement addHub;

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Select FQLC
	 * 
	 * @throws Exception
	 * 
	 * @throws Exception
	 * 
	 * 
	 */

	public void _addDelArea(String al_name, String fo_name, WebDriver driver) throws Exception {
		Thread.sleep(7000);
		WebActionUtil.clickElement(Opslink, driver, "Ops");
		WebActionUtil.clickElement(viewArea, driver, "View Area");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(addnew, driver, "Add new area");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		String rnd1 = WebActionUtil.generateRandomString(3);
		WebActionUtil.clearAndType(areaName, rnd1, "AreaName", driver);
		WebActionUtil.selectbyVisibletext(driver, foName, fo_name);
		WebActionUtil.selectbyVisibletext(driver, alName, al_name);
		WebActionUtil.clickElement(savebtn, driver, "Save");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		String arname1 = areaCreated.getText();
		WebActionUtil.verifyContainsText(arname1, rnd1, "Area Created");
		WebActionUtil.clickElement(delArea, driver, "Delete Area");
		WebActionUtil.clickElement(yes, driver, "Yes");
		Thread.sleep(10000);
		WebActionUtil.notContainsText(areaCreated, rnd1, driver);

	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Add new client
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public void _addclient() throws Exception {

		WebActionUtil.clickElement(Opslink, driver, "Ops");
		WebActionUtil.clickElement(clients, driver, "Clients");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(addnew, driver, "Add new client");
		String clnt = WebActionUtil.generateRandomString(3);
		WebActionUtil.type(clientName, clnt, "Client name", driver);
		WebActionUtil.type(clientSupervisor, clnt, "client supervisor", driver);
		String num = WebActionUtil.generateRandomNumber();
		WebActionUtil.type(supervisorContact, num, "Supervisor contact", driver);
		WebActionUtil.clickElement(savebtn, driver, "Save");
	}

	public WebElement selectClientArea(WebDriver driver, String hubName) {
		return driver.findElement(By
				.xpath("//*[text()='Hub Area']/../..//*[@class='dropdown-text-div  up ']/div[@class='dropdown-popup-div']//*[text()=\'"
						+ hubName + "\']"));
	}

	public static String[] sData = GenericLib.toReadExcelData(GenericLib.sPortalTestDataPath, "add_client",
			"AddEditClient_17_18");

	public void _addhub(WebDriver driver, String hname, String harea, String latitude, String longtitude,
			String clntName) throws Exception {

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		System.out.println(clkclientHub(driver, clntName));
		WebActionUtil.scrollIntoViewAndClick(driver, clkclientHub(driver, clntName));
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.type(hubname, hname, "HubName", driver);

		WebActionUtil.clickElement(this.hubarea, this.driver, "Hub Area");
		WebActionUtil.scrollIntoViewAndClick(driver, selectClientArea(this.driver, sData[BaseLib.gv.hubcount]));

		WebActionUtil.type(lat, latitude, "Latitude", driver);
		WebActionUtil.type(longt, longtitude, "Longtitude", driver);
		WebActionUtil.clickElement(savehub, driver, "Save");
	}

	public void _validatehub(WebDriver driver, String cntName, String hname) throws IOException {

		// WebActionUtil.scrollIntoViewAndClick(driver, createdHub);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);

		// WebActionUtil.scrollIntoViewAndClick(driver,
		// clkclnt(driver,cntName));

		System.out.println(clkclnt(driver, cntName));
		// WebActionUtil.verifyElementIsDisplayed(verifyhub(driver,hname),
		// driver, "New Hub");

	}

	public void _editDelhub(WebDriver driver, String updatehub, String hname) throws Exception {

		WebActionUtil.clickElement(selecttestHub(driver, hname), driver, "Edithub");

		WebActionUtil.clickElement(this.hubarea, this.driver, "Hub Area");
		WebActionUtil.scrollIntoViewAndClick(driver, selectClientArea(this.driver, sData[BaseLib.gv.hubupdate]));
		WebActionUtil.clickElement(update, driver, "Update");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);

		WebActionUtil.verifyElementContainsText(verifyhub(driver, updatehub), updatehub, driver);

		// del hub

		WebActionUtil.clickElement(deleteHub(driver, updatehub), driver, "Delete");
		WebActionUtil.clickElement(yesbtn, driver, "Yes");

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);

		String updatedhub = driver.getPageSource();

		if (updatedhub.contentEquals(updatehub)) {
			MyExtentListners.test.fail("Hub not deleted ");
		} else {
			MyExtentListners.test.pass("Hub deleted");
		}
	}

	public void verifyLongLat(WebDriver driver, String hname, String clientname, String wrngLat, String wrngLong)
			throws Exception {

		WebActionUtil.clickElement(Opslink, driver, "Ops");
		WebActionUtil.clickElement(clients, driver, "Clients");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(clkclientHub(driver, clientname), driver, "Add hub on a particular client");
		WebActionUtil.clearAndType(hubname, hname, "Hub name", driver);
		WebActionUtil.clickElement(this.hubarea, this.driver, "Hub Area");
		WebActionUtil.scrollIntoViewAndClick(driver, selectClientArea(this.driver, sData[BaseLib.gv.hubcount]));
		WebActionUtil.clickElement(save, driver, "Save");
		WebActionUtil.verifyElementContainsText(addHubcard, "mandatory", driver);// remove
																					// hard
																					// code
		WebActionUtil.type(lat, wrngLat, "Latitude", driver);
		WebActionUtil.type(longt, wrngLong, "Longtitude", driver);
		WebActionUtil.clickElement(save, driver, "Save");
		WebActionUtil.verifyElementContainsText(addHubcard, "valid number", driver);// remove
																					// hard
	}

	public void _validateClient(WebDriver driver) throws Exception {

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoView(driver, createdClient);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.verifyElementIsDisplayed(createdClient, driver, "New Client");
		WebActionUtil.scrollIntoView(driver, createdHub);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.verifyElementIsDisplayed(createdHub, driver, "New Client");

	}

	public void _editClient(WebDriver driver, String emailid) throws Exception {

		WebActionUtil.clickElement(Opslink, driver, "Ops");
		WebActionUtil.clickElement(clients, driver, "Clients");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
		WebActionUtil.clickElement(clientEdit, driver, "Edit Client");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
		email.sendKeys(Keys.chord(Keys.CONTROL, emailid), emailid);
		// WebActionUtil.clearAndType(email, emailid, "Email", driver);
		WebActionUtil.clickElement(savebtn, driver, "save");

	}

	public void _addhub(WebDriver driver, String hname, String harea, String latitude, String longtitude)
			throws Exception {

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(addHub, driver, "Add Hub");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.type(hubname, hname, "HubName", driver);

		WebActionUtil.clickElement(this.hubarea, this.driver, "Hub Area");
		WebActionUtil.scrollIntoViewAndClick(driver, selectClientArea(this.driver, sData[BaseLib.gv.hubcount]));

		WebActionUtil.type(lat, latitude, "Latitude", driver);
		WebActionUtil.type(longt, longtitude, "Longtitude", driver);
		WebActionUtil.clickElement(savehub, driver, "Save");
	}

	public void _validatehub(WebDriver driver, String hname) throws IOException {

		WebActionUtil.scrollIntoViewAndClick(driver, createdHub);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		// System.out.println(newhub);
		// System.out.println(hname);
		WebActionUtil.verifyElementIsDisplayed(verifyhub(driver, hname), driver, "New Hub");

	}

	public void saveHub(WebDriver driver, String latt, String lonngt) throws Exception {

		WebActionUtil.clearAndType(lat, latt, "Latitude", driver);
		WebActionUtil.clearAndType(longt, lonngt, "Longtitude", driver);
		WebActionUtil.clickElement(save, driver, "Save");

	}

	public void verifyHubinBooking(WebDriver driver, String cltname, String areaname, String hubname) throws Exception {

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(viewArea, driver, "View Area");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);

		WebActionUtil.scrollIntoViewAndClick(driver, clickArea(driver, areaname));

		WebActionUtil.clickElement(newBook, driver, "Add new booking");

		WebActionUtil.clearAndTypeEnter(clientName, cltname, "Client name", driver);
		WebActionUtil.actionClickAndType(clientDropDown, driver, "client", cltname);

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);

		WebActionUtil.clickElement(clienthubDropdwn, driver, "Client hub");
		System.out.println(clkHubDropdown(driver, hubname));
		WebActionUtil.actionClickAndType(clkHubDropdown(driver, hubname), driver, "hub drop down", hubname);

	}

	public WebElement getArea(WebDriver driver, String filepath, String sheet, String testcase) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		return driver.findElement(By.xpath("//*[text()='" + sData[BaseLib.gv.arCount] + "']"));
	}

	@FindBy(xpath = "//*[text()='Add New Booking']")
	private WebElement addNewBookingBtn;

	public void clickAddNewBooking(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.addNewBookingBtn, driver, "Created Button");
	}

	public void verifyBookingPage(WebDriver driver) throws Exception {
		WebActionUtil.verifyElementIsDisplayed(this.addNewBookingBtn, driver, "Add new booking button");
	}

	public WebElement getPublishButton(WebDriver driver, String filepath, String sheet, String testcase,
			String timeStamp2, String vehicle) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		return driver.findElement(By.xpath("//*[text()='" + sData[BaseLib.gv.cnCount] + "']/../../../..//*[text()='"
				+ timeStamp2 + "']/../..//*[text()='" + vehicle + "']/../..//*[text()='" + sData[BaseLib.gv.drCount]
				+ "']/../../../../..//*[text()='Publish']"));
	}
	
	
	
	
	public WebElement getcalendartoPublish(WebDriver driver, String filepath, String sheet, String testcase,
			String timeStamp2, String vehicle) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		return driver.findElement(By.xpath("//*[text()='" + sData[BaseLib.gv.cnCount] + "']/../../../..//*[text()='"
				+ timeStamp2 + "']/../..//*[text()='" + vehicle + "']/../..//*[text()='" + sData[BaseLib.gv.drCount]
				+ "']/../../../../..//*[@class='publish-date-select']"));
	}
	
	
	
	
	public WebElement getBookingCheckBpox(WebDriver driver, String filepath, String sheet, String testcase,
			String timeStamp,String vehicle) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		return driver.findElement(By.xpath("//*[text()='" + sData[BaseLib.gv.cnCount] + "']/../../../..//*[text()='"
				+ timeStamp + "']/../..//*[text()='" + sData[BaseLib.gv.vnCount] + "']/../..//*[text()='"
				+ sData[BaseLib.gv.drCount]
				+ "']/../../../../..//*[text()='Publish']/..//*[@class='checkbox-wrapper-div']"));
	}

	public void verifyBookingSuccess(WebDriver driver, String filepath, String sheet, String testcase,
			String timeStamp2, String vehicle) throws Exception {
		WebActionUtil.scrollIntoView(driver,
				this.getPublishButton(driver, filepath, sheet, testcase, timeStamp2, vehicle));
		WebActionUtil.verifyElementIsDisplayed(
				this.getPublishButton(driver, filepath, sheet, testcase, timeStamp2, vehicle), driver,
				"Booking successfull");
	}

	@FindBy(xpath = "//*[text()='Booking Published']")
	private WebElement publishedAlert;

	public void verifyPublishedPopup(WebDriver driver) throws Exception {
		WebActionUtil.verifyElementIsDisplayed(this.publishedAlert, driver, "Published Alert Displayed");
	}

	
	
	public void clickYday(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(this.yday, driver, "Yesterday");
	}
	public void deleteBooking(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp,String vehicle)
			throws Exception {
		WebActionUtil.scrollIntoViewAndClick(driver,
				this.getBookingCheckBpox(driver, filepath, sheet, testcase, timeStamp,vehicle));
		WebActionUtil.scrollIntoViewAndClick(driver, this.trashBtn);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		Thread.sleep(3000);
		WebActionUtil.clickElement(this.yes, driver, "yes button");
	}
}