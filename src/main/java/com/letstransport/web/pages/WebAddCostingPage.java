package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.WebActionUtil;

public class WebAddCostingPage {

	WebDriver driver;

	public WebAddCostingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Add Costing']")
	private WebElement addCostingBtn;

	public void scheduledVehicleCosting(WebDriver driver) throws IOException, Exception {
		WebActionUtil.clickElement(this.addCostingBtn, driver, "Add costing btn");
	}

}
