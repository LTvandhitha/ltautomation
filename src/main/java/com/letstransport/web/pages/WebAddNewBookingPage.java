package com.letstransport.web.pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.inject.PrivateBinder;
import com.letstransport.android.pages.AddVehicle;
import com.letstransport.init.InitializePages;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import groovy.transform.Field;

public class WebAddNewBookingPage {
	WebDriver driver;

	public WebAddNewBookingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//*[text()='Ops']")
	private WebElement Opslink;

	public WebElement clickOpslink() {
		return Opslink;
	}

	@FindBy(xpath = "//ul[@class='sub-menu']/a/li[text()='View Areas']")
	private WebElement viewAreaLink;

	public void _clickOnViewArea() throws Exception {
		WebActionUtil.waitForElement(viewAreaLink, driver, "click on view Area", 20);
		WebActionUtil.actionClick(viewAreaLink, driver, "click on view Area");
	}

	@FindBy(xpath = "//div[@id='5674053578784768']")
	private WebElement clientInputName;

	@FindBy(xpath = "//h3[text()='BTM']")
	private WebElement area;

	public void _clickOnArea() throws Exception {
		WebActionUtil.waitForElement(area, driver, "Area", 50);
		// WebActionUtil.actionClick(area, driver, "Area");*/
		/*
		 * WebActionUtil.isEleDisplayed(area, driver, "Area", 50);
		 * JavascriptExecutor executor = (JavascriptExecutor)driver;
		 * executor.executeScript("arguments[0].click();", area);
		 * Thread.sleep(5000);
		 */
		WebActionUtil.clickByJavaSriptExecutor(area, driver, "Area");
	}

	@FindBy(xpath = "//div[text()='Add New Booking']")
	private WebElement addNewBooking;

	public void _clickOnAddNewBooking() throws IOException, Exception {
		WebActionUtil.clickByJavaSriptExecutor(addNewBooking, driver, "Add New Booking");
		// WebActionUtil.clickElement(addNewBooking, driver, "Add New Booking");
		// Thread.sleep(1000);
	}

	@FindBy(xpath = "//input[@type='text'][1]/following-sibling::label[text()='Client Name']")
	private WebElement clientNameTextField;
	/*
	 * @FindBy(xpath="//input[@type='text'][1]") private WebElement client;
	 */
	@FindBy(xpath = "//input[@type='text'][1]/following-sibling::label[text()='Vehicle Number']")
	private WebElement vehicleNoTextField;
	@FindBy(xpath = "//label[text()='Vehicle Number']/../following-sibling::div/descendant::div")
	private WebElement vehicleinputTextField;
	@FindBy(xpath = "//label[text()='Vehicle Owner Contact']/../input")
	private WebElement vehOwnerContact;
	@FindBy(xpath = "//label[text()='Vehicle Owner Name']/../input")
	private WebElement vehOwnerName;
	@FindBy(xpath = "//label[text()='Driver Name']/../input")
	private WebElement driverNmae;
	@FindBy(xpath = "//label[text()='Driver Contact']/../input")
	private WebElement driverPhonNum;
	@FindBy(xpath = "//div[@id='reporting_point']/span[text()='Select an option']")
	private WebElement clientHub;
	@FindBy(xpath = "//div[@id='5cca7e297c290e2f01d5b446']")
	private WebElement NM;
	@FindBy(xpath = "//div[@id='pricing_id']")
	private WebElement clientPricing;
	@FindBy(xpath = "//div[@id='5697869507985408']")
	private WebElement clientPricingType;
	@FindBy(xpath = "//div[@id='default_driver_pricing']/span[text()='Select an option']")
	private WebElement driverPricing;
	@FindBy(xpath = "//div[@id='5640311980163072']")
	private WebElement driverpricingType;
	@FindBy(xpath = "//div[@id='5697869507985408']")
	private WebElement LONEE;
	@FindBy(xpath = "//div[@id='vehicle_type']")
	private WebElement vecType;
	@FindBy(xpath = "//div[@class='sc-brqgnP cVLoyQ']/input/following-sibling::div[@id='5066549580791808']")
	private WebElement vecInputType;
	@FindBy(xpath = "//div[@id='default_driver_vehicle_no']/descendant::div/following-sibling::div")
	private WebElement vecNoDropDown;
	@FindBy(xpath = "//div[text()='Save']")
	private WebElement saveBtn;
	@FindBy(xpath = "//div[@class='sc-brqgnP cVLoyQ']/div[@id='5066549580791808']")
	private WebElement inputVecTextBox;
    @FindBy(xpath = "//div[@class='base-booking-list-wrapper']")
	private List<WebElement> vecDetail;
	@FindBy(xpath = "//div[text()='Publish']")
	private WebElement publishBtn;
	@FindBy(xpath = "//div[text()='Publish']")
	private List<WebElement> listpublishBtn;
	@FindBy(xpath = "//div[@class='sc-hSdWYo gEddZk']")
	private WebElement bookingTym;
	@FindBy(xpath = "//label[text()='Pickup Time']/following-sibling::span")
	private List<WebElement> pickupTime;
	@FindBy(xpath = "//div[text()='Publish']/../div[2]")
	private List<WebElement> checkbox;;
	@FindBy(xpath = "//div[@class='mytrash']/div[2]")
	private WebElement deleteBtn;
    @FindBy(xpath="//div[@class='confirmation-box-buttons']/div[text()='Yes']")
    private WebElement yesBtn;

	public void _actionOnAddBookingPage(String vN, String dN) throws Exception {
        Thread.sleep(2000);
		WebActionUtil.actionClickAndType(clientNameTextField, driver, "Client Name", "LONEE");
        Thread.sleep(7000);
        WebActionUtil.actionClick(clientInputName, driver, "client input name");
		Thread.sleep(7000);
        WebActionUtil.actionClickAndType(vehicleNoTextField, driver, "vehicleNo", vN);
        Thread.sleep(7000);
		WebActionUtil.actionClick(vehicleinputTextField, driver, "Vehicle Input Text field");
        String ownerText = vehOwnerName.getAttribute("value");
		String ownerContact = vehOwnerContact.getAttribute("value");
        Thread.sleep(2000);
		WebActionUtil.actionClickAndType(driverNmae, driver, "Driver name", dN);
        WebActionUtil.pressEnterKey();
		Thread.sleep(2000);
		WebActionUtil.actionClickAndType(driverPhonNum, driver, "Driver Phone", ownerContact);
		WebActionUtil.pressEnterKey();
		Thread.sleep(1000);
		WebActionUtil.actionClick(clientHub, driver, "ClientHub");
		WebActionUtil.actionClick(NM, driver, "NM");
		WebActionUtil.actionClick(clientPricing, driver, "Client Pricing");
		WebActionUtil.clickElement(clientPricingType, driver, "ClientPricing");
		WebActionUtil.actionClick(driverPricing, driver, "Driver Pricing");
		WebActionUtil.actionClick(driverpricingType, driver, "DriverPricing");
		WebActionUtil.clickElement(bookingTym, driver, "Booking");
		Thread.sleep(1000);
		String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
		String[] part = getcelldata.split(":");
		String part1 = part[0];
		String part2 = part[1];
		String[] part3 = part2.split(" ");
		String min = part3[0];
		String timezon = part3[1];

		String HOUR = "//*[text()='Hour']/../..//span[text()=" + part1 + "]";
		String MINUTE = "//*[text()='Minute']/../..//span[text()='00']/../..//span[text()=" + min + "]";
		String TIMEZONE = "//*[text()='AM/PM']/../..//span[text()=\'" + timezon + "\']";
		WebElement Hour = driver.findElement(By.xpath(HOUR));
		WebElement Minute = driver.findElement(By.xpath(MINUTE));
		WebElement Timezone = driver.findElement(By.xpath(TIMEZONE));
		WebElement DONE = driver.findElement(By.xpath("//*[text()='DONE']"));
		WebActionUtil.scrollIntoViewAndClick(driver, Hour);
		WebActionUtil.scrollIntoViewAndClick(driver, Minute);
		WebActionUtil.scrollIntoViewAndClick(driver, Timezone);
		WebActionUtil.clickElement(DONE, driver, "Done");

		WebActionUtil.clickElement(vecType, driver, "Vehicle Type");

		WebActionUtil.clickElement(inputVecTextBox, driver, "VEhicle Input type");
		Thread.sleep(1000);
		WebActionUtil.clickElement(saveBtn, driver, "Save");
		Thread.sleep(1000);
		driver.navigate().refresh();
		
	}

	
	public void _ValidateBookingReflectDriverMode(String vN, String dN) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		Thread.sleep(1000);
		WebActionUtil.clickElement(clickOpslink(), driver, "ops");
		_clickOnViewArea();
		_clickOnArea();
		Thread.sleep(3000);
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,15000)");
		for (WebElement we11 : listpublishBtn) {
			if (we11.isDisplayed()) {
				for (int i = 0; i < checkbox.size(); i++) {
					WebActionUtil.clickElement(checkbox.get(i), driver, "checkbox");
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,-15000)");
					WebActionUtil.clickElement(deleteBtn, driver, "Delete");
					WebActionUtil.clickElement(yesBtn, driver, "YesBtn");
					jse.executeScript("window.scrollBy(0,15000)");
				}
			}
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-15000)");
			_clickOnAddNewBooking();

			_actionOnAddBookingPage(vN, dN);
			Thread.sleep(4000);

			jse.executeScript("window.scrollBy(0,15000)");
			for (WebElement we : pickupTime) {
				String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1,
						1);
				if (we.getText().equals(getcelldata)) {
					Thread.sleep(1000);
					WebActionUtil.clickElement(publishBtn, driver, "Publish");
					Thread.sleep(5000);
					
				}

			}
		}
	}

	@FindBy(xpath = "//div[@class='elem1']")
	private List<WebElement> timeinFo;
	@FindBy(xpath = "//span[@class='details-span vehicle-span']")
	private List<WebElement> vehNoFO;
	@FindBy(xpath = "//span[@class='details-span driver-span']")
	private List<WebElement> driverNameFO;
	@FindBy(xpath = "//a/li[text()='Home']")
	private WebElement homeLink;

	
}