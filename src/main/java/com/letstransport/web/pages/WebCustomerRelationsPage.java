package com.letstransport.web.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.init.InitializePages;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class WebCustomerRelationsPage {
	 WebDriver driver;
	public static String bookintime = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
	public static String BookinTime1=ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 2);
	public static String truncatedBookinTime =  bookintime .substring(1);
	public static String truncatedBookinTime1 =  BookinTime1 .substring(1);
	
	
	public static String bookintimeYesterday = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 5);
	public static String bookintimeToday = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 4);
	public static String bookintimeTomorrow = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 3);
	public static String truncatedBookinTimeYesterday =  bookintimeYesterday .substring(1);
	public static String truncatedBookinTimeToday  =   bookintimeToday.substring(1);
	public static String truncatedBookinTimeTomorrow   =   bookintimeTomorrow.substring(1);
	
	public static String bookintimeYesterdayNotVerifiedVeh = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
	public static String bookintimeTodayNotVerifiedVeh = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 2);
	public static String bookintimeTomorrowNotVerifiedVeh = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 3);
	public static String truncatedBookinTimeYesterdayNotVerifiedVeh  =  bookintimeYesterdayNotVerifiedVeh.substring(1);
	public static String truncatedBookinTimeTodayNotVerifiedVeh  = bookintimeTodayNotVerifiedVeh.substring(1);
	public static String truncatedBookinTimeTomorrowNotVerifiedVeh  = bookintimeTomorrowNotVerifiedVeh.substring(1);
	
	public static String baseprice = WebActionUtil.generateRandomNumberfornDigits(3);
	public static String ownername;
	public static String adhocOwnerVecNum;
	public static String adhocPhoneNum;
	
	public WebCustomerRelationsPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//li[text()='Customer Relations']")
	private WebElement customerRelationLink;

	@FindBy(xpath = "//span[text()='ADD NEW ENTERPRISE ON-DEMAND BOOKING']")
	private WebElement addNewDemandBookingLink;

	@FindBy(xpath = "//span[text()='Client Name']/../../input")
	private WebElement clientnameTextField;

	@FindBy(xpath = "//span[@class='confirmation-image-wrapper']/img")
	private WebElement img;

	@FindBy(xpath = "//div[@id='react-select-2--option-0']")
	private WebElement clientNameDropDown;

	@FindBy(xpath = "//li[text()='Bkl']")
	private WebElement BKL;
	@FindBy(xpath = "//span[@class='date-list-item-span' and text()='YESTERDAY']/following-sibling::span")
	private WebElement yesterdayBookingCount;
	@FindBy(xpath = "//span[@class='date-list-item-span' and text()='TOMORROW']/following-sibling::span")
	private WebElement tomorrowBookincount;

	public WebElement getClientName(WebDriver driver, String clientName) {
		return driver.findElement(By.xpath("//li[text()='" + clientName + "']"));
	}
	

	@FindBy(xpath = "//div[text()='Bkl']")
	private WebElement BKLText;
	@FindBy(xpath = "//div[@class='input-type-time-text']")
	private WebElement time;
	/*@FindBy(xpath="//div[text()=\'" + basePrice + "\']/../../descendant::div/child::div[text()='Bkl']/../following-sibling::div[2]/div/div/span[@class='attendance-text']")
     private WebElement goingCustomerRealationLink;*/
	public WebElement getGoinText(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time + "\']/../../descendant::div/child::div[text()='Bkl']/../following-sibling::div[2]/div/div/span[@class='attendance-text']"));
	}
	public WebElement getAttendenceYesterday(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()='" + time + "'][1]/../preceding-sibling::div/div[text()='Bkl']/../following-sibling::div[2]/div/div/span[2][text()='(P) Going']"));
	}
	public WebElement getVecAndClientName(WebDriver driver, String name) {
		return driver.findElement(By.xpath("//span[text()='" + name + "']/../following-sibling::div"));
	}

	@FindBy(xpath = "//div[@class='save-button' and text()='SAVE']")
	private WebElement saveBtn;
	@FindBy(xpath = "//div[@id='react-select-2--value']//descendant::div[@class='Select-input']/input")
	private WebElement clientInput;
	@FindBy(xpath = "//li[text()='0.1 Ton-Bike']")
	private WebElement selectVecDropDown;
	@FindBy(xpath = "//div[@class='Select-menu-outer']")
	private WebElement outermenu;
	@FindBy(xpath = "//span[@class='date-list-item-span' and text()='YESTERDAY']")
	private WebElement yesterdayTab;
    /*@FindBy(xpath="//span[text()='6:15 PM']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[2]/div[2]")
    private WebElement ownername;*/
	@FindBy(xpath = "//span[@class='date-list-item-span' and text()='TOMORROW']")
	private WebElement tomorrowTab;

	@FindBy(xpath = "//span[@class='date-list-item-span' and text()='TODAY']/following-sibling::span")
	private WebElement todaysCount;
	//span[text()='12:10 PM']/../../preceding-sibling::div/following-sibling::div/span[text()='Ent OD']/../following-sibling::div[2]/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[1]/div
public WebElement getvechileDetail(WebDriver driver,String time){
	return driver.findElement(By.xpath("//span[text()=\'" + time
				+ "\']/../../preceding-sibling::div/following-sibling::div/span[text()='Ent OD']/../following-sibling::div[2]/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[1]/div/a/div"));
}
public WebElement getNotVerifiedVechileDetail(WebDriver driver,String time){
	return driver.findElement(By.xpath("//span[text()='N/A']/../../preceding-sibling::div[3]/div/span[text()=\'" + time
			+ "\']/../../following-sibling::div[1]/descendant::div[3]/span/../../../../following-sibling::div/div/a/div[text()='Add Vehicle Details'][1]"));
}
////div/span[text()='3:25 PM']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div/descendant::a/div[text()='Add Vehicle Details']
public WebElement getAddVecDetail(WebDriver driver,String time){
	return driver.findElement(By.xpath("//div/span[text()=\'" + time
			+ "\']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div/descendant::a/div[text()='Add Vehicle Details']"));
}
public static WebElement getownerName(WebDriver driver,String time){
	return driver.findElement(By.xpath("//span[text()=\'" + time
				+ "\']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[2]/div[2]"));
}

public static WebElement getVendorNameCustomerRelation(WebDriver driver,String time){
	return driver.findElement(By.xpath("//div[text()=\'" + time
			+ "\']/../following-sibling::div/descendant::div[2]/span[2]/../../../following-sibling::div/div[1]"));
}

public static WebElement getVendorVecNoCustomerRelation(WebDriver driver,String time){
	return driver.findElement(By.xpath("//div[text()=\'" + time
			+ "\']/../following-sibling::div/descendant::div[2]/span[2]/../../../following-sibling::div/div[2]"));
}

public static WebElement getVendorPhoneCustomerRelation(WebDriver driver,String time){
	return driver.findElement(By.xpath("//div[text()=\'" + time
			+ "\']/../following-sibling::div/descendant::div[2]/span[2]/../../../following-sibling::div/div[3]"));
}

public static WebElement getAdhocOwnerNum(WebDriver driver,String time){
	return driver.findElement(By.xpath("//span[text()=\'" + time
			+ "\']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[2]/div[3]"));
	
}
public static WebElement getAdhocPhoneNum(WebDriver driver,String time){
	return driver.findElement(By.xpath("//span[text()=\'" + time
			+ "\']/../../following-sibling::div/descendant::div[3]/span[text()='(P) Going']/../../../../following-sibling::div[2]/div[4]"));
}
	public WebElement getEntOdImg(WebDriver driver, String time) {
		return driver.findElement(By.xpath("//span[text()=\'" + time
				+ "\'][1]/../../preceding-sibling::div/following-sibling::div/span[@class='ent-od-div']"));
	}

	@FindBy(xpath = "//div[text()='Customer Relations']")
	private WebElement customerRelationsLink;
	@FindBy(xpath = "//div[@class='drop-down-elem ']/h3[text()='(P) Going']")
	private WebElement goingg;
	public WebElement getgoinglink(WebDriver driver,String time){
		
		return driver.findElement(By.xpath("//span[text()=\'" + time+ "\']/../../following-sibling::div/descendant::span[text()='Select ']/../descendant::div[11]/h3[text()='(P) Going']"));
	}
	public WebElement getVendorName(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time
				+ "\']/../preceding-sibling::div/div[1]/../following-sibling::div[2]/descendant::div[2]/span[2][text()='(P) Going']/../../../following-sibling::div/div[1]"));
	}
	public WebElement getVendorvecNo(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time
				+ "\']/../preceding-sibling::div/div[1]/../following-sibling::div[2]/descendant::div[2]/span[2][text()='(P) Going']/../../../following-sibling::div/div[2]"));
	}
	public WebElement getVendorPhoneNo(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time
				+ "\']/../preceding-sibling::div/div[1]/../following-sibling::div[2]/descendant::div[2]/span[2][text()='(P) Going']/../../../following-sibling::div/div[3]"));
	}
	@FindBy(xpath="//label[text()='Vendor Name']/../input")
	private WebElement vendorName;
	public void vendorName(WebDriver driver,String vendorname) throws Exception{
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		//String nameOfVendor = WebActionUtil.generateRandomString(4);
		WebActionUtil.clearAndType(vendorName, vendorname, "vendor name", driver);
	}
	
	@FindBy(xpath="//label[text()='Adhoc Vehicle Rate']/../input")
	private WebElement adhocVecRate;

	@FindBy(xpath = "//span[@class='upload-icon']")
	private WebElement uploadIcon;
	@FindBy(xpath = "//span[text()='Base Price']/../../input")
	private WebElement basePriceTextField;
	@FindBy(xpath = "//div[@class='revenue-info']")
	private WebElement basepriceinRevenue;
	@FindBy(xpath = "//li[text()='OPM']")
	private WebElement opmLink;
	@FindBy(xpath = "//div[text()='YESTERDAY']")
	private WebElement yesterdayTabOpm;
	@FindBy(xpath = "//div[text()='TOMORROW']")
	private WebElement tomorrowTabOpm;
	@FindBy(xpath="//label[text()='Enter Vehicle Number']/../input")
	private WebElement addBackUpTextBox;
	@FindBy(xpath="//div[text()='Next']")
	private WebElement nextButton;
	@FindBy(xpath="//div[@id='react-select-5--value']/descendant::div[2]/input")
	private WebElement selectVendorInputtextField;
	@FindBy(xpath=" //div[@id='react-select-8--value']/div[text()='Select Vendor...']")
	private WebElement selectVendor;
	@FindBy(xpath="//div[@class='Select-menu-outer']")
	private WebElement select;
	/*@FindBy(xpath="//div[text()='Select Vendor...']/../descendant::div[2]/input")
	private WebElement selectVendor;*/
	@FindBy(xpath="//div[@id='react-select-8--option-0']")
	private WebElement selectVendorOption1;
    @FindBy(xpath="//div[text()='DONE']")
    private WebElement doneBtn;
    public void done(WebDriver driver) throws Exception{
    	WebActionUtil.clickElement(doneBtn, driver, "click on done");
    	Thread.sleep(1000);
    }
	public void yesterdayTabOPM(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(yesterdayTabOpm, driver, "Yesterday");
		Thread.sleep(2000);
	}
	public void nextButton(WebDriver driver) throws Exception{
		WebActionUtil.clickElement(nextButton, driver, "click on next button");
	}
	@FindBy(xpath="//label[text()='Vendor Mobile no.']/../input")
	private WebElement vendorMobileNo;
	public void vendorMobileNo(WebDriver driver) throws Exception{
		WebActionUtil.clickElement(vendorMobileNo, driver, "vendor mobile no");
		WebActionUtil.clearAndType(vendorMobileNo, "7896543212","vendor mobile no" , driver);
	}
	@FindBy(xpath="//label[text()='Adhoc Vehicle Rate']/../input")
	private WebElement adhocVechileRate;
	public void adhocVechileRate(WebDriver driver,String AdhocVechileRate) throws Exception{
		WebActionUtil.clickElement(adhocVechileRate, driver, "Adhoc Vec Rate");
		WebActionUtil.clearAndType(adhocVechileRate, AdhocVechileRate,"Adhoc Vec Rate" , driver);
	}
	public void addBackupTextBox(WebDriver driver,String verifiedVec) throws Exception{
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		WebActionUtil.clearAndType(addBackUpTextBox, verifiedVec, "vec no text field", driver);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
	}
	public void vendorDetails(WebDriver driver) throws Exception{
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 200);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
	}
	@FindBy(xpath="//div[text()='Yes']")
	private WebElement yes;
	public void adhocVecRate(WebDriver driver) throws Exception{
		WebActionUtil.waitForElement(adhocVecRate, driver, "Adhoc Vec Rate", 500);
		WebActionUtil.clickElement(adhocVecRate, driver,"Adhoc Vec Rate");
		Thread.sleep(2000);
		WebActionUtil.clearAndType(adhocVecRate, "83", "Adhoc Vec Rate", driver);
		
	}
	

	public void tomorrowTabOPM(WebDriver driver) throws Exception {
		//WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 11000, driver);
		//Thread.sleep(5000);
		WebActionUtil.clickElement(tomorrowTabOpm, driver, "Tomorrow tab");
		Thread.sleep(500);
	}

	public void opm(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(opmLink, driver, "click on opm");
	}

	public WebElement getbaseprice(WebDriver driver, String basePrice) {
		return driver.findElement(By.xpath("//div[text()=\'" + basePrice + "\']"));
	}
	public WebElement addVechileDetails(WebDriver driver,String time){
		return driver.findElement(By.xpath("//span[text()=\'" + time + "\']/../../preceding-sibling::div/following-sibling::div/span[text()='Ent OD']/../following-sibling::div[3]/div/a/div[text()='Add Vehicle Details']"));
	}

	/*@FindBy(xpath = "//span[text()=\'" + basePrice + "\']/../../../div[2]/span/../following-sibling::div[2]/descendant::div[3]/span[text()='Select ']")
	private WebElement attendenceYest;*/
	public WebElement getAttendenceYesterdayOPM(WebDriver driver,String time){
		return driver.findElement(By.xpath("//span[text()=\'" + time + "\']/../../preceding-sibling::div/following-sibling::div/h2/../following-sibling::div[2]/div[1]/div[1]/div[1]/span[text()='Select ']"));
	}

	public WebElement getBookedTime(WebDriver driver, String time) {
		return driver.findElement(By.xpath("//span[text()=\'" + time + "\']"));
	}
	public void selectVendor(WebDriver driver) throws Exception{
		WebActionUtil.waitForElement(selectVendor, driver, "select vendor", 1000);
		WebActionUtil.actionClick(selectVendor, driver, "select vendor");
		WebActionUtil.clickElement(selectVendorOption1, driver, "select vendor type");
	}

	public WebElement getbasepriceWithClient(WebDriver driver, String basePrice) {
		return driver.findElement(
				By.xpath("//div[text()=\'" + basePrice + "\']/../../descendant::div[@class='client-name actionable']"));
	}

	public WebElement getclientHubLocationWithBasePrice(WebDriver driver, String basePrice) {
		return driver.findElement(By
				.xpath("//div[text()=\'" + basePrice + "\']/../../descendant::div[@class='client-hub location-icon']"));
	}

	public void customerRealtionLink(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(customerRelationLink, driver, "Wait for customer relation link", 60);
		WebActionUtil.clickElement(customerRelationLink, driver, "click on customer relation link");
	}

	public void customerRelationsLink1(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(customerRelationsLink, driver, "click on CR Link");
	}

	public void uploademailLink(WebDriver driver) throws Exception {
		Thread.sleep(1000);
		WebActionUtil.waitForElement(uploadIcon, driver, "upload email link", 2000);
		WebActionUtil.actionClick(uploadIcon, driver, "upload Email Link");
		WebActionUtil.uploadFile(GenericLib.enterpath);
		WebActionUtil.waitForElement(img, driver, "image", 3000);
	}

	public void yesterdayTab(WebDriver driver) throws Exception {
		Thread.sleep(500);
		WebActionUtil.waitForElement(yesterdayTab, driver, "wait for yesterday", 1000);
		WebActionUtil.clickElement(yesterdayTab, driver, "click on yesterday");
		Thread.sleep(500);
	}
	public void vendordetails(WebDriver driver) throws Exception{
		WebActionUtil.clickElement(selectVendor, driver, "click on vendor");
	}

	public void tomorrowTab(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(tomorrowTab, driver, "tomorrow Tab", 1000);
		//WebActionUtil.clickElement(tomorrowTab, driver, "tomorrow Tab");
		WebActionUtil.actionClick(tomorrowTab, driver, "tomorrow Tab");
	}

	public void addNewBookingLink(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(addNewDemandBookingLink, driver, "wait for addNew Booking", 20);
		WebActionUtil.clickElement(addNewDemandBookingLink, driver, "click on Add New Booking");

	}

	public void baseprice(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(basePriceTextField, driver, "BasePrice");
		WebActionUtil.clearAndType(basePriceTextField, baseprice, "Base Price", driver);
	}

	public void clientNameTextField(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(clientnameTextField, driver, "wait for client name text field", 10);
		WebActionUtil.clickElement(clientnameTextField, driver, "click on client name Text Field");
		WebActionUtil.clearAndType(clientnameTextField, GenericLib.clientnameinput, "BKL", driver);
		WebActionUtil.clickElement(BKL, driver, "BKL");
		Thread.sleep(1000);
	}

	public void time(WebDriver driver,int row,int col) throws Exception {
	    Thread.sleep(2000);
		WebActionUtil.actionClick(time, driver, "click on time");
		//String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
		String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", row,col);
		String[] part = getcelldata.split(":");
		String part1 = part[0];
		String part2 = part[1];
		String[] part3 = part2.split(" ");
		String min = part3[0];
		String timezon = part3[1];

		String HOUR = "//*[text()='Hour']/../..//span[text()=" + part1 + "]";
		String MINUTE = "//*[text()='Minute']/../..//span[text()='00']/../..//span[text()=" + min + "]";
		String TIMEZONE = "//*[text()='AM/PM']/../..//span[text()=\'" + timezon + "\']";
		
		WebElement Hour = driver.findElement(By.xpath(HOUR));
		Thread.sleep(3000);
		WebElement Minute = driver.findElement(By.xpath(MINUTE));
		WebElement Timezone = driver.findElement(By.xpath(TIMEZONE));
		WebElement DONE = driver.findElement(By.xpath("//*[text()='DONE']"));
		WebActionUtil.scrollIntoViewAndClick(driver, Hour);
		WebActionUtil.scrollIntoViewAndClick(driver, Minute);
		WebActionUtil.scrollIntoViewAndClick(driver, Timezone);
		WebActionUtil.clickElement(DONE, driver, "Done");
		
	}
	public void time1(WebDriver driver) throws Exception{
		Thread.sleep(7000);
		WebActionUtil.actionClick(time, driver, "click on time");
		String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 2);
		String[] part = getcelldata.split(":");
		String part1 = part[0];
		String part2 = part[1];
		String[] part3 = part2.split(" ");
		String min = part3[0];
		String timezon = part3[1];

		String HOUR = "//*[text()='Hour']/../..//span[text()=" + part1 + "]";
		String MINUTE = "//*[text()='Minute']/../..//span[text()='00']/../..//span[text()=" + min + "]";
		String TIMEZONE = "//*[text()='AM/PM']/../..//span[text()=\'" + timezon + "\']";
		
		WebElement Hour = driver.findElement(By.xpath(HOUR));
		Thread.sleep(3000);
		WebElement Minute = driver.findElement(By.xpath(MINUTE));
		WebElement Timezone = driver.findElement(By.xpath(TIMEZONE));
		WebElement DONE = driver.findElement(By.xpath("//*[text()='DONE']"));
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, Hour);
		WebActionUtil.scrollIntoViewAndClick(driver, Minute);
		WebActionUtil.scrollIntoViewAndClick(driver, Timezone);
		WebActionUtil.clickElement(DONE, driver, "Done");
		
	}
	public String getTime(WebDriver driver) throws Exception{
		String gettime=" ";
		Thread.sleep(1000);
		WebActionUtil.actionClick(time, driver, "click on time");
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, 1);
		String getcelldata1=dateFormat.format(cal.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    	Date date3 = sdf.parse(getcelldata1);
    	SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm aa");
    	String getcelldata =sdf2.format(date3);
    	String[] part = getcelldata.split(":");
    	String part1 = part[0];
    	String part2 = part[1];
		String[] part3 = part2.split(" ");
		String min=part3[0];
		int minute = Integer.parseInt(min);
		minute = ((minute/5)*5)+5;
		String temp = Integer.toString(minute);
		String timezon = part3[1];
		String HOUR = "//*[text()='Hour']/../..//span[text()=" + part1 + "]";
		String MINUTE = "//*[text()='Minute']/../..//span[text()='00']/../..//span[text()=" + temp + "]";
		System.out.println(MINUTE);
		String TIMEZONE = "//*[text()='AM/PM']/../..//span[text()=\'" + timezon + "\']";
		Thread.sleep(100);
		WebElement Hour = driver.findElement(By.xpath(HOUR));
		WebElement Minute = driver.findElement(By.xpath(MINUTE));
		WebElement Timezone = driver.findElement(By.xpath(TIMEZONE));
		WebElement DONE = driver.findElement(By.xpath("//*[text()='DONE']"));
		WebActionUtil.scrollIntoViewAndClick(driver, Hour);
		WebActionUtil.scrollIntoViewAndClick(driver, Minute);
		WebActionUtil.scrollIntoViewAndClick(driver, Timezone);
		WebActionUtil.actionClick(DONE, driver, "Done");
		return gettime;
		
	}
	

	public void selectVhicleType(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(getVecAndClientName(driver, GenericLib.vechiltTypeInput), driver, "vehicle input");
		WebActionUtil.clickElement(selectVecDropDown, driver, "select dropdown menu");
	}

	public void selectClientType(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(clientInput, driver, "Wait for client input text field", 20);
		WebActionUtil.actionClick(clientInput, driver, "client input");
		WebActionUtil.actionClick(clientNameDropDown, driver, "clientNameDropDown");
		Thread.sleep(1000);
	}

	public void saveBtn(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(saveBtn, driver, "wait for save Button", 1000);
		WebActionUtil.clickElement(saveBtn, driver, "click on save Button");
	}

	public void EnTODBookingForYesterday(WebDriver driver) throws Exception {
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		yesterdayTab(driver);
        WebActionUtil.waitForElement(yesterdayBookingCount, driver, "yesterdayBookingCount", 1000);
        String countYes = yesterdayBookingCount.getText();
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 5);
	    selectVhicleType(driver);
		selectClientType(driver);
		Thread.sleep(3000);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		baseprice(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		saveBtn(driver);
	    WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(4000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		WebActionUtil.waitForElement(yesterdayTab, driver, "yesterday", 1000);
		yesterdayTab(driver);
		WebActionUtil.waitForElement(yesterdayBookingCount, driver, "yesterday", 1000);
		String afternewbookincount = yesterdayBookingCount.getText();
		WebActionUtil.verifyNotEqualsText("count", countYes, afternewbookincount);
		Thread.sleep(2000);
		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");

	}
	public void EnTODBookingForYesterdayNotVerifiedVehcile(WebDriver driver) throws Exception {
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		Thread.sleep(4000);
        yesterdayTab(driver);
        Thread.sleep(1000);
        String countYes = yesterdayBookingCount.getText();
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 1);
	    selectVhicleType(driver);
		selectClientType(driver);
		Thread.sleep(2000);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		baseprice(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		saveBtn(driver);
	    WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		Thread.sleep(9000);
		yesterdayTab(driver);
		Thread.sleep(1000);
		String afternewbookincount = yesterdayBookingCount.getText();
		WebActionUtil.verifyNotEqualsText("count", countYes, afternewbookincount);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");

	}
	
	public void EnTODBookingForTomorrowNotVerifiedVehcile(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		Thread.sleep(2000);
		tomorrowTab(driver);
        Thread.sleep(500);
        String countYes = yesterdayBookingCount.getText();
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 3);
	    selectVhicleType(driver);
		selectClientType(driver);
		Thread.sleep(2000);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		baseprice(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		saveBtn(driver);
		//WebActionUtil.clickElement(yes, driver, "Yes Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 14000, driver);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		Thread.sleep(9000);
		tomorrowTab(driver);
		Thread.sleep(1000);
		//String afternewbookincount = yesterdayBookingCount.getText();
		//WebActionUtil.verifyNotEqualsText("count", countYes, afternewbookincount);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");
	}
	
	
	
	public void EnTODBookingForTodayNotVerifiedVehcile(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		
        Thread.sleep(1000);
      
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 2);
	    selectVhicleType(driver);
		selectClientType(driver);
		Thread.sleep(2000);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		baseprice(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		saveBtn(driver);
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		Thread.sleep(4000);
		
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");
	}

	public void verify_EnTODBookingForYesterday(WebDriver driver) throws Exception {
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 6000, driver);
		yesterdayTabOPM(driver);
		Thread.sleep(2000);
		WebActionUtil.scrollIntoView(driver, getBookedTime(driver, truncatedBookinTimeYesterday));
		Thread.sleep(500);
		String EntOdImg = getEntOdImg(driver,  truncatedBookinTimeYesterday).getText();
		WebActionUtil.verifyContainsText(EntOdImg, GenericLib.bookinType, "EntOD Is present");
	}

	public void EnTODBookingForToday(WebDriver driver) throws Exception {
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 25000, driver);
		Thread.sleep(3000);
		String todaybookingcount = todaysCount.getText();
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 4);
		selectVhicleType(driver);
		selectClientType(driver);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		baseprice(driver);
		saveBtn(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		Thread.sleep(7000);
		String afterbookincount = todaysCount.getText();
		WebActionUtil.verifyNotEqualsText("count", todaybookingcount, afterbookincount);

		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");

	}

	public void verify_EnTODBookingForToday(WebDriver driver) throws Exception {
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
	
		WebActionUtil.scrollIntoView(driver, getBookedTime(driver, truncatedBookinTimeToday));
		String EntOdImg = getEntOdImg(driver, truncatedBookinTimeToday).getText();
		WebActionUtil.verifyContainsText(EntOdImg, GenericLib.bookinType, "EntOD Is present");
	}

	public void EnTODBookingForTommorrow(WebDriver driver) throws Exception {
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.waitForElement(tomorrowTab, driver, "Tomorrow", 60);
		tomorrowTab(driver);
		WebActionUtil.waitForElement(tomorrowBookincount, driver, "TomorrowBookingCount", 60);
		String bookingcount = tomorrowBookincount.getText();
		addNewBookingLink(driver);
		clientNameTextField(driver);
		time(driver, 1, 3);
		selectVhicleType(driver);
		selectClientType(driver);
		uploademailLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		baseprice(driver);
		saveBtn(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 25000, driver);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 30000, driver);
		WebActionUtil.waitForElement(tomorrowTab, driver, "Tomorrow", 6000);
		Thread.sleep(3000);
		tomorrowTab(driver);
		WebActionUtil.waitForElement(tomorrowBookincount, driver, "tomorrowBookincount", 2000);
		String afternewbookincount = tomorrowBookincount.getText();
		System.out.println( afternewbookincount);
		WebActionUtil.verifyNotEqualsText("count", afternewbookincount, bookingcount);
        Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getbasepriceWithClient(driver, baseprice));
		String clientname = GenericLib.clientnameinput;
		String clientname1 = getbasepriceWithClient(driver, baseprice).getText();
		WebActionUtil.verifyContainsText(clientname1, clientname, "client name");

	}
	
	

	public void verify_EnTODBookingForTommorrow(WebDriver driver) throws Exception {
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(1000);
		tomorrowTabOPM(driver);
		Thread.sleep(1000);
		
		WebActionUtil.scrollIntoView(driver, getBookedTime(driver, truncatedBookinTimeTomorrow));
		String EntOdImg = getEntOdImg(driver, truncatedBookinTimeTomorrow).getText();
		WebActionUtil.verifyContainsText(EntOdImg, GenericLib.bookinType, "EntOD Is present");
	}

	public void EnTODAttendenceForYesterday(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTimeYesterday));
		Thread.sleep(2000);
		WebActionUtil.actionClick(getgoinglink(driver, truncatedBookinTimeYesterday), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		}
	public void EnTODAttendenceForYesterdayNotVerifiedVeh(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTimeYesterdayNotVerifiedVeh ));
		Thread.sleep(5000);
		WebActionUtil.actionClick(getgoinglink(driver,truncatedBookinTimeYesterdayNotVerifiedVeh), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		}
	
	public void EnTODAttendenceForTomorrowNotVerifiedVeh(WebDriver driver) throws Exception {
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTimeTomorrowNotVerifiedVeh));
		Thread.sleep(2000);
		WebActionUtil.actionClick(getgoinglink(driver,truncatedBookinTimeTomorrowNotVerifiedVeh ), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		}
	public void EnTODAttendenceForTodayNotVerifiedVeh(WebDriver driver) throws Exception{
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTimeTodayNotVerifiedVeh ));
		Thread.sleep(2000);
		WebActionUtil.actionClick(getgoinglink(driver, truncatedBookinTimeTodayNotVerifiedVeh  ), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
	}
	
	public void EnTODAttendenceForYesterday1(WebDriver driver) throws Exception{
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTime1));
		Thread.sleep(1000);
		WebActionUtil.actionClick(getgoinglink(driver, truncatedBookinTime1), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(1000);
	}
	public void EnTODAttendenceFortomorrow(WebDriver driver) throws Exception{
		//Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver, truncatedBookinTimeTomorrow));
		//WebActionUtil.actionClick(goingg, driver, "Going");
		Thread.sleep(100);
		WebActionUtil.actionClick(getgoinglink(driver, truncatedBookinTimeTomorrow), driver, "click on going");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(1000);
	}
	public void verify_EnTODAttendenceFortomorrow(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		tomorrowTab(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getAttendenceYesterday(driver,  bookintimeTomorrow));
	    WebActionUtil.waitForElement(getAttendenceYesterday(driver, bookintimeTomorrow), driver, "getAttendenceTomorrow", 1000);
		String attendece = getAttendenceYesterday(driver, bookintimeTomorrow).getText();
		WebActionUtil.verifyContainsText(attendece, "(P) Going", "going text should be present");
	}
	
	
	
	public void verify_EnTODAttendenceForYesterday(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.waitForElement(yesterdayTab, driver, "yesterday", 1000);
		yesterdayTab(driver);
		WebActionUtil.waitForElement(yesterdayTab, driver, "yesterday", 2000);
		WebActionUtil.scrollIntoView(driver, getAttendenceYesterday(driver, bookintimeYesterday));
		WebActionUtil.waitForElement(getAttendenceYesterday(driver, bookintimeYesterday), driver, "gettingAttendenceyesterday", 1000);
		String attendece = getAttendenceYesterday(driver,bookintimeYesterday).getText();
		WebActionUtil.verifyContainsText(attendece, "(P) Going", "going text should be present");
		
	}
	public WebElement getCustomerRelationCheckBox(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time + "\'][1]/../following-sibling::div/descendant::div[2]/span[2]/../../../../../descendant::div[3]"));
	}
	public WebElement getYesterdayBookingInToday(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time + "\']/../following-sibling::div/descendant::div[2]/span[text()='Not Yet Arranged']"));
	}
	@FindBy(xpath="//div[@class='ent-od-option-button']")
	private WebElement repeatBookingForTodayLink;
	@FindBy(xpath="//span[text()='TODAY']")
	private WebElement todayLink;
	/*@FindBy(xpath="//div[text()='07:55 PM']/../preceding-sibling::div/descendant::div[3]/descendant::div[5]")
	private WebElement basePrice;
	*/
	public WebElement getBasePriceYesterdayWithTime(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" + time + "\']/../preceding-sibling::div/descendant::div[3]/descendant::div"));
	}
	public void verify_YesterdayRepeatBookingForToday(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		yesterdayTab(driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeYesterday));
		String bpYesterday = getBasePriceYesterdayWithTime(driver, bookintimeYesterday).getText();
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(7000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoView(driver, getYesterdayBookingInToday(driver, bookintimeYesterday));
		Thread.sleep(9000);
		String bpToday = getBasePriceYesterdayWithTime(driver, bookintimeYesterday).getText();
		WebActionUtil.verifyEqualsText("Base Price should be equal", bpYesterday,  bpToday);
	}
	public void verify_TomorrowRepeatBookingForToday(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		tomorrowTab(driver);
		Thread.sleep(500);
	    WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeTomorrow));
		String bpTomorrow = getBasePriceYesterdayWithTime(driver, bookintimeTomorrow).getText();
		System.out.println(bpTomorrow );
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(7000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoView(driver, getYesterdayBookingInToday(driver, bookintimeTomorrow));
		Thread.sleep(9000);
		String bpToday = getBasePriceYesterdayWithTime(driver, bookintimeTomorrow).getText();
		System.out.println( bpToday);
		WebActionUtil.verifyEqualsText("Base Price should be equal", bpTomorrow,  bpToday);
		Thread.sleep(3000);
	}
	public void EntodAttendenceForToday(WebDriver driver) throws Exception{
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, getAttendenceYesterdayOPM(driver,truncatedBookinTimeToday ));
		Thread.sleep(500);
		WebActionUtil.clickElement(getgoinglink(driver,truncatedBookinTimeToday ), driver, "click on going");
		Thread.sleep(3000);
		
		
	}
	public void verify_EntodAttendenceForToday(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 3000, driver);
		
		WebActionUtil.scrollIntoView(driver, getAttendenceYesterday(driver,  bookintimeToday  ));
		String attendece = getAttendenceYesterday(driver, bookintimeToday  ).getText();
		WebActionUtil.verifyContainsText(attendece, "(P) Going", "going text should be present");
	}
	public void EnTodAddVechileDetailsYesterday(WebDriver driver,String vehnum,String AdhocVechileRate) throws Exception{
		WebActionUtil.waitForElement(yesterdayTabOpm, driver, "yesterdayTabOpm", 500);
		yesterdayTabOPM(driver);
		//WebActionUtil.waitForElement(yesterdayTabOpm, driver, "yesterdayTabOpm", 3000);
		Thread.sleep(1000);
		/*String bookintime = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
		String truncatedBookinTime =  bookintime .substring(1);*/
		WebActionUtil.scrollIntoViewAndClick(driver, getvechileDetail(driver, truncatedBookinTimeYesterday));
		Thread.sleep(1000);
		addBackupTextBox(driver, vehnum);
		WebActionUtil.waitForElement(nextButton, driver, "nextButton", 500);
		nextButton(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
		selectVendor(driver);
		WebActionUtil.waitForElement(adhocVechileRate, driver, "adhocVechileRate", 500);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
	    WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		Thread.sleep(500);
		
	}
	public void EnTODAddnotVerifiedVechileDetails(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		WebActionUtil.scrollIntoViewAndClick(driver, getNotVerifiedVechileDetail(driver, truncatedBookinTimeYesterdayNotVerifiedVeh));
		Thread.sleep(9000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
        WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
        nextButton(driver);
		Thread.sleep(1000);
		vendorName(driver,vendorname);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(500);
		}
	public void EnTODAddnotVerifiedVechileDetailsForTomorrow(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		WebActionUtil.scrollIntoViewAndClick(driver, getNotVerifiedVechileDetail(driver, truncatedBookinTimeTomorrowNotVerifiedVeh ));
		Thread.sleep(3000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
        WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
        nextButton(driver);
		Thread.sleep(1000);
		vendorName(driver,vendorname);
	    vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(500);
		}
	
	
	
	
	
	public void EnTODAddnotVerifiedVechileDetailsForToday(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		WebActionUtil.scrollIntoViewAndClick(driver, getNotVerifiedVechileDetail(driver, truncatedBookinTimeTodayNotVerifiedVeh));
		Thread.sleep(3000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
        WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
     
		nextButton(driver);
		Thread.sleep(1000);
		vendorName(driver,vendorname);
		//Thread.sleep(1000);
		vendorMobileNo(driver);
		//Thread.sleep(1000);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(500);
		}
	
	
	
	public void EnTODAddnotVerifiedVechileDetails1(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		Thread.sleep(2000);
	
		WebActionUtil.scrollIntoViewAndClick(driver, getNotVerifiedVechileDetail(driver, truncatedBookinTime1));
		Thread.sleep(1000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
        WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
        nextButton(driver);
		Thread.sleep(1000);
		vendorName(driver,vendorname);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(1000);
		}
	
	public WebElement getAdhocOwnerName(WebDriver driver, String ownerNmae) {
		return driver.findElement(By.xpath("//div[@class='inline-card-div vehicle-info-div adhoc-info']/div[2]/span[text()=\'" +ownerNmae + "\']"));
	}
	public WebElement getAdhocOwnerNmaeInCustomerRelation(WebDriver driver, String ownerNmae,String vecRate,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +vecRate + "\']/../../following-sibling::div/descendant::div[text()=\'" +time + "\']/../following-sibling::div[2]/div[text()=\'" +ownerNmae + "\']"));
	}
	public WebElement getAdhocVechileNumber(WebDriver driver, String ownerNmae) {
		return driver.findElement(By.xpath("//div[@class='inline-card-div vehicle-info-div adhoc-info']/div[2]/span[text()=\'" +ownerNmae + "\']/../following-sibling::div[1]"));
	}
	public WebElement getAdhocPhoneNumber(WebDriver driver, String ownerNmae) {
		return driver.findElement(By.xpath("//div[@class='inline-card-div vehicle-info-div adhoc-info']/div[2]/span[text()=\'" +ownerNmae + "\']/../following-sibling::div[1]/following-sibling::div"));
	}
	public WebElement getOwnerNameOpm(WebDriver driver,String OwnerName){
		return driver.findElement(By.xpath("//div[@class='inline-card-div vehicle-info-div adhoc-info']/descendant::div/span[text()=\'" +OwnerName + "\']"));
	}
	public WebElement getOwnerNameCustomerRelations(WebDriver driver,String VendorName){
		return driver.findElement(By.xpath("//div[@class='od-booking-list-item-info driver-detail-info-div']/descendant::div[text()=\'" +VendorName + "\']"));
	}
	public WebElement getOwnerNameForNonVerifiedVeh(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div/h2[text()='BKL']/../following-sibling::div/div/span[text()=\'" +time + "\']/../../following-sibling::div[2]/descendant::div/a/div[text()='Add Vehicle Details']/../../../following-sibling::div/div[2]"));
	}
	
	////div/h2[text()='BKL']/../following-sibling::div/div/span[text()='3:25 PM']/../../following-sibling::div[3]/div[2]
	public void verify_EnTODAddnotVerifiedVechileDetailsYesterdayBooking(WebDriver driver,String vendorname) throws Exception{
		InitializePages wdInit=new InitializePages(driver);
		
		WebActionUtil.scrollIntoView(driver,  getOwnerNameOpm(driver, vendorname));
		Thread.sleep(1000);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(500);
	    yesterdayTab(driver);
	    Thread.sleep(500);
	    WebActionUtil.scrollIntoView(driver, getOwnerNameCustomerRelations(driver, vendorname));
	    if(getOwnerNameCustomerRelations(driver, vendorname).isDisplayed()){
	    	WebActionUtil.verifyElementIsDisplayed(getOwnerNameCustomerRelations(driver, vendorname), driver, "Vendor Name");
	    }
		
	}
	//div[text()='POPO']/../../div[2]/descendant::div[1][text()='08:10 PM']/../../../div/descendant::div[@class='checkbox-wrapper-div']
	public WebElement getVendorNameforNonVerifiedVechilecheckBox(WebDriver driver,String nameofVendor,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +nameofVendor + "\']/../../div[2]/descendant::div[1][text()=\'" +time + "\']/../../../div/descendant::div[@class='checkbox-wrapper-div']"));
	}
	public WebElement getVechileAdhocRate(WebDriver driver,String nameofVendor){
		return driver.findElement(By.xpath("//div[text()=\'" +nameofVendor + "\']/../../div[1]/descendant::div[6]/following-sibling::div[@class='revenue-info']"));
	}
	public WebElement getVechileAdhocRateTodayBooking(WebDriver driver,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +time + "\']/../../div[1]/descendant::div[3]/child::div[3]"));
	}
	public WebElement getVendorNameOfYesterdayBookingInTodayBookingn(WebDriver driver,String vecAdhocRate){
		return driver.findElement(By.xpath("/div[@class='od-booking-list-item-info client-info-div']/div[3]/div[3][text()=\'" +vecAdhocRate + "\']/../../following-sibling::div[3]/descendant::div[1]"));
	}
	public WebElement getVendorNameForCustomerRelation(WebDriver driver,String vendorName){
		return driver.findElement(By.xpath("//div[@class='od-booking-list-item-info driver-detail-info-div']/descendant::div[text()=\'" +vendorName + "\']"));
	}
	public WebElement getAdhocRateForCustomerRelation(WebDriver driver,String adhocVechileRate,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +time + "\']/../preceding-sibling::div/descendant::div[3]/child::div[3][text()=\'" +adhocVechileRate + "\']"));
	}
	public WebElement getAdhocRateforscrollingInTodayBooking(WebDriver driver,String VendorRate,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +VendorRate + "\']/../../following-sibling::div/div[text()=\'" +time + "\']"));
	}
	
	//div[text()='qaz']/../../../descendant::div/div[@class='checkbox-wrapper-div']
	public WebElement getSelectChekboxOfSpecifiedBooking(WebDriver driver,String vendorName){
		return driver.findElement(By.xpath("//div[text()=\'" +vendorName + "\']/../../../descendant::div/div[@class='checkbox-wrapper-div']"));
	}
	public WebElement getvendorNameInYesterdayBooking(WebDriver driver,String VendorRate,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +VendorRate + "\']/../../following-sibling::div/div[text()=\'" +time + "\']/../following-sibling::div[2]/div"));
	}
	//div[@class='od-booking-list-item-info driver-detail-info-div']/descendant::div[text()='lonee']/../../descendant::div/descendant::div[3]/div[2]
	public WebElement getBasePriceYesterday(WebDriver driver,String vendorNameYesterday){
		return driver.findElement(By.xpath("//div[@class='od-booking-list-item-info driver-detail-info-div']/descendant::div[text()=\'" +vendorNameYesterday + "\']/../../descendant::div/descendant::div[3]/div[3]"));
	}
	public WebElement getBasePriceToday(WebDriver driver,String time,String bp){
		return driver.findElement(By.xpath("//div[text()=\'" +time + "\']/../preceding-sibling::div/div[3]/descendant::div/following-sibling::div[text()=\'" +bp + "\']"));
	}
	//div[text()='116']/../../following-sibling::div/descendant::div[text()='01:45 PM']/../following-sibling::div[2]/div[1]
	public WebElement getVendorNameInTodayRepeatBooking(WebDriver driver,String bp,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +bp + "\']/../../following-sibling::div/descendant::div[text()=\'" +time + "\']/../following-sibling::div[2]/div[1]"));
	}
	public void verify_notverifiedVechileRepeatBookingForTodayfromYesterdayBooking(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		yesterdayTab(driver);
		Thread.sleep(2000);
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeYesterdayNotVerifiedVeh ));
		Thread.sleep(2000);
		String bpYesterday = getBasePriceYesterdayWithTime(driver, bookintimeYesterdayNotVerifiedVeh).getText();
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(7000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoView(driver, getYesterdayBookingInToday(driver, bookintimeYesterdayNotVerifiedVeh));
		Thread.sleep(9000);
		String bpToday = getBasePriceYesterdayWithTime(driver, bookintimeYesterdayNotVerifiedVeh).getText();
		WebActionUtil.verifyEqualsText("Base Price should be equal", bpYesterday,  bpToday);
	}
	
	
	public void verify_notverifiedVechileRepeatBookingForTodayfromTomorrowBooking(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		tomorrowTab(driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeTomorrowNotVerifiedVeh  ));
		String bpYesterday = getBasePriceYesterdayWithTime(driver, bookintimeTomorrowNotVerifiedVeh ).getText();
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(7000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoView(driver, getYesterdayBookingInToday(driver, bookintimeTomorrowNotVerifiedVeh ));
		Thread.sleep(9000);
		String bpToday = getBasePriceYesterdayWithTime(driver,bookintimeTomorrowNotVerifiedVeh ).getText();
		WebActionUtil.verifyEqualsText("Base Price should be equal", bpYesterday,  bpToday);
	}
	
	
	
	@FindBy(xpath="//div[@class='od-booking-list-item-info driver-detail-info-div']/div[text()='yGLh']/following-sibling::div[2]")
    private WebElement phoneNoYesterdayBooking;
	public WebElement getVendorNameInTodayForYesterday(WebDriver driver,String bp,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +bp + "\']/../../following-sibling::div/div[text()=\'" +time + "\']/../following-sibling::div[1]/descendant::div[2][@class='driver-attendance-label']/span[2][text()='(P) Going']/../../../following-sibling::div/div[1]"));
	}
	public WebElement getphoneNoYesterdayBooking(WebDriver driver,String vendorname){
		return driver.findElement(By.xpath("//div[@class='od-booking-list-item-info driver-detail-info-div']/div[text()=\'" +vendorname + "\']/following-sibling::div[2]"));
	}
	public WebElement getvecNoYrsterdayBooking(WebDriver driver,String vendorname){
		return driver.findElement(By.xpath("//div[@class='od-booking-list-item-info driver-detail-info-div']/div[text()=\'" +vendorname + "\']/following-sibling::div[1]"));
	}
	public WebElement getVendorVecNoInTodayForYesterday(WebDriver driver,String bp,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +bp + "\']/../../following-sibling::div/div[text()=\'" +time + "\']/../following-sibling::div[1]/descendant::div[2][@class='driver-attendance-label']/span[2][text()='(P) Going']/../../../following-sibling::div/div[1]/following-sibling::div[1]"));
	}
	public WebElement getVendorPhoneNoInTodayForYesterday(WebDriver driver,String bp,String time){
		return driver.findElement(By.xpath("//div[text()=\'" +bp + "\']/../../following-sibling::div/div[text()=\'" +time + "\']/../following-sibling::div[1]/descendant::div[2][@class='driver-attendance-label']/span[2][text()='(P) Going']/../../../following-sibling::div/div[1]/following-sibling::div[2]"));
	}
	public void _verifyYesterdayBookingWithDriverDetailsInTodayBooking(WebDriver driver,String vendorname) throws Exception{
		 Thread.sleep(4000);
		 WebActionUtil.scrollIntoView(driver, getSelectChekboxOfSpecifiedBooking(driver, vendorname));
		 Thread.sleep(8000);
		 String bpyesterdsay = getBasePriceYesterday(driver, vendorname).getText();
		 String[] bpPrice = bpyesterdsay.split(" ");
		 String price = bpPrice[0];
		 System.out.println(price);
		 System.out.println(bpyesterdsay);
		 String vecNo = getvecNoYrsterdayBooking(driver, vendorname).getText();
		 String phnNo = getphoneNoYesterdayBooking(driver, vendorname).getText();
		 System.out.println(vecNo);
		 System.out.println( phnNo);
	     WebActionUtil.scrollIntoViewAndClick(driver,getSelectChekboxOfSpecifiedBooking(driver, vendorname));
	     Thread.sleep(4000);
	     WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
	     Thread.sleep(8000);
		 WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		 WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		 driver.navigate().refresh();
		 WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		 Thread.sleep(4000);
		 WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		 Thread.sleep(4000);
		 WebActionUtil.scrollIntoView(driver, getBasePriceToday(driver,bookintime , price));
		 Thread.sleep(3000);
		String VendorNameInTodayBooking = getVendorNameInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorNameInTodayBooking);
		String VendorVecNoInTodayBooking=getVendorVecNoInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorVecNoInTodayBooking);
		String VendorPhnNoInTodayBooking=getVendorPhoneNoInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorPhnNoInTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorNameInTodayBooking, vendorname);
		WebActionUtil.verifyContainsEqualText(VendorVecNoInTodayBooking, vecNo);
		WebActionUtil.verifyContainsEqualText(VendorPhnNoInTodayBooking, phnNo);
	}
	public void _verifyYesterdayBookingWithDriverDetailsInTodayBooking1(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, yesterdayTab);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, getallDriverDetail(driver, bookintimeYesterdayNotVerifiedVeh, 1));
		Thread.sleep(1000);
		String vecNo=getallDriverDetail(driver, bookintimeYesterdayNotVerifiedVeh, 2).getText();
		String vendorPhoneNO = getallDriverDetail(driver, bookintimeYesterdayNotVerifiedVeh, 3).getText();
		String VendorName=getallDriverDetail(driver,  bookintimeYesterdayNotVerifiedVeh, 1).getText();
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeYesterdayNotVerifiedVeh));
	    Thread.sleep(2000);
		WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(2000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 30000, driver);
		Thread.sleep(8000);
		WebActionUtil.scrollIntoView(driver,getallDriverDetail(driver, bookintimeYesterdayNotVerifiedVeh, 2));
		Thread.sleep(2000);
		String vendorPhoneNoTodayBooking = getallDriverDetail(driver,bookintimeYesterdayNotVerifiedVeh, 3).getText();
		String VendorNameTodayBooking=getallDriverDetail(driver,bookintimeYesterdayNotVerifiedVeh, 1).getText();
		WebActionUtil.verifyContainsEqualText(vendorPhoneNO , vendorPhoneNoTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorName , VendorNameTodayBooking);
	}
	public void _verifyTomorrowBookingWithDriverDetailsInTodayBookingForNonVerifiedVeh(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, tomorrowTab);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, getallDriverDetail(driver, bookintimeTomorrowNotVerifiedVeh, 1));
		Thread.sleep(1000);
		String vecNo=getallDriverDetail(driver, bookintimeTomorrowNotVerifiedVeh, 2).getText();
		String vendorPhoneNO = getallDriverDetail(driver, bookintimeTomorrowNotVerifiedVeh, 3).getText();
		String VendorName=getallDriverDetail(driver,  bookintimeTomorrowNotVerifiedVeh, 1).getText();
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeTomorrowNotVerifiedVeh));
	    Thread.sleep(1000);
		WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(2000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 30000, driver);
		Thread.sleep(8000);
		WebActionUtil.scrollIntoView(driver,getallDriverDetail(driver, bookintimeTomorrowNotVerifiedVeh, 2));
		Thread.sleep(2000);
		String vendorPhoneNoTodayBooking = getallDriverDetail(driver,bookintimeTomorrowNotVerifiedVeh, 3).getText();
		String VendorNameTodayBooking=getallDriverDetail(driver,bookintimeTomorrowNotVerifiedVeh, 1).getText();
		WebActionUtil.verifyContainsEqualText(vendorPhoneNO , vendorPhoneNoTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorName , VendorNameTodayBooking);
	}
	
	public WebElement getVendorName1(WebDriver driver,String venName){
		return driver.findElement(By.xpath("//div[text()=\'" +venName + "\']"));
	}
	public void _verifyTomorrowBookingWithDriverDetailsInTodayBooking(WebDriver driver,String vendorname) throws Exception{
		 Thread.sleep(500);
		 WebActionUtil.scrollIntoView(driver, getSelectChekboxOfSpecifiedBooking(driver, vendorname));
		 System.out.println(3000);
		 String bpyesterdsay = getBasePriceYesterday(driver, vendorname).getText();
		 String[] bpPrice = bpyesterdsay.split(" ");
		 String price = bpPrice[0];
		 System.out.println(price);
		 System.out.println(bpyesterdsay);
		 String vecNo = getvecNoYrsterdayBooking(driver, vendorname).getText();
		 String phnNo = getphoneNoYesterdayBooking(driver, vendorname).getText();
		 System.out.println(vecNo);
		 System.out.println( phnNo);
	     WebActionUtil.scrollIntoViewAndClick(driver,getSelectChekboxOfSpecifiedBooking(driver, vendorname));
	     Thread.sleep(2000);
	     WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
	     Thread.sleep(2000);
		 WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		 WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		 driver.navigate().refresh();
		 WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		 Thread.sleep(5000);
		 WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		 Thread.sleep(3000);
		 WebActionUtil.scrollIntoView(driver, getBasePriceToday(driver,bookintime , price));
		 Thread.sleep(2000);
		String VendorNameInTodayBooking = getVendorNameInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorNameInTodayBooking);
		String VendorVecNoInTodayBooking=getVendorVecNoInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorVecNoInTodayBooking);
		String VendorPhnNoInTodayBooking=getVendorPhoneNoInTodayForYesterday(driver, price, bookintime).getText();
		System.out.println(VendorPhnNoInTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorNameInTodayBooking, vendorname);
		WebActionUtil.verifyContainsEqualText(VendorVecNoInTodayBooking, vecNo);
		WebActionUtil.verifyContainsEqualText(VendorPhnNoInTodayBooking, phnNo);
	
		
		
	}
	//span[text()='yGLh']/../../preceding-sibling::div[1]/div/a/div[text()='Add Vehicle Details']
	public WebElement getAddVechileDetails(WebDriver driver,String vendorName){
		return driver.findElement(By.xpath("//span[text()=\'" +vendorName + "\']/../../preceding-sibling::div[1]/div/a/div[text()='Add Vehicle Details']"));
	}
	public void verify_toModifyDriverDetailsForYesterdayBooking(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		InitializePages wdInit=new InitializePages(driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getAddVecDetail(driver, truncatedBookinTimeYesterdayNotVerifiedVeh));
		String ownerName = getOwnerNameForNonVerifiedVeh(driver, truncatedBookinTimeYesterdayNotVerifiedVeh).getText();
		Thread.sleep(3000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
		WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
		nextButton(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		WebActionUtil.clearAndType(vendorName, vendorname, "vendor name", driver);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
		Thread.sleep(1000);
		yesterdayTab(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getOwnerNameCustomerRelations(driver, vendorname));
		Thread.sleep(3000);
		WebActionUtil.verifyNotEqualsText("vendor name", ownerName, vendorname);
		
		
	}
	
	
	
public void verify_toModifyDriverDetailsFor_TomorrowBooking(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		InitializePages wdInit=new InitializePages(driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getAddVecDetail(driver, truncatedBookinTimeTomorrowNotVerifiedVeh));
		String ownerName = getOwnerNameForNonVerifiedVeh(driver, truncatedBookinTimeTomorrowNotVerifiedVeh).getText();
		Thread.sleep(3000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
		WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
		nextButton(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		WebActionUtil.clearAndType(vendorName, vendorname, "vendor name", driver);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
		Thread.sleep(1000);
		tomorrowTab(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getOwnerNameCustomerRelations(driver, vendorname));
		Thread.sleep(3000);
		WebActionUtil.verifyNotEqualsText("vendor name", ownerName, vendorname);
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	public void verify_toModifyDriverDetailsForTodayBooking(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		
		InitializePages wdInit=new InitializePages(driver);
		WebActionUtil.scrollIntoViewAndClick(driver, getAddVecDetail(driver, truncatedBookinTimeTodayNotVerifiedVeh ));
		String ownerName = getOwnerNameForNonVerifiedVeh(driver, truncatedBookinTimeTodayNotVerifiedVeh ).getText();
		Thread.sleep(3000);
		WebActionUtil.waitForElement(addBackUpTextBox, driver, "vec no text field", 1000);
		WebActionUtil.clickElement(addBackUpTextBox, driver, "click on select vendor Text field");
		Thread.sleep(500);
		WebActionUtil.clearAndType(addBackUpTextBox, vehnum, "Vechile Number", driver);
		nextButton(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		WebActionUtil.clearAndType(vendorName, vendorname, "vendor name", driver);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
		Thread.sleep(1000);
		/*yesterdayTab(driver);
		Thread.sleep(1000);*/
		WebActionUtil.scrollIntoView(driver, getOwnerNameCustomerRelations(driver, vendorname));
		Thread.sleep(3000);
		WebActionUtil.verifyNotEqualsText("vendor name", ownerName, vendorname);
		
		
		
	}
	public void verify_toModifyDriverDetailsForTomorrowBooking(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		WebActionUtil.scrollIntoViewAndClick(driver, tomorrowTab);
		InitializePages wdInit=new InitializePages(driver);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_WebCustomerRelationsPage.customerRelationsLink1(driver);
		wdInit.o_WebCustomerRelationsPage.opm(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		wdInit.o_HomePage.clickOnAllList(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(500);
		wdInit.o_HomePage.selectArea1(driver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(500);
		tomorrowTabOPM(driver);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver,  getOwnerNameOpm(driver, vendorname));
		Thread.sleep(1000);
		WebActionUtil.clickElement(getAddVechileDetails(driver, vendorname), driver, "click on add vechile detail");
		String vehnum1=MobileActionUtil.generateRandomString(2,4);
		WebActionUtil.clearAndType(addBackUpTextBox, vehnum1, "Vechile Number", driver);
		nextButton(driver);
		String vendornameModify = WebActionUtil.generateRandomString(4);
	    //vendorName(driver,vendorname);
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		//String nameOfVendor = WebActionUtil.generateRandomString(4);
		WebActionUtil.clearAndType(vendorName, vendornameModify, "vendor name", driver);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(500);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		tomorrowTab(driver);
		//WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		/*Thread.sleep(6000);
		tomorrowTab(driver);*/
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getVendorName1(driver, vendornameModify));
		WebActionUtil.verifyNotEqualsText("vendor name", vendornameModify, vendorname);
		WebActionUtil.verifyNotEqualsText("vechile num", vehnum1, vehnum);
	}
	public void verify_notverifiedVechileRepeatBookingForTodayfromTommorrowBooking(WebDriver driver,String vendorname) throws Exception{
		
        Thread.sleep(2000);
	    WebActionUtil.scrollIntoView(driver, getSelectChekboxOfSpecifiedBooking(driver, vendorname));
	    Thread.sleep(3000);
	    String bpyesterdsay = getBasePriceYesterday(driver, vendorname).getText();
	    String[] bpPrice = bpyesterdsay.split(" ");
	    String price = bpPrice[0];
	    System.out.println(price);
	    System.out.println(bpyesterdsay);
		WebActionUtil.scrollIntoViewAndClick(driver,getSelectChekboxOfSpecifiedBooking(driver, vendorname));
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getBasePriceToday(driver,bookintime , price));
		Thread.sleep(3000);
		String nameOfVendor = getVendorNameInTodayRepeatBooking(driver, price, bookintime).getText();
	    WebActionUtil.verifyNotEqualsText("NameOfVendorForTodayAndYesterday", nameOfVendor, vendorname);
		
	}
	public void verify_EnTODAddnotVerifiedVechileDetailsTomorrowBooking(WebDriver driver,String vendorname) throws Exception{
		InitializePages wdInit=new InitializePages(driver);
		WebActionUtil.scrollIntoView(driver, getownerName(driver, truncatedBookinTimeTomorrowNotVerifiedVeh  ));
		Thread.sleep(1000);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		Thread.sleep(1000);
		tomorrowTab(driver);
		Thread.sleep(300);
	    WebActionUtil.scrollIntoView(driver, getOwnerNameCustomerRelations(driver, vendorname));
	    Thread.sleep(2000);
	    if(getOwnerNameCustomerRelations(driver, vendorname).isDisplayed()){
	    	
	    	WebActionUtil.verifyElementIsDisplayed(getOwnerNameCustomerRelations(driver, vendorname), driver, "Vendor Name");
	    }
	}
	
	public void verify_EnTODAddnotVerifiedVechileDetailsTodayBooking(WebDriver driver) throws Exception{
		InitializePages wdInit = new InitializePages(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getownerName(driver, truncatedBookinTimeTodayNotVerifiedVeh ));
		Thread.sleep(3000);
		String ownername = getownerName(driver, truncatedBookinTimeTodayNotVerifiedVeh ).getText();
		System.out.println("adhoc owner name :"+ownername);
		String adhocOwnerVecNum = getAdhocOwnerNum(driver, truncatedBookinTimeTodayNotVerifiedVeh ).getText();
		System.out.println("adhoc owner VecNo :"+adhocOwnerVecNum);
		String adhocPhoneNum = getAdhocPhoneNum(driver, truncatedBookinTimeTodayNotVerifiedVeh ).getText();
		System.out.println("adhoc owner Phonenum :"+adhocPhoneNum);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getVendorNameCustomerRelation(driver, bookintimeTodayNotVerifiedVeh));
		Thread.sleep(3000);
		String vendorName = getVendorNameCustomerRelation(driver,bookintimeTodayNotVerifiedVeh).getText();
		System.out.println(vendorName );
		String vendorVecNo = getVendorVecNoCustomerRelation(driver,bookintimeTodayNotVerifiedVeh).getText();
		System.out.println(vendorVecNo);
		String vendorPhnNo = getVendorPhoneCustomerRelation(driver, bookintimeTodayNotVerifiedVeh).getText();
		System.out.println(vendorPhnNo );
		//WebActionUtil.verifyContainsText(vendorName, ownername);
		WebActionUtil.verifyContainsEqualText(vendorName,ownername);
		WebActionUtil.verifyContainsText(vendorVecNo, adhocOwnerVecNum );
		WebActionUtil.verifyContainsText(vendorPhnNo, adhocPhoneNum);
	}
	public void toEditDriverDetailsInTodayBooking(WebDriver driver,String vendorname,String vehnum,String AdhocVechileRate) throws Exception{
		Thread.sleep(2000);
		WebActionUtil.scrollIntoView(driver,  getOwnerNameOpm(driver, vendorname));
		Thread.sleep(1000);
		WebActionUtil.clickElement(getAddVechileDetails(driver, vendorname), driver, "click on add vechile detail");
		Thread.sleep(2000);
		String vehnum1=MobileActionUtil.generateRandomString(2,4);
		WebActionUtil.clearAndType(addBackUpTextBox, vehnum1, "Vechile Number", driver);
		Thread.sleep(2000);
		nextButton(driver);
		String vendornameModify = WebActionUtil.generateRandomString(4);
	    //vendorName(driver,vendorname);
		WebActionUtil.clickElement(vendorName, driver, "Vendor Name");
		//String nameOfVendor = WebActionUtil.generateRandomString(4);
		WebActionUtil.clearAndType(vendorName, vendornameModify, "vendor name", driver);
		vendorMobileNo(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		InitializePages wdInit = new InitializePages(driver);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getVendorName1(driver, vendornameModify));
		Thread.sleep(3000);
		WebActionUtil.verifyNotEqualsText("vendor name", vendornameModify, vendorname);
		WebActionUtil.verifyNotEqualsText("vechile num", vehnum1, vehnum);
	}
	
	public void EnTodAddVechileToday(WebDriver driver,String vehnum,String AdhocVechileRate) throws Exception{
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, getvechileDetail(driver, truncatedBookinTimeToday ));
		Thread.sleep(1000);
		addBackupTextBox(driver, vehnum);
		Thread.sleep(500);
		nextButton(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(1000);
		selectVendor(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(2000);
		
	}
	public void EnTodAddVechileTommorrow(WebDriver driver,String vehnum,String AdhocVechileRate) throws Exception{
		Thread.sleep(2000);
		tomorrowTabOPM(driver);
		Thread.sleep(500);
		WebActionUtil.scrollIntoViewAndClick(driver, getvechileDetail(driver, truncatedBookinTimeTomorrow ));
		Thread.sleep(1000);
		addBackupTextBox(driver, vehnum);
		Thread.sleep(300);
		nextButton(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(1000);
		selectVendor(driver);
		adhocVechileRate(driver,AdhocVechileRate);
		done(driver);
	    WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
	    Thread.sleep(2000);
	}
	public void verify_EnTodAddVechileTommorrow(WebDriver driver) throws Exception{
		InitializePages wdInit = new InitializePages(driver);
		
		WebActionUtil.scrollIntoView(driver, getownerName(driver, truncatedBookinTimeTomorrow ));
		Thread.sleep(2000);
		String ownername = getownerName(driver, truncatedBookinTimeTomorrow ).getText();
		System.out.println(ownername);
		String adhocOwnerVecNum = getAdhocOwnerNum(driver, truncatedBookinTimeTomorrow ).getText();
		System.out.println(adhocOwnerVecNum);
		String adhocPhoneNum = getAdhocPhoneNum(driver, truncatedBookinTimeTomorrow ).getText();
		System.out.println(adhocPhoneNum);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		Thread.sleep(1000);
		tomorrowTab(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getVendorNameCustomerRelation(driver, bookintimeTomorrow));
		Thread.sleep(3000);
		String vendorName = getVendorNameCustomerRelation(driver, bookintimeTomorrow).getText();
		System.out.println(vendorName );
		String vendorVecNo = getVendorVecNoCustomerRelation(driver,bookintimeTomorrow).getText();
		System.out.println(vendorVecNo);
		String vendorPhnNo = getVendorPhoneCustomerRelation(driver, bookintimeTomorrow).getText();
		System.out.println(vendorPhnNo );
		WebActionUtil.verifyContainsEqualText(vendorName, ownername);
		WebActionUtil.verifyContainsEqualText(vendorVecNo, adhocOwnerVecNum );
		WebActionUtil.verifyContainsEqualText(vendorPhnNo, adhocPhoneNum);
	}
	public void verify_EnTodAddVechileToday(WebDriver driver) throws Exception{
		InitializePages wdInit = new InitializePages(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getownerName(driver, truncatedBookinTimeToday ));
		Thread.sleep(2000);
		String ownername = getownerName(driver, truncatedBookinTimeToday ).getText();
		System.out.println(ownername);
		String adhocOwnerVecNum = getAdhocOwnerNum(driver, truncatedBookinTimeToday ).getText();
		System.out.println(adhocOwnerVecNum);
		String adhocPhoneNum = getAdhocPhoneNum(driver, truncatedBookinTimeToday ).getText();
		System.out.println(adhocPhoneNum);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getVendorNameCustomerRelation(driver, bookintimeToday));
		Thread.sleep(3000);
		String vendorName = getVendorNameCustomerRelation(driver,bookintimeToday).getText();
		System.out.println(vendorName );
		String vendorVecNo = getVendorVecNoCustomerRelation(driver, bookintimeToday).getText();
		System.out.println(vendorVecNo);
		String vendorPhnNo = getVendorPhoneCustomerRelation(driver, bookintimeToday).getText();
		System.out.println(vendorPhnNo );
		WebActionUtil.verifyContainsEqualText(vendorName, ownername);
		WebActionUtil.verifyContainsEqualText(vendorVecNo, adhocOwnerVecNum );
		WebActionUtil.verifyContainsEqualText(vendorPhnNo, adhocPhoneNum);
	

	}
	public void verify_EnTodAddVechileDetailsYesterday(WebDriver driver) throws Exception{
		InitializePages wdInit = new InitializePages(driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoView(driver, getownerName(driver, truncatedBookinTimeYesterday));
		Thread.sleep(500);
		String ownername = getownerName(driver, truncatedBookinTimeYesterday).getText();
		System.out.println("adhoc owner :"+ ownername);
		String adhocOwnerVecNum = getAdhocOwnerNum(driver, truncatedBookinTimeYesterday).getText();
		System.out.println( "adhoc vecNo :"+adhocOwnerVecNum);
		String adhocPhoneNum = getAdhocPhoneNum(driver, truncatedBookinTimeYesterday).getText();
		System.out.println("adhoc phoneNum :"+adhocPhoneNum);
		wdInit.o_HomePage.leftNavigationalBar(driver);
		wdInit.o_HomePage.oPM(driver);
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.waitForElement(yesterdayTab, driver, "yesterdayTab",500);
		yesterdayTab(driver);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, getVendorNameCustomerRelation(driver, bookintimeYesterday));
		Thread.sleep(500);
		String vendorName = getVendorNameCustomerRelation(driver, bookintimeYesterday).getText();
		System.out.println("Vendor Name:"+vendorName );
		String vendorVecNo = getVendorVecNoCustomerRelation(driver, bookintimeYesterday).getText();
		System.out.println("Vendor VecNo:"+vendorVecNo);
		String vendorPhnNo = getVendorPhoneCustomerRelation(driver, bookintimeYesterday).getText();
		System.out.println("Vendor PhoneNo:"+vendorPhnNo );
		WebActionUtil.verifyContainsEqualText(vendorName, ownername);
		WebActionUtil.verifyContainsEqualText(vendorVecNo, adhocOwnerVecNum );
		WebActionUtil.verifyContainsEqualText(vendorPhnNo, adhocPhoneNum);
	}
	
	public WebElement getCheckBoxForVerifiedVechile(WebDriver driver, String vecNo,String time) {
		return driver.findElement(By.xpath("//div[text()=\'" +  vecNo
				+ "\']/../preceding-sibling::div/following-sibling::div/descendant::div[text()=\'" +  time
				+ "\']//../following-sibling::div/descendant::div[2]/span[2]/../../../../../descendant::div[3]"));
	}
	public static WebElement getDriverDetail(WebDriver driver,String time,String vechilenum){
		return driver.findElement(By.xpath("//div[text()=\'" + vechilenum
				+ "\']/../../child::div[2]/descendant::div[text()=\'" +  time
				+ "\']"));
	}
	public static WebElement getallDriverDetail(WebDriver driver,String time,int n){
		return driver.findElement(By.xpath("//div[text()=\'" +  time
				+ "\']/../following-sibling::div/descendant::div[2]/span[2][text()='(P) Going']/../../../following-sibling::div/div[\'" +  n
				+ "\']"));
	}
	
	public static WebElement getVendorNameDetail(WebDriver driver,String time,String VechileNo){
		return driver.findElement(By.xpath("//div[text()=\'" + time
				+ "\']/../following-sibling::div[2]/descendant::div[2][text()=\'" + VechileNo
				+ "\']/preceding-sibling::div"));
	}
	public static WebElement getVendorVecNoforDriverDetail(WebDriver driver,String time,String baseprice){
		return driver.findElement(By.xpath("//div[text()=\'" +  baseprice
				+ "\']/../../following-sibling::div/descendant::div[text()=\'" +  time
				+ "\']/../following-sibling::div[2]/div[@class='info-div number-plate-icon info-vehicle-number']"));
	}
	public static WebElement getVendorPhoneNoforDriverDetail(WebDriver driver,String time,String VechileNo){
		return driver.findElement(By.xpath("//div[text()=\'" + time
				+ "\']/../following-sibling::div[2]/descendant::div[2][text()=\'" + VechileNo
				+ "\']/following-sibling::div"));
	}
	
	@FindBy(xpath="//span[text()='Do you want to copy driver details?']/../../descendant::div[@class='checkbox-div ']")
	private WebElement copyDriverDetailsChkBox;
	@FindBy(xpath="//div[@class='checkbox-div selected']/../../following-sibling::div/descendant::div[9]")
	private WebElement basepriceforCheckboxSelected;
	public void  copyDriverDetailsForTodayRepeatBookingFromYesterdayBooking(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, yesterdayTab);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, getallDriverDetail(driver, bookintimeYesterday, 1));
		Thread.sleep(1000);
		String vecNo=getallDriverDetail(driver, bookintimeYesterday, 2).getText();
		String vendorPhoneNO = getallDriverDetail(driver, bookintimeYesterday, 3).getText();
		String VendorName=getallDriverDetail(driver,  bookintimeYesterday, 1).getText();
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeYesterday));
	    WebActionUtil.waitForElement(copyDriverDetailsChkBox, driver,"copyDriverDetailsChkBox",2000);
	    Thread.sleep(1000);
		WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(2000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 30000, driver);
		Thread.sleep(8000);
		WebActionUtil.scrollIntoView(driver, getDriverDetail(driver, bookintimeYesterday, vecNo));
		Thread.sleep(2000);
		String vendorPhoneNoTodayBooking = getallDriverDetail(driver, bookintimeYesterday, 3).getText();
		String VendorNameTodayBooking=getallDriverDetail(driver, bookintimeYesterday, 1).getText();
		WebActionUtil.verifyContainsEqualText(vendorPhoneNO , vendorPhoneNoTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorName , VendorNameTodayBooking);
		 }
	public WebElement getVerifiedVechileBasePrice(WebDriver driver,String vecRate){
		return driver.findElement(By.xpath("//div[text()=\'" +  vecRate
				+ "\']/../preceding-sibling::div[1]/div/div/span[2][text()='(P) Going']/../../../preceding-sibling::div/div[3]/div[3]"));
	}
	public WebElement getScrollToYesterdayBasePrice(WebDriver driver,String bp){
		return driver.findElement(By.xpath("//div[text()=\'" +  bp+ "\']"));
	}
	public WebElement getverifiedVechileDetails(WebDriver driver,String bp){
		return driver.findElement(By.xpath("//div[text()=\'" +  bp+ "\']/../../following-sibling::div[3]/div"));
	}
	public WebElement getverifiedVechileDetailsVechileNo(WebDriver driver,String bp){
		return driver.findElement(By.xpath("//div[text()=\'" +  bp+ "\']/../../following-sibling::div[3]/div/following-sibling::div"));
	}
	public WebElement getverifiedVechileDetailsPhoneNumber(WebDriver driver,String bp){
		return driver.findElement(By.xpath("//div[text()=\'" +  bp+ "\']/../../following-sibling::div[3]/div/following-sibling::div/following-sibling::div"));
	}
	public void repeatBookingOfYesterdayForToday(WebDriver driver,String verifiedvehNum) throws Exception{
		Thread.sleep(3000);
		WebActionUtil.scrollIntoView(driver, getDriverDetail(driver, bookintime, verifiedvehNum));
		Thread.sleep(2000);
		String bpYesterday = getVerifiedVechileBasePrice(driver, verifiedvehNum).getText();
		String[] bp = bpYesterday.split(" ");
		String basePrice = bp[0];
		String vendorPhoneNO = getVendorPhoneNoforDriverDetail(driver, bookintime, verifiedvehNum).getText();
		System.out.println(vendorPhoneNO);
		String VendorName=getVendorNameDetail(driver,  bookintime, verifiedvehNum).getText();
		System.out.println(VendorName);
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintime));
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		Thread.sleep(3000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(5000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(5000);
		WebActionUtil.scrollIntoView(driver, getScrollToYesterdayBasePrice(driver, basePrice));
		Thread.sleep(4000);
		String vendornameToday = getverifiedVechileDetails(driver, basePrice).getText();
		System.out.println(vendornameToday);
		String vechileNumber = getverifiedVechileDetailsVechileNo(driver, basePrice).getText();
		System.out.println(vechileNumber);
		String phoneNumber = getverifiedVechileDetailsPhoneNumber(driver, basePrice).getText();
		System.out.println(phoneNumber);
		//WebActionUtil.verifyContainsEqualText(verifiedvehNum, vechileNumber);
		WebActionUtil.verifyNotEqualsText("vechile no", verifiedvehNum,vechileNumber);
		WebActionUtil.verifyNotEqualsText("phone no", phoneNumber,vendorPhoneNO);
		WebActionUtil.verifyNotEqualsText("vendor name",vendornameToday,VendorName);
		
		
	}
	
	public void copyDriverDetailsForTodayRepeatBookingFromTomorrowBooking(WebDriver driver) throws Exception{
		customerRealtionLink(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, driver);
		Thread.sleep(1000);
		WebActionUtil.scrollIntoViewAndClick(driver, tomorrowTab);
		Thread.sleep(500);
		WebActionUtil.scrollIntoView(driver, getallDriverDetail(driver,  bookintimeTomorrow, 1));
		Thread.sleep(3000);
		String vendorPhoneNO = getallDriverDetail(driver,  bookintimeTomorrow, 3).getText();
		String VendorName=getallDriverDetail(driver,  bookintimeTomorrow, 1).getText();
		String vendorVehNo=getallDriverDetail(driver,  bookintimeTomorrow, 2).getText();
		WebActionUtil.scrollIntoViewAndClick(driver, getCustomerRelationCheckBox(driver, bookintimeTomorrow));
		Thread.sleep(2000);
		WebActionUtil.clickElement(copyDriverDetailsChkBox, driver, "click on driver details chechbOX");
		Thread.sleep(9000);
		WebActionUtil.scrollIntoViewAndClick(driver, repeatBookingForTodayLink);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		Thread.sleep(6000);
		WebActionUtil.scrollIntoViewAndClick(driver, todayLink);
		Thread.sleep(4000);
		driver.navigate().refresh();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 25000, driver);
		Thread.sleep(5000);
		WebActionUtil.scrollIntoView(driver, getallDriverDetail(driver,  bookintimeTomorrow, 3));
		Thread.sleep(3000);
		String vendorPhoneNoTodayBooking = getallDriverDetail(driver,  bookintimeTomorrow, 3).getText();
		String VendorNameTodayBooking=getallDriverDetail(driver,  bookintimeTomorrow, 1).getText();
		WebActionUtil.verifyContainsEqualText(vendorPhoneNO , vendorPhoneNoTodayBooking);
		WebActionUtil.verifyContainsEqualText(VendorName , VendorNameTodayBooking);
	}
}