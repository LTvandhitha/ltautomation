package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.paulhammant.ngwebdriver.ByAngular;

//import ch.qos.logback.core.joran.action.ActionUtil;
import io.appium.java_client.android.AndroidDriver;

public class WebDriverHelpline {

	// Webdriver driver instance creation
	WebDriver driver;

	public WebDriverHelpline(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Home Page **/
	
		
	@FindBy(xpath="//*[@id='mainContainer']/div/div[3]/div/div[1]/div[1]/div/ul/div/div")
	private WebElement dwnArrow;

	public WebElement clickdwnArrow() {
		return dwnArrow;
	}
	
	
	@FindBy(xpath="//*[contains(text(),'Helpline')]")
	private WebElement helpLineMenu;

	public WebElement clickhelpLineMenu() {
		return helpLineMenu;
	}
	
	@FindBy(xpath="//*[@class='tab active-tab' and text()='TODAY']")
	private WebElement todayTab;

	public WebElement clicktodayTab() {
		return todayTab;
	}
	
	@FindBy(xpath="//*[@class='tab inactive-tab' and text()='YESTERDAY']")
	private WebElement yesTab;

	public WebElement clickyesTab() {
		return yesTab;
	}
	
	@FindBy(xpath="//*[@class='tab inactive-tab' and text()='TOMORROW']")
	private WebElement tomTab;

	public WebElement clicktomTab() {
		return tomTab;
	}
	
	@FindBy(xpath="//*[@class='arrow_down']")
	private WebElement dwn;

	public WebElement clickdwn() {
		return dwn;
	}
	
	
	
	@FindBy(xpath="//*[@class='arrow_up']")
	private WebElement up;

	public WebElement clickup() {
		return up;
	}
	
	@FindBy(xpath="//*[text()='Payments']")
	private WebElement payments;

	public WebElement clkpayments() {
		return payments;
	}
	
	@FindBy(xpath="//*[text()='View Vehicle Payments']")
	private WebElement viewPayments;

	public WebElement viewVehPayments() {
		return viewPayments;
	}
	
	@FindBy(xpath="//*[contains(text(),'Payment Details')]")
	private WebElement paymentText;

	public WebElement viewpaymentText() {
		return paymentText;
	}
	
	@FindBy(xpath="//*[@class='profile-info-div  ']")
	private WebElement downOPM;

	public WebElement ClkdownOPM() {
		return downOPM;
	}
	
	@FindBy(xpath="//input[@class='vehicle-number-txtbox']")
	private WebElement vehNumtxt;

	public WebElement clkvehNumtxt() {
		return vehNumtxt;
	}
	
	@FindBy(xpath="//button[@class='search-btn']")
	private WebElement srchbtn;

	public WebElement clksrchbtn() {
		return srchbtn;
	}
	
	public WebElement entervehicletext(WebDriver driver, String vehnum) {
		return driver.findElement(By.xpath("//*[text()=\'"+vehnum+"\']"));
	}
			
	@FindBy(xpath="//*[text()='Booking Details']")
	private WebElement bookDetails;

	public WebElement clkbookDetails() {
		return bookDetails;
	}
	
	
	

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : To verify if DriverHelpline menu is present 
	 *
	 * @throws Exception
	 * 
	 * 
	 */

	public void driverhelpmenu(WebDriver driver) throws Exception {


		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		WebActionUtil.verifyElementIsDisplayed(todayTab, driver, "today tab in home page");
		
		try{
			
			dwn.click();
		boolean dp=  up.isDisplayed();
		
		if (dp)
		{
		MyExtentListners.test.pass(" There are no bookings to validate");
		}
		else{
			MyExtentListners.test.fail(" There are no bookings to validate");
		}
		}
		catch( Exception e)
		
		{
			MyExtentListners.test.pass(" There are no bookings to validate");
		
		}
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(yesTab, driver, "yesterday tab in home page");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		try{
			dwn.click();
			boolean dp=  up.isDisplayed();
			
			if (dp)
			{
			MyExtentListners.test.pass(" There are no bookings to validate");
			}
			else{
				MyExtentListners.test.fail(" There are no bookings to validate");
			}
			}
			catch( Exception e)
			
			{
				MyExtentListners.test.pass(" There are no bookings to validate");
			}
		
		WebActionUtil.clickElement(tomTab, driver, "tomorrow tab in home page");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		try{
		dwn.click();
		boolean dp=  up.isDisplayed();
		
		if (dp)
		{
		MyExtentListners.test.pass(" There are no bookings to validate");
		}
		else{
			MyExtentListners.test.fail(" There are no bookings to validate");
		}
		}
		catch( Exception e)
		
		{
			MyExtentListners.test.pass(" There are no bookings to validate");
		}
	}

	
	public void downOPMclk(WebDriver driver) throws Exception {
		
		WebActionUtil.clickElement(downOPM, driver, "Down arrow");
		
		
	}
	
	
	
	public void scrollclickhelpLineMenu(WebDriver driver) throws Exception {
		
		WebActionUtil.scrollIntoViewAndClick(driver, helpLineMenu);		
	}
	

	public void driverhelplinePayments(WebDriver driver) throws Exception {
	
		WebActionUtil.clickElement(payments, driver, "Payments");
		WebActionUtil.clickElement(viewPayments, driver, "View vehicle payments");
		WebActionUtil.verifyElementIsDisplayed(paymentText, driver, "Payment details page");
		
		//vehicle number search
		
		WebActionUtil.clearAndTypeEnter(vehNumtxt, "KA02EC1234", "Vehicle number text feild", driver);
		WebActionUtil.clickElement(srchbtn, driver, "Search button");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		WebActionUtil.verifyElementIsDisplayed(entervehicletext(driver,"KA02EC1234"), driver, "Vehcile number");
		
	    WebActionUtil.scrollIntoViewAndClick(driver, bookDetails);
	    WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	    WebActionUtil.verifyElementIsDisplayed(bookDetails, driver, "Booking details page");
	}
}
