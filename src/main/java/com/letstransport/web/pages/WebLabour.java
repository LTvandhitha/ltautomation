

package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.paulhammant.ngwebdriver.ByAngular;

//import ch.qos.logback.core.joran.action.ActionUtil;
import io.appium.java_client.android.AndroidDriver;

public class WebLabour {

	// Webdriver driver instance creation
	WebDriver driver;

	public WebLabour(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Home Page **/

	@FindBy(xpath = "//*[text()='Add Helper']")
	private WebElement addHelper;

	public WebElement ClkaddHelper() {
		return addHelper;
	}
	
	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;

	public WebElement clkdoneBtn() {
		return doneBtn;
	}
	
	
	@FindBy(xpath = "//*[text()='Labour Module']")
	private WebElement LabModule;

	public WebElement clkLabModule() {
		return LabModule;
	}
	
	
	@FindBy(xpath = "//*[text()='Helper List']")
	private WebElement helperlist;

	public WebElement clkhelperlist() {
		return helperlist;
	}
	
	@FindBy(xpath = "//*[text()='Add New Helper']")
	private WebElement addnewhelper;

	public WebElement clkaddnewhelper() {
		return addnewhelper;
	}
	
	
	
	
	@FindBy(xpath = "//*[@class='_8fa24117']")
	private WebElement selectvendor1;

	public WebElement clkselectvendor1() {
		return selectvendor1;
	}
	
	@FindBy(xpath = "//div[@role='listbox']")
	private WebElement typeHelperDropdown;

	public WebElement clktypeHelperDropdown() {
		return typeHelperDropdown;
	}
	
	
	@FindBy(xpath = "//div[@class='_81782ee3']")
	private WebElement textverify;

	public WebElement verifydropdown() {
		return textverify;
	}
	
	@FindBy(xpath = "//input[@id='name']")
	private WebElement enterHelpername;

	public WebElement inputHelpername() {
		return enterHelpername;
	}
	
	@FindBy(xpath = "//input[@id='phoneNumber']")
	private WebElement enterPhname;

	public WebElement inputenterPhname() {
		return enterPhname;
	}
	
	
	@FindBy(xpath = "//*[text()='Helper Added Successfully']")
	private WebElement helperSuccess;

	public WebElement verifyhelperSuccess() {
		return helperSuccess;
	}
	
	
	@FindBy(xpath = "//*[text()='Add Another Helper']")
	private WebElement addHelperbtn;

	public WebElement addAnotherHelper() {
		return addHelperbtn;
	}
	
	@FindBy(xpath = "//*[@class=' css-2b097c-container']")
	private WebElement selecthub;

	public WebElement selecthubhelper() {
		return selecthub;
	}
	
	@FindBy(xpath = "//p[text()='Select Vendor']")
	private WebElement selectVendor;

	public WebElement selectVendorhelper() {
		return selectVendor;
	}
	
	@FindBy(xpath = "//*[text()='Labour exists with phone number']")
	private WebElement labExists;

	public WebElement verfylabExists() {
		return labExists;
	}
	
	@FindBy(xpath = "//*[@id='app']/div[2]/div/div[1]/img")
	private WebElement backbtn;

	public WebElement clickbackbtn() {
		return backbtn;
	}
	
	@FindBy(xpath = "//*[text()='opm']")
	private WebElement loginopm;

	public WebElement clickloginopm() {
		return loginopm;
	}
	
	
	@FindBy(xpath = "//*[@id='app']/div[2]/div/div[1]/div/div[1]/img")
	private WebElement lab_hamMenu;

	public WebElement clicklab_hamMenu() {
		return lab_hamMenu;
	}
	
	
	
	
	@FindBy(xpath = "//*[contains(text(),'switch')]")
	private WebElement switchlogin;

	public WebElement clickswitchlogin() {
		return switchlogin;
	}
	
	
	@FindBy(xpath = "//*[text()='View Helpers']")
	private WebElement viewHelpers;

	public WebElement clickviewHelpers() {
		return viewHelpers;
	}
	
	
	@FindBy(xpath = "//*[text()='Search by client']")
	private WebElement srlistbox;

	public WebElement clicklistbox() {
		return srlistbox;
	}
	

	@FindBy(xpath = "//*[@id='phoneNumber']")
	private WebElement phNumtxtbox;

	public WebElement enterphNum() {
		return phNumtxtbox;
	}



	public void navigateLabourModule(WebDriver driver) throws IOException{
		
		WebActionUtil.scrollIntoViewAndClick(driver, LabModule);
		
	}
	
	
	
	public void verify_addhelperpage(WebDriver driver) throws IOException, InterruptedException{
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.clickElement(addHelper, driver, "Add Helper button");
		WebActionUtil.verifyElementIsDisplayed(helperlist, driver, "helper list");		
		WebActionUtil.verifyElementIsDisplayed(addnewhelper, driver, "Add new helper");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 50000, driver);
		Thread.sleep(5000);
		WebActionUtil.actionClick(addnewhelper, driver, "Click Add new helper page");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		
		WebActionUtil.clickElement(typeHelperDropdown, driver, "Type of helper drop down");
		
		WebActionUtil.verifyElementContainsText(typeHelperDropdown, "Direct", driver);
		WebActionUtil.verifyElementContainsText(textverify, "Vendor", driver);
		WebActionUtil.verifyElementContainsText(textverify, "Managed Helper", driver);
		WebActionUtil.verifyElementContainsText(textverify, "Adhoc Helper", driver);
			
		
	}
	
	
	public WebElement Selecthelper(WebDriver driver, String labtype) {
		return driver.findElement(By.xpath("//*[text()='"+labtype+"']"));
		
		
			}
	
	public WebElement Selectvendor(WebDriver driver, String ventype) {
		return driver.findElement(By.xpath("//li[text()='"+ventype+"']"));
			
			
			}
	
	
	public WebElement SelectHub(WebDriver driver, String hubtype) {
		return driver.findElement(By.xpath("//*[text()='"+hubtype+"']"));
		
			
			}
	
	public void validate_addvendorhelper(WebDriver driver,String labtype,String helpername) throws Exception{
		
		
		WebActionUtil.actionClick(Selecthelper(driver,labtype), driver,"Add helper");
		WebActionUtil.clearAndType(enterHelpername, helpername, "Helper name", driver);
		String phNum= WebActionUtil.generateRandomNumberfornDigits1(10);
		WebActionUtil.clearAndType(enterPhname, phNum, "Phone number", driver);
		WebActionUtil.clickElement(doneBtn, driver, "Done");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.verifyElementIsDisplayed(helperSuccess, driver,"Helper added successfully"+ "\'" + labtype + "\'");
	
	}
	
	
	
	public void validate_addmanagedhelper(WebDriver driver,String labtype,String helpername, String helpername1,String hub) throws Exception{
		
		WebActionUtil.clickElement(typeHelperDropdown, driver, "Type of helper drop down");
		WebActionUtil.actionClick(Selecthelper(driver,labtype), driver,"Add helper");
		WebActionUtil.clickElement(selectVendor, driver, "Select vendor");
		WebActionUtil.clickElement(Selectvendor(driver,helpername), driver, "Vendor");
		WebActionUtil.clearAndType(enterHelpername, helpername1, "Helper name", driver);
		String phNum= WebActionUtil.generateRandomNumberfornDigits1(10);
		WebActionUtil.clearAndType(enterPhname, phNum, "Phone number", driver);
		WebActionUtil.clickElement(selecthub, driver, "Hub");
		WebActionUtil.actionClick(SelectHub(driver,hub), driver, "Hub");
		WebActionUtil.clickElement(doneBtn, driver, "Done");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.verifyElementIsDisplayed(helperSuccess, driver,"Helper added successfully"+ "\'" + labtype + "\'");
	
	}
	
	
	
	
	public String validate_addadhochelper(WebDriver driver,String labtype,String helpername2,String hub) throws Exception{
		
		WebActionUtil.clickElement(typeHelperDropdown, driver, "Type of helper drop down");
		WebActionUtil.actionClick(Selecthelper(driver,labtype), driver,"Add helper");
		WebActionUtil.clearAndType(enterHelpername, helpername2, "Helper name", driver);
		String phNum= WebActionUtil.generateRandomNumberfornDigits1(10);
		WebActionUtil.clearAndType(enterPhname, phNum, "Phone number", driver);
		WebActionUtil.clickElement(selecthub, driver, "Hub");
		WebActionUtil.actionClick(SelectHub(driver,hub), driver, "Hub");
		WebActionUtil.clickElement(doneBtn, driver, "Done");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.verifyElementIsDisplayed(helperSuccess, driver,"Helper added successfully"+ "\'" + labtype + "\'");

		return phNum;
	}
	public void addanotherHelper( WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(addHelperbtn, driver, "Add another helper button");
		
	}
	
	
	public void clickBackbtn(WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(backbtn, driver, "back button");
		
	}
	
	
	public void labour_hamburgermenu(WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(lab_hamMenu, driver, "Hamburger menu");
		
	}
	
	public void switch_logins(WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(switchlogin, driver, "Switch logins");
		
	}
	
	
	
	public void logintoOPM(WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(loginopm, driver, "opm login");
		
	}


	public void validate_OPM_helperview(WebDriver driver,String searchby,String phNum,String helpername2) throws Exception{
		
		
		WebActionUtil.clickElement(viewHelpers, driver, "view helper");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='_6cac75a0']", 35000, driver);

		Thread.sleep(10000);
		WebActionUtil.clickElement(srlistbox, driver, "click search by drop down");
		
		WebActionUtil.actionClick(SelectHub(driver,searchby), driver, "Hub");
	
		WebActionUtil.clearAndType(phNumtxtbox, phNum, "Phone number", driver);
		
		
		WebActionUtil.clickElement(doneBtn, driver, "Done");
//		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 35000, driver);
		Thread.sleep(10000);		
		WebActionUtil.verifyElementIsDisplayed(Selecthelper(driver,helpername2), driver, "Helper visible to OPM");
	}



	
public void validate_duplicateaddvendorhelper(WebDriver driver,String labtype,String helpername,String helpername1,String hub) throws Exception{
	String phNum= WebActionUtil.generateRandomNumberfornDigits1(10);
	

	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	WebActionUtil.actionClick(Selecthelper(driver,labtype), driver,"Add helper");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	WebActionUtil.clickElement(selectVendor, driver, "Select vendor");
	WebActionUtil.clickElement(Selectvendor(driver,helpername), driver, "Vendor");
	WebActionUtil.clearAndType(enterHelpername, helpername1, "Helper name", driver);
	
	WebActionUtil.clearAndType(enterPhname, phNum, "Phone number", driver);
	WebActionUtil.clickElement(selecthub, driver, "Hub");
	WebActionUtil.actionClick(SelectHub(driver,hub), driver, "Hub");
	WebActionUtil.clickElement(doneBtn, driver, "Done");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	WebActionUtil.verifyElementIsDisplayed(helperSuccess, driver,"Helper added successfully"+ "\'" + labtype + "\'");
	WebActionUtil.clickElement(addHelperbtn, driver, "Add another helper button");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	
	//once more
	WebActionUtil.clickElement(typeHelperDropdown, driver, "Type of helper drop down");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	WebActionUtil.actionClick(Selecthelper(driver,labtype), driver,"Add helper");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	
	
	WebActionUtil.clickElement(selectvendor1, driver, "Select vendor");
	WebActionUtil.clickElement(Selectvendor(driver,helpername), driver, "Vendor");
	WebActionUtil.clearAndType(enterHelpername, helpername1, "Helper name", driver);
	
	WebActionUtil.clearAndType(enterPhname, phNum, "Phone number", driver);
	WebActionUtil.clickElement(selecthub, driver, "Hub");
	WebActionUtil.actionClick(SelectHub(driver,hub), driver, "Hub");
	WebActionUtil.clickElement(doneBtn, driver, "Done");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
	WebActionUtil.verifyElementIsDisplayed(labExists, driver, "Labour already exists with the mobile number  message");
	
	
	
	}
	}
	

//>>>>>>> Stashed changes
