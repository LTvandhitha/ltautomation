package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class WebLoadBoardPage {

	WebDriver driver;

	public WebLoadBoardPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** LoadBoardPage **/

	@FindBy(xpath = "//*[text()='Create Listing']")
	private WebElement createLisitngBtn;

	public WebElement clickOnCreateLisitngbtn() {
		return createLisitngBtn;
	}

	@FindBy(xpath = "//*[text()='Search Client...']")
	private WebElement searchClientfield;

	@FindBy(xpath = "//*[text()='Search Area...']")
	private WebElement searchAreaField;

	@FindBy(xpath = "//*[text()='Search Area...']/..//input")
	private WebElement searchAreaInputfield;

	@FindBy(xpath = "//*[text()='Search Client...']/..//input")
	private WebElement searchClientInputfield;

	public WebElement clickSelectClient() {
		return searchClientfield;
	}

	@FindBy(xpath = "//*[@class='Select-menu-outer']")
	private WebElement searchResult;

	public WebElement clickResult() {
		return searchResult;
	}

	@FindBy(xpath = "//*[text()='Select hub...']")
	private WebElement searchHub;

	public WebElement clickSelectHub() {
		return searchHub;
	}

	@FindBy(xpath = "//*[@class='date-time-picker-wrapper-div date-custom-wrapper-style date-time-picker-invalid ']")
	private WebElement datePicker;

	public WebElement clickDatePicker() {
		return datePicker;
	}

	@FindBy(xpath = "//div[text()='Next']")
	private WebElement nextBtn;

	public WebElement clickNextBtn() {
		return nextBtn;
	}

	@FindBy(xpath = "//*[text()='Vehicle Tonnage']/../div[@class='input-dropdown-area']")
	private WebElement vehicleTon;

	public WebElement clickVehicleTonnage() {
		return vehicleTon;
	}

	@FindBy(xpath = "//*[text()='Vehicle Type']/../div[@class='input-dropdown-area']")
	private WebElement vehicleType;

	public WebElement clickVehicleType() {
		return vehicleType;
	}

	@FindBy(xpath = "//*[text()='Vehicle Body']/../div[@class='input-dropdown-area']")
	private WebElement vehicleBody;

	public WebElement clickVehicleBody() {
		return vehicleBody;
	}

	@FindBy(xpath = "//*[text()='Select Material']/../div[@class='input-dropdown-area']")
	private WebElement selectMaterial;

	public WebElement clickSelectMaterial() {
		return selectMaterial;
	}

	@FindBy(xpath = "//*[text()='Select Work Type']/../div[@class='input-dropdown-area']")
	private WebElement selectWorkType;

	public WebElement clickSelectWorkType() {
		return selectWorkType;
	}

	@FindBy(xpath = "//*[text()='Enter Revenue']/../div[@class='input-amount-div']//input")
	private WebElement RevenueTextBox;

	public WebElement enterRevenueTextBox() {
		return RevenueTextBox;
	}

	@FindBy(xpath = "//*[text()='Enter Amount To Be Paid']/../div[@class='input-amount-div']//input")
	private WebElement amountToBePaidTextBox;

	public WebElement enteramountToBePaidTextBox() {
		return amountToBePaidTextBox;
	}

	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;

	@FindBy(xpath = "//*[text()='Past']")
	private WebElement pastBtn;

	public WebElement clickDone() {
		return doneBtn;
	}

	@FindBy(xpath = "//*[text()='Refresh']")
	private WebElement refreshBtn;

	@FindBy(xpath = "//*[text()='Filters']")
	private WebElement filtersBtn;

	public WebElement clickRefreshBtn() {
		return refreshBtn;
	}

	@FindBy(xpath = "//*[text()='Kms']/..//input")
	private WebElement totalDistance;

	@FindBy(xpath = "//*[text()='From Date']/..//*[text()='Select Date']")
	private WebElement fromDateFilters;

	@FindBy(xpath = "//*[text()='To Date']/..//*[text()='Select Date']")
	private WebElement toDateFilters;

	@FindBy(xpath = "//*[@class='block numbers  ']//*[text()='1']")
	private WebElement fromDateSelected;

	@FindBy(xpath = "//*[@class='number-span  selected-block  ']")
	private WebElement toDateSelected;

	@FindBy(xpath = "//*[text()='Apply']")
	private WebElement applyBtn;

	@FindBy(xpath = "//*[@class='Select-input']")
	private WebElement selectStop1;

	@FindBy(xpath = "//*[@class='Select-input']//input")
	private WebElement selectStop2;

	@FindBy(xpath = "//*[@class='Select-menu-outer']//*[text()='Gurugram']")
	private WebElement gurugramStop;

	@FindBy(xpath = "//*[@class='Select-menu-outer']//*[text()='Delhi']")
	private WebElement delhiStop;

	@FindBy(xpath = "//*[text()='Hrs']/..//input")
	private WebElement totalDuration;

	@FindBy(xpath = "//*[text()='Number of Vehicles']/..//input[@type='number']")
	private WebElement numberOfVehicles;

	@FindBy(xpath = "//*[text()='Up Country']")
	private WebElement upCountry;

	@FindBy(xpath = "//*[text()='Multiple Point']")
	private WebElement multiplePoint;

	@FindBy(xpath = "//*[@class='edit-icon']")
	private WebElement editIcon;

	@FindBy(xpath = "//*[@class='quote-amount']")
	private WebElement qouteAmount;

	@FindBy(xpath = "(//*[text()='Quote Status']/..//div[2])[2]")
	private WebElement acceptQuoteStatus;

	@FindBy(xpath = "(//*[text()='Quote Status']/..//div[2]//div)[2]")
	private WebElement dismissQuoteStatus;

	@FindBy(xpath = "(//*[text()='OPM Approval']/..//div[2])[2]")
	private WebElement acceptOPMApproval;

	@FindBy(xpath = "(//*[text()='OPM Approval']/..//div[2]//div)[2]")
	private WebElement dismissOPMApproval;

	@FindBy(xpath = "//*[@class='close-btn-popUp']")
	private WebElement closeBtn;

	@FindBy(xpath = "//*[text()='Do you want to delete current listing?']/..//*[text()='Confirm']")
	private WebElement confirmBtn;

	@FindBy(xpath = "(//*[text()='Convert to Booking']/..//div)[4]")
	private WebElement convertBooking;

	@FindBy(xpath = "(//*[text()='Cancel']/..//div)[1]")
	private WebElement cancelBooking;

	@FindBy(xpath = "//*[text()='All Listings']")
	private WebElement allListingsBtn;

	@FindBy(xpath = "//*[text()='Convert to Booking']/..//*[text()='Booking Created']")
	private WebElement bookingCreatedtext;

	@FindBy(xpath = "//*[text()='Convert to Booking']/..//*[text()='removed']")
	private WebElement bookingRemovedtext;

	@FindBy(xpath = "//*[text()='Load More']")
	private WebElement loadMore;

	public WebElement selectVehicleTonnage(WebDriver driver, String weight) {
		return driver.findElement(By
				.xpath("//*[text()='Vehicle Tonnage']/../div[@class='input-dropdown-area']/div[@class='dropDown-popUp normal']//*[text()=\'"
						+ weight + "\']"));
	}

	public WebElement selectVehicleType(WebDriver driver, String type) {
		return driver.findElement(By
				.xpath("//*[text()='Vehicle Type']/../div[@class='input-dropdown-area']/div[@class='dropDown-popUp normal']//*[text()=\'"
						+ type + "\']"));
	}

	public WebElement selectVehicleBody(WebDriver driver, String body) {
		return driver.findElement(By
				.xpath("//*[text()='Vehicle Body']/../div[@class='input-dropdown-area']/div[@class='dropDown-popUp normal']//*[text()=\'"
						+ body + "\']"));
	}

	public WebElement selectVehicleMaterial(WebDriver driver, String material) {
		return driver.findElement(By
				.xpath("//*[text()='Select Material']/../div[@class='input-dropdown-area']/div[@class='dropDown-popUp displaced']//*[text()=\'"
						+ material + "\']"));
	}

	public WebElement selectWorkType(WebDriver driver, String workType) {
		return driver.findElement(By
				.xpath("//*[text()='Select Work Type']/../div[@class='input-dropdown-area']/div[@class='dropDown-popUp displaced']//*[text()=\'"
						+ workType + "\']"));
	}

	public String getListingReportingTime(WebDriver driver, String client, String hub, String reportingTime) {
		return driver.findElement(By.xpath("//*[@class='listing-bar-div']//*[text()=\'" + client
				+ "\']/../..//*[text()=\'" + hub + "\']/../../..//*[text()=\'" + reportingTime + "\']")).getText();
	}

	public WebElement getListing(WebDriver driver, String client, String hub, String reportingTime) {
		return driver.findElement(By.xpath("//*[@class='listing-bar-div']//*[text()=\'" + client
				+ "\']/../..//*[text()=\'" + hub + "\']/../../..//*[text()=\'" + reportingTime + "\']"));
	}

	public String getListingHubName(WebDriver driver, String client, String hub) {
		return driver
				.findElement(By.xpath(
						"//*[@class='listing-bar-div']//*[text()=\'" + client + "\']/../..//*[text()=\'" + hub + "\']"))
				.getText();
	}

	public String getListingClientName(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//*[@class='listing-bar-div']//*[text()=\'" + client + "\']")).getText();
	}

	public WebElement getListingClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//*[@class='listing-bar-div']//*[text()=\'" + client + "\']"));
	}

	public WebElement clickHub(WebDriver driver, String hub) {
		return driver.findElement(By.xpath("(//*[text()='" + hub + "'])[2]"));
	}

	public WebElement getListingEdit(WebDriver driver, String client, String hub, String reportingTime) {
		return driver.findElement(
				By.xpath("(//*[@class='listing-bar-div']//*[text()=\'" + client + "\']/../..//*[text()=\'" + hub
						+ "\']/../../..//*[text()=\'" + reportingTime + "\']/../../../../..//*[@class='img-btn'])[1]"));
	}

	public WebElement getListingDelete(WebDriver driver, String client, String hub, String reportingTime) {
		return driver.findElement(
				By.xpath("(//*[@class='listing-bar-div']//*[text()= '" + client + "']/../..//*[text()='" + hub
						+ "']/../../..//*[text()='" + reportingTime + "']/../../../../..//*[@class='img-btn'])[2]"));
	}

	public WebElement getListingDeletewithoutTime(WebDriver driver, String client, String hub) {
		return driver.findElement(By.xpath("(//*[@class='listing-bar-div']//*[text()= '" + client
				+ "']/../..//*[text()='" + hub + "']/../../../../..//*[@class='img-btn'])[2]"));
	}

	public WebElement getListingApproved(WebDriver driver, String client, String hub, String reportingTime,
			String amount) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/../../../../../..//*[text()='" + reportingTime
				+ "']/../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../..//*[text()='Approved']/..//span[text()='1/1']"));		
	}

	public WebElement bookingCreated(WebDriver driver, String text) {
		return driver.findElement(
				By.xpath("//*[text()='All Listings']/..//*[@class='dropDown-popUp normal']//*[text()='" + text + "']"));
	}
	
	public WebElement getStartPoint(WebDriver driver, String startpoint) {
		return driver.findElement(By
				.xpath("//*[@class='Select-menu-outer']//*[text()='"+startpoint+"']"));
	}
	
	public WebElement getEndPoint(WebDriver driver, String endpoint) {
		return driver.findElement(By
				.xpath("//*[@class='Select-menu-outer']//*[text()='"+endpoint+"']"));
	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public void createListing(String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		System.out.println("<<<<<<<<<<<<<<<<" + rowCount + ">>>>>>>>>>>>>>>>");
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.clickElement(this.createLisitngBtn, this.driver, "create listing");
			WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
			// Multimap<String, String> multimap = ArrayListMultimap.create();
			if (!(sData[BaseLib.gv.cCount].equalsIgnoreCase("NA"))) {
				try {
					WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
					WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
					WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name",
							this.driver);
					WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
				} catch (Exception e) {
					WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
					WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
					WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name",
							this.driver);
					WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);

				}
			}
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
			if (!(sData[BaseLib.gv.hcount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.searchHub, this.driver, " Hub name", sData[BaseLib.gv.hcount]);
			}
			WebActionUtil.actionClick(this.clickDatePicker(), this.driver, "Date picker ");
			if (!(sData[BaseLib.gv.rdtCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.HandleDateTime(this.driver, timeStamp, "n");
				Thread.sleep(4000);
				WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
				WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
				WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
				Thread.sleep(5000);
			}
			WebActionUtil.type(this.totalDistance, "100", "total distance ", this.driver);
			WebActionUtil.type(this.totalDuration, "2", "total duration", this.driver);
			WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
			WebActionUtil.type(this.numberOfVehicles, "1", "number of vehicles ", this.driver);
			WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
			Thread.sleep(5000);
			WebActionUtil.clickElement(this.vehicleTon, this.driver, "Vehicle Tonnage ");
			if (!(sData[BaseLib.gv.vtcount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleTonnage(this.driver, sData[BaseLib.gv.vtcount]));
			}
			WebActionUtil.clickElement(this.vehicleType, this.driver, "Vehicle Type ");
			if (!(sData[BaseLib.gv.vtyCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleType(this.driver, sData[BaseLib.gv.vtyCount]));
			}
			WebActionUtil.waitTillPageLoad("Loading vehicle type", this.driver, "Vehicle Details page", 300);
			WebActionUtil.clickElement(this.vehicleBody, this.driver, "Vehicle Body ");
			if (!(sData[BaseLib.gv.vbCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleBody(this.driver, sData[BaseLib.gv.vbCount]));
			}
			WebActionUtil.clickElement(this.selectMaterial, this.driver, " Material to ship");
			if (!(sData[BaseLib.gv.smCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleMaterial(this.driver, sData[BaseLib.gv.smCount]));
			}
			WebActionUtil.clickElement(this.selectWorkType, this.driver, "Work type ");
			if (!(sData[BaseLib.gv.swtCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectWorkType(this.driver, sData[BaseLib.gv.swtCount]));
			}
			Thread.sleep(2000);
			WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
			if (!(sData[BaseLib.gv.rCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.RevenueTextBox, this.driver, " Revenue amount",
						sData[BaseLib.gv.rCount]);
			}
			if (!(sData[BaseLib.gv.atpCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.amountToBePaidTextBox, this.driver, " Amount to be paid",
						sData[BaseLib.gv.atpCount]);
			}
			WebActionUtil.clickElement(this.doneBtn, this.driver, "Done button");
		}
	}

	public void createListingUpCountry(String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			WebActionUtil.clickElement(this.createLisitngBtn, this.driver, "create listing");
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			// Multimap<String, String> multimap = ArrayListMultimap.create();
			if (!(sData[BaseLib.gv.cCount].equalsIgnoreCase("NA"))) {
				try {
					WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
					WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name",
							this.driver);
				} catch (Exception e) {
					WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
					WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name",
							this.driver);
				}
			}
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
			if (!(sData[BaseLib.gv.hcount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.searchHub, this.driver, " Hub name", sData[BaseLib.gv.hcount]);
			}
			WebActionUtil.actionClick(this.clickDatePicker(), this.driver, "Date picker ");
			if (!(sData[BaseLib.gv.rdtCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.HandleDateTime(this.driver, timeStamp, "r");
				WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
			}
			WebActionUtil.clickElement(this.upCountry, this.driver, "upcountry button ");
			WebActionUtil.waitTillPageLoad("upcountry selected", this.driver, "LoadBoard", 120);
			//WebActionUtil.actionClickAndType(this.selectStop1, driver, "Select stops", "Mumbai");
//<<<<<<< HEAD
//			WebActionUtil.actionClick(this.selectStop1, driver, "Select stops");
//			WebActionUtil.actionClick(this.mumbaiStop, driver, "Select stops");
//			WebActionUtil.waitTillPageLoad("Mumbai selected", this.driver, "LoadBoard", 120);
//=======
			WebActionUtil.actionClick(this.selectStop1, driver, "Select stop 1");
			WebActionUtil.actionClick(this.getStartPoint(driver, sData[BaseLib.gv.spCount]), driver, "Select stop 1");
			Thread.sleep(2000);
			WebActionUtil.waitTillPageLoad("Mumbai selected", this.driver, "LoadBoard", 120);
			WebActionUtil.waitForVisiblityofElement("//span[text()='"+sData[BaseLib.gv.spCount]+"']", 300, driver);
//>>>>>>> branch 'master' of https://gitlab.com/qa_lt/automation.git
			//WebActionUtil.actionClickAndType(this.selectStop2, driver, "Select stops");
			WebActionUtil.actionClick(this.selectStop2, driver, "Select stop 2");
			WebActionUtil.actionClick(this.getEndPoint(driver, sData[BaseLib.gv.epCount]), driver, "Select stop 2");
			WebActionUtil.waitTillPageLoad("Client hub name ", this.driver, " Listing page", 300);
			WebActionUtil.waitForVisiblityofElement("//span[text()='"+sData[BaseLib.gv.epCount]+"']", 300, driver);
            //Thread.sleep(2000);
			WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
			WebActionUtil.type(this.numberOfVehicles, "1", "number of vehicles ", this.driver);
			Thread.sleep(4000);
			WebActionUtil.clickElement(this.vehicleTon, this.driver, "Vehicle Tonnage ");
			if (!(sData[BaseLib.gv.vtcount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleTonnage(this.driver, sData[BaseLib.gv.vtcount]));
			}
			WebActionUtil.clickElement(this.vehicleType, this.driver, "Vehicle Type ");
			if (!(sData[BaseLib.gv.vtyCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleType(this.driver, sData[BaseLib.gv.vtyCount]));
			}
			WebActionUtil.waitTillPageLoad("Loading vehicle type", this.driver, "Vehicle Details page", 300);
			WebActionUtil.clickElement(this.vehicleBody, this.driver, "Vehicle Body ");
			if (!(sData[BaseLib.gv.vbCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleBody(this.driver, sData[BaseLib.gv.vbCount]));
			}
			WebActionUtil.clickElement(this.selectMaterial, this.driver, " Material to ship");
			if (!(sData[BaseLib.gv.smCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectVehicleMaterial(this.driver, sData[BaseLib.gv.smCount]));
			}
			WebActionUtil.clickElement(this.selectWorkType, this.driver, "Work type ");
			if (!(sData[BaseLib.gv.swtCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.selectWorkType(this.driver, sData[BaseLib.gv.swtCount]));
			}
			WebActionUtil.clickElement(this.nextBtn, this.driver, "Next button ");
			if (!(sData[BaseLib.gv.rCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.RevenueTextBox, this.driver, " Revenue amount",
						sData[BaseLib.gv.rCount]);
			}
			if (!(sData[BaseLib.gv.atpCount].equalsIgnoreCase("NA"))) {
				WebActionUtil.actionClickAndType(this.amountToBePaidTextBox, this.driver, " Amount to be paid",
						sData[BaseLib.gv.atpCount]);
			}
			WebActionUtil.clickElement(this.doneBtn, this.driver, "Done button");
		}
	}

	public void refreshListing() throws Exception {
		WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 120);
		WebActionUtil.clickElement(this.refreshBtn, this.driver, "Refresh button");
	}

	public void searchListing(WebDriver driver, String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			try {
				WebActionUtil.scrollIntoView(this.driver,
						this.getListing(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
				WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
			} catch (Exception e) {
				WebActionUtil.scrollIntoViewAndClick(this.driver, this.loadMore);
//<<<<<<< HEAD
//				WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 120);
//=======
//				WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
//>>>>>>> branch 'master' of https://gitlab.com/qa_lt/automation.git
				WebActionUtil.scrollIntoView(this.driver,
						this.getListing(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
				WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
			}
		}
	}

	public void selectListing(WebDriver driver, String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			try {
				WebActionUtil.scrollIntoViewAndClick(this.driver,
						this.getListing(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
			} catch (Exception e) {
				try {
					WebActionUtil.scrollIntoViewAndClick(this.driver, this.loadMore);
					WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
					WebActionUtil.scrollIntoViewAndClick(this.driver, this.getListing(this.driver,
							sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
					WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
				} catch (Exception ex) {
					WebActionUtil.scrollIntoViewAndClick(this.driver, this.loadMore);
					WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);
					WebActionUtil.scrollIntoViewAndClick(this.driver, this.getListing(this.driver,
							sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
					WebActionUtil.waitTillPageLoad("load more for listings", this.driver, "LoadBoard", 300);

				}
			}
		}
	}

	public void verifyCreatedListing(String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.scrollIntoView(this.driver,
					this.getListing(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
			WebActionUtil.verifyEqualsText("Client Name ",
					this.getListingClientName(this.driver, sData[BaseLib.gv.cCount]), sData[BaseLib.gv.cCount]);
			WebActionUtil.verifyEqualsText("Hub Name ",
					this.getListingHubName(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount]),
					sData[BaseLib.gv.hcount]);
			WebActionUtil.verifyEqualsText("Reporting time ", this.getListingReportingTime(this.driver,
					sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp), timeStamp);
		}
	}

	public void editListingAmount(WebDriver driver, String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.waitForVisiblityofElement("(//*[@class='listing-bar-div']//*[text()='LIBERTY']/..//*[@class='img-btn'])[1]", 120, driver);
			WebActionUtil.waitTillPageLoad("edit revenue and amount to be paid", this.driver, "LoadBoard", 120);
			WebActionUtil.clickElement(
					this.getListingEdit(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp),
					driver, "Edit button");
//<<<<<<< HEAD
//			WebActionUtil.waitTillPageLoad("wait for the amount to edit", this.driver, "LoadBoard", 120);
//			WebActionUtil.actionClickAndClear(this.RevenueTextBox, this.driver, " Revenue amount");
//=======
//			WebActionUtil.actionClickAndClear(this.RevenueTextBox, driver, " Revenue amount");
//			//WebActionUtil.deleteAndType(this.RevenueTextBox,, String.valueOf(Integer.parseInt(sData[BaseLib.gv.rCount]) + 50), "revenue amount", driver);
//>>>>>>> branch 'master' of https://gitlab.com/qa_lt/automation.git
			WebActionUtil.actionClickAndType(this.RevenueTextBox, this.driver, "Revenue amount",
					String.valueOf(Integer.parseInt(sData[BaseLib.gv.rCount]) + 50));
			WebActionUtil.actionClickAndClear(this.amountToBePaidTextBox, this.driver, " Amount to be paid");
			WebActionUtil.actionClickAndType(this.amountToBePaidTextBox, this.driver, "Amount to be paid",
					String.valueOf(Integer.parseInt(sData[BaseLib.gv.atpCount]) + 50));
			WebActionUtil.clickElement(this.doneBtn, this.driver, "Done button");
			WebActionUtil.waitTillPageLoad("edit revenue and amount to be paid", this.driver, "LoadBoard", 120);
		}
		Thread.sleep(2000);
	}

	public void updateQuoteAmount(WebDriver driver, String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.waitTillPageLoad("Update quote amount", this.driver, "LoadBoard", 120);
			WebActionUtil.clickElement(this.editIcon, this.driver, "Edit button");
			WebActionUtil.actionClickAndClear(this.qouteAmount, driver, "qoute status");
			WebActionUtil.type(this.qouteAmount, String.valueOf(Integer.parseInt(sData[BaseLib.gv.atpCount]) + 50),
					"Edit Quote Amount", this.driver);
			WebActionUtil.clickElement(this.clickHub(driver, sData[BaseLib.gv.hcount]), this.driver, "click on hub");
			WebActionUtil.waitTillPageLoad("Update quote amount", this.driver, "LoadBoard", 120);

		}
	}

	public void ApproveQuoteStatus(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("apporved quote status", this.driver, "LoadBoard", 300);
		WebActionUtil.waitForVisiblityofElement("(//*[text()='Quote Status']/..//div[2])[2]", 120, driver); 
		WebActionUtil.clickElement(this.acceptQuoteStatus, this.driver, "Accept Qoute Status");
		WebActionUtil.waitTillPageLoad("apporved quote status", this.driver, "LoadBoard", 300);

	}

	public void verifyAmount(WebDriver driver, String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.clickElement(
					this.getListingEdit(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp),
					driver, "Edit button");
			WebActionUtil.verifyEqualsText("verifying upated revenue amount", this.RevenueTextBox.getAttribute("value"),
					String.valueOf(Integer.parseInt(sData[BaseLib.gv.rCount]) + 50));
			WebActionUtil.verifyEqualsText("verifying updated amount", this.amountToBePaidTextBox.getAttribute("value"),
					String.valueOf(Integer.parseInt(sData[BaseLib.gv.atpCount]) + 50));
			WebActionUtil.clickElement(this.closeBtn, this.driver, "close button");
			WebActionUtil.waitTillPageLoad("verified updated amount", this.driver, "LoadBoard", 120);
		}
	}

	public void verifyUpdtdStatusAmt(WebDriver driver, String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.verifyEqualsText("Verify updated quotes amount", this.qouteAmount.getAttribute("value"),
					String.valueOf(Integer.parseInt(sData[BaseLib.gv.atpCount]) + 50));
		}
	}

	public void deleteListing(WebDriver driver, String filepath, String sheet, String timeStamp) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.scrollIntoViewAndClick(this.driver,
					this.getListingDelete(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount], timeStamp));
			WebActionUtil.waitTillPageLoad("delete the listing", this.driver, "LoadBoard", 120);
			WebActionUtil.clickElement(this.confirmBtn, this.driver, "confirm btn");
			WebActionUtil.waitTillPageLoad("confirm button", this.driver, "LoadBoard", 120);
		}
	}
	
	public void deleteDupListing(WebDriver driver, String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.scrollIntoViewAndClick(this.driver,
					this.getListingDeletewithoutTime(this.driver, sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount]));
			WebActionUtil.waitTillPageLoad("delete the listing", this.driver, "LoadBoard", 120);
			WebActionUtil.clickElement(this.confirmBtn, this.driver, "confirm btn");
			WebActionUtil.waitTillPageLoad("confirm button", this.driver, "LoadBoard", 120);
			Thread.sleep(5000);
		}
	}

	public void DismissQuoteStatus(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Dismiss quote status", this.driver, "LoadBoard", 120);
		WebActionUtil.clickElement(this.dismissQuoteStatus, this.driver, "Dismiss Qoute Status");
		WebActionUtil.waitTillPageLoad("Dismiss quote status", this.driver, "LoadBoard", 120);
	}

	public void ApproveOPM(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		WebActionUtil.waitForVisiblityofElement("(//*[text()='OPM Approval']/..//div[2])[2]", 300, driver);
		WebActionUtil.waitTillPageLoad("OPM approval status", this.driver, "LoadBoard", 300);
		WebActionUtil.clickElement(this.acceptOPMApproval, this.driver, "OPM approval Status");
		WebActionUtil.waitTillPageLoad("OPM approval status", this.driver, "LoadBoard", 300);
		Thread.sleep(3000);
	}

	public void DismissOPMApproval(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		WebActionUtil.clickElement(this.dismissOPMApproval, this.driver, "Dismiss OPM approval");
		WebActionUtil.waitTillPageLoad("Dismiss OPM approval", this.driver, "LoadBoard", 120);
		Thread.sleep(2000);
	}

	public void refreshPage(WebDriver driver) throws Exception {
		WebActionUtil.reloadPage(driver);
		WebActionUtil.waitTillPageLoad("reloading the page", this.driver, "LoadBoard", 120);

	}

	public void verifyApprovedListing(WebDriver driver, String filepath, String sheet, String timeStamp)
			throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil
					.verifyEqualsText(
							"Approved count number", this.getListingApproved(this.driver, sData[BaseLib.gv.cCount],
									sData[BaseLib.gv.hcount], timeStamp, sData[BaseLib.gv.atpCount]).getText(),
							"1/1");
		}
	}

	public void convertBooking(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickByJavaSriptExecutor(this.convertBooking, this.driver, "convert Booking");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);

	}

	public void cancelBooking(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickByJavaSriptExecutor(this.cancelBooking, this.driver, "cancel Booking");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
	}

	public void goToCrtdBookingList(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.pastBtn, this.driver, "past button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.allListingsBtn, this.driver, "all button");
		WebActionUtil.clickElement(this.bookingCreated(this.driver, "Booking Created"), this.driver, "Booking button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);

	}

	public void goToRemovedList(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.pastBtn, this.driver, "past button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.allListingsBtn, this.driver, "all button");
		WebActionUtil.clickElement(this.bookingCreated(this.driver, "Removed Listings"), this.driver, "Booking button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);

	}

	public void verifyBookingCreated(WebDriver driver) throws Exception {
//<<<<<<< HEAD
//		WebActionUtil.isElementDisplayed(this.bookingCreatedtext, driver, "Booking created", 120);
//=======
//		WebActionUtil.isElementDisplayed(this.bookingCreatedtext, driver, "Booking created", 300);
//>>>>>>> branch 'master' of https://gitlab.com/qa_lt/automation.git
//	}
//
//	public void verifyBookingRemoved(WebDriver driver) throws Exception {
//<<<<<<< HEAD
//		WebActionUtil.isElementDisplayed(this.bookingRemovedtext, driver, "Booking removed", 120);
//=======
//		WebActionUtil.isElementDisplayed(this.bookingRemovedtext, driver, "Booking removed", 300);
//>>>>>>> branch 'master' of https://gitlab.com/qa_lt/automation.git
	}

	public void allListings(WebDriver driver, String status) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.allListingsBtn, this.driver, "all button");
		WebActionUtil.clickElement(this.bookingCreated(this.driver, status), this.driver, "status of listings");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
	}

	public void selectFilterByDate(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(this.filtersBtn, this.driver, "filters button");
		WebActionUtil.clickElement(this.fromDateFilters, this.driver, "From Date");
		WebActionUtil.clickElement(this.fromDateSelected, this.driver, "From Date 1");
		WebActionUtil.clickElement(this.toDateFilters, this.driver, "To Date");
		WebActionUtil.clickElement(this.toDateSelected, this.driver, "selected today");
		WebActionUtil.clickElement(this.applyBtn, this.driver, "apply button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
	}

	public void selectFilterByHubAndAraNme(WebDriver driver, String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.clickElement(this.filtersBtn, this.driver, "filters button");
			try {
				WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
				WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name", this.driver);
			} catch (Exception e) {
				WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
				WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name", this.driver);
			}
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.waitTillPageLoad("Area name ", this.driver, " Listing page", 300);
			WebActionUtil.clickElement(this.searchAreaField, this.driver, "Mangalore");
			WebActionUtil.type(this.searchAreaInputfield, sData[BaseLib.gv.aNCount], " Area name", this.driver);
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.clickElement(this.applyBtn, this.driver, "apply button");
			WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		}
	}

	public void selectPstFilterByHubAndAraNme(WebDriver driver, String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
			WebActionUtil.clickElement(this.pastBtn, this.driver, "past button");
			WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
			WebActionUtil.clickElement(this.filtersBtn, this.driver, "filters button");
			try {
				WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
				WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name", this.driver);
			} catch (Exception e) {
				WebActionUtil.clickElement(this.searchClientfield, this.driver, sData[BaseLib.gv.cCount]);
				WebActionUtil.type(this.searchClientInputfield, sData[BaseLib.gv.cCount], " Client name", this.driver);
			}
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.waitTillPageLoad("Area name ", this.driver, " Listing page", 300);
			WebActionUtil.clickElement(this.searchAreaField, this.driver, "Mangalore");
			WebActionUtil.type(this.searchAreaInputfield, sData[BaseLib.gv.aNCount], " Client name", this.driver);
			WebActionUtil.waitTillLoaderFinishes("client name ", this.driver, " Listing popup", 300);
			WebActionUtil.actionClick(this.searchResult, this.driver, " Client name");
			WebActionUtil.clickElement(this.applyBtn, this.driver, "apply button");
			WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		}
	}

	public void pastButton(WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
		WebActionUtil.clickElement(this.pastBtn, this.driver, "Past button");
		WebActionUtil.waitTillPageLoad("Loadboard", this.driver, "Listing page", 300);
	}
}