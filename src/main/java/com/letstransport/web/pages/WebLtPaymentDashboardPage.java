package com.letstransport.web.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class WebLtPaymentDashboardPage {
	WebDriver driver;

	public WebLtPaymentDashboardPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@class='username-box']/input")
	private WebElement userInputBox;

	@FindBy(xpath = "//*[@class='password-box']/input")
	private WebElement passInputBox;

	@FindBy(xpath = "//*[@class='sign-in-button']")
	private WebElement signInBtn;
	
	 
	
	
	

	public void pricingPage_Login(String Uname, String Pass, WebDriver driver) throws Exception {
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.type(this.userInputBox, Uname, "UserName", driver);
		WebActionUtil.type(this.passInputBox, Pass, "Password", driver);
		WebActionUtil.clickElement(this.signInBtn, driver, "Next Button");
		WebActionUtil.waitTillPaymentPageLoad("Sign In button", driver, "pricing page", 120);
	}
public void validate_selectReject_DriverPaymentPayout(WebDriver driver,String createdIntoVerified, String adhocVechileRate) throws Exception{
		
		WebActionUtil.scrollIntoView(driver, getPayOutChkBox(driver, createdIntoVerified, adhocVechileRate));
		WebActionUtil.clickElement(getRejectChkBox(driver, createdIntoVerified, adhocVechileRate), driver, "RejectCheckBox");
		WebActionUtil.clickElement(submitBtn, driver, "Submit Btn");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		String popupmsg = pendingPopUp.getText();
		System.out.println(popupmsg);
		boolean PopMsg = popupmsg.contains("Your request has been");
		System.out.println(PopMsg);
	}
	public void validate_PendingPayout_FotTodayEntodBooking(WebDriver driver,String vehNo, String adhocVechileRate) throws Exception{
		WebActionUtil.clickElement(this.pendingPayout, driver, "Pending payout");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		WebActionUtil.actionClick(dateFromTextField, driver, "Date From TextField");
		Thread.sleep(2000);
		WebActionUtil.clickOnCurrentDate(calenderdate,"/", 0);
		Thread.sleep(2000);
		WebActionUtil.clickElement(dateToTextField, driver, "Date To TextField");
		Thread.sleep(500);
		WebActionUtil.clickOnCurrentDate(calenderdate,"/", 0);
		WebActionUtil.clickElement(applyBtn, driver, "Apply Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.scrollIntoView(driver, getRaised_VehNo(driver, vehNo));
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, vehNo), driver, vehNo);
		String clientName = getClientName(driver, vehNo).getText();
		String cityName = getCityName(driver, vehNo).getText();
		String amountgenerated = getAmount(driver, vehNo).getText();
		String validateReason = getValidateReason(driver, vehNo).getText();
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, clientName), driver, "Bkl");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, cityName), driver, "Bangalore");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, amountgenerated), driver,adhocVechileRate);
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, validateReason), driver, "Valid");
		String payout = getPayout(driver, vehNo).getText();
		 String reject = getReject(driver, vehNo).getText();
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,payout), driver, "Payout");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,reject), driver, "Reject");
		
		
		
	}
	@FindBy(xpath="//div[@class='popup-message-container']/p")
	private WebElement pendingPopUp;

	public WebElement getClientName(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[1]"));
		
				}
	public WebElement getCityName(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[2]"));
		
				}
	public WebElement getAmount(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[3]"));
		
				}
	public WebElement getValidateReason(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[4]"));
		
				}
	//@FindBy(xpath="//span[text()='YY08YY0817']/../following-sibling::div[5]/div/label[1]/input/following-sibling::span")
	
	public WebElement getPayOutChkBox(WebDriver driver, String vecNo,String amount) {
	
		return driver.findElement(By.xpath("//span[text()=\'"+vecNo+ "\']/../following-sibling::div[3]/span[text()=\'"+amount+ "\']/../following-sibling::div[2]/div/label[1]/input/following-sibling::span"));
	}
	public WebElement getRejectChkBox(WebDriver driver, String vecNo,String amount) {
		
		return driver.findElement(By.xpath("//span[text()=\'"+vecNo+ "\']/../following-sibling::div[3]/span[text()=\'"+amount+ "\']/../following-sibling::div[2]/div/label[2]/input/following-sibling::span"));
	}
	@FindBy(xpath="//p[text()='Amount Selected']/../child::h3")
	private WebElement amountSelected;
	@FindBy(xpath="//div[@class='pm-footer']/div[text()='Submit']")
	private WebElement submitBtn;
	@FindBy(xpath="//p[text()='Utilized Budget']/../child::h3")
	private WebElement utilizeBudget;
	@FindBy(xpath="//p[text()='Available Budget']/../child::h3")
	private WebElement availableBudget;
	
	public void select_payoutDriverPayment(WebDriver driver,String createdIntoVerified, String adhocVechileRate) throws Exception{
		String availableBgtBeforePayout = availableBudget.getText();
		
		WebActionUtil.scrollIntoView(driver, getPayOutChkBox(driver, createdIntoVerified, adhocVechileRate));
		WebActionUtil.clickElement(getPayOutChkBox(driver, createdIntoVerified, adhocVechileRate), driver, "PayoutCheckBox");
		String amountsel = amountSelected.getText();
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,amountsel), driver, adhocVechileRate);
		WebActionUtil.clickElement(submitBtn, driver, "Submit Btn");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		String popupmsg = pendingPopUp.getText();
		System.out.println(popupmsg);
		boolean PopMsg = popupmsg.contains("Your request has been");
		System.out.println(PopMsg);
		
		
		
	}
	
	
	//configure validation
	
	@FindBy(xpath = "//*[text()='Pending Payout']")
	private WebElement pendingPayout;

	@FindBy(xpath = "//*[text()='Select from date']//*[@class='calendar-icon']")
	private WebElement frmDate;


	@FindBy(xpath = "//*[text()='Select to date']//*[@class='calendar-icon']")
	private WebElement toDate;
	
	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(By.xpath("//*[@class='calendar']//*[text()='"+date+"']"));
		
				}
	
	public WebElement selectDate(WebDriver driver, int date) {
		return driver.findElement(By.xpath("//*[@class='calendar']//*[text()='"+date+"']"));
		
				}
	public WebElement verifyElementbytext(WebDriver driver, String data) {
		return driver.findElement(By.xpath("//*[text()=\'"+data + "\']"));
	}
	
	@FindBy(xpath = "//*[text()='Select city...']")
	private WebElement drpDownCity;
	
	@FindBy(xpath = "//*[text()='Apply']")
	private WebElement btnApply;
	
	
	public WebElement selCity(WebDriver driver, String city) {
		return driver.findElement(By.xpath("//*[text()=\'"+city+ "\']"));
	}
	
	@FindBy(xpath = "//*[@id='pendingPayout0']")
	private WebElement chkbox;
	
	@FindBy(xpath = "//*[@id='pendingPayout1']")
	private WebElement chkbox1;
	
	
	
	public void navigate_ValidatePendingPayout(WebDriver driver) throws IOException{
		WebActionUtil.clickElement(this.pendingPayout, driver, "Pending payout");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
				
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Select from date"), driver, "Select from date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Select to date"), driver, "Select from date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Clear All"), driver, "Select from date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Total Budget"), driver, "Select from date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Available Budget"), driver, "Select from date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Utilized Budget"), driver, "Select from date");
				
		
		
	}
	@FindBy(xpath="//div[@class='pm-content']/descendant::div[2][@class='filter-input date-input   purple-theme' and text()='Select from date']")
	private WebElement dateFromTextField;
	@FindBy(xpath="//div[@class='pm-content']/child::div[2]/div[@class='filter-input date-input   purple-theme' and text()='Select to date']")
	private WebElement dateToTextField;
	@FindBy(xpath="//div[@class='pm-content']/child::div[4][@class='pm-button pm-white pm-round' and text()='Apply']")
	private WebElement applyBtn;
	@FindBy(xpath = "//div[@id='content_calender']/descendant::div/span[@class='number-span  selected-block  ']")
	private WebElement calenderdate;
	
	public WebElement getRaised_VehNo(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']"));
		
				}

	
	@FindBy(xpath="//span[@class='number-span  selected-block  ']/../preceding-sibling::div[1]/span[@class='number-span   ']")
	private WebElement previousDay;
	public  void selectAndClickdate(WebDriver driver) throws Exception{
		DateFormat df = new SimpleDateFormat("dd/MM/yy");
	     Calendar calender = Calendar.getInstance();
	     calender.add(Calendar.DATE, -1);
	     String updated = df.format(calender.getTime());
	     String[] sysDate = updated.split("/");
	     String systemdate = sysDate[0];
	     String starting = "0";
		 String changedate = starting.concat(systemdate);
	     if(previousDay.getText().equals(systemdate)){
	    	 WebActionUtil.clickElement(previousDay, driver, "Previous Date");
	     }
	    	 else if (changedate.equals(changedate)) {
	 			WebActionUtil.clickElement(previousDay, driver, "Previous Date");

	 		}

	     }
	public WebElement getPayout(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[5]/div/label[1][@class='pm-radio-btn pm-radio-green ']"));
		
				}
	public WebElement getReject(WebDriver driver, String vehNo) {
		return driver.findElement(By.xpath("//span[text()=\'"+vehNo + "\']/../following-sibling::div[5]/div/label[2][@class='pm-radio-btn pm-radio-red ']"));
		
				}
	
	public void validate_PendingPayout_FotYesterdayEntodBooking(WebDriver driver,String vehNo, String adhocVechileRate) throws Exception{
		WebActionUtil.clickElement(this.pendingPayout, driver, "Pending payout");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		Thread.sleep(500);
		WebActionUtil.clickElement(dateFromTextField, driver, "Date From TextField");
		Thread.sleep(2000);
		selectAndClickdate(driver);
	    Thread.sleep(500);
		WebActionUtil.clickElement(dateToTextField, driver, "Date To TextField");
		Thread.sleep(500);
		selectAndClickdate(driver);
		Thread.sleep(500);
		WebActionUtil.clickElement(applyBtn, driver, "Apply Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.scrollIntoView(driver, getRaised_VehNo(driver, vehNo));
		String clientName = getClientName(driver, vehNo).getText();
		String cityName = getCityName(driver, vehNo).getText();
		String amountgenerated = getAmount(driver, vehNo).getText();
		String validateReason = getValidateReason(driver, vehNo).getText();
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, clientName), driver, "Bkl");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, cityName), driver, "Bangalore");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, amountgenerated), driver,adhocVechileRate);
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver, validateReason), driver, "Valid");
		String payout = getPayout(driver, vehNo).getText();
		 String reject = getReject(driver, vehNo).getText();
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,payout), driver, "Payout");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,reject), driver, "Reject");
		
	}
	
	
	public void selectdates(WebDriver driver ,int timeStamp, String city ) throws Exception{
		
		WebActionUtil.clickElement(frmDate, driver, "Select from date");
		
		WebActionUtil.clickElement(selectDate(driver,timeStamp), driver, " Select today");
		
		
		WebActionUtil.clickElement(toDate, driver, "Select to date");
		
		WebActionUtil.clickElement(selectDate(driver,timeStamp), driver, " Select today");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		
		
		WebActionUtil.clickElement(btnApply, driver, "Apply");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, driver);
		
	}
	
	
	public void verifyTransactionTable( WebDriver driver) throws IOException{ 
	
	
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Booking date"), driver, "Transaction date");
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Request Date"), driver, "Request Date");
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Vehicle number"), driver, "Vehicle number");
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Client"), driver, "Client");
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Amount"), driver, "Amount");
	WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,"Validation Reason"), driver, "ValidationReason");
	WebActionUtil.verifyElementIsDisplayed(chkbox, driver, "Checkbox");
	WebActionUtil.verifyElementIsDisplayed(chkbox1, driver, "Checkbox");
	
	}
	                                         
	public void verifyBookingPaymentRequested(WebDriver driver, String VehNum, String filepath, String sheet, String testcase ) throws IOException{
		
		
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		
		
		
//		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,transdate), driver, "Transaction date");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,VehNum), driver, "Vehicle Number");
		WebActionUtil.verifyElementIsDisplayed(verifyElementbytext(driver,sData[BaseLib.gv.cnCount]), driver, "client");
		
		
	}
	
	
}


