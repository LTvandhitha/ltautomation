package com.letstransport.web.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.init.InitializePages;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class WebTemplatePage {
	WebDriver driver;

	public WebTemplatePage(WebDriver driver) {
		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//div[@class='template-select options-child highlight']/div[1]/div[1]")
	private WebElement selectTemplate;

	public void _clickOnSelectTemplate() throws Exception {
		
		WebActionUtil.waitForElement(selectTemplate, driver, "Selecttemplate", 20);
		//Thread.sleep(3000);
		// WebActionUtil.clickElement(selectTemplate, driver, "Selecttemplate");
		Thread.sleep(6000);
		WebActionUtil.actionClick(selectTemplate, driver, "selectTemplate");
		Thread.sleep(3000);
	}

	@FindBy(xpath = "//li[text()='LONEE']")
	private WebElement templateText;

	public void _clickOnTemplateText() throws Exception {
		WebActionUtil.waitForElement(templateText, driver, "TemplateText", 50);
		Thread.sleep(1000);
		WebActionUtil.actionClick(templateText, driver, "TemplateText");
		Thread.sleep(2000);
	}

	@FindBy(xpath = "//div[@class='booking-card-container']/descendant::div/span[@class='details-span vehicle-span']")
	private List<WebElement> vehicleno;

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[2]")
	private WebElement vehicleNumber;
	@FindBy(xpath = "//div[@class='row']/descendant::div[2]/input")
	private WebElement inputVehicleNo;

	public void _clickAndEnterVehicleNo(String entervecno) throws Exception {
		WebActionUtil.waitForElement(vehicleNumber, driver, "VehicleNo", 20);
		WebActionUtil.actionClick(vehicleNumber, driver, "Vehicle");
		WebActionUtil.pressEnterKey();
		Thread.sleep(1000);
		WebActionUtil.clearAndTypeEnter(inputVehicleNo, entervecno, "Vehicle", driver);
	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[4]")
	private WebElement openingKms;
	@FindBy(xpath = "//div[@class='cell selected editing']/input[1]")
	private WebElement inputOpeningKms;

	public void _clickOnOpeningKms() throws Exception {
		WebActionUtil.waitForElement(openingKms, driver, "OpeningKMS", 20);
		WebActionUtil.actionClick(openingKms, driver, "OpeningKMS");
		WebActionUtil.pressEnterKey();
		WebActionUtil.clearAndTypeEnter(inputOpeningKms, "200", "OpeningKMS", driver);

	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[5]")
	private WebElement closingKms;
	@FindBy(xpath = "//div[@class='cell selected editing']/input[1]")
	private WebElement inputClosingKms;

	public void _clickOnClosingKms() throws Exception {
		WebActionUtil.waitForElement(closingKms, driver, "ClosingKMS", 20);
		WebActionUtil.actionClick(closingKms, driver, "closingKMS");
		WebActionUtil.pressEnterKey();
		// WebActionUtil.clearAndTypeEnter(inputClosingKms, "400", "ClosingKMS",
		// driver);
		WebActionUtil.clearAndTypeEnter(inputClosingKms, GenericLib.closingKmss, "ClosingKMS", driver);
	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[6]")
	private WebElement openingTime;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input hour']")
	private WebElement hourTime;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input min']")
	private WebElement minTime;

	public void _clickOnOpeningTime() throws Exception {
		WebActionUtil.waitForElement(openingTime, driver, "ClosingKMS", 20);
		WebActionUtil.actionClick(openingTime, driver, "ClosingKMS");
		WebActionUtil.pressEnterKey();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, 1);
		String newTime = sdf.format(cal.getTime());
		String[] timesplit = newTime.split(":");
		String part01 = timesplit[0];
		String part02 = timesplit[1];
		WebActionUtil.clearAndTypeEnter(hourTime, part01, "Hour", driver);
		WebActionUtil.clearAndTypeEnter(minTime, part02, "Minute", driver);
	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[7]")
	private WebElement closingTime;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[1]")
	private WebElement inputDateTextField01;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[2]")
	private WebElement inputDateTextField02;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[3]")
	private WebElement inputDateTextField03;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input hour']")
	private WebElement hourTime1;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input min']")
	private WebElement minTime1;

	public void _clickOnClosingTime() throws Exception {
		WebActionUtil.waitForElement(closingTime, driver, "ClosingTime", 20);
		WebActionUtil.actionClick(closingTime, driver, "ClosingTime");
		WebActionUtil.pressEnterKey();
		String todaydate = WebActionUtil.getSystemDate();
		DateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println("Format defined");
		Date date2 = new Date();
		System.out.println("Date object creation");
		String today = dateFormat2.format(date2);
		String[] parts = today.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		String part3 = parts[2];
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, 3);
		String newTime = sdf.format(cal.getTime());
		String[] timesplit = newTime.split(":");
		String part01 = timesplit[0];
		String part02 = timesplit[1];
		WebActionUtil.clearAndTypeEnter(inputDateTextField1, part1, "DD", driver);
		WebActionUtil.clearAndTypeEnter(inputDateTextField2, part2, "MM", driver);
		WebActionUtil.clearAndTypeEnter(inputDateTextField3, part3, "yyyy", driver);
		WebActionUtil.clearAndTypeEnter(hourTime1, part01, "Hour", driver);
		WebActionUtil.clearAndTypeEnter(minTime1, part02, "Minute", driver);
		WebActionUtil.pressEnterKey();
	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[8]")
	private WebElement booking;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input hour']")
	private WebElement hourTimeBooking;
	@FindBy(xpath = "//div[@class='date-input-editor']/div/input[@class='date-input min']")
	private WebElement minTimeBooking;

	public void _clickOnBooking() throws Exception {
		WebActionUtil.waitForElement(booking, driver, "Booking", 20);
		WebActionUtil.actionClick(booking, driver, "Booking");
		WebActionUtil.pressEnterKey();
		String time = GenericLib.entertime;
		String[] timesplit = time.split(":");
		String part1 = timesplit[0];
		String part2 = timesplit[1];
		WebActionUtil.clearAndTypeEnter(hourTimeBooking, part1, "BookingHour", driver);
		WebActionUtil.clearAndTypeEnter(minTimeBooking, part2, "BookingMinute", driver);
		WebActionUtil.pressEnterKey();

	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[3]")
	private WebElement dateTextField;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[1]")
	private WebElement inputDateTextField1;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[2]")
	private WebElement inputDateTextField2;
	@FindBy(xpath = "//div[@class='date-input-box date']/input[3]")
	private WebElement inputDateTextField3;

	public void _clickAndEnterDate() throws Exception {
		WebActionUtil.waitForElement(dateTextField, driver, "Date", 20);
		WebActionUtil.actionClick(dateTextField, driver, "Date");
		WebActionUtil.pressEnterKey();
		String todaydate = WebActionUtil.getSystemDate();
		DateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
		Date date2 = new Date();
		String today = dateFormat2.format(date2);
		String[] parts = today.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		String part3 = parts[2];
		WebActionUtil.clearAndTypeEnter(inputDateTextField1, part1, "Date", driver);
		Thread.sleep(500);
		WebActionUtil.clearAndTypeEnter(inputDateTextField2, part2, "Date", driver);
		Thread.sleep(500);
		WebActionUtil.clearAndTypeEnter(inputDateTextField3, part3, "Date", driver);
		WebActionUtil.pressEnterKey();
		Thread.sleep(2000);

	}

	@FindBy(xpath = "//div[text()='Freeze']")
	private WebElement freezeButton;

	public void _ClickOnFreezeButton() throws Exception {
		WebActionUtil.waitForElement(freezeButton, driver, "Freeze", 20);
		WebActionUtil.actionClick(freezeButton, driver, "Freeze");
		Thread.sleep(1000);
	}

	@FindBy(xpath = "//div[@class='button-panel']/div[3][text()='Save']")
	private WebElement saveButton;

	public void _clickOnSaveButton() throws Exception {
		WebActionUtil.waitForElement(saveButton, driver, "Save", 20);
		WebActionUtil.actionClick(saveButton, driver, "Save");
		Thread.sleep(1000);

	}

	@FindBy(xpath = "//div[@class='options-footer']/div[text()='Fetch Logsheets']")
	private WebElement fetchLogSheet;

	@FindBy(xpath = "//div[@class='action-buttons']/div[2][1]")
	private WebElement editinfo;

	public void _getTextOfEditInfo() throws Exception {
		WebActionUtil.waitForElement(editinfo, driver, "EditInfo", 20);
		WebActionUtil.gettext(editinfo, driver, "EditInfo");
		Thread.sleep(1000);
	}

	public void _EnterLogSheetInfo(WebDriver driver, String entervecno) throws Exception {
		InitializePages ip = new InitializePages(driver);
		ip.o_WebLogSheetPage._UploadPOD_ENTOD(driver);
		ip.o_WebLogSheetPage._clickOnEnterinfo();
		_clickOnSelectTemplate();
		Thread.sleep(1000);
		_clickOnTemplateText();
		Thread.sleep(1000);
		_clickAndEnterVehicleNo(entervecno);
		_clickAndEnterDate();
		_clickOnOpeningKms();
		_clickOnClosingKms();
		_clickOnOpeningTime();
		_clickOnClosingTime();
		_clickOnBooking();
		_ClickOnFreezeButton();
		Thread.sleep(1000);
		_clickOnSaveButton();
		Thread.sleep(3000);

	}
	@FindBy(xpath="//div[@id='react-select-7--value']/div[text()='Select...']")
	private WebElement select;
	@FindBy(xpath="//div[text()='Edit Info']")
	private WebElement edInfo;
	@FindBy(xpath="//div[text()='Delete']")
	private WebElement delButton;
	@FindBy(xpath="//div[@class='back-home-button']/a[text()='Back To Home']")
	private WebElement backToHomeBtn;
	

	public void _VerifyEnterLogSheetInfo(WebDriver driver, String entervecno) throws Exception {
        InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		wdInit.o_WebLogSheetPage._clickOnEnterinfo();
		_clickOnSelectTemplate();
		Thread.sleep(1000);
		_clickOnTemplateText();
		Thread.sleep(12000);
		_clickAndEnterVehicleNo(entervecno);
		_clickAndEnterDate();
		_clickOnOpeningKms();
		_clickOnClosingKms();
		_clickOnOpeningTime();
		_clickOnClosingTime();
		_clickOnBooking();
		_ClickOnFreezeButton();
		
		
		
		Thread.sleep(1000);
		_clickOnSaveButton();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		String text = editinfo.getText();
		WebActionUtil.verifyContainsText(text, GenericLib.editinfobutton);
		}
	

	@FindBy(xpath = "//div[@class='unmap-button']")
	private WebElement deleteBtn;
	@FindBy(xpath = "//div[@class='back-button']")
	private WebElement backBtn;
	@FindBy(xpath = "//div[@class='count-parent unmapped']")
	private WebElement unmapped;
	@FindBy(xpath = "//div[@class='vehicle-icon vehicle-info scheduled']")
	private List<WebElement> unmappedvechileNo;

	public void _deleteUnmappedLogSheet(WebDriver driver, String entervecno) throws Exception {
		InitializePages ip = new InitializePages(driver);
		ip.o_WebLogSheetPage._UploadPOD_ENTOD(driver);
		ip.o_WebLogSheetPage._clickOnEnterinfo();
		_clickOnSelectTemplate();
		Thread.sleep(1000);
		_clickOnTemplateText();
		Thread.sleep(1000);
		_clickAndEnterVehicleNo(entervecno);
		_clickAndEnterDate();
		_clickOnOpeningKms();
		_clickOnClosingKms();
		_clickOnOpeningTime();
		_clickOnClosingTime();
		_clickOnBooking();
		// _ClickOnFreezeButton();
		WebActionUtil.waitForElement(deleteBtn, driver, "wait for delete Button", 500);
		WebActionUtil.actionClick(deleteBtn, driver, "click on delete button");
		WebActionUtil.waitTillPageLoad(driver, 500);
		driver.switchTo().alert().accept();
		Thread.sleep(5000);
		WebActionUtil.actionClick(backBtn, driver, "click on back Button");
		WebActionUtil.clickElement(unmapped, driver, "click on unmapped");
		for (WebElement we : unmappedvechileNo) {
			WebActionUtil.verifyNotEqualsText("After deleting logsheet,no longer it will be visible in unmapped",
					entervecno, we.getText());
		}

	}
}