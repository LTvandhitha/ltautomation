package com.letstransport.web.pages;

import java.io.IOException;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.WebActionUtil;

public class WebVehiclesPage {
	WebDriver driver;

	public WebVehiclesPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath ="//*[@class='verification-tab-item ' and text()='Vehicle']")
	private WebElement vehTab;

	public WebElement clkVehTab() {
		return vehTab;
	}
	public WebElement getVecNo(WebDriver driver, String vehnum) {
		return driver.findElement(By.xpath("//div[text()='" + vehnum + "']"));
	}

	

	@FindBy(xpath ="//*[@class='verification-tab-item selected' and text()='Vehicle']")
	private WebElement vehTabSel;

	public WebElement clkVehTabSel() {
		return vehTabSel;
	}
	
	
	@FindBy(xpath ="//*[@class='verification-tab-item ' and text()='Verified']")
	private WebElement verTab;

	public WebElement clkVerTab() {
		return verTab;
	}
	
	

	
	@FindBy(xpath ="//*[@class='verification-tab-item ' and text()='Created']")
	private WebElement CreatedTab;

	public WebElement ClkCreateTab() {
		return CreatedTab;
	}
	
	
	@FindBy(xpath ="//*[@class='verification-tab-item selected' and text()='Uploaded']")
	private WebElement uploadedTab;

	public WebElement ClkuploadTab() {
		return uploadedTab;
	}
	
	@FindBy(xpath ="//*[@class='verification-list']/div[@class='verification-card-data disabled'][1]")
	private WebElement fstVeh;

	public WebElement verifyfstVeh() {
		return fstVeh;
	}
	
	@FindBy(xpath ="//*[@class='verification-list']/div[@class='verification-card-data '][1]")
	private WebElement fstuploadVeh;

	public WebElement verifyfstupld() {
		return fstuploadVeh;
	}
	@FindBy(xpath="//div[text()='FQLC Supervisor']")
	private WebElement fqlcSupervisor;
	
	@FindBy(xpath ="//*[text()='Verification Dashboard']")
	private WebElement verDash;

	public WebElement clkverDash() {
		return verDash;
	}	
	
	@FindBy(xpath ="//*[text()='Delete Vehicle']")
	private WebElement delBtn;

	public WebElement clkdelBtn() {
		return delBtn;
	}
	
	
	@FindBy(xpath ="//*[@class='verification-button accept-button ' and text()='Delete vehicle']")
	private WebElement delBtn2;

	public WebElement clkdelBtn2() {
		return delBtn2;
	}
	
	
	
	
	
	@FindBy(xpath ="//*[@class='verification-list']/div[@class='verification-card-data disabled'][1]//*[@class='delete-details']")
	private WebElement delveh;

	public WebElement clkDelVeh() {
		return delveh;
	}
	
	
	@FindBy(xpath ="//*[@class='radio-button-display' and text()='Invalid vehicle']")


//	@FindBy(xpath="//*[@class='radio-circle']/..//*[text()='Invalid vehicle']")

	private WebElement delvehradio;

	public WebElement clkDelVehRadio() {
		return delvehradio;
	}
	
	@FindBy(xpath ="//*[contains(text(),'vehicle Deleted')]")
	private WebElement verifyDel;

	public WebElement verifydeleted() {
		return verifyDel;
	}
	
	

//	@FindBy(xpath ="//*[@class='verification-button reject-button' and text()='Reject Document']")
	
	@FindBy(xpath="//*[contains(text(),'Reject Doc')]")
	private WebElement rejDoc;

	public WebElement clkrejVeh() {
		return rejDoc;
	}
	
	
	
	@FindBy(xpath ="//*[@class='radio-button-display' and text()='Expired document']")
	private WebElement radioBtn;

	public WebElement clkradioBtn() {
		return radioBtn;
	}
	
	@FindBy(xpath ="//*[text()='Close']")
	private WebElement close;

	public WebElement clkClose() {
		return close;
	}
	
	@FindBy(xpath ="//*[@class='verification-tab-item ' and text()='Rejected']")
	private WebElement rejTab;

	public WebElement clkrejTab() {
		return rejTab;
	}
	
	
	@FindBy(xpath ="//*[@class='verification-tab-item ' and text()='Submitted']")
	private WebElement subTab;

	public WebElement clksubTab() {
		return subTab;
	}
	
	@FindBy(xpath ="//*[text()='Submitted']")
	private WebElement submitTab;
	
	

	public WebElement clksumbitTab() {
		return submitTab;
	}
	
	public WebElement selectVehicle(WebDriver driver, String vehnum) {
	    return driver.findElement(By.xpath("//*[text()='"+vehnum+"']"));
	}
	
	
	@FindBy(xpath ="//*[@class='radio-circle']/..//*[text()='Invalid vehicle']")
	private WebElement delreason;

	public WebElement clkdelreason() {
		return delreason;
	}
	
	
	@FindBy(xpath ="//*[text()='Vehicle Number on RC']/..//*[@type='text']")
	private WebElement vehnumRC;

	public WebElement entervehnumRC() {
		return vehnumRC;
	}
	
	
	
	
	
	
	@FindBy(xpath ="//*[text()='Name on FC']/..//*[@type='text']")
	private WebElement vehFC;

	public WebElement entervehFC() {
		return vehFC;
	}
	
	@FindBy(xpath ="//*[text()='Name on insurance']/..//*[@type='text']")
	private WebElement insuranceName;

	public WebElement enterInsurance() {
		return insuranceName;
	}
	
	
	@FindBy(xpath ="//*[text()='Insurance Company']/..//*[@type='text']")
	private WebElement insuranceComp;

	public WebElement enterInsuranceComp() {
		return insuranceComp;
	}
	
	
	@FindBy(xpath ="//*[text()='Insurance Amount']/..//*[@type='text']")
	private WebElement insuranceAmt;

	public WebElement enterInsuranceAmt() {
		return insuranceAmt;
	}
	
//	@FindBy(xpath ="//*[text()='open']/..//*[@class='radio-button-display']/../*[@class='radio-circle']")
	@FindBy(xpath="//*[@class='radio-circle']/..//*[text()='tarpaulin']")
	private WebElement openRadiobtn;

	public WebElement clkopenRadiobtn() {
		return openRadiobtn;
	}
	
	@FindBy(xpath="//div[text()='FQLC Supervisor']")
	private WebElement fqlcsupervisor;
	
	public void fqlcSupervisor(WebDriver driver) throws Exception{
		WebActionUtil.clickElement(fqlcsupervisor, driver, "FqlcSupervisor");
	}
	
	@FindBy(xpath="//li[text()='FQLC']")
	private WebElement fqlc;
	public void fqlcAction(WebDriver driver) throws Exception{
		//WebActionUtil.selectbyVisibletext(driver, fqlc, "FQLC");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", fqlc);
	}
	@FindBy(xpath="//ul/li[text()='OPM']")
	private WebElement opm;
	public void opmAction(WebDriver driver) throws IOException{
		
		WebActionUtil.scrollIntoViewAndClick(driver, opm);
//		JavascriptExecutor executor = (JavascriptExecutor)driver;
//		executor.executeScript("arguments[0].click();",  opm);
	}
	
	//*[@class='radio-circle']/..//*[text()='Invalid vehicle']
	
	
	
	@FindBy(xpath ="//*[text()='Owner Name']/..//*[@type='text']")
	private WebElement ownerName;

	public WebElement enterOwnerName() {
		return ownerName;
	}
	
	
	@FindBy(xpath ="//*[text()='Model Name']/..//*[@type='text']")
	private WebElement modName;

	public WebElement entermodName() {
		return modName;
	}

	@FindBy(xpath ="//*[text()='Valid from']/..//*[@class='date-input dd']")
	private WebElement validFrom;

	public WebElement entervalidFrom() {
		return validFrom;
	}
	
	@FindBy(xpath ="//*[text()='Done']")
	private WebElement done;

	public WebElement clickDone() {
		return done;
	}
	

	@FindBy(xpath ="//*[text()='Valid Upto']/..//*[@class='date-input dd']")
	private WebElement validTo;

	public WebElement entervalidTo() {
		return validTo;
	}
	
	
	

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb as FQLC & verify if added vehicle from Partner app
	 *  is created under verification dashboard
	 * 
	 * 
	 * 
	 */
	
	public void Navigate_VerifyVehicle(WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(this.verDash, driver, "Verification Dashboard");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(vehTab, driver, "Vehicle Tab");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(CreatedTab, driver, "Created Tab");
	}
	
	
	public void verifyvehicle_Created(WebDriver driver,String vehnum) throws IOException{
		
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
	WebActionUtil.verifyElementContainsText(fstVeh, vehnum, driver);
		
			}
	
	
	
	
	
	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb as FQLC & verify if added vehicle from Partner app
	 *  can be deleted, which is in Created status.( invalid vehicle)
	 */
	
	public void verifyvehicle_Deletion(WebDriver driver) throws IOException{
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		String vehnum_del=fstVeh.getText();
//		System.out.println(vehnum_del);
		
		WebActionUtil.clickElement(delveh, driver, "Trash Vehicle Icon");
		WebActionUtil.clickElement(delBtn, driver, "Delete vehicle button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		
		WebActionUtil.notContainsText(fstVeh, vehnum_del, driver);
		
		
		
	}
	
	
	
	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb as FQLC & verify if the uploaded documents are in Uploaded Tab.
	 * @throws Exception 
	 */
	public void Navigate_VerifyUploadDocs(WebDriver driver) throws IOException, Exception{
        Thread.sleep(3000);
		WebActionUtil.clickElement(this.verDash, driver, "Verification Dashboard");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		WebActionUtil.clickElement(vehTab, driver, "Vehicle Tab");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		WebActionUtil.clickElement(uploadedTab, driver, "Uplaoded Tab");
		Thread.sleep(1000);
}
	
	
public void verifyVehicledocs_upload(WebDriver driver,String vehnum) throws IOException{
		

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.verifyElementContainsText(fstuploadVeh, vehnum, driver);
		System.out.println("pass");
			
				}
	

	
public void verify_uploadVehdel( WebDriver driver) throws IOException, InterruptedException{
	
	
	WebActionUtil.clickElement(fstuploadVeh, driver, "First vehicle");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.clickElement(delBtn, driver, "Delete Button");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.clickElement(delvehradio, driver, "Radio button");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.clickElement(delBtn2, driver, "Delete Vehicle");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	WebActionUtil.isEleDisplayed(verifyDel, driver, "verify deletion", 20);
	
		
}

	
	

public void verify_uploadVehReject( WebDriver driver,String vehnum) throws IOException, InterruptedException, AWTException{
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
	WebActionUtil.clickElement(fstuploadVeh, driver, "First vehicle");
	
	WebActionUtil.zoomout();
	
	Thread.sleep(10000);
	
//	WebActionUtil.actionClick(rejDoc, driver, "Reject Document");
//	WebActionUtil.actionClick(radioBtn, driver, "radio button");
//	WebActionUtil.actionClick(rejDoc, driver, "Reject Document");
				
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);		
	WebActionUtil.scrollIntoViewAndClick(driver, radioBtn);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);		
	WebActionUtil.scrollIntoViewAndClick(driver, radioBtn);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);
	
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);		
	WebActionUtil.scrollIntoViewAndClick(driver, radioBtn);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);
	
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);		
	WebActionUtil.scrollIntoViewAndClick(driver, radioBtn);
	WebActionUtil.scrollIntoViewAndClick(driver, rejDoc);
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	
	WebActionUtil.scrollIntoViewAndClick(driver,close);
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	System.out.println("pass");
		
}


	
public void verify_Doc(WebDriver driver, String vhnumRC, String onName, String md ) throws Exception{
	
	//RC submit
	Thread.sleep(5000);
	
	WebActionUtil.clickElement(fstuploadVeh, driver, "Uploaded vehicle");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 7000, driver);
	WebActionUtil.zoomout();
	
	/*WebActionUtil.clearAndType(vehnumRC, vhnumRC, "vehicle Number RC", driver);
	
	WebActionUtil.clearAndType(ownerName, onName, "Owner name", driver);
	WebActionUtil.clearAndType(modName, md, "Model Name", driver);
	WebActionUtil.clearAndType(validFrom, "12011980", "Valid From", driver);*/
	WebActionUtil.clearAndType(validTo, "12011980","Valid To", driver);
	
	WebActionUtil.scrollIntoViewAndClick(driver, done);
	WebActionUtil.scrollIntoViewAndClick(driver, done);
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
	
	
	//FC submit
	
	WebActionUtil.clearAndType(vehFC, "tester", "Vehicle FC", driver);
	WebActionUtil.clearAndType(validTo, "12011980","Valid To", driver);
	WebActionUtil.scrollIntoViewAndClick(driver, done);
//	WebActionUtil.scrollIntoViewAndClick(driver, done); 
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
	
	
	//insurance submit
	
	
	WebActionUtil.clearAndType(insuranceName, "insurancetest", "Insurance Name", driver);
	WebActionUtil.type(insuranceName, "test", "insurance name", driver);
	WebActionUtil.clearAndType(validTo, "12011980","Valid To", driver);
	WebActionUtil.clearAndType(insuranceComp, "testcomp", "Insurance Comp", driver);
	WebActionUtil.clearAndType(insuranceAmt, "20000", "Insurance Amt", driver);
	WebActionUtil.scrollIntoViewAndClick(driver, done);
	WebActionUtil.scrollIntoViewAndClick(driver, done);
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	
	//back image submit
	
	WebActionUtil.scrollIntoViewAndClick(driver,openRadiobtn);
//	WebActionUtil.scrollIntoViewAndClick(openRadiobtn, driver, "Open");
	WebActionUtil.scrollIntoViewAndClick(driver, done);		
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	WebActionUtil.scrollIntoViewAndClick(driver,close);

	
}


	
	
	public void verify_uploadSubmit(WebDriver driver, String vehnum) throws IOException{
		
WebActionUtil.scrollIntoViewAndClick(driver,subTab);	
WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 300, driver);
WebActionUtil.verifyElementContainsText(fstuploadVeh, vehnum, driver);


}
	
	
	public void Navigate_fqlcSupervisorSubmitted(WebDriver driver) throws IOException, Exception{
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,verDash);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']",8000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,vehTab);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 8000, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,submitTab);
		Thread.sleep(5000);
		
		
	}
	

	
	
	public void Navigate_fqlcSupervisorUploaded(WebDriver driver,String vehnum) throws IOException{
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,verDash);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,vehTab);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, uploadedTab);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.verifyElementContainsText(fstuploadVeh, vehnum, driver);
		
		
	}
	
	
	public WebElement selectSpecifiedVehicle(WebDriver driver, String vehNum) {
		return driver.findElement(By.xpath("//*[@class='vehicle-number']/..//*[text()='"+vehNum+"']"));
	}
	
	
	
	
	public void fqlcSupervisor_verify(WebDriver driver,String vehnum) throws IOException, AWTException, InterruptedException{
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		Thread.sleep(5000);
		WebActionUtil.scrollIntoViewAndClick(driver, selectVehicle(driver,vehnum));
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.zoomout();
		for ( int i = 0 ;i<=3;i++){
		WebActionUtil.scrollIntoViewAndClick(driver, done);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		}
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, close);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,vehTabSel);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.scrollIntoViewAndClick(driver,verTab);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 500, driver);
		WebActionUtil.verifyElementContainsText(selectVehicle(driver,vehnum), vehnum, driver);
		
	}
	
	
	
public void verify_submitVehdel( WebDriver driver) throws IOException{
	
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.clickElement(fstuploadVeh, driver, "Submitted vehicle");
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.scrollIntoViewAndClick(driver, delBtn);
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.scrollIntoViewAndClick(driver,delreason);
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.scrollIntoViewAndClick(driver, delBtn2);
	WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200, driver);
	WebActionUtil.isEleDisplayed(verifyDel, driver, "verify deletion", 20);
}



}
