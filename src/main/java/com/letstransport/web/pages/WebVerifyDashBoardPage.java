package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class WebVerifyDashBoardPage {
	WebDriver driver;

	public WebVerifyDashBoardPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	@FindBy(xpath = "//li[text()='Created']")
	private WebElement createdTab;

	public WebElement getCreatedBtn() {
		return createdTab;
	}

	@FindBy(xpath = "//li[text()='Uploaded']")
	private WebElement uploadedTab;

	public WebElement getUploadedBtn() {
		return uploadedTab;
	}

	@FindBy(xpath = "//div[text()='Reject Document']")
	private WebElement rejectDocumentBtn;

	public WebElement getRejectDocumentBtn() {
		return rejectDocumentBtn;
	}

	@FindBy(xpath = "//span[text()='Expired document']")
	private WebElement expiredDocumentBtn;

	public WebElement getExpiredDocumentBtn() {
		return expiredDocumentBtn;
	}

	@FindBy(xpath = "//div[text()='Done']")
	private WebElement doneBtn;

	public WebElement getDoneBtn() {
		return doneBtn;
	}

	@FindBy(xpath = "//span[text()='Close']")
	private WebElement closeBtn;

	public WebElement getCloseBtn() {
		return closeBtn;
	}

	@FindBy(xpath = "//li[text()='Rejected']")
	private WebElement rejectedTab;

	@FindBy(xpath = "//li[text()='Vehicle']")
	private WebElement vehiclePage;

	public WebElement getVehiclePage() {
		return vehiclePage;
	}

	@FindBy(xpath = "//div[text()='Driving Licence Number']/..//input")
	private WebElement driverLicenceInputBox;

	@FindBy(xpath = "//div[text()='Driving Licence Number']/..//input")
	private List<WebElement> driverLicenceInputBoxs;

	@FindBy(xpath = "//div[text()='PAN Number']/..//input")
	private WebElement panInputBox;

	public WebElement getDriverLicenceBox() {
		return driverLicenceInputBox;
	}

	@FindBy(xpath = "//div[text()='Date of Birth']/..//input[@class='date-input dd']")
	private WebElement dayInputBox;

	@FindBy(xpath = "//div[text()='Date of Birth']/..//input[@class='date-input mm']")
	private WebElement monthInputBox;

	@FindBy(xpath = "//div[text()='Date of Birth']/..//input[@class='date-input yyyy']")
	private WebElement yearInputBox;

	@FindBy(xpath = "//div[text()='Name on Card']/..//input")
	private WebElement driverNameInputBox;

	@FindBy(xpath = "//div[text()='Name on PAN Card']/..//input")
	private WebElement panNameInputBox;

	@FindBy(xpath = "//div[text()='Date of Expiry']/..//input[@class='date-input dd']")
	private WebElement dofInputBox;

	@FindBy(xpath = "//div[text()='Date of Expiry']/..//input[@class='date-input mm']")
	private WebElement mofInputBox;

	@FindBy(xpath = "//div[text()='Date of Expiry']/..//input[@class='date-input yyyy']")
	private WebElement yofInputBox;

	@FindBy(xpath = "//div[text()='IFSC Code']/..//input")
	private WebElement ifscInputBox;

	@FindBy(xpath = "//div[text()='IFSC Code']/..//input")
	private List<WebElement> ifscInputBoxs;

	@FindBy(xpath = "//div[text()='Account Number']/..//input")
	private WebElement accountNumInputBox;

	@FindBy(xpath = "//div[text()='Beneficiary Name']/..//input")
	private WebElement benifitiaryNameInputBox;

	@FindBy(xpath = "//div[text()='Next Profile']")
	private WebElement nextProfileBtn;

	@FindBy(xpath = "//li[text()='Pending']")
	private WebElement pendingTab;

	@FindBy(xpath = "//li[text()='Submitted']")
	private WebElement submittedTab;

	@FindBy(xpath = "//li[text()='Verified']")
	private WebElement verifiedTab;

	@FindBy(xpath = "//div[@class='vehicle-number']")
	private List<WebElement> vehicleNumbers;

	@FindBy(xpath = "//div[text()='Driving Licence (Optional)']/..//div[@class='image-gallery inline-display']")
	private WebElement dlImage;

	@FindBy(xpath = "//div[text()='PAN Card *']/..//div[@class='image-gallery inline-display']")
	private WebElement pnImage;

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws IOException
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public void clickOnCreatedTab(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.createdTab, driver, "Created Button");
	}

	public String getSignupNumber(WebDriver driver, String Number) throws IOException {
		return driver.findElement(By.xpath("//div[text()='Partner Mobile']/../..//div[text()='" + Number + "']"))
				.getText();
	}

	public String getSignupDate(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath("//div[text()='Partner Mobile']/../..//div[text()='" + Number
				+ "']//following-sibling::div[text()='" + Date + "']")).getText();
	}

	public WebElement getDocumentUploadedIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath(
				"//div[text()='Partner Mobile']/../..//div[text()='" + Number + "']//following-sibling::div[text()='"
						+ Date + "']/..//div[@class='status-icon uploaded']/../..//div[text()='Personal Documents']"));
	}

	public WebElement getBankDocumentUploadedIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath("//div[text()='Partner Mobile']/../..//div[text()='" + Number
				+ "']//following-sibling::div[text()='" + Date
				+ "']/..//div[@class='status-icon uploaded']/../..//div[text()='Bank Account Documents']"));
	}

	public void verifySignUpNumber(WebDriver driver, String number, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Created tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.verifyContainsText(this.getSignupNumber(driver, number), number);
		WebActionUtil.verifyContainsText(this.getSignupDate(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")),
				WebActionUtil.fetchSystemDateTime("d MMM"));
	}

	public void clickOnUploadedTab(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.uploadedTab, driver, "Uploaded Tab");
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
	}

	public void refreshPage(WebDriver driver) throws IOException {
		driver.navigate().refresh();
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
	}
	
	public void verifyUploadDocuements(WebDriver driver, String number, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
		Thread.sleep(7000);
		WebActionUtil.assertElementDisplayed(
				this.getDocumentUploadedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document");
	}

	public void rejectUploadDocuements(WebDriver driver, String number, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(
				this.getDocumentUploadedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.waitTillPageLoad("Reject Driver lience", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Pan card");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.waitTillPageLoad("Reject Pan card", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Bank account");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Bank Account");
		WebActionUtil.waitTillPageLoad("Reject Bank account", driver, "Bank Accont details page", 90);
		WebActionUtil.clickElement(this.closeBtn, driver, "close button");
	}

	public void clickOnRejectedTab(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.rejectedTab, driver, "Rejected Tab");
	}

	public WebElement getDocumentRejectedIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath(
				"//div[text()='Partner Mobile']/../..//div[text()='" + Number + "']//following-sibling::div[text()='"
						+ Date + "']/..//div[@class='status-icon rejected']/../..//div[text()='Personal Documents']"));
	}

	public WebElement getDocumentPendingIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath("//div[text()='Partner Mobile']/../..//div[text()='" + Number
				+ "']//following-sibling::div[text()='" + Date
				+ "']/..//div[@class='status-icon pending']/../..//div[text()='Bank Account Documents']"));
	}

	public WebElement getDocumentVerifiedIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath(
				"//div[text()='Partner Mobile']/../..//div[text()='" + Number + "']//following-sibling::div[text()='"
						+ Date + "']/..//div[@class='status-icon verified']/../..//div[text()='Personal Documents']"));
	}

	public void verifyRejectedDocuements(WebDriver driver, String number, String filepath, String sheet,
			String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Rejected tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.assertElementDisplayed(
				this.getDocumentRejectedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document in rejected tab");
		Thread.sleep(2000);
	}

	public void approveDriverLicence(WebDriver driver, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("driver license", driver, " Verification DashBoard ", 90);
		WebActionUtil.assertElementDisplayed(this.dlImage, driver, "DL image");
		WebActionUtil.actionClickAndClear(this.driverLicenceInputBox, driver, "Driver licence");
		WebActionUtil.type(this.driverLicenceInputBox, sData[BaseLib.gv.dlCount], "Driver licence", driver);
		WebActionUtil.actionClickAndClear(this.dayInputBox, driver, "day of birth");
		WebActionUtil.actionClickAndClear(this.monthInputBox, driver, "month of birth");
		WebActionUtil.actionClickAndClear(this.yearInputBox, driver, "year of birth");
		WebActionUtil.type(this.dayInputBox, sData[BaseLib.gv.dobCount].split("-")[0].trim(), "day of birth", driver);
		WebActionUtil.type(this.monthInputBox, sData[BaseLib.gv.dobCount].split("-")[1].trim(), "month of birth",
				driver);
		WebActionUtil.type(this.yearInputBox, sData[BaseLib.gv.dobCount].split("-")[2].trim(), "year of birth", driver);
		WebActionUtil.actionClickAndClear(this.driverNameInputBox, driver, "driver name");
		WebActionUtil.clearAndType(this.driverNameInputBox, sData[BaseLib.gv.dnCount], "driver name", driver);
		WebActionUtil.actionClickAndClear(this.dofInputBox, driver, "day of expiry");
		WebActionUtil.actionClickAndClear(this.mofInputBox, driver, "month of expiry");
		WebActionUtil.actionClickAndClear(this.yofInputBox, driver, "year of expiry");
		WebActionUtil.type(this.dofInputBox, sData[BaseLib.gv.doeCount].split("-")[0].trim(), "day of expiry", driver);
		WebActionUtil.type(this.mofInputBox, sData[BaseLib.gv.doeCount].split("-")[1].trim(), "month of expiry",
				driver);
		WebActionUtil.type(this.yofInputBox, sData[BaseLib.gv.doeCount].split("-")[2].trim(), "year of expiry", driver);
		Thread.sleep(8000);
		try {
			WebActionUtil.clickElement(this.doneBtn, driver, "done button");
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		} catch (Exception e) {
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		}
		if (this.ifscInputBoxs.isEmpty()) {
			WebActionUtil.clickElement(this.doneBtn, driver, "done button");
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		} else {
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		}
	}

	public void approvePanCard(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Pan card ", driver, " Verification DashBoard ", 90);
		WebActionUtil.assertElementDisplayed(this.pnImage, driver, "PAN image");
		WebActionUtil.actionClickAndClear(this.panInputBox, driver, "Pan Card");
		WebActionUtil.type(this.panInputBox, sData[BaseLib.gv.pnoCount], "Pan Card", driver);
		WebActionUtil.actionClickAndClear(this.dayInputBox, driver, "day of birth");
		WebActionUtil.actionClickAndClear(this.monthInputBox, driver, "month of birth");
		WebActionUtil.actionClickAndClear(this.yearInputBox, driver, "year of birth");
		WebActionUtil.clearAndType(this.dayInputBox, sData[BaseLib.gv.dobCount].split("-")[0].trim(), "day of birth",
				driver);
		WebActionUtil.clearAndType(this.monthInputBox, sData[BaseLib.gv.dobCount].split("-")[1].trim(),
				"month of birth", driver);
		WebActionUtil.clearAndType(this.yearInputBox, sData[BaseLib.gv.dobCount].split("-")[2].trim(), "year of birth",
				driver);
		WebActionUtil.actionClickAndClear(this.panNameInputBox, driver, "pan Name");
		WebActionUtil.clearAndType(this.panNameInputBox, sData[BaseLib.gv.dnCount], "Pan name", driver);
		Thread.sleep(8000);
		try {
			WebActionUtil.clickElement(this.doneBtn, driver, "done button");
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		} catch (Exception e) {
			WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
		}
		if (this.driverLicenceInputBoxs.isEmpty()) {
			try {
				WebActionUtil.clickElement(this.doneBtn, driver, "done button");
				WebActionUtil.waitTillPageLoad("done button", driver, " Verification DashBoard ", 90);
			} catch (Exception e) {
				WebActionUtil.waitTillPageLoad("Pan card ", driver, " Verification DashBoard ", 90);
			}
		} else {
			WebActionUtil.waitTillPageLoad("Pan card ", driver, " Verification DashBoard ", 90);
		}
	}

	public void approveBankAccount(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.actionClickAndClear(this.ifscInputBox, driver, "IFSC CODE");
		WebActionUtil.type(this.ifscInputBox, sData[BaseLib.gv.ifscCount], "IFSC Code", driver);
		WebActionUtil.actionClickAndClear(this.accountNumInputBox, driver, "Account Number");
		WebActionUtil.type(this.accountNumInputBox, sData[BaseLib.gv.anCount], "Account Number", driver);
		WebActionUtil.actionClickAndClear(this.benifitiaryNameInputBox, driver, "Benifitiary Name");
		WebActionUtil.type(this.benifitiaryNameInputBox, sData[BaseLib.gv.bnCount], "Benifitiary Name", driver);
		WebActionUtil.waitTillPageLoad("Bank docuement Page ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(this.doneBtn, driver, "done button");
		WebActionUtil.waitTillPageLoad("Bank docuement Page ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(this.closeBtn, driver, "Close button");
		WebActionUtil.waitTillPageLoad("Bank Account ", driver, " Verification DashBoard ", 90);

	}

	public void clickOnPendingTab(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.pendingTab, driver, "Pending Tab");
	}

	public void verifyPendingDocuements(WebDriver driver, String number, String filepath, String sheet, String testcase)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Pending tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.assertElementDisplayed(
				this.getDocumentPendingIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document in pending tab");
		Thread.sleep(8000);
	}

	public void clickOnSubmittedTab(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.submittedTab, driver, "Submitted Tab");
	}

	public void verifySubmittedDocuements(WebDriver driver, String number, String filepath, String sheet,
			String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Submitted tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.verifyContainsText(this.getSignupNumber(driver, number), number);
		WebActionUtil.verifyContainsText(this.getSignupDate(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")),
				WebActionUtil.fetchSystemDateTime("d MMM"));
	}

	public void clickOnUploadedIcon(WebDriver driver, String number, String filepath, String sheet, String testcase)
			throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(
				this.getDocumentUploadedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document");
	}

	public WebElement getDocumentSubmittedIcon(WebDriver driver, String Number, String Date) throws IOException {
		return driver.findElement(By.xpath(
				"//div[text()='Partner Mobile']/../..//div[text()='" + Number + "']//following-sibling::div[text()='"
						+ Date + "']/..//div[@class='status-icon submitted']/../..//div[text()='Personal Documents']"));
	}

	public void rejectSubmittedDocuements(WebDriver driver, String number, String filepath, String sheet,
			String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Uploaded tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(
				this.getDocumentSubmittedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.waitTillPageLoad("Reject Driver lience", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Pan card");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject driver liscene");
		WebActionUtil.waitTillPageLoad("Reject Pan card", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Bank account");
		WebActionUtil.clickElement(this.expiredDocumentBtn, driver, "Expired Docuements");
		WebActionUtil.clickElement(this.rejectDocumentBtn, driver, "reject Bank Account");
		WebActionUtil.waitTillPageLoad("Reject Bank account", driver, "Bank Accont details page", 90);
		WebActionUtil.clickElement(this.closeBtn, driver, "close button");
	}

	public void clickOnDoneBtn(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
	}

	public void clickOnCloseBtn(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.closeBtn, driver, "close button");
	}

	public void approveSubmittedDocuements(WebDriver driver, String number, String filepath, String sheet,
			String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Submitted tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(
				this.getDocumentSubmittedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document");
		WebActionUtil.clickElement(this.doneBtn, driver, "approve driver liscene");
		WebActionUtil.waitTillPageLoad("approve driver liscene", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.doneBtn, driver, "approve driver liscene");
		WebActionUtil.waitTillPageLoad("approve Pan card", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.doneBtn, driver, "approve Bank Docuements");
		WebActionUtil.waitTillPageLoad("approve Bank Docuements", driver, "Personal docuement page", 90);
		WebActionUtil.clickElement(this.closeBtn, driver, "close button");
	}

	public void clickOnVerifiedTab(WebDriver driver) throws IOException {
		WebActionUtil.waitTillPageLoad("Verified Tab ", driver, " Verification DashBoard ", 90);
		WebActionUtil.clickElement(this.verifiedTab, driver, "Verified Tab");
		WebActionUtil.waitTillPageLoad("Verified Tab ", driver, " Verification DashBoard ", 90);
	}

	public void verifyVerifiedDocuements(WebDriver driver, String number, String filepath, String sheet,
			String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.assertElementDisplayed(
				this.getDocumentVerifiedIcon(driver, number, WebActionUtil.fetchSystemDateTime("d MMM")), driver,
				"Personal Document in verified Tab");
	}

	public String getVehicleNumber(WebDriver driver) throws Exception {
		return WebActionUtil.vehicleNumber(this.vehicleNumbers, 40);
	}
}
