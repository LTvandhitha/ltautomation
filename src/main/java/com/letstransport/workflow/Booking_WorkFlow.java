package com.letstransport.workflow;

import org.testng.annotations.BeforeSuite;

import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;

public class Booking_WorkFlow extends BaseLib implements TestDataCoulmns {
	
	InitializePages wdInit = new InitializePages(gv.wDriver);
	InitializePages adInit = new InitializePages(gv.aDriver);
	
	public String getRandomVehicle() throws Exception{
	wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
	wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
	wdInit.o_HomePage.navigateToVerifyDashBoardViaFQLCSuper(gv.wDriver);
	wdInit.o_VerifyDashBoardPage.getVehiclePage().click();
	wdInit.o_VerifyDashBoardPage.clickOnVerifiedTab(gv.wDriver);
	String vehNum = wdInit.o_VerifyDashBoardPage.getVehicleNumber(gv.wDriver);
	return vehNum;
	}
}
