package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class DriverPayment_Auto_01 extends BaseLib implements TestDataCoulmns {

		
	
	@Test(enabled = true, priority = 1, description = " Test case : To validate Payout pending page &  requested payment should not be available for OPM")		
	public void DriverPayment_Auto_01() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("DriverPayment_Auto_01");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE  : To validate Payout pending page &  requested payment should not be available for OPM", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		
		//<<<<<<<<<<<<To navigate to Pending Payment Dashboard module>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		wdInit.o_WebLtPaymentDashboardPage.navigate_ValidatePendingPayout(gv.wDriver);		
		wdInit.o_WebLtPaymentDashboardPage.verifyTransactionTable(gv.wDriver);
		
		
		//To Validate requested payment should not be available for OPM 
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, gv.wDriver);
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, gv.wDriver);
		
		wdInit.o_HomePage.NavigateOPM(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.verifyOPMFin(gv.wDriver);
		
		
		
			}

	
		@AfterMethod
	public void reset() {
//		gv.wDriver.close();
		

	}

}
