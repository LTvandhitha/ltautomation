/*Precondition to run this script
TC_Auto_Request_Schedule_Vehicle_Payment"/>
TC_Auto_Verify_Requested_Vehicle_Payment"/>
/*/

package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class DriverPayment_Auto_02 extends BaseLib implements TestDataCoulmns {

		
	
	@Test(enabled = true, priority = 1, description = " Test case : To validate payment request for schedule booking should be visible to finance team")		
	public void DriverPayment_Auto_02() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("DriverPayment_Auto_02");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE  : To validate payment request for schedule booking should be visible to finance team", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		
		//<<<<<<<<<<<<To navigate to Pending Payment Dashboard module>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		wdInit.o_WebLtPaymentDashboardPage.navigate_ValidatePendingPayout(gv.wDriver);		
		
//		String SysDate= WebActionUtil.fetchcurrentSystemDateTime();
//		String day=wdInit.o_BulkUpdate.returnday(SysDate);
	
//		String withoutzeroday = wdInit.o_BulkUpdate.removeZero(day);
	
		
		int yday =WebActionUtil.fetchyday();
		
		wdInit.o_WebLtPaymentDashboardPage.selectdates(gv.wDriver,yday,"Bangalore");
		
		
		
		
		
		wdInit.o_WebLtPaymentDashboardPage.verifyBookingPaymentRequested(gv.wDriver,gv.pVEHICLE_NUMBER, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03);
		
	
	}
		@AfterMethod
	public void reset() {
//		gv.wDriver.close();
		

	}

}
