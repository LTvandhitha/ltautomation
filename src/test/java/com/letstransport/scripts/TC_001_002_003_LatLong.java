package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_001_002_003_LatLong extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To verify if latitude & longtitude fields are mandatory")

	public void TC_001_002_003_LatLong() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_001_002_003_LatLong");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : To verify if latitude & longtitude fields are mandatory", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			

		wdInit.o_HomePage._login( gv.wDriver,gv.sPortal_Username, gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		/*verify if longtitude/latitude fields are mandatory & accepts only numeric values 
		within -180 to 180 & -90 to 90*/
		
		
		String hname= MobileActionUtil.generateRandomString(2,2);
				
		wdInit.o_addarea.verifyLongLat(gv.wDriver,hname,GenericLib.clientName,GenericLib.wLat,GenericLib.wLong);
		
		//save the hub
		
		wdInit.o_addarea.saveHub(gv.wDriver,GenericLib.latitude,GenericLib.longtitude);
		
		
		/*<<<<<<<<<<<<<<verify if added new hub is displayed in booking page>>>>>>>>>>>>>>>>>>*/
		
	/* feature change- bug is present which dev team is aware of hence this section is commented*/
 		
//		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
//	
//
//		wdInit.o_addarea.verifyHubinBooking(gv.wDriver,GenericLib.clientName ,GenericLib.harea,hname); 
		
		
		
		}
	
	
		@AfterMethod
	public void reset() {
		gv.wDriver.close();
		

	}

}
