package com.letstransport.scripts;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_005__BulkUpdate extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To verify  if mandatory fields must  be present before csv upload")

	public void TC_005__BulkUpdate() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_001__BulkUpdate");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE :  To verify  if mandatory fields must  be present before csv upload", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			

		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		//<<<<<<<<<<<<To verify bulk booking upload>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		String SysDate= WebActionUtil.fetchcurrentSystemDateTime();    
		
		String ticNum= WebActionUtil.generateRandomNumberfornDigits(4);
			   
		
		String excelValues[] = {GenericLib.filelocation,SysDate,ticNum,GenericLib.ValidateHQNamedata,GenericLib.ValidateVehNumdata,GenericLib.ValidateVehTypedata};
			
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_BulkUpdate.CustomerRelations(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_BulkUpdate.miscOrders(gv.wDriver);
		wdInit.o_BulkUpdate.bulk_upload(gv.wDriver,GenericLib.client);
		
		wdInit.o_BulkUpdate.downloadFile_bulk(GenericLib.filelocation);
		
		 //add values into excel 
		
		List<String> vowelsList = Arrays.asList(excelValues);
		WebActionUtil.csvwrite_String(GenericLib.filelocation,vowelsList );
		
		
		wdInit.o_BulkUpdate.choosefile(gv.wDriver);
		
		
		WebActionUtil.uploadFile(GenericLib.filelocation);
		
	
		wdInit.o_BulkUpdate.verify_failure(gv.wDriver);
		
	}	
	
	
	
	
		@AfterMethod
	public void reset() {
		gv.wDriver.close();
		

	}

}
