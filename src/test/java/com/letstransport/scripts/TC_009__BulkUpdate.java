
package com.letstransport.scripts;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_009__BulkUpdate extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To validate user shoud be able to export booking created")

	public void TC_009__BulkUpdate() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_009__BulkUpdate");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : To validate user shoud be able to export booking created", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			

		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);		
		
		
		
	//<<<<<<<<<<<<To verify bulk booking upload>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		    
		String SysDate= WebActionUtil.fetchcurrentSystemDateTime();    
				

		
		String day=wdInit.o_BulkUpdate.returnday(SysDate);
		
		
		String ticNum= WebActionUtil.generateRandomNumberfornDigits(4);
		
		
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_BulkUpdate.CustomerRelations(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_BulkUpdate.miscOrders(gv.wDriver);
		wdInit.o_BulkUpdate.bulk_upload(gv.wDriver,GenericLib.client);
		
		
		
		
	//download	
		
		wdInit.o_BulkUpdate.downloadFile_bulk(GenericLib.filelocation);

			   
		WebActionUtil.csvwrite(GenericLib.filelocation,SysDate,ticNum,GenericLib.ValidateHQNamedata,GenericLib.ValidateVehNumdata,GenericLib.ValidateVehTypedata,
					GenericLib.ValidateAmountdata,GenericLib.ValidateNamedata,GenericLib.ValidateAccNumdata,GenericLib.ValidateIFSCdata,GenericLib.ValidateMobNumdata);
	
		wdInit.o_BulkUpdate.choosefile(gv.wDriver);
		
		WebActionUtil.uploadFile(GenericLib.filelocation); 
		
		
		wdInit.o_BulkUpdate.confirm_page(gv.wDriver);
		wdInit.o_BulkUpdate.ok(gv.wDriver);
		
		
		//goto OPM home page , download & check if the misc file can be exported with misc
		
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);	
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		
		wdInit.o_HomePage.oPM(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 2000, gv.wDriver);
		
		wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		Thread.sleep(5000);
		wdInit.o_HomePage._clickOnfloatingIcon(gv.wDriver);
		Thread.sleep(5000);
		wdInit.o_HomePage._clickOn_Exportfile(gv.wDriver);
		
		wdInit.o_HomePage.exportMiscFile(gv.wDriver);
		
		wdInit.o_BulkUpdate.downloadFile_bulk(GenericLib.filelocation);
		
		WebActionUtil.verifyCsv_AllData(GenericLib.filelocation,GenericLib.ValidateticNumdata);
	}
		
		
					
		
		@AfterMethod
	public void reset() {
//			gv.wDriver.close();
			
		

	}

}

