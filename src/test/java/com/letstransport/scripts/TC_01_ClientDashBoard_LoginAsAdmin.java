package com.letstransport.scripts;

import org.seleniumhq.jetty9.server.handler.ContextHandler.Availability;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;

import io.appium.java_client.android.AndroidDriver;

public class TC_01_ClientDashBoard_LoginAsAdmin extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = " Test case : Onboard Client")
    public void TC_01_ClientDashBoard_LoginAsAdmin() throws Exception{
		
		
		// assign category
				MyExtentListners.test.assignCategory("TC_01_ClientDashBoard_LoginAsAdmin");
				// write testcase name in report
				MyExtentListners.test
						.info(MarkupHelper.createLabel("TESTCASE : TC_01_ClientDashBoard_LoginAsAdmin", ExtentColor.CYAN));
				
				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_ClientDashboardPage.ClientDashBoard_Admin_login(gv.dCLIENT_URL,gv.dCLIENT_MOB,gv.wDriver);
	}

}
