package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;

public class TC_02_ClientDashBoard_viewUserList extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = " Test case : Onboard Client")
	 public void TC_02_ClientDashBoard_ClientDashBoard_viewUserList() throws Exception{
		// assign category
		MyExtentListners.test.assignCategory("TC_02_ClientDashBoard_viewUserList");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE :TC_02_ClientDashBoard_viewUserList", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		//wdInit.o_ClientDashboardPage.ClientDashBoard_Admin_ViewUserList(gv.dCLIENT_URL,gv.dCLIENT_MOB,gv.wDriver,gv.userOfClientD,gv.hub1OfClientD,gv.hub2OfClientD,gv.editClient,gv. delClient);
	}

}
