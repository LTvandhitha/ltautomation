package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_03_ClientDashBoard_addUser_byClient extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = " Test case : Onboard Client")
	 public void TC_03_ClientDashBoard_editUser_FromUserList() throws Exception{
		// assign category
		MyExtentListners.test.assignCategory("TC_03_ClientDashBoard_addUser_byClient");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TC_03_ClientDashBoard_addUser_byClient", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		String username = WebActionUtil.generateRandomString(4);
		String phNo = WebActionUtil.generateRandomNumberfornDigits(10);
		/*wdInit.o_ClientDashboardPage.login_ToAdmin_ClientDashBoard(gv.dCLIENT_URL,gv.dCLIENT_MOB,gv.wDriver);
		wdInit.o_ClientDashboardPage.addUser_byClient(gv.wDriver,username,phNo);
		wdInit.o_ClientDashboardPage.VerifyaddUser_byClient( gv.wDriver, username, phNo);*/
		
	}
}
