package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_06__07_AddDelVehicle extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Add & Delete Vehicle in partner app ")

	public void TC_06__07_AddDelVehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_06__07_AddDelVehicle");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : Add & Delete Vehicle in partner app as a new/existing user", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		//<<<<<<<<<<<<Add Vehicle in Partner App & verify as a new/existing user>>>>>>>>>>>>>>>>>>>>
		
		
		MobileActionUtil.waitForinvisiblityofElement("//*[name()='svg']//*[name()='g']//*[name()='circle']", 100, gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		//MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		//MobileActionUtil.waitTillProgressBarLoad("Jobs around you ", gv.aDriver, "Listings", 300);
		
//		Thread.sleep(50000);
		//adInit.a_addveh._handleReferral(gv.aDriver);
	    
		//generate a vehicle number
	    
		String vehnum=MobileActionUtil.generateRandomString(2,4);
	   
		//add vehicle from Partner App & verify if its created
	   
//	    adInit.a_addveh._addVehicles(gv.aDriver,vehnum);		
//		adInit.a_addveh._verifyVehicles(gv.aDriver,vehnum);
		
		//<<<<<<<<<<<<Verify if vehicle is created in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addarea._clickOPMFQLC(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addveh.Navigate_VerifyVehicle(gv.wDriver);
		wdInit.o_addveh.verifyvehicle_Created(gv.wDriver, vehnum);
		Thread.sleep(20000);
		
		//<<<<<<<<<<<<Verify if vehicle can be deleted from FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	    wdInit.o_addveh.verifyvehicle_Deletion(gv.wDriver);
	
    	//<<<<<<<<<<<<<Verify if vehicle is deleted from Partner App>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
//		adInit.a_addveh._verifyVehiclesDel(gv.aDriver, vehnum);  
	
	}
	
	
	@AfterMethod
	public void reset() {
				gv.wDriver.close();

	}

}
