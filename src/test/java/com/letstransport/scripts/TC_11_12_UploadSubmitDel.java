package com.letstransport.scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;

public class TC_11_12_UploadSubmitDel extends BaseLib implements TestDataCoulmns  {
	@Test(enabled = true, priority = 1, description = " Test case : Submit Vehicle documents for verification & verify if it can be deleted ")

	public void TC_11_12_UploadSubmitDel() throws Throwable {
		// assign category
		MyExtentListners.test.assignCategory("TC_11_12_UploadSubmitDel");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : Submit Vehicle documents for verification & verify if it can be deleted", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		
		//<<<<<<<<<<<<Upload vehicle documents of a vehicle in Partner App>>>>>>>>>>>>>>>>>>>>
		
		MobileActionUtil.waitForinvisiblityofElement("//*[name()='svg']//*[name()='g']//*[name()='circle']", 2000, gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		//		MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		//		MobileActionUtil.waitTillProgressBarLoad("Jobs around you ", gv.aDriver, "Listings", 300);
		
		//	    adInit.a_addveh._handleReferral(gv.aDriver);
		
		Thread.sleep(20000);
	    
	    String vehnum=MobileActionUtil.generateRandomString(2,4);
	       
	    //Add a vehicle	    
		adInit.a_addveh._addVehicles(gv.aDriver,vehnum);

		//upload RC documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"RC");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
//		//upload FC documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"FC");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
		
//		//Upload Insurance documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"Insurance");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
		//scroll down 
		
		WebElement ele = gv.aDriver.findElement(By.xpath("//*[contains(text(),'Back Image')]"));
		MobileActionUtil.scrollIntoViewAndClick(gv.aDriver, ele);
		
		
		//Upload back image 
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"Back Image");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		
		
			
		
		//<<<<<<<<<<<<<<<verify if uploaded vehicle docs can be submitted>>>>>>>>>>>>>>>>
		
		
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addarea._clickOPMFQLC(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addveh.Navigate_VerifyUploadDocs(gv.wDriver);
		wdInit.o_addveh.verify_Doc(gv.wDriver,GenericLib.vnRC,GenericLib.ownName,GenericLib.modName);
		wdInit.o_addveh.verify_uploadSubmit(gv.wDriver, vehnum);
		

	
	//<<<<<<<<<<<<<<<verify if submitted vehicle doc can be deleted by FQLC supervisor>>>>>>>>>>>>>>>>
		Thread.sleep(8000);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addarea._clickOPMFQLCSupervisor(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addveh.Navigate_fqlcSupervisorSubmitted(gv.wDriver);
//		wdInit.o_addveh.verify_submitVehdel(gv.wDriver);
	}
	@AfterMethod
	public void reset() {
		gv.wDriver.close();

	}
}
