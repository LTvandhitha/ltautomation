package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_15__16_AddDelArea extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Add & Delete Area from portal")

	public void TC_15__16_AddDelArea() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_15__16_AddDelArea");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : Add  & Delete Area from portal", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
			
		
		
		//<<<<<<<<<<<<Add & Delete Area from portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
		

	


		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
	

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addarea._addDelArea(GenericLib.alname,GenericLib.foname, gv.wDriver);
		
	
		
	}
	

		

	@AfterMethod
	public void reset() {

		gv.wDriver.close();


	}

}
