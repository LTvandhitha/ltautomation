package com.letstransport.scripts;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.TimeZone;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Create_Shchedule_Booking_DP extends BaseLib implements TestDataCoulmns {

		@Test(enabled = true, priority = 1, description = " Test case : Create a Scheduled booking,UploadTripsheet & Verify TripSheet ")
		public void TC_0028_Scheduled_Booking_Create_Publish() throws Exception {
			// assign category
			MyExtentListners.test.assignCategory("TC_Auto_Create_Shchedule_Booking_Driver_Payments");
			// write testcase name in report
			MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : Create a Scheduled booking,UploadTripsheet & Verify TripSheet ", ExtentColor.CYAN));
			
			InitializePages wdInit = new InitializePages(gv.wDriver);
			InitializePages adInit = new InitializePages(gv.aDriver);
			
			
			
			
			//login ,create a booking & mark attendance 
			
			wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
			wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			
			
			//FO Admin to mark yesterdays attendance
			
			int yday =WebActionUtil.fetchyday();
			
			
		
			wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
			wdInit.o_HomePage.navigateFOAdmin(gv.wDriver);
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_HomePage.viewAreas(gv.wDriver);
			wdInit.o_addarea.getArea(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03).click();
			wdInit.o_addarea.clickAddNewBooking(gv.wDriver);
			wdInit.o_AddBookingPage.createBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.portalTimeStamp,gv.pVEHICLE_NUMBER);
			wdInit.o_addarea.verifyBookingPage(gv.wDriver);
			wdInit.o_addarea.verifyBookingSuccess(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.createdTimeStamp,gv.pVEHICLE_NUMBER);
			wdInit.o_addarea.getcalendartoPublish(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.createdTimeStamp,gv.pVEHICLE_NUMBER).click();
			
			
			
			
			wdInit.o_UploadTripSheetPage.selectrequireddate(yday);	
			wdInit.o_addarea.getPublishButton(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.createdTimeStamp,gv.pVEHICLE_NUMBER).click();
			wdInit.o_addarea.verifyPublishedPopup(gv.wDriver);
//////	wdInit.o_addarea.deleteBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.createdTimeStamp,gv.pVEHICLE_NUMBER);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, gv.wDriver);
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, gv.wDriver);			
			
			wdInit.o_HomePage.homeLink(gv.wDriver);
////			wdInit.o_HomePage.allList(gv.wDriver);
			
			wdInit.o_addarea.clickYday(gv.wDriver);
			
			wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03);
			wdInit.o_HomePage.verifyingBookingPublished(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.displayedTimeStamp,gv.pVEHICLE_NUMBER);
			wdInit.o_HomePage.selectGoing(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.displayedTimeStamp,gv.pVEHICLE_NUMBER);
			
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 10000, gv.wDriver);
			
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_HomePage.navigateFOAdmin(gv.wDriver);
			wdInit.o_HomePage.oPM(gv.wDriver);
			WebActionUtil.waitTillPageLoad("Home Page", gv.wDriver, "Home Page", 20000);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200000, gv.wDriver);
			Thread.sleep(5000);
			wdInit.o_HomePage.allList(gv.wDriver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 200000, gv.wDriver);
			//Upload a tripsheet
			wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03);
					
			wdInit.o_UploadTripSheetPage.uploadTripSheet(gv.wDriver,gv.sImage);
			
			//Map tripsheet	
			wdInit.o_UploadTripSheetPage.mapTripSheet(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.pVEHICLE_NUMBER,gv.openingTimeStamp,gv.closingTimeStamp);
			
			
			//verify tripsheet
			wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);		
			
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_HomePage.oPM(gv.wDriver);
			wdInit.o_HomePage.tripsheetExecutive(gv.wDriver);

			wdInit.o_VerifyTripSheetPage.verifyTripSheet(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.pVEHICLE_NUMBER);
			
			wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);		
			
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
			wdInit.o_HomePage.oPM(gv.wDriver);
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_HomePage.payments(gv.wDriver);
			wdInit.o_HomePage.detailedVehiclePayments(gv.wDriver);
			wdInit.o_DetailedVehiclePaymentsPage.requestPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.pVEHICLE_NUMBER);
			
//			wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);	
			//add click ok 
			
//			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
//			wdInit.o_HomePage.payments(gv.wDriver);
//			wdInit.o_HomePage.processVehiclePayments(gv.wDriver);
//			wdInit.o_ProcessVehiclePaymentsPage.verifyPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQ_STATUS,gv.pVEHICLE_NUMBER);
//			
//			wdInit.o_HomePage.clickLogout(gv.wDriver);
		}

}
