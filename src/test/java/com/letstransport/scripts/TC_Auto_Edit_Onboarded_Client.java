package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Edit_Onboarded_Client extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Delete Onboarded Client")

	public void TC_Auto_Edit_Onboarded_client() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Edit_Onboarded_client");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Edit_Onboarding_client", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		String grName= WebActionUtil.generateRandomString(5);
		
		wdInit.o_ClientOnboardingPage.COB_Sales_Login(gv.sSALE_USER, gv.sSALE_PASS,gv.sCLIENT_URL, gv.wDriver);
		wdInit.o_ClientOnboardingPage.addNewGroup(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		wdInit.o_ClientOnboardingPage.addNewEntity(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		wdInit.o_ClientOnboardingPage.addNewVertical(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		
		wdInit.o_ClientOnboardingPage.addNewGSTNo(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewGSTDetails(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewHUB(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, grName);
		String cl= wdInit.o_ClientOnboardingPage.addNewCommercial(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, grName,gv.sRAISED_FILE,gv.sCOMMERCIAL_PATH);

		

		wdInit.o_ClientOnboardingPage.editClient(gv.wDriver, grName);
		wdInit.o_ClientOnboardingPage.editGroup(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2,grName);
		wdInit.o_ClientOnboardingPage.editEntity(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2,grName);
		wdInit.o_ClientOnboardingPage.editVertical(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2,grName);
		wdInit.o_ClientOnboardingPage.editGSTNo(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2);
		wdInit.o_ClientOnboardingPage.editGSTDetails(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2);
		wdInit.o_ClientOnboardingPage.editHUB(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2, grName);
		wdInit.o_ClientOnboardingPage.editCommercials(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding_2, grName,gv.sCOMMERCIAL_PATH);
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
	}
}
