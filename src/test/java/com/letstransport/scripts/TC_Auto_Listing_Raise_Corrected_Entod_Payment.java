package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Listing_Raise_Corrected_Entod_Payment extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "re-raise entod loadboard payment")

	public void TC_0072_Raise_rerequest_entod_loadboard_payment() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_User_is_able_to_Raise_Corrected_Entod_Payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_User_is_able_to_Raise_Corrected_Entod_Payment", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.downloadLCorrectedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCORRECTED_FILE,gv.sCOR_STATUS);
		wdInit.o_FinancePage.verifyLCSVData(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCORRECTED_FILE,gv.sCOR_STATUS);
		wdInit.o_FinancePage.raiseCorrectedRequest(gv.wDriver,gv.sCORRECTED_FILE,gv.sRAISED_FILE,gv.sCOR_STATUS,gv.sRAI_STATUS);
		wdInit.o_FinancePage.uploadLCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sRAISED_FILE,gv.sRAI_STATUS);
		//wdInit.o_FinancePage.deleteFile(gv.wDriver,gv.sCORRECTED_FILE);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}
}
