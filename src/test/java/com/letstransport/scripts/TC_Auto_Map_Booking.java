package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Map_Booking extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : map Tripsheet to the scheduled booking ")
	public void TC_0030_Map_tripsheet_To_Scheduled_Booking() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Mapping_of_Booking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Mapping_of_Booking", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01);
		wdInit.o_UploadTripSheetPage.mapTripSheet(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.sVEHICLE_NUMBER,gv.openingTimeStamp,gv.closingTimeStamp);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.allList(gv.wDriver);
		wdInit.o_UploadTripSheetPage.verifyMappedTripSheet(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.sVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
