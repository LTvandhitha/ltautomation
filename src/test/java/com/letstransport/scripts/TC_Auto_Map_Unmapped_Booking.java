package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Map_Unmapped_Booking extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : map an unmapped tripsheet to a booking ")
	public void TC_0031_Map_tripsheet_To_Scheduled_Booking() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Mapping_Unmapped_Booking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Mapping_Unmapped_Booking", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.ops(gv.wDriver);
		wdInit.o_HomePage.viewAreas(gv.wDriver);
		wdInit.o_addarea.getArea(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02).click();
		wdInit.o_addarea.clickAddNewBooking(gv.wDriver);
		wdInit.o_AddBookingPage.createBooking(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.portalTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_addarea.verifyBookingPage(gv.wDriver);
		wdInit.o_addarea.verifyBookingSuccess(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.createdTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_addarea.getPublishButton(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.createdTimeStamp,gv.sVEHICLE_NUMBER)
				.click();
		//wdInit.o_addarea.verifyPublishedPopup(gv.wDriver);
		wdInit.o_addarea.deleteBooking(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.createdTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.homeLink(gv.wDriver);
		wdInit.o_HomePage.allList(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02);
		wdInit.o_HomePage.verifyingBookingPublished(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet,
				TC_Booking_02,gv.displayedTimeStamp,gv.sVEHICLE_NUMBER);

		wdInit.o_HomePage.searchBooking(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.displayedTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_HomePage.selectGoing(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02,gv.displayedTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.uploadTripSheet(gv.wDriver, gv.sImage);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.allList(gv.wDriver);

		wdInit.o_HomePage.selectArea(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02);
		wdInit.o_UploadTripSheetPage.mapTripSheet(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet,
				TC_Booking_02, gv.sINVEHICLE_NUMBER,gv.openingTimeStamp,gv.closingTimeStamp);

		// wdInit.o_HomePage.selectArea(gv.wDriver,"Agara");
		// wdInit.o_TripSheetPage.floatingIcon(gv.wDriver);
		wdInit.o_UploadTripSheetPage.verifyUnMappedTripSheet(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet,
				TC_Booking_02,gv.sINVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.maptheUnmappedTripSheet();
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_UploadTripSheetPage.refreshPage(gv.wDriver);
		wdInit.o_HomePage.allList(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet, TC_Booking_02);
		wdInit.o_UploadTripSheetPage.verifyUnMappedTripSheet(gv.wDriver, GenericLib.sPortalTestDataPath, Booking_Sheet,
				TC_Booking_02, gv.sVEHICLE_NUMBER, gv.sINVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
