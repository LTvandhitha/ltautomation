package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Pay_Raised_Request extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : change status raised from to paid")
	public void TC_0054_request_Payment_Schedule_Vehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Payment_Raised_Request");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Payment_Raised_Request", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.payRaisedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}