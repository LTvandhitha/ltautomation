package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Raise_Corrected_Requested extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : change status from corrected to raised ")
	public void TC_0056_request_Payment_Schedule_Vehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Raising_Corrected_Payment_Request");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Raising_Corrected_Payment_Request", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.downloadCorrectedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCORRECTED_FILE,gv.sCOR_STATUS);
		wdInit.o_FinancePage.verifyCSVData(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCORRECTED_FILE,gv.sCOR_STATUS);
		wdInit.o_FinancePage.raiseCorrectedRequest(gv.wDriver,gv.sCORRECTED_FILE,gv.sRAISED_FILE,gv.sCOR_STATUS,gv.sRAI_STATUS);
		wdInit.o_FinancePage.uploadCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sRAISED_FILE,gv.sRAI_STATUS);
		//wdInit.o_FinancePage.deleteFile(gv.wDriver,gv.sCORRECTED_FILE);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
  }
}
