package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Raise_Re_requested_UC_Listing_Payment extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = " Raise re-request UC entod loadboard payment")

	public void TC_0078_Raise_rerequest_UCentod_loadboard_payment() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Auto_Raise_Re-requested_UC_Listing_Payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Auto_Raise_Re-requested_UC_Listing_Payment", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.downloadCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE);
		wdInit.o_FinancePage.verifyLData(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE,gv.sREQ_STATUS);
		
		wdInit.o_FinancePage.raiseRequest(gv.wDriver,gv.sREQUESTED_FILE,gv.sRAISED_FILE,gv.sREQ_STATUS,gv.sRAI_STATUS);
		wdInit.o_FinancePage.uploadLCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sRAISED_FILE,gv.sRAI_STATUS);
		wdInit.o_FinancePage.verifyLVehicleDetails(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment);
		wdInit.o_FinancePage.closePopUp(gv.wDriver);
		//wdInit.o_FinancePage.deleteFile(gv.wDriver,gv.sREQUESTED_FILE);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
  }
}
