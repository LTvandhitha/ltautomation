package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;

import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Register_Partner extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : signing up in partner app ")
	public void TC_001_partnerSignup() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Partner_Registration");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Partner_Registration", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		gv.aDriver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		try{
		adInit.a_login.partnerSignOut(gv.aDriver);}
		catch(Exception e){System.out.println("looks like app is already signedout");
		MobileActionUtil.switchToAndroidVeiw(gv.aDriver);}
		finally{
		adInit.a_login.partnerSignUp(gv.randomMobileNumber, gv.aDriver, gv.wDriver);
		adInit.a_homepage.enterName(GenericLib.sAppTestDataPath, Onboarding, TC_Signup, gv.aDriver);
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.navigateToVerifyDashBoardViaFQLC(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.clickOnCreatedTab(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.verifySignUpNumber(gv.wDriver, gv.randomMobileNumber, GenericLib.sAppTestDataPath,Onboarding, TC_Signup);

		//wdInit.o_VerifyDashBoardPage.verifySignUpNumber(gv.wDriver, "7627909170", GenericLib.sAppTestDataPath,
			//	Onboarding, TC_Signup);
		
	    MobileActionUtil.switchToWebView(gv.aDriver);
		adInit.a_homepage.clickOnProfile(gv.aDriver);
		adInit.a_AppProfilePage.uploadDrivingLicence(gv.aDriver, gv.wDriver);
		adInit.a_AppProfilePage.uploadPanCard(gv.aDriver, gv.wDriver);
		adInit.a_AppProfilePage.uploadBankAccountDocuments(gv.aDriver, gv.wDriver);
		//wdInit.o_VerifyDashBoardPage.clickOnUploadedTab(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.refreshPage(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.verifyUploadDocuements(gv.wDriver, gv.randomMobileNumber,GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.verifyUploadDocuements(gv.wDriver, "9792400059",
			//	GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		}
	}
}
