package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Request_Schedule_Vehicle_Payment extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : request payment for schedule vehicle ")
	public void TC_0034_request_Payment_Schedule_Vehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Requesting_Schedule_Vehicle_Payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Requesting_Schedule_Vehicle_Payment", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.payments(gv.wDriver);
		wdInit.o_HomePage.detailedVehiclePayments(gv.wDriver);
		wdInit.o_DetailedVehiclePaymentsPage.requestPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.pVEHICLE_NUMBER);
		//wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}
}
