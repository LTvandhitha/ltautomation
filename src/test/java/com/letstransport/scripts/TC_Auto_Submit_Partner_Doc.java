package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;

public class TC_Auto_Submit_Partner_Doc extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = " Test case : submit the created profile ")

	public void TC_002_approve_Docuements() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Submition_Partner_Doc");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Submition_Partner_Doc", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		MobileActionUtil.switchToWebView(gv.aDriver);
		adInit.a_homepage.clickOnProfile(gv.aDriver);
		adInit.a_AppProfilePage.changeDrivingLicence(gv.aDriver, gv.wDriver);
		adInit.a_AppProfilePage.changePanCard(gv.aDriver, gv.wDriver);
		adInit.a_AppProfilePage.changeBankAccountDocuments(gv.aDriver, gv.wDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.navigateToVerifyDashBoardViaFQLC(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.clickOnUploadedTab(gv.wDriver);

		wdInit.o_VerifyDashBoardPage.clickOnUploadedIcon(gv.wDriver, gv.randomMobileNumber, GenericLib.sAppTestDataPath,
			Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.clickOnUploadedIcon(gv.wDriver,"9450139514", GenericLib.sAppTestDataPath,
			//Onboarding, TC_Signup);
		
		wdInit.o_VerifyDashBoardPage.approvePanCard(gv.wDriver, GenericLib.sAppTestDataPath,
				Personal_And_Bank_Docuements, TC_Submit_Profile);
		wdInit.o_VerifyDashBoardPage.approveDriverLicence(gv.wDriver, GenericLib.sAppTestDataPath,
				Personal_And_Bank_Docuements, TC_Submit_Profile);
		wdInit.o_VerifyDashBoardPage.approveBankAccount(gv.wDriver, GenericLib.sAppTestDataPath,
				Personal_And_Bank_Docuements, TC_Submit_Profile);

		wdInit.o_VerifyDashBoardPage.clickOnPendingTab(gv.wDriver);
		//wdInit.o_VerifyDashBoardPage.verifyPendingDocuements(gv.wDriver, "9450139514",
			//GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		wdInit.o_VerifyDashBoardPage.verifyPendingDocuements(gv.wDriver, gv.randomMobileNumber,
			GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		MobileActionUtil.switchToWebView(gv.aDriver);
		adInit.a_homepage.verifyBankAffidavit(gv.aDriver);
		adInit.a_homepage.clickOnAgreeBtn(gv.aDriver);

		wdInit.o_VerifyDashBoardPage.clickOnSubmittedTab(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.verifySubmittedDocuements(gv.wDriver, gv.randomMobileNumber,
			GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.verifySubmittedDocuements(gv.wDriver, "9450139514",
			//GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
