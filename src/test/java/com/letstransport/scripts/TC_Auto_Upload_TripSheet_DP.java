package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Upload_TripSheet_DP extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : Upload a tripsheet ")
	public void TC_0029_Upload_tripsheet() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Uploading_TripSheet_DP");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Uploading_TripSheet_DP", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03);
		wdInit.o_HomePage.searchBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.displayedTimeStamp,gv.pVEHICLE_NUMBER);
		wdInit.o_HomePage.selectGoing(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.displayedTimeStamp,gv.pVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.uploadTripSheet(gv.wDriver,gv.sImage);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}
}
