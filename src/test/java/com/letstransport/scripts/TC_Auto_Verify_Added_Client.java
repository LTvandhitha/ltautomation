package com.letstransport.scripts;

import org.openqa.selenium.UnhandledAlertException;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Verify_Added_Client extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : verify if onboarded client is visible in group after hub approval ")

	public void TC_TC_Auto_Verify_Added_Client() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Auto_Verify_Added_Client");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : TC_Auto_Verify_Added_Client", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		
		String grName= WebActionUtil.generateRandomString(5);
		
		wdInit.o_ClientOnboardingPage.COB_Sales_Login(gv.sSALE_USER, gv.sSALE_PASS,gv.sCLIENT_URL, gv.wDriver);
		wdInit.o_ClientOnboardingPage.addNewGroup(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		wdInit.o_ClientOnboardingPage.addNewEntity(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		wdInit.o_ClientOnboardingPage.addNewVertical(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,grName);
		
		wdInit.o_ClientOnboardingPage.addNewGSTNo(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewGSTDetails(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewHUB(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, grName);
		String cl= wdInit.o_ClientOnboardingPage.addNewCommercial(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, grName,gv.sRAISED_FILE,gv.sCOMMERCIAL_PATH);
		
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
		
		wdInit.o_ClientOnboardingPage.COB_Finance_Login(gv.sFINANCE_USER, gv.sFINANCE_PASS,gv.sCLIENT_URL, gv.wDriver);
		
		
//		System.out.println(cl);
		wdInit.o_ClientOnboardingPage.Approve_Client(gv.wDriver,cl);
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
		
		
		wdInit.o_login.quick_Fo_Login(gv.sPortal_Username, gv.sPortal_Password,gv.sPortal_Url, gv.wDriver);
//		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.ops(gv.wDriver);
		wdInit.o_HomePage.pendingHubs(gv.wDriver);
		System.out.println(cl);
		wdInit.o_PendingHubsPage.addHubs(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,cl);

		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
		wdInit.o_ClientOnboardingPage.quick_COB_Sales_Login(gv.sSALE_USER, gv.sSALE_PASS,gv.sCLIENT_URL, gv.wDriver);
		wdInit.o_ClientOnboardingPage.verfiyAfterAddingHubs(gv.wDriver, cl);
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
	}
}
