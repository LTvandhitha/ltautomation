package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Verify_Mapped_TripSheet_DP extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : verify mapped tripsheet ")
	public void TC_0032_verify_Mapped_Tripsheet() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Mapped_TripSheet_DP");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Mapped_TripSheet_DP", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.tripsheetExecutive(gv.wDriver);
		wdInit.o_VerifyTripSheetPage.verifyTripSheet(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_03,gv.pVEHICLE_NUMBER);
		wdInit.o_UploadTripSheetPage.navigateBack(gv.wDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
