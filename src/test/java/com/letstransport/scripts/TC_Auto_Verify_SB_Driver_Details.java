package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Verify_SB_Driver_Details extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : verify schedule driver's details ")
	public void TC_0033_verify_Schedule_Driver() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Schedule_Booking_Driver_Details");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Schedule_Booking_Driver_Details", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.driverVerification(gv.wDriver);
		try {
			wdInit.o_DriverVerificationPage.driverVerification(gv.wDriver, GenericLib.sPortalTestDataPath,
					Driver_Verify_Sheet, TC_Driver_Verify, gv.sImage);
		}
		catch(Exception e){System.out.println("Looks like Vehicle is already Verified");}
		finally {
			wdInit.o_DriverVerificationPage.verifyDriverVerified(gv.wDriver, GenericLib.sPortalTestDataPath,
					Driver_Verify_Sheet, TC_Driver_Verify);
			wdInit.o_HomePage.clickLogout(gv.wDriver);
		}
	}
}
