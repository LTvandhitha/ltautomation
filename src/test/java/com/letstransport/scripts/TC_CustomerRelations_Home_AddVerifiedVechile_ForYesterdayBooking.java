package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_AddVerifiedVechile_ForYesterdayBooking extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "Test case :To verify if we add  verified vehcile details in yesterday booking  it should reflect under yesterday booking of customer relation.")
	public void TC_AddVerifiedVehicleDetails_ForYesterdayBooking() throws Exception {
		// assign category
		MyExtentListners.test
				.assignCategory("TC_CustomerRelations_Home_AddVerifiedVechile_ForYesterdayBooking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel(
				"TESTCASE :est case :To verify if we add  verified vehcile details in yesterday booking  it should reflect under yesterday booking of customer relation.",
				ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations",
				"TC_CustomerRelations_Home_001");
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);

		wdInit.o_HomePage.driverVerificationlink(gv.wDriver);
		wdInit.o_DriverVerificationPage.adhocStatus(gv.wDriver);
		wdInit.o_DriverVerificationPage.verifiedLink(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, gv.wDriver);
		String verifiedvehNum = wdInit.o_DriverVerificationPage.getVerifiedVecNo(gv.wDriver);
		System.out.println(verifiedvehNum);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage._clickOnHomeLink(gv.wDriver);
		wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, gv.wDriver);
		String AdhocVechileRate=WebActionUtil.generateRandomNumberfornDigits(3);
		wdInit.o_WebCustomerRelationsPage.EnTodAddVechileDetailsYesterday(gv.wDriver, verifiedvehNum,AdhocVechileRate);
		wdInit.o_WebCustomerRelationsPage.verify_EnTodAddVechileDetailsYesterday(gv.wDriver);

		

	}
}
