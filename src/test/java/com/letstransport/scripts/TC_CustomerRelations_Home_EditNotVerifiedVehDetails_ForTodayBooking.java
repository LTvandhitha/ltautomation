package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForTodayBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "To verify if we edit driver details of non verified vehicle in today booking ,edited driver details should reflect under Today Booking ")
	public void TC_ENTOD_EditNotVerifiedVehDetails_ForTodayBooking() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForTodayBooking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :To verify if we edit driver details of non verified vehicle in today booking ,edited driver details should reflect under Today Booking ", ExtentColor.CYAN));

		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
	    wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
		
		String vehnum=MobileActionUtil.generateRandomString(2,4);
		String vendorname = WebActionUtil.generateRandomString(4);
		String AdhocVechileRate=WebActionUtil.generateRandomNumberfornDigits(3);
		wdInit.o_WebCustomerRelationsPage.verify_toModifyDriverDetailsForTodayBooking(gv.wDriver, vendorname,vehnum,AdhocVechileRate);
	}
}
