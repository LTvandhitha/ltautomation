package com.letstransport.scripts;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_CustomerRelations_Home_VerifiedvehRepeatBookingwithDriverDetails_ForTomorrowBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "Test case : To verify if we Repeat Booking with DriverDetails of tomorrow booking ,it should reflect in Today Booking")
	public void TC_RepeatBookingwithDriverDetails_ForTomorrowBooking() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_VerifiedvehRepeatBookingwithDriverDetails_ForTomorrowBooking");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if we Repeat Booking with DriverDetails of tomorrow booking ,it should reflect in Today Booking", ExtentColor.CYAN));

				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				
				wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				 wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				 wdInit.o_HomePage.oPM(gv.wDriver);
				 wdInit.o_WebCustomerRelationsPage.copyDriverDetailsForTodayRepeatBookingFromTomorrowBooking(gv.wDriver);
	}
}
