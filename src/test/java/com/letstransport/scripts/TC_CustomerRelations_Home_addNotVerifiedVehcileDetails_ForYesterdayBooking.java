package com.letstransport.scripts;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_addNotVerifiedVehcileDetails_ForYesterdayBooking extends BaseLib implements TestDataCoulmns {
		@Test(enabled = true, priority = 1, description = "Test case : To verify if we add not verified vehcile details in yesterday booking  it should reflect under yesterday booking of customer relation. ")
		public void TC_addNotVerifiedVehcileDetails_ForYesterdayBooking () throws Exception {
			// assign category
			MyExtentListners.test.assignCategory(" TC_CustomerRelations_Home_addNotVerifiedVehcileDetails_ForYesterdayBooking");
			// write testcase name in report
			MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if we add not verified vehcile details in yesterday booking  it should reflect under yesterday booking of customer relation. ", ExtentColor.CYAN));

			
			InitializePages wdInit = new InitializePages(gv.wDriver);
			InitializePages adInit = new InitializePages(gv.aDriver);
			
			wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
			wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		 

			wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_HomePage.oPM(gv.wDriver);
			String vendorname = WebActionUtil.generateRandomString(4);
			wdInit.o_WebCustomerRelationsPage.EnTODBookingForYesterdayNotVerifiedVehcile(gv.wDriver);
			wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
			wdInit.o_WebCustomerRelationsPage.customerRelationsLink1(gv.wDriver);
			wdInit.o_WebCustomerRelationsPage.opm(gv.wDriver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, gv.wDriver);
			wdInit.o_HomePage.clickOnAllList(gv.wDriver);
			wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
			wdInit.o_WebCustomerRelationsPage.yesterdayTabOPM(gv.wDriver); 
			wdInit.o_WebCustomerRelationsPage.EnTODAttendenceForYesterdayNotVerifiedVeh(gv.wDriver);
			Thread.sleep(6000);
			String vehnum=MobileActionUtil.generateRandomString(2,4);
			String AdhocVechileRate=WebActionUtil.generateRandomNumberfornDigits(3);
			wdInit.o_WebCustomerRelationsPage.EnTODAddnotVerifiedVechileDetails(gv.wDriver,vendorname,vehnum,AdhocVechileRate);
			wdInit.o_WebCustomerRelationsPage.verify_EnTODAddnotVerifiedVechileDetailsYesterdayBooking(gv.wDriver,vendorname);
			
		}
}
