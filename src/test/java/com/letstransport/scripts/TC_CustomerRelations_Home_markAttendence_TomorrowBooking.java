package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_markAttendence_TomorrowBooking extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "To verify if Mark attendence for Tomorrow booked vechile ,it should reflect under customer Relation  Tomorrow Booking")
	public void TC_ENTOD_Attendence_ForTomorrow() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_markAttendence_TomorrowBooking");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if Mark attendence for Tomorrow booked vechile ,it should reflect under customer Relation  Tomorrow Bookin", ExtentColor.CYAN));

				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 3000, gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.tomorrowTabOPM(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.EnTODAttendenceFortomorrow(gv.wDriver);
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.verify_EnTODAttendenceFortomorrow(gv.wDriver);
				
				
	}
}
