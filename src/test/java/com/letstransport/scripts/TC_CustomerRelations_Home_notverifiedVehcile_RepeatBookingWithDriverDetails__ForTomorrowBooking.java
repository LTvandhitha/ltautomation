package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_CustomerRelations_Home_notverifiedVehcile_RepeatBookingWithDriverDetails__ForTomorrowBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "To verify if we repeat booking of not verified vehcile with  driver details for Tomorrow Booking it should reflect in today booking ")

	public void TC_RepeatBookingWithDriverDetails_ForNotVerifiedVechileTomorrowBooking() throws Exception{
		
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_notverifiedVehcile_RepeatBookingWithDriverDetails__ForTomorrowBooking");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if we repeat booking of not verified vehcile with  driver details for Tomorrow Booking it should reflect in today booking ", ExtentColor.CYAN));

				
				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
			    wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage._verifyTomorrowBookingWithDriverDetailsInTodayBookingForNonVerifiedVeh(gv.wDriver);
		
		
		
	}
}
