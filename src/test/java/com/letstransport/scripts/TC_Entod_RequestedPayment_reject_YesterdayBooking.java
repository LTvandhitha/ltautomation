package com.letstransport.scripts;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Entod_RequestedPayment_reject_YesterdayBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "TC_Entod_RequestedPayment_reject_YesterdayBooking")
	public void TC_Entod_DriverPayoutPayment_TodayBooking() throws Exception{
		// assign category
		MyExtentListners.test.assignCategory("TC_Entod_RequestedPayment_reject_YesterdayBooking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel(
				"TC_Entod_RequestedPayment_reject_YesterdayBooking",ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations","TC_CustomerRelations_Home_001");
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
        wdInit.o_HomePage.driverVerificationlink(gv.wDriver);
		wdInit.o_DriverVerificationPage.adhocStatus(gv.wDriver);
		wdInit.o_DriverVerificationPage.createdStatus(gv.wDriver);
		String createdIntoVerified = wdInit.o_DriverVerificationPage.getcreatedVecDetails(gv.wDriver);
		System.out.println(createdIntoVerified);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage._clickOnHomeLink(gv.wDriver);
		Thread.sleep(3000);
		wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 20000, gv.wDriver);
        wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations","TC_CustomerRelations_Home_001");
		Thread.sleep(10000);
		String adhocVechileRate = WebActionUtil.generateRandomNumberfornDigits(3);
		wdInit.o_WebCustomerRelationsPage.EnTodAddVechileDetailsYesterday(gv.wDriver, createdIntoVerified,adhocVechileRate);
		
		wdInit.o_DetailVechilePayment._EntodDetailVechilePayment(gv.wDriver,createdIntoVerified);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
        wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
        wdInit.o_WebLtPaymentDashboardPage.validate_PendingPayout_FotYesterdayEntodBooking(gv.wDriver,createdIntoVerified,adhocVechileRate);
        wdInit.o_WebLtPaymentDashboardPage.validate_selectReject_DriverPaymentPayout(gv.wDriver, createdIntoVerified, adhocVechileRate);
	}

}
