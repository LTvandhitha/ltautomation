package com.letstransport.scripts;


import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_POD_deleteLogsheetInfo extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = " verify if user is able to delete Log Sheet Info")
	public void TC_POD_VerifyDeleteLogSheet() throws Exception{
		MyExtentListners.test.assignCategory("TC_POD_deleteLogsheetInfo");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :verify if user is able to delete Log Sheet Info", ExtentColor.CYAN));
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		/*wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, gv.wDriver);*/
		wdInit.o_WebLogSheetPage.Verify_Remove_LogsheetInfo(gv.wDriver);
		
	}

}
