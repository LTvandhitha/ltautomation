package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_POD_enterLogSheetInfo extends BaseLib implements TestDataCoulmns{
	@Test(enabled=true,priority=1,description="verify user is able to enter logsheet info")
	public void TOC_POD_EnterLogSheetInfo() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_POD_enterLogSheetInfo");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : verify user is able to enter logsheet info", ExtentColor.CYAN));
				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				
				String entervecno=wdInit.o_HomePage._getvecNo1();
				wdInit.o_WebTemplatePage._VerifyEnterLogSheetInfo(gv.wDriver,entervecno);			
				
	}

}
