package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_POD_toCreateLogsheetTemplate extends BaseLib implements TestDataCoulmns  {
	@Test(enabled=true,priority=1,description="create logsheet Template")
	
	public void TC_POD_createLogsheet() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory(" TC_POD_toCreateLogsheetTemplate");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :  TC_POD_toCreateLogsheetTemplate", ExtentColor.CYAN));
                InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, gv.wDriver);
				wdInit.o_WebLogSheetPage.Verify_toCreateLogSheetTemplate(gv.wDriver);
				
	}

}
