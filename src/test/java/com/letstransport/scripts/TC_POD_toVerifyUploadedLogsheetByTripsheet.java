package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_POD_toVerifyUploadedLogsheetByTripsheet extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = " To verify if TE( Tripsheet Executive) is able to Verify the LogSheet")
	public void TC_POD_VerifyLogSheetByTripheet() throws Exception{
		MyExtentListners.test.assignCategory("TC_POD_toVerifyUploadedLogsheetByTripsheet");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if TE( Tripsheet Executive) is able to Verify the LogSheet", ExtentColor.CYAN));
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		/*wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, gv.wDriver);*/
		String entervecno=wdInit.o_HomePage._getvecNo1();
		wdInit.o_WebLogSheetPage.Verify_logSheetByTripsheet(gv.wDriver,entervecno);
	}
}
