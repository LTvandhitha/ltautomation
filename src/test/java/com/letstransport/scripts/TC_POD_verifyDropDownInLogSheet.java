package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_POD_verifyDropDownInLogSheet extends BaseLib implements TestDataCoulmns {
	@Test(enabled=true,priority=1,description="To verify both drop down is present in logsheet page")
	public void TC_POD_DropDownInLogSheetPage() throws Exception{
		// assign category
		MyExtentListners.test.assignCategory("TC_POD_verifyDropDownInLogSheet");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :To verify both drop down is present in logsheet page", ExtentColor.CYAN));
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
        wdInit.o_HomePage.verify_dropdownInLogSheetPage();
	}
	

}
