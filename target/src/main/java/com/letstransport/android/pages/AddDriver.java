package com.letstransport.android.pages;

import java.util.List;

import org.apache.tools.ant.taskdefs.Recorder.VerbosityLevelChoices;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.init.InitializePages;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.android.AndroidDriver;

public class AddDriver {
	AndroidDriver driver;

	public AddDriver(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;

		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='_467a410f']")
	private WebElement backBtn1;

	public void _clickOnBackButton() throws Exception {
		MobileActionUtil.waitForElement(backBtn1, driver, "Click On Back Button", 50);
		// MobileActionUtil.clickElement(backBtn1, driver,"Click On Back
		// Button");
		MobileActionUtil.actionClick(backBtn1, driver, "BackButton1");
	}

	@FindBy(xpath = "//div[@class='_47118558']")
	private List<WebElement> changeDriver1;
	@FindBy(xpath = "//div[@class='_8c5dd9dc']")
	private List<WebElement> drivername1;
	@FindBy(xpath = "//div[@class='d3fa8fd0'][1] ")
	private WebElement addDriverLink;
	@FindBy(xpath = "//div[text()='ADD DRIVER']")
	private WebElement addDriver;
	@FindBy(xpath = "//div[@class='d3fa8fd0']")
	private List<WebElement> addDriverList;
	@FindBy(xpath = "//div[@class='bd4bce33']")
	private List<WebElement> adddriver1;
	@FindBy(xpath = "//div[@class='c4c69f04']/input[1]")
	private WebElement driverMobileNoTextField;
	@FindBy(xpath = "//div[@class='c4c69f04']/input[@placeholder='Driver Name']")
	private WebElement driverName;
	@FindBy(xpath = "//div[@class='_00d0dffd']")
	private WebElement addDriverinfo;
	@FindBy(xpath = "//*[@id='app']/div/div[1]/div[1]")
	private WebElement backBtn;
	@FindBy(xpath = "//a[@class='_6d103638']/span[@class='_3ab5a0bd']")
	private WebElement notification;
	@FindBy(xpath = "//div[@class='f5774068'][1]")
	private WebElement notificationmsg;
	@FindBy(xpath = "//div[@class='_21b2491e null _66b16520'][1]")
	private WebElement drivermsg;
	/*
	 * @FindBy(xpath = "//div[text()='Change Driver'][1]") private WebElement
	 * changeDriver;
	 */
	@FindBy(xpath = "//div[text()='Change Driver']")
	private List<WebElement> changeDriver;
	@FindBy(xpath = "//div[@class='_00d0dffd']")
	private WebElement savechanges;
	@FindBy(xpath = "//android.widget.FrameLayout/android.widget.FrameLayout")
	private List<WebElement> notificationele;
	@FindBy(xpath = "//div[@class='_8c5dd9dc'][1]")
	private WebElement drivername;
	@FindBy(xpath = "//div[@class='_780dc3e0'][1]")
	private WebElement changedrivertext;
	@FindBy(xpath = "//div[@class='_780dc3e0'][1]")
	private WebElement nameofDriver;
	@FindBy(xpath = "//div[@class='_74d0a281']")
	private List<WebElement> vehicleVerified;
	@FindBy(xpath = "//div[@class='_71efa8c4']")
	private List<WebElement> vehNo;
	@FindBy(xpath = "//div[@class='_16d8d5b0']")
	private List<WebElement> vehiclepage;
	@FindBy(xpath = "//div[@class='cad2ed5b']/div[2]/child::div/following-sibling::span[text()='Driver']")
	private WebElement Driver;
	@FindBy(xpath = "//div[@class='efdec1dd']")
	private List<WebElement> timeOnDriverMode;

	@FindBy(xpath = "//div[@class='dd8121ec undefined']//div[text()='Vehicle verified'][1]")
	private WebElement vehicleVerified1;
	/*
	 * @FindBy(xpath="//div[@class='bc1e0e0c']") private List<WebElement>
	 * reacheHub;
	 */
	@FindBy(xpath = "//div[text()='Reached at Hub']")
	private WebElement reachedAtHub;
	@FindBy(xpath = "//div[text()='Reached at Hub'][1]")
	private WebElement reachedHub;
	@FindBy(xpath = "//div[@class='e0feddfb']/input")
	private WebElement openingKms;
	@FindBy(xpath = "//div[text()='Done']")
	private WebElement done;
	@FindBy(xpath = "//div[text()='End Trip']")
	private WebElement endTrip;
	@FindBy(xpath = "//div[text()='Trip completed'][1]")
	private WebElement tripCompleted;
	@FindBy(xpath = "//div[@class='f05c4d95 d1287d6d'][1]")
	private WebElement tripcom;

	/*
	 * @FindBy(xpath = "//div[text()='Vehicle verified'][1]") private WebElement
	 * vehicleVerified1;
	 */

	@FindBy(xpath = "//div[text()='ADD DRIVER'][1]")
	private WebElement addDriver1st;

	@FindBy(xpath = "//*[contains(text(),'Vehicles')]")
	private WebElement vehTab;
	@FindBy(xpath = "//div[@class='dd8121ec undefined'][1]")
	private WebElement vecverified;
	@FindBy(xpath = "//div[@class='_47118558'][1]")
	private WebElement chngdriver;

	public void _driverAddedToExistingProfile(AndroidDriver driver) throws Exception {
		MobileActionUtil.clickElement(this.vehTab, driver, "click on vehicle");
		Thread.sleep(10000);
		/*if (this.vehicleVerified1.isDisplayed() && this.addDriver1st.isDisplayed()) {
			MobileActionUtil.clickElement(addDriver, driver, "click on Add Driver");
			MobileActionUtil.clickElement(driverMobileNoTextField, driver, "Click On mobile no Text Field");
			String mobileno = MobileActionUtil.generateRandomNumber(10);
			MobileActionUtil.clearAndType(driverMobileNoTextField, mobileno, "entermobile no", driver);
			MobileActionUtil.clickElement(driverName, driver, "Driver");
			String name = MobileActionUtil.generateRandomCString(4);
			MobileActionUtil.clearAndType(driverName, name, "Nmae of Driver", driver);
			MobileActionUtil.clickElement(addDriverinfo, driver, "Add driver info");
			Thread.sleep(4000);
			_clickOnBackButton();
			Thread.sleep(7000);
			MobileActionUtil.clickElement(notification, driver, "Notification");
			Thread.sleep(1000);
			String text = notificationmsg.getText();
			MobileActionUtil.verifyContainsText(text, name, "driver verification");
		} else {
			MobileActionUtil.scrollToElement(driver, 0.95, 0.50);
			MobileActionUtil.scrollToElement(driver, 0.80, 0.50);*/
			MobileActionUtil.scrollIntoView(driver, addDriver);
			
			Thread.sleep(5000);
			if (this.vehicleVerified1.isDisplayed() && this.addDriver1st.isDisplayed()) {
				MobileActionUtil.clickElement(addDriver, driver, "click on Add Driver");
				MobileActionUtil.clickElement(driverMobileNoTextField, driver, "Click On mobile no Text Field");
				Thread.sleep(2000);
				String mobileno = MobileActionUtil.generateRandomNumber(10);
				MobileActionUtil.clearAndType(driverMobileNoTextField, mobileno, "entermobile no", driver);
				MobileActionUtil.clickElement(driverName, driver, "Driver");
				String name = MobileActionUtil.generateRandomCString(4);
				MobileActionUtil.clearAndType(driverName, name, "Nmae of Driver", driver);
				MobileActionUtil.clickElement(addDriverinfo, driver, "Add driver info");
				Thread.sleep(15000);
				_clickOnBackButton();
				Thread.sleep(500);
				MobileActionUtil.clickElement(notification, driver, "Notification");
				Thread.sleep(1000);
				String text = notificationmsg.getText();
				MobileActionUtil.verifyContainsText(text, name, "driver verification");

			}

		}
	//}

	public void _TrackReportTime() throws Exception {
		WebActionUtil.clickElement(Driver, driver, "Driver");
		Thread.sleep(7000);
		MobileActionUtil.clickElement(reachedHub, driver, "click on reached hub");
		MobileActionUtil.clickElement(openingKms, driver, "click on opening kms");
		MobileActionUtil.clearAndType(openingKms, " 3", "type in opening kms", driver);
		MobileActionUtil.clickElement(done, driver, "click on done");
		Thread.sleep(6000);
		if (endTrip.isDisplayed()) {
			MobileActionUtil.clickElement(endTrip, driver, "End Trip");
		} else {
			MobileActionUtil.clickElement(done, driver, "click on done");
		}
		Thread.sleep(7000);
		MobileActionUtil.clickElement(openingKms, driver, "click on opening kms");
		MobileActionUtil.clearAndType(openingKms, " 4", "type in opening kms", driver);
		MobileActionUtil.clickElement(done, driver, "click on done");
		Thread.sleep(6000);
		String text = tripcom.getText();
		if (tripCompleted.isDisplayed()) {
			MobileActionUtil.verifyElementIsDisplayed(tripCompleted, driver, "Trip Completed");
		} else {
			MobileActionUtil.clickElement(done, driver, "click on done");
			MobileActionUtil.verifyElementIsDisplayed(tripCompleted, driver, "Trip Completed");

		}

	}

	/*@FindBy(xpath = "//span[@class='b27cfa8f']")
	private WebElement chkbox;*/
	@FindBy(xpath="//span[@class='b27cfa8f']/preceding-sibling::input")
	private WebElement chkbox;
	@FindBy(xpath = "//div[@class='_780dc3e0'][1]")
	private WebElement namofDriver;

	public void _changeDriverAddedToExistingProfile() throws Exception {
		InitializePages ip = new InitializePages(driver);
		/*ip.a_homepage._pageload();*/
		Thread.sleep(3000);
		MobileActionUtil.clickElement(ip.a_addveh.clkVehicles(), driver, "click on vehicle");
		Thread.sleep(10000);
		if (vecverified.getText().equals("Vehicle verified") && chngdriver.getText().equals("Change Driver")) {
			MobileActionUtil.actionClick(chngdriver, driver, "click on change driver");
			MobileActionUtil.clickElement(chkbox, driver, "click on checkbox");
			MobileActionUtil.clickElement(savechanges, driver, "SaveChange");
			Thread.sleep(8000);
			String drivernamee = namofDriver.getText();
			_clickOnBackButton();
			Thread.sleep(500);
			MobileActionUtil.clickElement(notification, driver, "Notification");
			String text = drivermsg.getText();
			String[] text1 = text.split(" ");
			String name2 = text1[1];
			MobileActionUtil.verifyContainsTextIgnoreCase(name2, drivernamee, driver);

		}
	}

	public void _clickOnDriver() throws Exception {
		Thread.sleep(10000);
		InitializePages ip = new InitializePages(driver);
		ip.a_addveh._clickOnBackButton();
		Thread.sleep(3000);
		WebActionUtil.clickElement(Driver, driver, "Driver");
		Thread.sleep(10000);
		for (WebElement we : timeOnDriverMode) {
			String getcelldata = ExcelLibrary.getExcelData(GenericLib.sPortalTestDataPath, "WebAddNewBooking", 1, 1);
			if (we.getText().equals(getcelldata)) {

				MobileActionUtil.verifyElementIsDisplayed(reachedAtHub, driver, "Reached at Hub");
				Thread.sleep(1000);

			} else {
				MobileActionUtil.swipeBottomToTop(1, driver, 0.90, 0.50);
				StringBuilder sb = new StringBuilder();

				sb.append(getcelldata);
				sb.deleteCharAt(0);
				String str = sb.toString();
				System.out.println(str);
				if (we.getText().equals(str)) {
					Thread.sleep(500);
					MobileActionUtil.verifyContainsText(we.getText(), str, "Time");
					Thread.sleep(500);
				}
			}
		}

	}

	// }
	@FindBy(xpath = "//div[@class='_27510ffd']")
	private WebElement vecNoinChangdriver;

	public void _validateDriverTaggedToProfile() throws Exception {
		InitializePages ip = new InitializePages(driver);
		MobileActionUtil.clickElement(ip.a_addveh.clkVehicles(), driver, "click on vehicle");
		Thread.sleep(5000);
		String name = nameofDriver.getText();
		System.out.println(name);
		Thread.sleep(10000);
		/*if (addDriverLink.getText().equals("ADD DRIVER")) {
			MobileActionUtil.clickElement(addDriver, driver, "click on Add Driver");
			MobileActionUtil.clickElement(driverMobileNoTextField, driver, "Click On mobile no Text Field");
			String mobileno = MobileActionUtil.generateRandomNumber(10);
			MobileActionUtil.clearAndType(driverMobileNoTextField, mobileno, "entermobile no", driver);
			MobileActionUtil.clickElement(driverName, driver, "Driver");
			MobileActionUtil.clearAndType(driverName, name, "Nmae Of Driver", driver);
			MobileActionUtil.clickElement(addDriverinfo, driver, "Add driver info");
			Thread.sleep(4000);
			if (addDriverLink.getText().equals(name)) {
				String name2 = addDriverLink.getText();
				MobileActionUtil.verifyEqualsText("name", name, name2);

			} else {
                MobileActionUtil.swipeBottomToTop(5, driver, 0.85, 0.75);*/
		        MobileActionUtil.scrollIntoView(driver, addDriver);
				MobileActionUtil.clickElement(addDriver, driver, "click on Add Driver");
				MobileActionUtil.clickElement(driverMobileNoTextField, driver, "Click On mobile no Text Field");
				String mobileno1 = MobileActionUtil.generateRandomNumber(10);
				MobileActionUtil.clearAndType(driverMobileNoTextField, mobileno1, "entermobile no", driver);
				MobileActionUtil.clickElement(driverName, driver, "Driver");
				MobileActionUtil.clearAndType(driverName, name, "Nmae Of Driver", driver);
				MobileActionUtil.clickElement(addDriverinfo, driver, "Add driver info");
				Thread.sleep(4000);
				if (addDriverLink.getText().equals(name)) {
					String name2 = addDriverLink.getText();
					MobileActionUtil.verifyEqualsText("name", name, name2);

				}

			}

		

	}

