package com.letstransport.android.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.android.AndroidDriver;

public class AppHomePage {
	// android driver instance creation
	// WebDriver wdriver;
	AndroidDriver driver;

	public AppHomePage(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;

		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Home Page **/
	@FindBy(xpath = "//a[@href='/profile']")
	private WebElement profileBtn;

	@FindBy(xpath = "//div[text()='NTP']/../../..//div[text()='04th Oct, 11 : 15 PM']/../../../..//div[text()='Booking Amount']/..//div[text()='3000']/../../..//div[text()='Apply']")
	private WebElement applyBtn;

	@FindBy(xpath = "//div[text()='You will soon receive a call from a LetsTransport Agent.']/..//div[text()='Ok']")
	private WebElement oKbtn;

	@FindBy(xpath = "//*[text()='Yes Cancel']")
	private WebElement cancelBtn;

	@FindBy(xpath = "//*[text()='Apply']")
	private WebElement applybtn;

	@FindBy(xpath = "//*[text()='Go Back']/../..//*[text()='Applied']")
	private WebElement appliedBtn;

	@FindBy(xpath = "//*[text()='Go Back']")
	private WebElement goBackbtn;

	@FindBy(xpath = "//a[@href='/mybookings']//div[text()='Bookings']")
	private WebElement bookingsBtn;

	@FindBy(xpath = "//div[text()='Booking Amount']/..//div[text()='3000']/../../..//div[text()='Apply']/../img[@src='/src/images/like-icon.svg']")
	private WebElement BookinBtn;

	@FindBy(xpath = "//div[text()='+ Add Name']/..")
	private WebElement addNameBtn;

	@FindBy(xpath = "//input[@placeholder='Please Tell Us Your Name']")
	private WebElement nameInputBox;

	@FindBy(xpath = "//input[@placeholder='Please Tell Us Your Name']/../../..//div[@class='ebf51e7e']/div")
	private WebElement nameArrowBox;

	@FindBy(xpath = "//a[@href='/profile']/..//div[@class='_2f060a3f']")
	private WebElement profileName;

	@FindBy(xpath = "//div[text()='I agree']")
	private WebElement agreeBtn;
	
	@FindBy(xpath = "//*[text()='Filters']")
	private WebElement filtersBtn;
	
	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;
	
	@FindBy(xpath = "//*[text()='Apply Filter']")
	private WebElement applyFiltersBtn;
	
	@FindBy(xpath = "//*[@placeholder='Select Your Vehicle']")
	private WebElement selectYourVehicle;

	@FindBy(xpath = "//div[text()='The Bank Account details below will be used for payment purposes.']")
	private WebElement bankAffidavitPopUp;

	public WebElement searchBooking() {
		return BookinBtn;
	}

	public WebElement selectProfile() {
		return profileBtn;
	}

	public WebElement clickApply() {
		return applyBtn;
	}

	public WebElement clickBookings() {
		return bookingsBtn;
	}

	@FindBy(xpath = "//div[@class='_0db876c1']")
	private WebElement loader;
	
	@FindBy(xpath = "//*[text()='Select a vehicle ...']")
	private WebElement selectVehicle;
	
	@FindBy(xpath = "//*[@class=' css-i3p6f-option']")
	private WebElement defaultVehicle;

	@FindBy(xpath = "//*[text()='Upcountry']")
	private WebElement upCountryTab;
	
	@FindBy(xpath = "//*[text()='Proceed']")
	private WebElement proceedBtn;

	public WebElement searchListing(String hub, String amount, String reportingTime) {
		return this.driver.findElement(By.xpath("//*[text()='" + hub + "']/../../../../../..//*[text()='"
				+ reportingTime + "']/../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../../..//*[@class='faa3ef29']"));
	}

	public WebElement searchUpcountryListing(String start, String end, String amount, String reportingTime) {
		return this.driver.findElement(By.xpath("//*[text()='" + start + "']/../../..//*[text()='" + end
				+ "']/../../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../..//*[text()='" + reportingTime + "']/../../../../..//*[@class='faa3ef29']"));
	}

	public WebElement getAppliedListing(String hub, String amount, String reportingTime) {
		return this.driver.findElement(By.xpath("//*[text()='" + hub + "']/../../../../../..//*[text()='"
				+ reportingTime + "']/../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../../../../../../..//*[text()='Applied']"));
	}

	public WebElement searchListing1(String hub, String amount, String reportingTime) {
		return this.driver
				.findElement(By.xpath("//*[text()='" + hub + "']/../../../../../..//*[text()='" + reportingTime
						+ "']/../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount + "']"));
	}
	
	public WebElement searchUCListing(String start,String end, String amount, String reportingTime) {
		return this.driver
				.findElement(By.xpath("//*[text()='" + start + "']/../../..//*[text()='" + end
				+ "']/../../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../..//*[text()='" + reportingTime + "']"));
	}

	public String verifyLisitngStatus(AndroidDriver driver, String hub, String amount, String reportingTime,
			String Status) {
		return driver.findElement(By.xpath("//*[text()='" + hub + "']/../../../../../..//*[text()='" + reportingTime
				+ "']/../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../../..//*[text()='" + Status + "']")).getText();
	}

	public String verifyUpCntryLstngStats(AndroidDriver driver, String start, String end, String amount,
			String reportingTime, String status) {
		return driver.findElement(By.xpath("//*[text()='" + start + "']/../../..//*[text()='" + end
				+ "']/../../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../..//*[text()='" + reportingTime + "']/../../../../..//*[text()='" + status + "']"))
				.getText();
	}

	public WebElement getUpCntryLstngAplied(AndroidDriver driver, String start, String end, String amount,
			String reportingTime, String status) {
		return driver.findElement(By.xpath("//*[text()='" + start + "']/../../..//*[text()='" + end
				+ "']/../../../../../..//*[text()='Booking Amount']/../../..//*[text()='" + amount
				+ "']/../../../..//*[text()='" + reportingTime + "']/../../../../..//*[text()='" + status + "']"));
	}
	
	public WebElement getVehicleType(AndroidDriver driver, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='"+vehicle+"']"));
	}
	
	public WebElement setVehicle(AndroidDriver driver,int num) {
		return driver.findElement(By.xpath("(//*[@class=' css-i3p6f-option'])["+num+"]"));
	}

	public void applyListing(String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoViewAndClick(this.driver,
				this.searchListing(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp));
	}

	public void applyListingUpCountry(String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoViewAndClick(this.driver, this.searchUpcountryListing(sData[BaseLib.gv.sPCount],
				sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount], timeStamp));
	}

	public void rejectListing(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoViewAndClick(this.driver, this.getAppliedListing(sData[BaseLib.gv.chCount],
				sData[BaseLib.gv.baCount], timeStamp));

	}

	public void clickOkAndWait() throws Exception {
		//MobileActionUtil.clickElement(this.applybtn, this.driver, "apply button ");
		//MobileActionUtil.waitTillPageLoad("Apply button ", this.driver, "Lisitng page ", 30);
		Thread.sleep(2000);
		MobileActionUtil.clickElement(this.oKbtn, this.driver, "Ok button ");
		Thread.sleep(4000);		
		MobileActionUtil.clickElement(this.goBackbtn, this.driver, "Back button ");
		Thread.sleep(2000);
	}

	public void UndoApply() throws Exception {
		Thread.sleep(2000);
		MobileActionUtil.clickElement(this.appliedBtn, this.driver, "applied button ");
		MobileActionUtil.waitTillPageLoad("Applied button ", this.driver, "Lisitng page ", 30);
		MobileActionUtil.clickElement(this.cancelBtn, this.driver, "Cancel button ");
		MobileActionUtil.waitTillPageLoad("Cancel button ", this.driver, "Lisitng page ", 300);
		MobileActionUtil.clickElement(this.goBackbtn, this.driver, "Back button ");
		Thread.sleep(2000);
	}

	public void verifyAfterApply(String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		/*
		 * int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		 * String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		 * for (int i = 0; i < rowCount; i++) { String[] sData =
		 * GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
		 * System.out.println("Value of "+sData[BaseLib.gv.chCount]);
		 * MobileActionUtil.verifyEqualsText("Applied Listing ", "Applied",
		 * this.verifyLisitngStatus(this.driver,
		 * sData[BaseLib.gv.chCount],sData[BaseLib.gv.baCount],sData[BaseLib.gv.
		 * rdCount]));
		 * 
		 * }
		 */
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.verifyEqualsText("Applied Listing ", "Applied", this.verifyLisitngStatus(this.driver,
				sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp, "Applied"));
	}

	public void verifyUpCntryLstngAfterApply(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		/*
		 * int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		 * String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		 * for (int i = 0; i < rowCount; i++) { String[] sData =
		 * GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
		 * System.out.println("Value of "+sData[BaseLib.gv.chCount]);
		 * MobileActionUtil.verifyEqualsText("Applied Listing ", "Applied",
		 * this.verifyLisitngStatus(this.driver,
		 * sData[BaseLib.gv.chCount],sData[BaseLib.gv.baCount],sData[BaseLib.gv.
		 * rdCount]));
		 * 
		 * }
		 */
		MobileActionUtil.waitTillPageLoad("Loading listing page ", this.driver, "Lisitng page ", 300);
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.verifyEqualsText("Applied Listing ", "Applied", this.verifyUpCntryLstngStats(this.driver,
				sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount], timeStamp, "Applied"));

	}

	public void enterName(String filepath, String sheet, String testcase, AndroidDriver driver) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.clickElement(this.addNameBtn, driver, "Add Name button ");
		MobileActionUtil.type(this.nameInputBox, sData[BaseLib.gv.pnCount], "Partner Name", driver);
		MobileActionUtil.clickElement(this.nameArrowBox, driver, "Name Arrow button");
		// MobileActionUtil.waitTillPageLoad("Name Arrow button ", driver, "Home
		// page ", 300);
		Thread.sleep(5000);
		MobileActionUtil.verifyEqualsText("Varify the Partner Name", this.profileName.getText(),
				sData[BaseLib.gv.pnCount]);
	}

	public void clickOnProfile(AndroidDriver driver) throws Exception {
		Thread.sleep(2000);
		MobileActionUtil.clickElement(this.profileBtn, driver, "Profile link ");
	}

	public void clickOnAgreeBtn(AndroidDriver driver) throws Exception {
		MobileActionUtil.clickElement(this.agreeBtn, driver, "Agree button ");
		Thread.sleep(5000);
	}

	public void verifyBankAffidavit(AndroidDriver driver) throws Exception {
		Thread.sleep(7000);
		MobileActionUtil.isEleDisplayed(this.bankAffidavitPopUp, driver, "Bank Affidavit PopUp");
	}

	public void switchToUpCountry(AndroidDriver driver) throws Exception {
		MobileActionUtil.clickElement(this.upCountryTab, driver, "Up country tab");
		MobileActionUtil.waitTillPageLoad("Up Country Tab", this.driver, "Up Country Listing page ", 300);
	}

	public void _pageload() throws Exception {
		MobileActionUtil.implicit_Wait_ID(loader);
	}

	public void verifyAfterDismiss(String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchListing1(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.verifyEqualsText("Cancelled Listing ", "Cancelled", this.verifyLisitngStatus(this.driver,
				sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp, "Cancelled"));
	}
	
	public void verifyUCAfterDismiss(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchUCListing(sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount],timeStamp));
		MobileActionUtil.verifyEqualsText("Cancelled Listing ", "Cancelled", this.verifyUpCntryLstngStats(this.driver,
				sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount], timeStamp, "Cancelled"));
	}

	public void verifyAfterBooking(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchListing1(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.verifyEqualsText("Booked Listing ", "Booked", this.verifyLisitngStatus(this.driver,
				sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp, "Booked"));
	}
	
	public void verifyUCLAfterBooking(String filepath, String sheet, String testcase, String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchUCListing(sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount],sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.verifyEqualsText("Booked Listing ", "Booked", this.verifyUpCntryLstngStats(this.driver,
				sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount],sData[BaseLib.gv.baCount], timeStamp, "Booked"));
	}

	public void verifyAfterReject(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchListing1(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.isEleDisplayed(
				this.searchListing(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp),
				driver, "Arrow button");
	}

	public void getUpCntryLstngApled(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		/*
		 * int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		 * String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		 * for (int i = 0; i < rowCount; i++) { String[] sData =
		 * GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
		 * System.out.println("Value of "+sData[BaseLib.gv.chCount]);
		 * MobileActionUtil.verifyEqualsText("Applied Listing ", "Applied",
		 * this.verifyLisitngStatus(this.driver,
		 * sData[BaseLib.gv.chCount],sData[BaseLib.gv.baCount],sData[BaseLib.gv.
		 * rdCount]));
		 * 
		 * }
		 */
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoViewAndClick(this.driver, this.getUpCntryLstngAplied(this.driver, sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount],
				sData[BaseLib.gv.baCount], timeStamp, "Applied"));

	}

	public void verifyAfterRejectUpCountry(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchUpcountryListing(sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.isEleDisplayed(
				this.searchUpcountryListing(sData[BaseLib.gv.sPCount], sData[BaseLib.gv.ePCount], sData[BaseLib.gv.baCount], timeStamp),
				this.driver, "Arrow button");
	}
	
	public void verifyListing(String filepath, String sheet, String testcase,String timeStamp) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		MobileActionUtil.scrollIntoView(this.driver,
				this.searchListing1(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp));
		MobileActionUtil.isEleDisplayed(
				this.searchListing(sData[BaseLib.gv.chCount], sData[BaseLib.gv.baCount], timeStamp),
				driver, "Arrow button");
	}
	
	public void vehicleFilter(AndroidDriver driver) throws Exception{
		MobileActionUtil.clickElement(this.filtersBtn, driver, "filters button");
		MobileActionUtil.clickElement(this.selectYourVehicle, driver, "select Your Vehicle");
		MobileActionUtil.scrollIntoViewAndClick(driver, this.getVehicleType(driver, "Omni 800 kg"));
		MobileActionUtil.clickElement(this.doneBtn, driver, "Done button");
		MobileActionUtil.clickElement(this.applyFiltersBtn, driver, "Apply filters");
		Thread.sleep(2000);
	}
	
	public void selectVehicle(AndroidDriver driver,int number) throws Exception {
		MobileActionUtil.clickElement(this.selectVehicle, driver, "Select Vehicle ");
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.setVehicle(driver,number), driver, "default Vehicle ");
		MobileActionUtil.clickElement(this.proceedBtn, driver, "Proceed Btn ");
	}
	
	public void clickApply(AndroidDriver driver) throws Exception {
		Thread.sleep(1000);
		MobileActionUtil.clickElement(this.applybtn, this.driver, "apply button ");

	}
	
	public void partnerSignOut(AndroidDriver driver) throws Exception {
		MobileActionUtil.clickElement(this.profileName, this.driver, "profile button ");

	}
}
