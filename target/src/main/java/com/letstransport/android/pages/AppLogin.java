package com.letstransport.android.pages;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class AppLogin {
	// android driver instance creation
	AndroidDriver<MobileElement> driver;

	public AppLogin(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	@FindBy(xpath = "//android.widget.ImageButton[@resource-id='in.letstransport.vendor_app.test:id/get_otp_button']")
	private WebElement otpBtnNext;

	@FindBy(xpath = "//android.widget.LinearLayout[@index='3']")
	private WebElement bangloreBtn;

	@FindBy(xpath = "//android.widget.ImageButton[@resource-id='in.letstransport.vendor_app.test:id/next_button']")
	private WebElement nextBtn;

	@FindBy(xpath = "//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']")
	private WebElement allowBtn;

	@FindBy(xpath = "//span[contains(text(),'English')]")
	private WebElement englishLanguage;

	@FindBy(xpath = "//android.widget.EditText[@resource-id='in.letstransport.vendor_app.test:id/otp_view']")
	private WebElement otpview;

	@FindBy(xpath = "//div[contains(text(),'Select Language')]/..//div[@class='ebf51e7e']/div")
	private WebElement selectLanguageBtn;

	@FindBy(xpath = "//div[contains(text(),'Enter Referral Code')]/..//div[@class='ebf51e7e']/div")
	private WebElement referalCodeBtn;
	
	@FindBy(xpath = "//a[@href='/profile']")
	private WebElement profileBtn;
	
	@FindBy(xpath = "//a[@href='/profile']")
	private List<WebElement> profileBtns;
	
	@FindBy(xpath = "//div[text()='Log out']")
	private WebElement logoutBtn;
	
	@FindBy(xpath = "//div[text()='Are you sure you want to logout?']/..//span[text()='Yes']")
	private WebElement yesBtn;

	public WebElement tapNextBtn() {
		return nextBtn;
	}

	public WebElement selectNumber(String Number) {
		return this.driver.findElement(By.xpath("//android.widget.TextView[@text=\'" + Number + "\']"));
	}

	public WebElement MobileNumberPopUp() {
		return this.driver.findElement(By.xpath("//android.widget.Button[@text='NONE OF THE ABOVE']"));
	}

	public void signUp(String filepath, String sheet) throws Exception {
		int rowCount = ExcelLibrary.getExcelRowCount(filepath, sheet);
		String[] sTestCaseIds = GenericLib.readTestCaseIds(filepath, sheet);
		for (int i = 0; i < rowCount; i++) {
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, sTestCaseIds[i]);
			if ((sData[BaseLib.gv.mCount].equalsIgnoreCase("091644 01565"))) {
				MobileActionUtil.clickElement(this.selectNumber(sData[BaseLib.gv.mCount]), this.driver,
						"Mobile Number ");
				break;
			}
		}
		MobileActionUtil.clickElement(this.otpBtnNext, this.driver, "Arrow button ");
		Thread.sleep(5000);
		/*
		 * driver.openNotifications(); Thread.sleep(2000); String aaa =
		 * driver.findElementByXPath(
		 * "//android.widget.TextView[@text='VMLETSTR']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]"
		 * ).getText(); Thread.sleep(20000);
		 * System.out.println("aaaaaaaaaaaaaaaaaa "+aaa);
		 * driver.pressKeyCode(AndroidKeyCode.BACK);
		 * 
		 * /*
		 * 
		 * String otp = driver.findElement(By.
		 * xpath("//*[contains(text(),'is your login OTP')]")).getText().
		 * split(" is")[0]; System.out.println(otp); driver.navigate().back();
		 * Thread.sleep(20000);
		 * 
		 * driver.startActivity("com.google.android.apps.messaging",
		 * "com.google.android.apps.messaging.ui.ConversationListActivity");
		 * String getOTPValue = driver.
		 * findElementByXPath("//android.widget.TextView[contains(@text='is your login OTP')]"
		 * ).getText().split(" is")[0].trim(); System.out.println(getOTPValue);
		 */
		// below elements is to handle Google location services
		// driver.findElementByXPath("//android.widget.TextView[contains(@text='To
		// continue, turn on device location, which uses Google�s location
		// service ')]");
		// driver.findElementByXPath("//android.widget.Button[contains(@text='OK')]");

	}

	public void partnerSignUp(String number, AndroidDriver adriver, WebDriver wdriver) throws Exception {
		//MobileActionUtil.clickElement(this.MobileNumberPopUp(), adriver, "Choose number popup");
		MobileActionUtil.enterMobileNumber(number, adriver);
		// Thread.sleep(1000);
		MobileActionUtil.clickElement(this.otpBtnNext, adriver, "Arrow button ");
		// Thread.sleep(3000);
		MobileActionUtil.clickElement(this.otpview, adriver, "input box ");
		MobileActionUtil.enterOTP(adriver, wdriver);
		// Thread.sleep(3000);
		// MobileActionUtil.clickElement(this.bangloreBtn, adriver, "Banglore
		// city ");
		MobileActionUtil.clickElement(this.nextBtn, adriver, "Next button ");
		Thread.sleep(8000);
		// MobileActionUtil.clickElement(this.allowBtn, adriver, "Location allow
		// button ");
		MobileActionUtil.switchToWebView(adriver);
		// MobileActionUtil.waitTillPageLoad("City button ", driver, "Home page
		// ", 300);
		// MobileActionUtil.waitTillElementVisible(this.englishLanguage, driver,
		// "English Language", 30);
		//MobileActionUtil.switchToView(driver);
		Thread.sleep(15000);
		MobileActionUtil.verifyEqualsText("Verifing the Partner Login ", this.englishLanguage.getText(), "English");
		MobileActionUtil.clickElement(this.englishLanguage, adriver, " Select Language ");
		MobileActionUtil.clickElement(this.selectLanguageBtn, adriver, "Arrow button ");
		// MobileActionUtil.waitTillPageLoad("Select Language button ", adriver,
		// "Home page ", 300);
		Thread.sleep(25000);
		// MobileActionUtil.waitForElement(this.referalCodeBtn, adriver,
		// "English language", 300);
		MobileActionUtil.clickElement(this.referalCodeBtn, adriver, " Referral Button ");
		Thread.sleep(6000);
		// MobileActionUtil.waitTillPageLoad("referal btn", adriver, "Home
		// page", 300);
	}
	
	public void partnerSignOut(AndroidDriver adriver) throws Exception {
		MobileActionUtil.switchToWebView(adriver);	
		MobileActionUtil.clickElement(this.profileBtn, adriver, "profile button");
		MobileActionUtil.clickElement(this.logoutBtn, adriver, "logout button");
		MobileActionUtil.clickElement(this.yesBtn, adriver, "yes button");
		MobileActionUtil.switchToAndroidVeiw(adriver);
		Thread.sleep(5000);
	}
}
