package com.letstransport.android.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.letstransport.init.InitializePages;


import io.appium.java_client.android.AndroidDriver;

public class AppUpcomingBookingsPage {
	AndroidDriver driver;

	public AppUpcomingBookingsPage(AndroidDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}


	/** upcoming Page **/	
	@FindBy(xpath="//div[text()='KA02KJ9870']/../../../../../..//*[text()='30th Aug']/../../../../../..//*[text()='3 : 40 PM']")
	private WebElement upcomingBooking;
	
	
	public WebElement searchUpcomingBooking() {
		return upcomingBooking;
	}
	
}