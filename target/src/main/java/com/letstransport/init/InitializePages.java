package com.letstransport.init;

import org.openqa.selenium.WebDriver;

import com.letstransport.android.pages.AppLogin;
import com.letstransport.android.pages.AppProfilePage;

import com.letstransport.android.pages.AddDriver;

import com.letstransport.android.pages.AddVehicle;
import com.letstransport.android.pages.AppHomePage;
import com.letstransport.android.pages.AppUpcomingBookingsPage;
import com.letstransport.web.pages.AddBookingPage;
import com.letstransport.web.pages.ClientAndDriverPricingPage;
import com.letstransport.web.pages.ClientOnboardingPage;
import com.letstransport.web.pages.DetailVechilePayment;
import com.letstransport.web.pages.DetailedVehiclePaymentsPage;
import com.letstransport.web.pages.DriverExpensesPage;
import com.letstransport.web.pages.DriverVerificationPage;
import com.letstransport.web.pages.EnterpriseODArrangedPage;
import com.letstransport.web.pages.FinancePage;
import com.letstransport.web.pages.PendingHubsPage;
import com.letstransport.web.pages.ProcessVehiclePaymentsPage;
import com.letstransport.web.pages.UploadTripSheetPage;
import com.letstransport.web.pages.VerifyTripSheetPage;
import com.letstransport.web.pages.WebAddArea;
import com.letstransport.web.pages.WebAddCostingPage;
import com.letstransport.web.pages.WebAddNewBookingPage;
import com.letstransport.web.pages.WebAdvancePaymentPage;
import com.letstransport.web.pages.WebBulkUpdate;
import com.letstransport.web.pages.WebCustomerRelationsPage;
import com.letstransport.web.pages.WebDriverHelpline;
import com.letstransport.web.pages.WebHomePage;
import com.letstransport.web.pages.WebLoadBoardPage;
import com.letstransport.web.pages.WebLogSheetPage;
import com.letstransport.web.pages.WebLoginPage;

import com.letstransport.web.pages.WebLabour;
import com.letstransport.web.pages.WebLtPaymentDashboardPage;
import com.letstransport.web.pages.WebTemplatePage;
import com.letstransport.web.pages.WebVehiclesPage;
import com.letstransport.web.pages.WebVerifyDashBoardPage;

import io.appium.java_client.android.AndroidDriver;

public class InitializePages {

	/** APP CLASSES **/

	public AppLogin a_login = null;
	public AppHomePage a_homepage = null;
	public AppUpcomingBookingsPage a_upcomingBookingsPage = null;
	public AppProfilePage a_AppProfilePage = null;
	public AddVehicle a_addveh= null;
	public AddDriver  a_adddriver=null;



	/** PORTAL CLASSES **/

	public WebLoginPage o_login = null;
	public WebHomePage o_HomePage = null;
	public WebLoadBoardPage o_LoadBoardPage = null;
	public WebAddArea o_addarea= null;
	public WebVehiclesPage o_addveh= null;
	public WebVerifyDashBoardPage o_VerifyDashBoardPage = null;
	public WebAddCostingPage o_WebAddCostingPage = null;
	public WebLtPaymentDashboardPage o_WebLtPaymentDashboardPage = null;
	public ClientAndDriverPricingPage o_ClientAndDriverPricingPage = null;
    public WebAdvancePaymentPage o_WebAdvancePaymentPage=null;
	public WebLogSheetPage o_WebLogSheetPage=null;
	public WebTemplatePage o_WebTemplatePage=null;
	public WebAddNewBookingPage o_WebAddNewBookingPage=null;
	public DetailVechilePayment o_DetailVechilePayment=null;
	public AddBookingPage o_AddBookingPage=null;
	public UploadTripSheetPage o_UploadTripSheetPage=null;
	public VerifyTripSheetPage o_VerifyTripSheetPage=null;
	public DriverVerificationPage o_DriverVerificationPage=null;
	public DetailedVehiclePaymentsPage o_DetailedVehiclePaymentsPage=null;
	public ProcessVehiclePaymentsPage o_ProcessVehiclePaymentsPage =null;
	public FinancePage o_FinancePage=null;
	public WebDriverHelpline o_DriverHelpline = null;
	public WebBulkUpdate o_BulkUpdate = null;
	public WebCustomerRelationsPage o_WebCustomerRelationsPage=null;
	//public TC_002__BulkUpdate o_ticknum= null;
	public EnterpriseODArrangedPage o_EntODArrangedPage=null;
	public DriverExpensesPage o_DriverExpensesPage=null;
	public ClientOnboardingPage o_ClientOnboardingPage=null;
	public PendingHubsPage o_PendingHubsPage=null;
    public WebLabour o_labour= null;


	/** APP CLASSES INITIALISATION **/

	public InitializePages(AndroidDriver driver) {

		a_login = new AppLogin(driver);
		a_homepage = new AppHomePage(driver);
		a_upcomingBookingsPage = new AppUpcomingBookingsPage(driver);
		a_AppProfilePage = new AppProfilePage(driver);
		a_addveh= new AddVehicle(driver);
        a_adddriver=new AddDriver(driver);
        



	}

	/** PORTAL CLASSES INITIALISATION **/

	public InitializePages(WebDriver driver) {

		o_login = new WebLoginPage(driver);
		o_HomePage = new WebHomePage(driver);
		o_LoadBoardPage = new WebLoadBoardPage(driver);
		o_VerifyDashBoardPage = new WebVerifyDashBoardPage(driver);
		o_WebAddCostingPage = new WebAddCostingPage(driver);
		o_WebLtPaymentDashboardPage = new WebLtPaymentDashboardPage(driver);
		o_addarea= new WebAddArea(driver);
		o_addveh= new WebVehiclesPage(driver);
		o_ClientAndDriverPricingPage = new ClientAndDriverPricingPage(driver);
        o_WebLogSheetPage = new WebLogSheetPage(driver);
		o_WebTemplatePage=new WebTemplatePage(driver);
		o_WebAddNewBookingPage=new  WebAddNewBookingPage(driver);
		o_WebAdvancePaymentPage=new WebAdvancePaymentPage(driver);
		o_DetailVechilePayment=new DetailVechilePayment(driver);
		o_AddBookingPage = new AddBookingPage(driver);
		o_UploadTripSheetPage = new UploadTripSheetPage(driver);
		o_VerifyTripSheetPage = new VerifyTripSheetPage(driver);
		o_DriverVerificationPage = new DriverVerificationPage(driver);
		o_DetailedVehiclePaymentsPage = new DetailedVehiclePaymentsPage(driver);
		o_ProcessVehiclePaymentsPage = new ProcessVehiclePaymentsPage(driver);
		o_FinancePage = new FinancePage(driver);
		o_DriverHelpline= new WebDriverHelpline( driver); 
		o_BulkUpdate= new WebBulkUpdate(driver);
		o_WebCustomerRelationsPage=new WebCustomerRelationsPage(driver);
		//o_ticknum = new TC_002__BulkUpdate();
		o_EntODArrangedPage = new EnterpriseODArrangedPage(driver);
		o_DriverExpensesPage =new DriverExpensesPage(driver);
		o_ClientOnboardingPage=new ClientOnboardingPage(driver);
		o_PendingHubsPage = new PendingHubsPage(driver);
		o_labour = new WebLabour (driver);
	}
	
}
