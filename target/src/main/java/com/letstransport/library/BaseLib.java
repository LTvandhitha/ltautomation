/***********************************************************************
* @author 				:		Vandhitha Rao
* @description			: 		Implemented Application Precondition and Postconditions
* @Variables			: 	  	Declared and Initialised AndroidDriver and WebDriver, Instance for GlobalVariables Page
* @BeforeSuiteMethod	: 		DB connection for xyz
* @BeforeTest			: 		Desired Capabilities for launching app and launching portal		
*/

package com.letstransport.library;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.letstransport.init.GlobalVariables;
import com.paulhammant.ngwebdriver.NgWebDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseLib {

	public static GlobalVariables gv = new GlobalVariables();

	/*
	 * Read Parameters from Jenkins
	 */

	static {

		if (gv.sUDID != null) {
			gv.iPort = Integer.parseInt(System.getProperty("PORT"));
			gv.sUDID = System.getProperty("UDID");
			gv.sVersion = System.getProperty("VERSION");
			gv.sDeviceName = System.getProperty("DEVICENAME");
			gv.browser = System.getProperty("CHROME");

		} else {

			int rowCount = ExcelLibrary.getExcelRowCount(GenericLib.sConfigPath, "config");
			System.out.println(" Total Row Count ============> " + rowCount);
			ArrayList<String> deviceCount = new ArrayList<String>();
			int runStatus = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config", "Run Status");
			for (int i = 1; i <= rowCount; i++) {

				if (ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, runStatus).equalsIgnoreCase("Yes")) {
					// System.out.println(i);
					deviceCount.add(ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, runStatus));
				}
			}
			System.out.println(deviceCount.size());

			if (String.valueOf(deviceCount.size()).equalsIgnoreCase("1")) {
				for (int i = 1; i <= rowCount; i++) {

					if (ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, runStatus)
							.equalsIgnoreCase("Yes")) {

						int port = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config", "Port");
						int udid = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config", "Device UDID");
						int devName = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config", "Device Name");
						int devVersion = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config",
								"Device Version");
						int browserType = GenericLib.getHeaderColumnIndex(GenericLib.sConfigPath, "config", "Browser");
						gv.iPort = Integer
								.parseInt(ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, port).trim());
						gv.sUDID = ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, udid).trim();
						gv.sDeviceName = ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, devName).trim();

						gv.sVersion = ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, devVersion).trim();
						gv.browser = ExcelLibrary.getExcelData(GenericLib.sConfigPath, "config", i, browserType).trim();

					}
				}
			} else {

				System.out.println("************PLEASE SELECT ONE DEVICE IN CONFIG******************");
			}
		}
	}

	/*
	 * This method initializes the database variables that requires to connect
	 * to the database before suite
	 */

	@BeforeSuite
	public void before_suite() throws Exception {

	}

	/**
	 * Description : This Function launch the app based on capabilities provided
	 * by testng.xml file
	 * 
	 * @param port
	 * @param UDID
	 * @param version
	 * @param deviceName
	 * @throws Exception
	 */

	@BeforeMethod
	public void _LaunchApp() throws Exception {

		// ** Launch App

		gv.capabilities = new DesiredCapabilities();
		gv.capabilities.setCapability("automationName", "Appium");
		//gv.capabilities.setCapability("automationName", "selendroid");
		gv.capabilities.setCapability("platformName", "Android");
		gv.capabilities.setCapability("platformVersion", gv.sVersion);
		gv.capabilities.setCapability("deviceName", gv.sDeviceName);
		gv.capabilities.setCapability("UDID", gv.sUDID);
		gv.capabilities.setCapability("chromedriverExecutable", GenericLib.sChromeDriverPath);
		// gv.capabilities.setCapability("deviceID", "192.168.43.101:5555");
		gv.capabilities.setCapability("appPackage", "in.letstransport.vendor_app.test");
		gv.capabilities.setCapability("appActivity", "in.letstransport.vendor_app.activity.SplashScreen");
		gv.capabilities.setCapability("fullReset", false);
		gv.capabilities.setCapability("noReset", true);
		gv.capabilities.setCapability("skipUnlock ", false);
		gv.capabilities.setCapability("appWaitDuration", 30000);
		gv.capabilities.setCapability("deviceReadyTimeout", 1);
		gv.capabilities.setCapability("intentAction", "android.intent.action.MAIN");
		gv.capabilities.setCapability("noSign", true);
		gv.capabilities.setCapability("autoGrantPermissions", true);
		gv.capabilities.setCapability("autoAcceptAlerts", true);
		gv.capabilities.setCapability("newCommandTimeout", 60000);
		// Skip the installation of io.appium.settings app and the UIAutomator 2 server.
		//gv.capabilities.setCapability("skipDeviceInitialization", true);
		//gv.capabilities.setCapability("skipServerInstallation", true);
		gv.capabilities.setCapability("adbExecTimeout", 120000);
		//gv.capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,
		// "60");
		System.out.println("http://127.0.0.1:" + gv.iPort + "/wd/hub");
		gv.aDriver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:" + gv.iPort + "/wd/hub"),
				gv.capabilities);
		System.out.println("----------------appium driver initialised-------------------");

		/** Launch Browser **/

		if (gv.browser.equalsIgnoreCase("Firefox")) {
			// FirefoxDriverManager.getInstance().version("0.17.0").setup();

			WebDriverManager.firefoxdriver().setup();

			gv.wDriver = new FirefoxDriver();
			gv.wDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
			gv.ngWebDriver = new NgWebDriver((FirefoxDriver) gv.wDriver);
		} else if (gv.browser.equalsIgnoreCase("Chrome")) {

			// ChromeDriverManager.getInstance().setup();

			WebDriverManager.chromedriver().setup();

			String downloadFilepath = GenericLib.sExportFilesDir;
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.prompt_for_download", "false");
			chromePrefs.put("download.default_directory", downloadFilepath);
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			options.setPageLoadStrategy(PageLoadStrategy.NONE);
			// DesiredCapabilities cap = DesiredCapabilities.chrome();
			// cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			// cap.setCapability(ChromeOptions.CAPABILITY, options);
			gv.wDriver = new ChromeDriver(options);
			gv.wDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
			gv.ngWebDriver = new NgWebDriver((ChromeDriver) gv.wDriver);
			gv.wDriver.get(gv.sPortal_Url);
			gv.wDriver.manage().window().maximize();
			gv.ngWebDriver.waitForAngularRequestsToFinish();
		} else {
			// InternetExplorerDriverManager.getInstance().setup();

			WebDriverManager.iedriver().setup();

			gv.wDriver = new InternetExplorerDriver();
			gv.wDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
			gv.ngWebDriver = new NgWebDriver((InternetExplorerDriver) gv.wDriver);
		}

		System.out.println("----------------Browser Launched-------------------");

	}

	@AfterMethod()
	public void after_suite() throws Exception {		

          gv.aDriver.quit();
          gv.wDriver.quit();

	}

	@AfterSuite
	public void OracleCloseConnection() throws Exception {

		// gv.aDriver.quit();
		// System.out.println("----------DB Connection Closed---------");

		/*
		 * String sFile = GenericLib.sDirPath + "/excelreport.properties";
		 * String fileName = GenericLib.getProprtyValue(sFile,
		 * "excelreport.file.name") + ".xlsx"; String excelDir =
		 * GenericLib.getProprtyValue(sFile, "excelreport.outputdir");
		 * System.out.println("Excel File Name " + fileName);
		 */
	}

}
