/***********************************************************************
 * @author 			:		Vandhitha Rao
 * @description		: 		This class contains action methods which is used for performing 
 * 							action while executing script such as Click, SendKeys 
 */

package com.letstransport.util;

import java.awt.AWTException;

import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.SystemPropertyUtils;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.utils.Reader;
import com.aventstack.extentreports.utils.Writer;
import com.github.javafaker.Faker;
import com.google.common.collect.Table;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.paulhammant.ngwebdriver.NgWebDriver;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class WebActionUtil {

	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void clickByJavaSriptExecutor(WebElement element, WebDriver driver, String elementName)
			throws Exception {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			waitTillPageLoad(driver, 60);
			isEleClickable(element, driver, elementName);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			WebActionUtil.waitTillPageLoad(driver, 30);
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	/*
	 * @author : Vandhitha Rao
	 * 
	 * Description : This method has fluent wait implementation for element to
	 * load which is polling every 250 miliseconds
	 */
	public static void waitForElement(WebElement element, WebDriver driver, String eleName, int seconds)
			throws IOException {
		try {
			logger.info("---------Waiting for visibility of element---------" + element);

			waitTillPageLoad(driver, 30);
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);

			logger.info("---------Element is visible---------" + element);
		} catch (Exception e) {

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		} catch (AssertionError e) {
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}

	public static void pressEnterKey() throws Exception {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * present page or not.
	 */
	public static void isEleDisplayed(WebElement element, WebDriver driver, String elementName, int seconds)
			throws IOException {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			waitTillPageLoad(driver, seconds);
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			System.out.println(elementName + "------ is displayed");
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed || " + "\'" + elementName
					+ "\'" + " is displayed ");
		} catch (RuntimeException e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * present page or not.
	 */
	public static boolean presenceOfElement(WebElement element, int seconds, int loop)
			throws IOException, InterruptedException {
		boolean flag = false;

		int count = loop;
		while (count > 0) {
			try {
				logger.info("---------Verifying element is displayed or not ---------");
				count--;
				element.isDisplayed();
				flag = true;
				break;

			} catch (RuntimeException e) {
				Thread.sleep(seconds * 1000);
				flag = false;
			}
		}
		return flag;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * present page or not without generating extent report for failure
	 */
	public static void isElementDisplayed(WebElement element, WebDriver driver, String elementName, int seconds)
			throws IOException {

		try {
			logger.info("---------Verifying element is displayed or not ---------");
			waitTillPageLoad(driver, seconds);
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			System.out.println(elementName + "------ is displayed");
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed || " + "\'" + elementName
					+ "\'" + " is displayed ");
		} catch (Exception e) {

			System.out.println(elementName + "------ is not displayed");
		}
	}

	public static String generateRandomNumberfornDigits(int n) {

		// int n = 10;
		// chose a Character random from this String
		String AlphaNumericString = "123456789";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}
	public static String generateRandomNumberfornDigits1(int n) {


		//int n = 10;
		// chose a Character random from this String
		String AlphaNumericString = "123456789";


		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}
	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * present page or not.
	 */
	public static void eleIsNotDisplayed(List<WebElement> element, WebDriver driver, String elementName, int seconds)
			throws IOException {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			waitTillPageLoad(driver, 30);
			/*
			 * Wait<WebDriver> wait = new
			 * FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(
			 * seconds)) .pollingEvery(Duration.ofMillis(250)).ignoring(
			 * NoSuchElementException.class);
			 * Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(
			 * element)) == null);
			 */
			Assert.assertTrue(element.isEmpty());
			System.out.println(elementName + "------ is not displayed");
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is not displayed || " + "\'"
					+ elementName + "\'" + " is not displayed ");
		} catch (RuntimeException e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed || " + "\'" + elementName + "\'" + " is displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(elementName + "------ is  displayed");
			throw e;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * present page or not.
	 */
	public static void eleIsNotDisplayed(WebElement element, WebDriver driver, String elementName, int seconds)
			throws NoSuchElementException, IOException {

		if (element.isDisplayed()) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed || " + "\'" + elementName + "\'" + " is displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(elementName + "------ is  displayed");

		} else {
			System.out.println(elementName + "------ is not displayed");
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is not displayed || " + "\'"
					+ elementName + "\'" + " is not displayed ");
		}

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify whether given webelemnt is
	 * displayed or not
	 */
	public static void verifyElementIsDisplayed(WebElement element, WebDriver driver, String elementName)
			throws IOException {
		try {
			logger.info("---------Verifying element is displayed or not ---------");
			waitTillPageLoad(driver, 30);
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofMinutes(1))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed  || " + "\'" + elementName
					+ "\'" + " is displayed ");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed  || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + elementName + "\'"
					+ " is displayed  || " + "\'" + elementName + "\'" + " is not displayed ", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(elementName + "------ is not displayed");
			throw e;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: this method will click on element which is provided.
	 */

	public static void clickElement(WebElement element, WebDriver driver, String elementName) throws IOException {

		try {
			logger.info("---------Verifying element is displayed or not ---------");

			waitTillPageLoad(driver, 70);
			isEleClickable(element, driver, elementName);
			// Thread.sleep(1000);

			element.click();
			WebActionUtil.waitTillPageLoad(driver, 30);
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: this method clear texts from text box/edit box and type the
	 * value which is provided
	 * 
	 */

	public static void clearAndType(WebElement element, String value, String elementName, WebDriver driver)
			throws Exception {
		try {
			logger.info("---------Method clear and type  ---------");
			waitTillPageLoad(driver, 30);
			element.clear();
			logger.info(elementName + " is cleared");
			element.sendKeys(value);
			logger.info(value + " is entered in " + elementName);
			logger.info(" hide keyboard");
			MyExtentListners.test.pass("Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName
					+ "\'" + " || User is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'");
		} catch (AssertionError error) {

			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type in " + "\'" + elementName + "\'");
		}

	}
	/*
	 * public static void longPress(WebElement element, WebDriver driver, String
	 * elementName) throws IOException {
	 * 
	 * try { isEleClickable(element, driver, elementName); Actions action = new
	 * Actions(driver); action.clickAndHold(element).click().build().perform();
	 * MyExtentListners.test.pass("Verify user is able to click on " + "\'" +
	 * elementName + "\'" + " ||  User is able to click on " + "\'" +
	 * elementName + "\'"); } catch (AssertionError error) {
	 * MyExtentListners.test.fail(MarkupHelper.
	 * createLabel("Verify user is able to click on " + "\'" + elementName +
	 * "\'" + "  || User is not able to click on " + "\'" + elementName + "\'",
	 * ExtentColor.RED));
	 * 
	 * MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
	 * Assert.fail("unable to Click on " + "\'" + elementName + "\'");
	 * 
	 * MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
	 * throw error; } catch (Exception e) {
	 * MyExtentListners.test.fail(MarkupHelper.
	 * createLabel("Verify user is able to click on " + "\'" + elementName +
	 * "\'" + " || User is not able to click on " + "\'" + elementName + "\'",
	 * ExtentColor.RED));
	 * 
	 * MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
	 * throw e; }
	 * 
	 * } <<<<<<< Updated upstream
	 */

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Method to type value in element
	 * 
	 */

	public static void type(WebElement element, String value, String elementName, WebDriver driver) throws Exception {
		try {
			logger.info("---------Method type  ---------");
			waitTillPageLoad(driver, 30);
			waitForElement(element, driver, elementName, 30);
			element.sendKeys(value);
			logger.info("---------hide keyboard  ---------");
			MyExtentListners.test.pass("Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName
					+ "\'" + " || User is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type on " + elementName);
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type in " + elementName);
		}

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Code to wait till page is load , Page status should be ready
	 */
	public static void waitTillPageLoad(WebDriver driver, int seconds) {

		WebDriverWait wait = new WebDriverWait(driver, seconds);
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		// Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = wd -> ((JavascriptExecutor) driver)
				.executeScript("return document.readyState").toString().equals("complete");

		// Get JS is Ready
		boolean jsReady = (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");

		// Wait Javascript until it is Ready!
		if (!jsReady) {
			System.out.println("JS in NOT Ready!");

			// Wait for Javascript to load
			wait.until(jsLoad);

		} else {
			System.out.println("JS is Ready!");

		}
		System.out.println(" page is in ready state ");
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description:Explicit wait to check element is clickable
	 */
	public static void isEleClickable(WebElement element, WebDriver driver, String eleName) throws IOException {
		try {
			logger.info("---------Method is Element clickable  ---------");
			System.out.println(element);
			long timeout = 30;
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			if (!(wait.until(ExpectedConditions.elementToBeClickable(element)) == null)) {
				Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
				System.out.println("Eleement is Clickable");
			} else {
				System.out.println("Eleement is not Clickable");
			}

			System.out.println(" element is clickable ");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + eleName + "\'" + " is clickable || "
					+ "\'" + eleName + "\'" + " is not clickable", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(" element is not clickable ");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + "\'" + eleName + "\'" + " is clickable || "
					+ "\'" + eleName + "\'" + " is not clickable", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			System.out.println(" element is not clickable ");
			throw e;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Fetch text from element and return as string
	 */

	public static String gettext(WebElement element, WebDriver driver, String elementName) throws IOException {
		logger.info("--------- get text from element  ---------");
		String eleText = null;
		try {
			waitTillPageLoad(driver, 30);
			isEleDisplayed(element, driver, elementName, 25);
			eleText = element.getText();
			if (eleText.equals(null)) {
				MyExtentListners.test.fail(MarkupHelper
						.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver)); // exception
				Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");
			}
		} catch (Exception e) {

			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver)); // exception
			Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");

		}
		return eleText;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Fetch text from element and return as string
	 */

	public static String getAttributeValue(WebElement element, WebDriver driver, String elementName)
			throws IOException {
		logger.info("--------- get text from element  ---------");
		String eleText = null;
		try {
			waitTillPageLoad(driver, 30);
			isEleDisplayed(element, driver, elementName, 25);
			eleText = element.getAttribute("value");
			if (eleText.equals(null)) {
				MyExtentListners.test.fail(MarkupHelper
						.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
				Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");
			}
		} catch (Exception e) {

			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
			Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");

		}
		return eleText;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Fetch CSS property Value
	 */

	public static String getCssPropertyValue(WebElement element, String property, WebDriver driver, String elementName)
			throws IOException {
		logger.info("--------- get text from element  ---------");
		String eleText = null;
		try {
			waitTillPageLoad(driver, 30);
			isEleDisplayed(element, driver, elementName, 25);
			eleText = element.getCssValue(property);
			if (eleText.equals(null)) {
				MyExtentListners.test.fail(MarkupHelper.createLabel(
						"Unable to fetch property " + property + "value from " + "\'" + elementName + "\'",
						ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
				Assert.fail("Unable to fetch Proprety Value from " + "\'" + elementName + "\'");
			}
		} catch (Exception e) {

			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Unable to fetch property " + property + "value from " + "\'" + elementName + "\'",
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
			Assert.fail("Unable to fetch Proprety Value from " + "\'" + elementName + "\'");

		}
		return eleText;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Fetch text from element and return as string
	 */

	public static String getAttributeTitle(WebElement element, WebDriver driver, String elementName)
			throws IOException {
		logger.info("--------- get text from element  ---------");
		String eleText = null;
		try {
			waitTillPageLoad(driver, 30);
			isEleDisplayed(element, driver, elementName, 25);
			eleText = element.getAttribute("title");
			if (eleText.equals(null)) {
				MyExtentListners.test.fail(MarkupHelper
						.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
				Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");
			}
		} catch (Exception e) {

			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Unable to fetch text from " + "\'" + elementName + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
			Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");

		}
		return eleText;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result contains in actual result
	 */

	public static void verifyContainsText(String actResult, String expResult, String desc) throws Exception {
		if (actResult.toLowerCase().contains(expResult.toLowerCase())) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
					+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);

		} else {

			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
					+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
					+ " does not contains  Actual :  " + actResult, ExtentColor.RED));

			throw new Exception();

		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to verify text contains validation
	 */
	public static void verifyContainsText(String actResult, String expResult) throws Exception {
		if (actResult.contains(expResult)) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
					+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
					+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
					+ " does not contains  Actual :  " + actResult, ExtentColor.RED));
			throw new Exception();
		}
	}

	public static void verifyContainsEqualText(String actResult, String expResult) throws Exception {
		if (actResult.equalsIgnoreCase(expResult)) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
					+ actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
					+ " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
					+ " does not contains  Actual :  " + actResult, ExtentColor.RED));
			throw new Exception();
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result equals in actual result
	 */

	public static void verifyEqualsText(String desc, String actResult, String expResult) throws Exception {
		if (expResult.equalsIgnoreCase(actResult)) {
			MyExtentListners.test.pass("Verify " + desc + " ||  Expected : " + "\'" + expResult + "\''"
					+ " eqauls  to Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + desc + "  || Expected : " + "\'" + expResult
					+ "\''" + " not eqauls to  Actual :  " + "\'" + actResult + "\'", ExtentColor.RED));
			throw new Exception();
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result equals in actual result
	 */

	public static void verifyEqualsTextWithNull(String desc, String actResult, String expResult) throws Exception {

		if (actResult == null || actResult.equals("") || expResult == null || expResult.equals("")) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(desc + "Does not have Value", ExtentColor.RED));
		} else if (expResult.equalsIgnoreCase(actResult)) {
			MyExtentListners.test.pass("Verify " + desc + " ||  Expected : " + "\'" + expResult + "\''"
					+ " eqauls  to Actual :  " + actResult);
		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify " + desc + "  || Expected : " + "\'" + expResult
					+ "\''" + " not eqauls to  Actual :  " + "\'" + actResult + "\'", ExtentColor.RED));
			throw new Exception();
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method verify expected result actual result is not
	 * equals
	 */
	public static void verifyNotEqualsText(String desc, String actResult, String expResult) throws Exception {
		if (!(expResult.equalsIgnoreCase(actResult))) {
			MyExtentListners.test.pass("Verify " + desc + " is printed on receipt or not" + " ||  Expected : " + "\'"
					+ expResult + "\''" + " not  to Actual :  " + actResult);
		} else {
			MyExtentListners.test
					.fail(MarkupHelper
							.createLabel(
									"Verify " + desc + " is printed on receipt or not" + "  || Expected : " + "\'"
											+ expResult + "\''" + "  eqauls to  Actual :  " + "\'" + actResult + "\'",
									ExtentColor.RED));
			throw new Exception();
		}
	}

	public static void verifyIsNull(String actResult, String desc) {
		if (actResult == null || actResult.equals("")) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify" + "\'" + desc + "\'" + " should not be NUL L"
					+ " || " + "\'" + desc + "\'" + " : " + "\'" + actResult + "\'" + " is not displayed || It is NULL",
					ExtentColor.RED));
			throw new RuntimeException();
		} else {
			MyExtentListners.test.pass("Verify" + "\'" + desc + "\'" + "  should not be NULL " + " || " + "\'" + desc
					+ "\'" + " : " + "\'" + actResult + "\'" + " is displayed");
		}
	}

	/**
	 * Mouse Hover on Element
	 *
	 */
	public static void mouseOverOnElement(WebDriver driver, WebElement element, String elementName) throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Actions act = new Actions(driver);
			act.moveToElement(element).build().perform();
			MyExtentListners.test.pass("Verify user is able to mouse hover on " + "\'" + elementName + "\'"
					+ " ||  User is able to mouse hover on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(
							"Verify user is able to mouse hover on " + "\'" + elementName + "\'"
									+ " ||  User is not able to mouse hover on " + "\'" + elementName + "\'",
							ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to to mouse hover on " + elementName);
		}
	}

	/**
	 * Double click on element.
	 * 
	 * @throws IOException
	 */
	public static void doubleClickOnElement(WebDriver driver, WebElement element, String elementName)
			throws IOException {

		try {
			waitTillPageLoad(driver, 30);
			Actions act = new Actions(driver);
			act.doubleClick(element).perform();
			MyExtentListners.test.pass("Verify user is able to double click on " + "\'" + elementName + "\'"
					+ " ||  User is  able to double click on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to double click on " + "\'" + elementName + "\'"
							+ " ||  User is not able to double click on " + "\'" + elementName + "\'",
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to to mouse hover on " + elementName);
		}

	}

	/**
	 * Right click on element.
	 * 
	 * @throws IOException
	 */
	public static void rightClickOnElement(WebDriver driver, WebElement element, String elementName)
			throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Actions act = new Actions(driver);
			act.contextClick(element).perform();
			MyExtentListners.test.pass("Verify user is able to perform right/context click on " + "\'" + elementName
					+ "\'" + " ||  User is  able to perform right/context click on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to perform right/context click on " + "\'" + elementName + "\'"
							+ " || User is not able to perform right/context click onn " + "\'" + elementName + "\'",
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to to mouse hover on " + elementName);
		}
	}

	/**
	 * Drag and drop.
	 * 
	 * @throws IOException
	 *
	 * 
	 */
	public static void dragAndDrop(WebDriver driver, WebElement source, WebElement destination, String scrEleName,
			String destEleName) throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Actions act = new Actions(driver);
			act.dragAndDrop(source, destination).perform();
			MyExtentListners.test.pass("Verify user is able to perfrom drag and drop from " + "\'" + scrEleName + "\'"
					+ "to " + "\'" + destEleName + "\'" + " || User is able to perfrom drag and drop from " + "\'"
					+ scrEleName + "\'" + "to " + "\'" + destEleName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Verify user is able to perfrom drag and drop from " + "\'" + scrEleName + "\'" + "to "
							+ "\'" + destEleName + "\'" + " || User is not able to perfrom drag and drop from " + "\'"
							+ scrEleName + "\'" + "to " + "\'" + destEleName + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, destination));
			Assert.fail("User is not able to perfrom drag and drop from " + "\'" + scrEleName + "\'" + "to " + "\'"
					+ destEleName + "\'");
		}
	}

	/**
	 * Selectby visibletext.
	 * 
	 * @throws IOException
	 *
	 */
	public static void selectbyVisibletext(WebDriver driver, WebElement element, String text) throws IOException {

		try {
			waitTillPageLoad(driver, 30);
			Select sel = new Select(element);
			sel.selectByVisibleText(text);
			MyExtentListners.test.pass("Verify user is able to select " + "\'" + text + "\'"
					+ " ||  User is able to select " + "\'" + text + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to select " + "\'" + text + "\'"
					+ " ||  User is not able to select " + "\'" + text + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("User is not able to select " + "\'" + text + "\'");
		}

	}

	/**
	 * Selectby value.
	 * 
	 * @throws IOException
	 *
	 */
	public static void selectbyValue(WebDriver driver, WebElement element, String value) throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Select sel = new Select(element);
			sel.selectByValue(value);

			// sel.selectByValue(value);
			// sel.selectByVisibleText(value);

			MyExtentListners.test.pass("Verify user is able to select by value  " + "\'" + value + "\'"
					+ " ||  User is able to select by value " + "\'" + value + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to select by value " + "\'" + value
					+ "\'" + " ||  User is not able to select by value " + "\'" + value + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("User is not able to select " + "\'" + value + "\'");
		}
	}

	/**
	 * Selectby index.
	 * 
	 * @throws IOException
	 *
	 */
	public static void selectbyIndex(WebDriver driver, WebElement element, int index) throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Select sel = new Select(element);
			sel.selectByIndex(index);
			MyExtentListners.test.pass("Verify user is able to select by index  " + "\'" + index + "\'"
					+ " ||  User is able to select by index " + "\'" + index + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to select by index " + "\'" + index
					+ "\'" + " ||  User is not able to select by index " + "\'" + index + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("User is not able to select  by index  " + "\'" + index + "\'");
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	public static boolean isAlertPresent(WebDriver driver, int sec) {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(sec))
				.pollingEvery(Duration.ofMillis(250)).ignoring(UnreachableBrowserException.class);
		boolean alerPresent = wait.until(ExpectedConditions.alertIsPresent()) != null;
		if (alerPresent) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method scrolls to particular element using javascript
	 * executor
	 */
	public static void scrollIntoView(WebDriver driver, WebElement ele) throws IOException {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});", ele);
		} catch (Exception e) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(" Unable to scroll to an element " + ele, ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
		}
	}

	public static void scrollIntoViewAndClick(WebDriver driver, WebElement webElement) throws IOException {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});", webElement);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", webElement);
		} catch (Exception e) {
			// MyExtentListners.test
			// .fail(MarkupHelper.createLabel(" Unable to scroll to an element "
			// + webElement, ExtentColor.RED));
			// MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
		}
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method helps to perform save with keyboard controls
	 */
	public static void enter(WebDriver driver) {
		Actions act = new Actions(driver);
		act.sendKeys(Keys.chord(Keys.ENTER)).build().perform();
	}

	public static String getInnerHtml(WebElement elem, WebDriver driver) {
		String htmlcode = elem.getAttribute("innerHTML");
		System.out.println(htmlcode);
		return htmlcode;

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method to scroll page downward 25% of window size
	 */
	public static void scrollDown(WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method to scroll page upward 25% of window size
	 */
	public static void scrollUp(WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-500)", "");
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Method to upload CSV File
	 * 
	 */

	public static void uploadFile(String filepath, WebElement element, WebDriver driver) throws Exception {

		if (driver instanceof FirefoxDriver) {
			try {
				Runtime.getRuntime().exec("./UploadFiles/firefoxtestcsvfile.exe");
			} catch (IOException e) {
				MyExtentListners.test.fail(MarkupHelper.createLabel(
						"Verify user is able to upload csv file in firefox browser || User is not able to upload csv file in firefox browser",
						ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			}
		} else if (driver instanceof ChromeDriver) {
			try {
				Runtime.getRuntime().exec(filepath);
			} catch (IOException e) {
				MyExtentListners.test.fail(MarkupHelper.createLabel(
						"Verify user is able to upload csv file in chrome browser || User is not able to upload csv file in chrome browser",
						ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			}
		} else {
			try {
				// Implement autoit code to upload file for ie browser
				Runtime.getRuntime().exec("");
			} catch (IOException e) {
				MyExtentListners.test.fail(MarkupHelper.createLabel(
						"Verify user is able to upload csv file in IE browser || User is not able to upload csv file in IE browser",
						ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			}
		}

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method will set any parameter string to the system's
	 * clipboard.
	 * 
	 */
	public static void setClipboardData(String string) {
		// StringSelection is a class that can be used for copy and paste
		// operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Method to upload file thorugh robot class
	 * 
	 */

	/*
	 * public static void uploadFile(String fileLocation) throws Exception {
	 * 
	 * 
	 * Robot robot=new Robot(); robot.setAutoDelay(2000); StringSelection
	 * stringselection=new StringSelection("fileLocation");
	 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
	 * stringselection, null); setClipboardData(fileLocation);
	 * robot.setAutoDelay(1000); robot.keyPress(KeyEvent.VK_CONTROL);
	 * robot.keyPress(KeyEvent.VK_V); robot.keyRelease(KeyEvent.VK_CONTROL);
	 * robot.keyRelease(KeyEvent.VK_V); robot.setAutoDelay(1000);
	 * robot.keyPress(KeyEvent.VK_ENTER); robot.keyRelease(KeyEvent.VK_ENTER); }
	 */

	public static void uploadFile(String fileLocation) {
		try {
			// Setting clipboard with file location
			StringSelection stringselection = new StringSelection("fileLocation");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.delay(250);
			Thread.sleep(5000);
			/*
			 * robot.keyPress(KeyEvent.VK_ENTER);
			 * robot.keyRelease(KeyEvent.VK_ENTER);
			 * robot.keyPress(KeyEvent.VK_CONTROL);
			 * robot.keyPress(KeyEvent.VK_V); robot.keyRelease(KeyEvent.VK_V);
			 * robot.keyRelease(KeyEvent.VK_CONTROL); Thread.sleep(2000);
			 * robot.keyPress(KeyEvent.VK_ENTER); robot.delay(150);
			 * robot.keyRelease(KeyEvent.VK_ENTER);
			 * robot.keyPress(KeyEvent.VK_ENTER); robot.delay(150);
			 * robot.keyRelease(KeyEvent.VK_ENTER);
			 */
			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);

			// Release Enter
			robot.keyRelease(KeyEvent.VK_ENTER);

			// Press CTRL+V
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// Release CTRL+V
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);

			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception exp) {
			exp.printStackTrace();
		}

	}

	/*
	 * @author: Vandhitha Rao
	 */
	public static void uploadFileUsingInput(WebElement element, String filePath) {
		element.sendKeys(filePath);
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: Method to validate String to Date Formate
	 * 
	 */
	public static void validateDateFormate(String stringDate) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(stringDate.trim());
		} catch (ParseException pe) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel("Date Format Exception for " + stringDate, ExtentColor.RED));
			throw pe;
		}

		/*
		 * LocalDate date = LocalDate.now(); DateTimeFormatter formatter =
		 * DateTimeFormatter.ofPattern(""); LocalDate parsedDate =
		 * LocalDate.parse(stringDate, formatter);
		 */
	}

	public static String capture(WebDriver driver, WebElement ele) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}

	public static String capture(WebDriver driver, List<WebElement> ele) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}

	public static String capture(WebDriver driver) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}

   public static String captureAlert(WebDriver driver) throws HeadlessException, AWTException {
		BufferedImage src = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			ImageIO.write(src, "png", new File(destPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return destPath;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to add minutes to current date refresh
	 */
	public static String addMinutesToDateTime(int minutes) throws InterruptedException {
		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MINUTE, minutes);
		Date finalNewDate = calendar.getTime();
		dateTimeInGMT.setTimeZone(istTimeZone);
		String finalNewDateString = dateTimeInGMT.format(finalNewDate);
		System.out.println(finalNewDateString);
		return finalNewDateString;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to add days to current date refresh
	 */
	public static String addDaysToDateTime(int days) throws InterruptedException {

		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		for (int i = 0; i < days;) {
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			// here even sat and sun are added
			// but at the end it goes to the correct week day.
			// because i is only increased if it is week day
			if (calendar.get(Calendar.DAY_OF_WEEK) > 1) {
				i++;
			}

		}
		Date finalNewDate = calendar.getTime();
		dateTimeInGMT.setTimeZone(istTimeZone);
		String finalNewDateString = dateTimeInGMT.format(finalNewDate);
		System.out.println(finalNewDateString);
		return finalNewDateString;

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to move to element and click refresh
	 */

	public static void actionClick(WebElement element, WebDriver driver, String elementName) throws IOException {

		try {
			isEleClickable(element, driver, elementName);
			Actions action = new Actions(driver);
			action.moveToElement(element).click().build().perform();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	public static void actionClickAndType(WebElement element, WebDriver driver, String elementName, String Value)
			throws IOException {

		try {
			isEleClickable(element, driver, elementName);
			Actions action = new Actions(driver);
			action.click(element).sendKeys(Value, Keys.ENTER).build().perform();
			MyExtentListners.test.pass("Verify user is able to click and type " + "\'" + elementName + "\'"
					+ " ||  User is able to click and type " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(
							"Verify user is able to click and type" + "\'" + elementName + "\'"
									+ "  || User is not able to click and type" + "\'" + elementName + "\'",
							ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to click and type " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test
					.fail(MarkupHelper.createLabel(
							"Verify user is able to click and type " + "\'" + elementName + "\'"
									+ " || User is not able to click and type " + "\'" + elementName + "\'",
							ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	/**
	 * Waits for JavaScript asynchronous logic in the web page to finish
	 * (Angular, React, etc).
	 * 
	 * @throws Exception
	 */
	public static void waitForAsyncCallsToFinish(WebDriver driver) throws Exception {
		try {
			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			NgWebDriver ngDriver = new NgWebDriver(jsExecutor);
			ngDriver.waitForAngularRequestsToFinish();
		} catch (Exception ex) {

			throw new Exception("The waitForAsyncCallsToFinish method failed" + ex);

		}
	}

	/**
	 * Waits for page to be ready
	 * 
	 * @throws Exception
	 */
	public static void waitToBeReady(JavascriptExecutor javascriptExecutor) {
		new NgWebDriver(javascriptExecutor).waitForAngularRequestsToFinish();
	}

	/**
	 * Select Value from List
	 * 
	 * @throws Exception
	 */

	public static void selectOption(List<WebElement> optionDrpDwn, String option, WebDriver driver,
			String validationType) throws Exception {

		for (int i = 0; i < optionDrpDwn.size(); i++) {

			if (validationType.equalsIgnoreCase("Equals")) {
				if (optionDrpDwn.get(i).getText().trim().equals(option)) {

					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("arguments[0].scrollIntoView(true);", optionDrpDwn.get(i));
					WebActionUtil.clickElement(optionDrpDwn.get(i), driver, option);
					break;
				}

			} else if (validationType.equalsIgnoreCase("Contains")) {
				if (optionDrpDwn.get(i).getText().trim().contains(option)) {
					WebActionUtil.clickElement(optionDrpDwn.get(i), driver, option);
					break;
				}
			}

		}
		waitForAsyncCallsToFinish(driver);
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to convert date and time in to this format
	 * yyyy-mm-ddThh-mm-ss
	 * 
	 */

	public static String convertDateToDiffFormat(String startDateString, String dateFormat) throws Exception {

		Date date1 = new Date(startDateString);

		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat(dateFormat);
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date1);
		Date finalNewDate = calendar.getTime();
		dateTimeInGMT.setTimeZone(istTimeZone);
		String finalNewDateString = dateTimeInGMT.format(finalNewDate);
		System.out.println(finalNewDateString);
		return finalNewDateString;

	}

	public static String convertDateToDiffFormat(String dateStr) throws ParseException {
		DateFormat srcDf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = srcDf.parse(dateStr);
		DateFormat destDf = new SimpleDateFormat("dd-MMMM-yyyy");
		dateStr = destDf.format(date);
		return dateStr;
	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to fetch the system date and time in
	 * yyyy-mm-ddThh-mm-ss
	 * 
	 */
	public static String getSystemDate() {
		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMMM-yyyy");
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		Date finalNewDate = calendar.getTime();
		dateTimeInGMT.setTimeZone(istTimeZone);
		String finalNewDateString = dateTimeInGMT.format(finalNewDate);
		System.out.println(finalNewDateString);
		return finalNewDateString;

	}

	/*
	 * @author:Vandhitha Rao
	 * 
	 * Description: This method is to add days to a paticular date
	 * 
	 */

	public static String addDaysToAParticularDate(String date, int day) throws ParseException {
		String oldDate = date;
		System.out.println(oldDate);
		// Specifying date format that matches the given date
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
		Calendar c = Calendar.getInstance();

		// Setting the date to the given date
		c.setTime(sdf.parse(oldDate));
		// Number of Days to add
		c.add(Calendar.DAY_OF_MONTH, day);
		// Date after adding the days to the given date
		String newDate = sdf.format(c.getTime());
		return newDate;

	}

	/**
	 * Compares the data in the two Excel files represented by the given input
	 * streams, closing them on completion
	 * 
	 * @param expected
	 *            can't be <code>null</code>
	 * @param actual
	 *            can't be <code>null</code>
	 * @throws Exception
	 *//*
		 * private void compareExcelFiles(InputStream expected, InputStream
		 * actual) throws Exception { try { Assertion.assertEquals(new
		 * XlsDataSet(expected), new XlsDataSet(actual)); } finally {
		 * IOUtils.closeQuietly(expected); IOUtils.closeQuietly(actual); } }
		 */

	/**
	 * @author Vandhitha Rao
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 * 
	 * 
	 * 
	 */
	public static void waitForPageToBeDisplayed(WebElement elementOnAction, String action, WebElement element,
			WebDriver driver) throws IOException, InterruptedException {

		boolean pageNotDisplayed = true;
		int i = 0;
		while (pageNotDisplayed && i < 5) {
			try {
				if (elementOnAction.isDisplayed()) {
					elementOnAction.click();
					pageNotDisplayed = true;
					Thread.sleep(500);
					i++;
				}

			} catch (NoSuchElementException e) {

			}
		}
	}

	public static void acceptJavaScripPopUp(WebDriver driver) {
		// To accept the alert (Clicking on the 'OK' button)
		driver.switchTo().alert().accept();

	}

	public static void cancelJavaScriptPopUp(WebDriver driver) {
		// To dismiss the alert (Clicking on the 'Cancel' button)
		driver.switchTo().alert().dismiss();
	}

	/**
	 * @author Vandhitha Rao
	 */

	public static void verifyIdenticalList(String desc, Collection actResult, Collection expResult, WebDriver driver)
			throws Exception {

		try {
			logger.info("---------Checking if both " + desc + " are identical-----------");
			Assert.assertEquals(actResult, expResult);
			logger.info("---------Both " + desc + " are identical-----------");
			MyExtentListners.test.pass("Verify Both " + desc + " are identical ||  Both " + desc + " are identical");

		} catch (AssertionError e) {
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Both " + desc + " are not identical-----------");
			MyExtentListners.test
					.fail("Verify Both " + desc + " are identical ||  Both " + desc + " are not identical");
			throw e;
		}

	}

	public static void selectDatefromPortalCalndar(String date, List<WebElement> tables) {

		String[] dateArray = date.split("-");
		String day = dateArray[0];
		String mnth = dateArray[1];
		String year = dateArray[2];
		int flag = 0;
		for (WebElement table : tables) {

			if (table.isDisplayed()) {

				List<WebElement> selectMonthYear = table.findElements(By.xpath("//tr[1]//th"));
				List<WebElement> displayedSelectMonthYear = new ArrayList<WebElement>();
				for (int i = 0; i < selectMonthYear.size(); i++) {
					if (selectMonthYear.get(i).isDisplayed()) {
						displayedSelectMonthYear.add(selectMonthYear.get(i));
					}

				}
				String displayedMonthYearString = displayedSelectMonthYear.get(1).getText();
				String[] monthYearINUI = displayedMonthYearString.split(" ");
				String MonthInUI = monthYearINUI[0];
				String YearINUI = monthYearINUI[1];

				while (!(year.equals(YearINUI))) {
					if (Integer.parseInt(year) > Integer.parseInt(YearINUI)) {
						displayedSelectMonthYear.get(2).click();
						flag = 2;
					} else if (Integer.parseInt(year) < Integer.parseInt(YearINUI)) {
						displayedSelectMonthYear.get(0).click();
						flag = 1;
					}

					selectMonthYear = table.findElements(By.xpath("//tr[1]//th"));
					displayedSelectMonthYear = new ArrayList<WebElement>();
					for (int i = 0; i < selectMonthYear.size(); i++) {
						if (selectMonthYear.get(i).isDisplayed()) {
							displayedSelectMonthYear.add(selectMonthYear.get(i));
						}

					}
					displayedMonthYearString = displayedSelectMonthYear.get(1).getText();
					monthYearINUI = displayedMonthYearString.split(" ");
					MonthInUI = monthYearINUI[0];
					YearINUI = monthYearINUI[1];
				}

				while (!(mnth.equals(MonthInUI))) {
					if (flag == 2) {
						displayedSelectMonthYear.get(2).click();
					}
					if (flag == 1) {
						displayedSelectMonthYear.get(0).click();
					}
					selectMonthYear = table.findElements(By.xpath("//tr[1]//th"));
					displayedSelectMonthYear = new ArrayList<WebElement>();
					for (int i = 0; i < selectMonthYear.size(); i++) {
						if (selectMonthYear.get(i).isDisplayed()) {
							displayedSelectMonthYear.add(selectMonthYear.get(i));
						}

					}
					displayedMonthYearString = displayedSelectMonthYear.get(1).getText();
					monthYearINUI = displayedMonthYearString.split(" ");
					MonthInUI = monthYearINUI[0];
					YearINUI = monthYearINUI[1];
				}
				List<WebElement> displayedDates = new ArrayList<WebElement>();
				List<WebElement> dates = table.findElements(By.xpath("//tr//td//span[@class='ng-binding']"));
				dates.addAll(table.findElements(By.xpath("//tr//td//span[@class='ng-binding text-info']")));
				for (int i = 0; i < dates.size(); i++) {
					if (dates.get(i).isDisplayed()) {
						displayedDates.add(dates.get(i));
					}

				}
				for (WebElement eachDate : displayedDates) {
					if (eachDate.isDisplayed()) {
						if (eachDate.getText().trim().equals(day)) {
							eachDate.click();
							break;
						}

					}
				}
				break;

			}
		}

	}

	public static String decimalRoundingOff(String data, String format) {

		String str = data;

		switch (format) {

		case ".0":
			if (!(str.contains("."))) {
				str = str + ".0";
			}
			break;

		case ".00":
			if (!(str.contains("."))) {
				str = str + ".00";
			} else if ((str.substring(str.indexOf('.'))).length() == 2) {
				str = str + "0";
			}
			break;
		}

		return str;
	}

	public static String changeDateTimeFormat(String time, String inputFormat, String outputFormat)
			throws ParseException {
		/*
		 * "hh:mm:ss a" "hh:mm a" "HH:mm"
		 */
		SimpleDateFormat displayFormat = new SimpleDateFormat(outputFormat);
		SimpleDateFormat parseFormat = new SimpleDateFormat(inputFormat);

		Date date = parseFormat.parse(time);
		String output = displayFormat.format(date);

		return output;
	}

	public static void verifyIdenticalMap(String desc, Map expResult, Map actResult, WebDriver driver)
			throws IOException {
		try {
			logger.info("---------Checking if both " + desc + " are identical-----------");
			Assert.assertEquals(actResult, expResult);
			logger.info("---------Both " + desc + " are identical-----------");
			MyExtentListners.test.pass("Verify Both " + desc + " are identical ||  Both " + desc + " are identical");

		} catch (AssertionError e) {
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Both " + desc + " are not identical-----------");
			MyExtentListners.test
					.fail("Verify Both " + desc + " are identical ||  Both " + desc + " are not identical");
			throw e;
		}
	}

	public static String roundingToDecimalPlace(String val, int precision) {
		Double value = Double.parseDouble(val);
		int scale = (int) Math.pow(10, precision);
		return Double.toString(((double) Math.round(value * scale) / scale));
	}

	/**
	 * Description: wait till page load until progress bar is invisible
	 * 
	 * @param eleName
	 * @param driver
	 * @param pageName
	 * @throws IOException
	 */

	public static void waitTillPageLoad(String eleName, WebDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of Loader  ---------");

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loader-backgroud-div")))),
					"On clicking" + eleName + " Page is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " user is redirected to "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		}
	}

	/**
	 * Description: This method is to capture screenshot along with string
	 * 
	 * @param driver
	 * @param screenShotName
	 * @return
	 * @throws IOException
	 */

	public static String capture(WebDriver driver, String screenShotName) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = MyExtentListners.screenShotPath + "/ " + sDate + ".png";
		try {
			File f = new File(destPath);
			if (!(f.exists())) {
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} catch (IOException e) {
	        e.printStackTrace();
		}
		return destPath;
	}

	public static void setAttributeValue(WebElement element, WebDriver driver, String elementName, String value)
			throws IOException {
		logger.info("--------- set text to the element  ---------");
		try {
			waitTillPageLoad(driver, 30);
			isEleDisplayed(element, driver, elementName, 25);
			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			jsExecutor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, "value", value);
			if (value.equals(null)) {
				MyExtentListners.test.fail(MarkupHelper
						.createLabel("Unable to setAttribute to the " + "\'" + elementName + "\'", ExtentColor.RED));
				MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
				Assert.fail("Unable to setAttribute to the " + "\'" + elementName + "\'");
			}
		} catch (Exception e) {

			MyExtentListners.test.fail(MarkupHelper
					.createLabel("Unable to setAttribute to the " + "\'" + elementName + "\'", ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element)); // exception
			Assert.fail("Unable to setAttribute to the " + "\'" + elementName + "\'");

		}
	}

	public static void waitTillLoaderFinishes(String eleName, WebDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of Loader  ---------");

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("Select-noresults")))),
					"On clicking" + eleName + " text box is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " client names are loaded "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " client names are loaded " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " client names are not loaded " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " client names are not loaded " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", text box is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " client names are not loaded " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " client names are not loaded " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", text box is on load, Unable to proceed");
			throw e;
		}
	}

	public static void HandleDateTime(WebDriver driver, String dateTime, String letter) throws Exception {

		logger.info("---------Method for selecting date and time ---------");
		String str2 = "";

		String[] today = dateTime.split(letter, 2);
		String[] arraystr = dateTime.split("[,:]");
		String[] arraystr1 = arraystr[2].split(" ");
		if (Integer.parseInt(arraystr[1].trim()) < 10)
			str2 = String.format("%02d", Integer.parseInt(arraystr[1].trim()));
		else {
			str2 = arraystr[1].trim();
		}
		String HOUR = "//*[text()='Hour']/../..//span[text()=" + str2 + "]";
		String MINUTE = "//*[text()='Minute']/../..//span[text()='00']/../..//span[text()=" + arraystr1[0] + "]";
		String TIMEZONE = "//*[text()='AM/PM']/../..//span[text()=\'" + arraystr1[1] + "\']";
		String date = "//div[@id='content_calender']//div[@class='block numbers  ']//span[text()=" + today + "]";
		WebElement Hour = driver.findElement(By.xpath(HOUR));
		WebElement Minute = driver.findElement(By.xpath(MINUTE));
		WebElement Timezone = driver.findElement(By.xpath(TIMEZONE));
		// WebElement Date = driver.findElement(By.xpath(date));
		WebElement DONE = driver.findElement(By.xpath("//*[text()='DONE']"));
		// WebElement dateIcon =
		// driver.findElement(By.xpath("//span[@class='date-input
		// white-theme']"));
		// try move to element and click
		// clickElement(dateIcon,driver,"Date Icon ");
		// clickElement(Date,driver,"Select date ");
		scrollIntoViewAndClick(driver, Hour);
		scrollIntoViewAndClick(driver, Minute);
		clickElement(Timezone, driver, "AM/PM ");
		clickElement(DONE, driver, "Done ");
		logger.info("---------exiting Handle date and time method---------");
	}

	public static void switchToWindow(WebDriver driver, int window_number) {
		List<String> windowlist = null;
		Set<String> windows = driver.getWindowHandles();
		windowlist = new ArrayList<String>(windows);
		String currentWindow = driver.getWindowHandle();
		if (!currentWindow.equalsIgnoreCase(windowlist.get(window_number - 1))) {
			driver.switchTo().window(windowlist.get(window_number - 1));
		}
	}

	public static void assertElementDisplayed(WebElement element, WebDriver driver, String elementName)
			throws IOException {

		try {
			logger.info("---------Verifying element is displayed or not ---------");
			Assert.assertEquals(true, element.isDisplayed());
			System.out.println(elementName + "------ is displayed");
			MyExtentListners.test.pass("Verify " + "\'" + elementName + "\'" + " is displayed || " + "\'" + elementName
					+ "\'" + " is displayed ");
		} catch (Exception e) {

			System.out.println(elementName + "------ is not displayed");
		}
	}

	// tabs starts from 0-9
	public static void switchToTab(WebDriver driver, int number) {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(number));
	}

	public static void waitTillPaymentPageLoad(String eleName, WebDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of Loader  ---------");

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loader-block-screen")))),
					"On clicking" + eleName + " Page is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " user is redirected to "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		}
	}

	/*
	 * Author: vandhitha description: method is used to switch to a new window
	 */
	public static void switchtoNewwindow(WebDriver driver) {
		String parentWinHandle = driver.getWindowHandle();
		// System.out.println(parentWinHandle);
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			System.out.println(handle);
			if (!handle.equals(parentWinHandle)) {
				driver.switchTo().window(handle);
				break;
			}
		}
	}

	/*
	 * Author: vandhitha description: waits till invisibility of an element
	 */

	public static void waitForinvisiblityofElement(String ele, Integer seconds, WebDriver driver) {
		WebDriverWait wait6 = new WebDriverWait(driver, 100);
		wait6.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(ele)));
	}

	/**
	 * Author: Vandhitha Description: This method verifies if an element
	 * contains expected text
	 * 
	 * @param actResult
	 * @param expResult
	 * @throws IOException
	 */

	public static void verifyElementContainsText(WebElement ele, String expStr, WebDriver driver) throws IOException {
		String actStr = ele.getText();
		System.out.println(actStr);
		if (actStr.toLowerCase().contains(expStr.toLowerCase())) {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
					+ "  || Expected : " + "\'" + expStr + "\''" + "contains  Actual :  " + actStr);

		} else {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
							+ " ||  Expected : " + "\'" + expStr + "\''" + " does not contains  Actual :  " + actStr,
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expStr));

		}
	}

	/**
	 * Author: Vandhitha Description: This method verifies if element doesnt
	 * contain expected text
	 * 
	 * @param actResult
	 * @param expResult
	 * @throws IOException
	 */

	public static void notContainsText(WebElement ele, String expStr, WebDriver driver) throws IOException {
		String actStr = ele.getText();
		// System.out.println(actStr);
		if (actStr.toLowerCase().contains(expStr.toLowerCase())) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
							+ " ||  Expected : " + "\'" + expStr + "\''" + " does not contains  Actual :  " + actStr,
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, expStr));

		} else {
			MyExtentListners.test.pass("Verify  Expected : " + "\'" + expStr + "\''" + " contains  Actual :  " + actStr
					+ "  || Expected : " + "\'" + expStr + "\''" + "contains  Actual :  " + actStr);

		}
	}

	/*
	 * @author : Vandhitha Rao
	 * 
	 * Description : To generate a random string to match a vehicle number
	 * 
	 * @return int
	 */

	public static String generateRandomString_Vehicle() {

		int n = 2;
		int m = 4;
		// chose a Character random from this String
		String AlphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String numString = "0123456789";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);
		StringBuilder sc = new StringBuilder(m);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaString.charAt(index));
		}

		for (int i = 0; i < m; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (numString.length() * Math.random());

			// add Character one by one in end of sb
			sc.append(numString.charAt(index));
		}

		return sb.toString() + sc.toString().substring(0, 2) + sb.toString() + sc.toString();
	}

	/*
	 * @author : Vandhitha Rao
	 * 
	 * Description : To generate a random string
	 * 
	 * @return int
	 */
	public static String generateRandomString() {

		int n = 3;
		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}

	public static String generateRandomString(int n) {

		// int n = 3;

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}

	/*
	 * @author : Vandhitha Rao
	 * 
	 * Description : To generate a random string
	 * 
	 * @return int
	 */

	public static String generateRandomNumber() {

		int n = 10;
		// chose a Character random from this String
		String AlphaNumericString = "0123456789";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}

	public static void clickOnCurrentDate(WebElement ele, String format, int n) {
		String todaydate = WebActionUtil.getSystemDate();
		DateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		System.out.println("Format defined");
		Date date2 = new Date();
		System.out.println("Date object creation");
		String today = dateFormat2.format(date2);
		String[] parts = today.split(format);
		String part1 = parts[0];
		String starting = "0";
		String changedate = starting.concat(part1);
		if (ele.getText().equals(part1)) {
			ele.click();
		} else if (changedate.equals(changedate)) {
			ele.click();

		}

	}

	public static void clearAndTypeEnter(WebElement element, String value, String elementName, WebDriver driver)
			throws Exception {
		try {
			logger.info("---------Method clear and type  ---------");
			waitTillPageLoad(driver, 30);
			element.clear();
			logger.info(elementName + " is cleared");
			element.sendKeys(value, Keys.ENTER);
			logger.info(value + " is entered in " + elementName);
			logger.info(" hide keyboard");
			MyExtentListners.test.pass("Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName
					+ "\'" + " || User is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type on " + "\'" + elementName + "\'");
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
					ExtentColor.RED));
			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type in " + "\'" + elementName + "\'");
		}
	}

	public static boolean isPresentAndDisplayed(final WebElement element) {
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	public static void zoomout() throws AWTException {
		Robot robot = new Robot();
		System.out.println("About to zoom out");

		for (int i = 0; i < 4; i++) {
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_SUBTRACT);
			robot.keyRelease(KeyEvent.VK_SUBTRACT);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		}

	}

	public static void zoomin() throws AWTException {
		Robot robot = new Robot();
		System.out.println("About to zoom inb");

		for (int i = 0; i < 10; i++) {
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ADD);
			robot.keyPress(KeyEvent.VK_ADD);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		}

	}

	//
	// WebElement element =
	// driver.findElement(By.xpath("//*[@class='verification-button
	// reject-button']"));
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", element);

	/**
	 * Author: Karthik KR Description: Method to long press on an element in the
	 * Webpage
	 * 
	 * @param element
	 * @param driver
	 * @param elementName
	 * @throws IOException
	 */

	public static void longPress(WebElement element, WebDriver driver, String elementName) throws IOException {

		try {
			isEleClickable(element, driver, elementName);
			Actions action = new Actions(driver);
			action.clickAndHold(element).click().build().perform();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}

	}

	public static void downloadFile(String fileLocation) throws Exception {
		StringSelection file = new StringSelection(fileLocation);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);

		Robot rb = new Robot();

		rb.setAutoDelay(1800); // Similar to thread.sleep

		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);

		rb.setAutoDelay(1800);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public static void downloadOverWrite(String fileLocation) throws Exception {
		StringSelection file = new StringSelection(fileLocation);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);

		
	
		Robot rb = new Robot();

		rb.setAutoDelay(1800); // Similar to thread.sleep
		
		
//		rb.keyPress(KeyEvent.VK_CONTROL);
//		rb.keyPress(KeyEvent.VK_C);
//		Thread.sleep(2000);
//		rb.keyRelease(KeyEvent.VK_CONTROL);
//		rb.keyRelease(KeyEvent.VK_C);

		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);

		rb.setAutoDelay(1800);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		rb.setAutoDelay(1800);
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		
		rb.setAutoDelay(1800);



		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		
	}

	public static void verifyCsvData(String filePath, int row, int col, String expResult) throws Exception {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		List<String[]> li = reader.readAll();
		if (li.get(row)[col].contains(expResult)) {
			reader.close();
			MyExtentListners.test.pass("Verify " + "data" + " ||  Expected : " + "\'" + expResult + "\''"
					+ " eqauls  to Actual :  " + li.get(row)[col]);

		} else {
			reader.close();
			MyExtentListners.test
					.fail(MarkupHelper.createLabel("Verify " + "data" + "  || Expected : " + "\'" + expResult + "\''"
							+ " not eqauls to  Actual :  " + "\'" + li.get(row)[col] + "\'", ExtentColor.RED));
			throw new Exception();

		}

	}

	public static void verifyCsv_AllData(String filePath, String expResult) throws Exception {
		CSVReader reader = new CSVReader(new FileReader(filePath));

		List<String[]> myEntries = reader.readAll();
		Iterator<String[]> iter = myEntries.iterator();

		while (iter.hasNext()) {
			String[] items = iter.next();

			for (String item : items) {
				System.out.println(item);
				if (item.equals(expResult)) {
					break;
				}

				reader.close();
				MyExtentListners.test.pass("Verify " + "data" + " ||  Expected : " + "\'" + expResult + "\''"
						+ " eqauls  to Actual :  " + item);

			}

		}
	}
	// {
	//
	// reader.close();
	// MyExtentListners.test
	// .fail(MarkupHelper.createLabel("Verify " + "data" + " || Expected : " +
	// "\'" + expResult + "\''"
	// + " not eqauls to Actual : " + "\\'", ExtentColor.RED));
	// throw new Exception();
	// }

	/*
	 * author: Vandhitha This method will count the number of rows in the file
	 */

	public static int rowcount(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

	/*
	 * Author: vandhitha Description: writes into a cvs file by skipping header
	 * row enhancement - to be passed as a string
	 */

	public static void csvwrite(String filepath, String data1, String data2, String data3, String data4, String data5,
			String data6, String data7, String data8, String data9, String data10) throws IOException {

		List<List<String>> rows = Arrays
				.asList(Arrays.asList(data1, data2, data3, data4, data5, data6, data7, data8, data9, data10));

		FileWriter csvWriter = new FileWriter(filepath, true);

		csvWriter.append("\n");

		for (List<String> rowData : rows) {
			csvWriter.append(String.join(",", rowData));
		}

		csvWriter.flush();
		csvWriter.close();

	}

	/*
	 * Author: vandhitha Description: writes into a cvs file by skipping header
	 * row
	 * 
	 */

	public static void csvwrite_String(String filepath, List<String> datas) throws IOException {

		List<List<String>> rows = Arrays.asList(datas);

		FileWriter csvWriter = new FileWriter(filepath, true);

		csvWriter.append("\n");

		for (List<String> rowData : rows) {
			csvWriter.append(String.join(",", rowData));
		}

		csvWriter.flush();
		csvWriter.close();

	}

	public static void deleteCsvFile(String filePath) {
		File file = new File(filePath);
		try {
			if (!file.exists()) {
				System.out.println("File doesn't exists");
			} else {
				file.delete();
				System.out.println("File deleted successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String prependZero(String date) {
		String str2 = "";
		String[] str = date.split("-");
		if (Integer.parseInt(str[0].trim()) < 10) {
			str2 = String.format("%02d", Integer.parseInt(str[0].trim()));
			str2 = str2 + "-" + str[1].trim() + "-" + str[2].trim();
		} else
			str2 = str[0].trim() + "-" + str[1].trim() + "-" + str[2].trim();
		return str2;
	}

	public static void scrollMousePointer(WebElement element) throws Exception {
		Point coordinates = element.getLocation();
		Robot robot = new Robot();
		robot.mouseMove(coordinates.getX(), coordinates.getY() + 120);
	}

	public static void actionClickAndClear(WebElement element, WebDriver driver, String elementName) throws Exception {

		try {
			isEleClickable(element, driver, elementName);
			Actions actions = new Actions(driver);
			actions.click(element).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE)
					.build().perform();
			MyExtentListners.test.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
		} catch (AssertionError error) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw error;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			throw e;
		}
	}

	public static void reloadPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	/*
	 * author: vandhitha
	 * 
	 */
	public static void uploadFile_confirm(String fileLocation) {
		try {
			// Setting clipboard with file location

			StringSelection file = new StringSelection(fileLocation);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);

			Robot rb = new Robot();

			rb.setAutoDelay(1800); // Similar to thread.sleep

			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);

			rb.keyRelease(KeyEvent.VK_CONTROL);
			rb.keyRelease(KeyEvent.VK_V);

			rb.setAutoDelay(1800);

			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);

			rb.setAutoDelay(1800);

			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);

			rb.setAutoDelay(1800);

			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception exp) {
			exp.printStackTrace();
		}

	}
	
	public static void csvwrite_rowcol(String path, String data ) throws IOException {
                FileWriter outFile = new FileWriter(path,true);
		        outFile.append("\n");
		        outFile.write(",,,,,,,,,,,,");
		        outFile.write(data);
		        outFile.close();
        
        }
	
	public static String fetchcurrentSystemDateTime(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		String dates= dtf.format(now);
		return dates;  
	}

	public static String fetchSystemDateTime(String pattern) {
		String dates = "";
		switch (pattern) {
		case "d-MMM-yyyy": {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
			LocalDateTime now = LocalDateTime.now();
			dates = dtf.format(now);
			break;
		}
		case "d MMM": {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
			LocalDateTime now = LocalDateTime.now();
			dates = dtf.format(now);
			break;
		}
		
		case "dd": {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
			LocalDateTime now = LocalDateTime.now();
			dates = dtf.format(now);
			break;
		}
		case "MMddyyyy": {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
			LocalDateTime now = LocalDateTime.now();
			dates = dtf.format(now);
			break;
		}
		// this case is for app data for listing
		case "dd-MMM hh:mmaa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat(pattern);
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] date = dates.split("-");
			char[] c = date[1].toCharArray();
			c[3] = ',';
			String newDate = String.valueOf(c);
			newDate = newDate.replace(",", ", ");
			int newMin=Integer.parseInt(newDate.substring(8, 10));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
				newDate=newDate.replace(newDate.substring(8, 10), String.valueOf(String.format("%02d", newMin)));
				newDate=newDate.replace(newDate.substring(5, 7), String.valueOf(Integer.parseInt(newDate.substring(5, 7))));
				dates = datePrefix(date[0]) + newDate;
			}else{
				newDate=newDate.replace(newDate.substring(8, 10), String.valueOf(String.format("%02d", 55)));
				newDate=newDate.replace(newDate.substring(5, 7), String.valueOf(Integer.parseInt(newDate.substring(5, 7))));
				dates = datePrefix(date[0]) + newDate;
			}
			break;
		}
		// this case is for portal data for listing
		case "dd-MMM hh:mm aa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat(pattern);
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] date = dates.split("-");
			char[] c = date[1].toCharArray();
			c[3] = ',';
			String newDate = String.valueOf(c);
			newDate = newDate.replace(",", ", ");
			int newMin=Integer.parseInt(newDate.substring(8, 10));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
				newDate=newDate.replace(newDate.substring(8, 10), String.valueOf(String.format("%02d", newMin)));
				newDate=newDate.replace(newDate.substring(5, 7), String.valueOf(Integer.parseInt(newDate.substring(5, 7))));
				dates = datePrefix(date[0]) + newDate;
			}else{
				newDate=newDate.replace(newDate.substring(8, 10), String.valueOf(String.format("%02d", 55)));
				newDate=newDate.replace(newDate.substring(5, 7), String.valueOf(Integer.parseInt(newDate.substring(5, 7))));
				dates = datePrefix(date[0]) + newDate;
			}
			break;
		}
		//case for opening time on the portal excel sheet
		case "h:mmaa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM hh:mmaa");
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] d = dates.trim().split(" ");
			int newMin=Integer.parseInt(d[1].substring(3, 5));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
				dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", newMin)));
				}
				else{
				dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", 55)));
			}
			break;
		}
		
		//case for closing time on the portal excel sheet
		case "hh:mmaa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM hh:mmaa");
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] d = dates.trim().split(" ");
			int newMin=Integer.parseInt(d[1].substring(3, 5));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
				dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", newMin)));
				}
				else{
				dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", 55)));
				}
			break;
		}
		
		//case for created time on the portal excel sheet
		case "hh:mm aa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM hh:mm aa");
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] d = dates.trim().split(" ");
			int newMin=Integer.parseInt(d[1].substring(3, 5));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
			dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", newMin)));
			dates=dates+" "+d[2];
			}
			else{
			dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", 55)));
			dates=dates+" "+d[2];
			}
			break;
		}
		
		//case for displayed time on the portal excel sheet
		case "h:mm aa": {
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("dd-MMM hh:mm aa");
			TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.MINUTE, 70);
			Date finalNewDate = calendar.getTime();
			dateTimeInGMT.setTimeZone(istTimeZone);
			dates = dateTimeInGMT.format(finalNewDate);
			String[] d = dates.trim().split(" ");
			int newMin=Integer.parseInt(d[1].substring(3, 5));
			newMin =((newMin+4)/5)*5;
			if(newMin!=60){
			dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", newMin)));
			dates=dates.replace(d[1].substring(0, 2), String.valueOf(Integer.parseInt(dates.substring(0, 2))));
			dates=dates+" "+d[2];
			}
			else{
			dates=d[1].replace(d[1].substring(3, 5), String.valueOf(String.format("%02d", 55)));
			dates=dates.replace(dates.substring(0, 2), String.valueOf(Integer.parseInt(dates.substring(0, 2))));
			dates=dates+" "+d[2];
			}
		}
		break;
	}
		return dates;
}

	public static String datePrefix(String pattern) {
		String date = "";
		switch (pattern) {
		case "01": {
			String one = "st ";
			date = pattern + one;
			break;
		}
		case "02": {
			String two = "nd ";
			date = pattern + two;
			break;
		}
		case "03": {
			String three = "rd ";
			date = pattern + three;
			break;
		}
		case "21": {
			String one = "st ";
			date = pattern + one;
			break;
		}
		case "22": {
			String two = "nd ";
			date = pattern + two;
			break;
		}
		case "23": {
			String three = "rd ";
			date = pattern + three;
			break;
		}
		case "31": {
			String one = "st ";
			date = pattern + one;
			break;
		}
		default: {
			String none = "th ";
			date = pattern + none;
			break;
		}
	}
		return date;
	}
	
	public static String vehicleNumber(List<WebElement>allElements,int max) throws Exception{
		int num= (int) (Math.random()*max);
		System.out.println(num);
		WebElement v = allElements.get(num);
		return v.getText();
	}
	
	public static void waitTillCOBPageLoad(String eleName, WebDriver driver, String pageName, int seconds)
			throws IOException {
		try {
			logger.info("---------Method waiting for invisibility of Loader  ---------");

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(seconds))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(
					(wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[local-name()='svg']//*[name()='circle']")))),
					"On clicking" + eleName + " Page is on load, Unable to proceed");
			MyExtentListners.test.pass(" Verify On clicking " + "\'" + eleName + "\''" + " user is redirected to "
					+ "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''");
		} catch (AssertionError e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		} catch (Exception e) {
			MyExtentListners.test.fail(MarkupHelper.createLabel(" Verify On clicking " + "\'" + eleName + "\''"
					+ " user is redirected to " + "\'" + pageName + "\''" + "  ||  On clicking " + "\'" + eleName
					+ "\''" + " user is not redirected to " + "\'" + pageName + "\''", ExtentColor.RED));

			MyExtentListners.test.addScreenCaptureFromPath(capture(driver, eleName));
			Assert.fail("On clicking " + "\'" + eleName + "\''" + ", Page is on load, Unable to proceed");
			throw e;
		}
	}
	public static void waitForVisiblityofElement(String ele, Integer seconds, WebDriver driver) {
		WebDriverWait wait6 = new WebDriverWait(driver, seconds);
		wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ele)));
	}
}
