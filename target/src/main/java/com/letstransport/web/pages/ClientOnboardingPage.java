package com.letstransport.web.pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class ClientOnboardingPage {

	WebDriver driver;

	public ClientOnboardingPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Select Group']/..//div[text()='Add New ']")
	private WebElement addGroup;

	@FindBy(xpath = "//label[text()='Group Name']/../..//input[@id='name']")
	private WebElement groupName;

	@FindBy(xpath = "//label[text()='Industry Type']/../..//option[text()='Select Industry']")
	private WebElement clickIndustry;

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveButton;
	
	@FindBy(xpath = "//div[text()='*Upload upto 5 documents(maximum size is 5 MB)']/..//button[text()='Save']")
	private WebElement sveButton;
	
	@FindBy(xpath = "(//div[text()='View']/../..//div)[2]")
	private WebElement commerceCheckBox;		
	
	@FindBy(xpath = "//div[text()=' Select from other commercial ']")
	private WebElement selectOtherCommercial;

	@FindBy(xpath = "//button[text()='Next']")
	private WebElement nextButton;

	@FindBy(xpath = "//button[text()='Submit']")
	private WebElement submitButton;

	@FindBy(xpath = "//span[text()='Close']")
	private WebElement closeButton;

	@FindBy(xpath = "//label[text()='Legal Entity Name']/../..//input[@id='name']")
	private WebElement entityName;

	@FindBy(xpath = "//label[text()='PAN Number']/../..//input[@id='pan_number']")
	private WebElement panNumber;

	@FindBy(xpath = "//label[text()='Registered office address']/../..//textarea[@id='registered_office_address']")
	private WebElement officeAddress;

	@FindBy(xpath = "//label[text()='Vertical Name']/../..//input[@id='name']")
	private WebElement verticalName;

	@FindBy(xpath = "//label[text()='GSTIN Number']/../..//input[@id='gstin_number']")
	private WebElement gstNumber;

	@FindBy(xpath = "//input[@id='is_dummy']")
	private WebElement checkBoxDummy;

	@FindBy(xpath = "//label[text()='Billing Address']/../..//textarea[@id='billing_address']")
	private WebElement billingAddress;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='FcNameOne']")
	private WebElement FcNameOne;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='FcEmailOne']")
	private WebElement FcEmailOne;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='FcContactOne']")
	private WebElement FcContactOne;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='FcNameTwo']")
	private WebElement FcNameTwo;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='FcEmailTwo']")
	private WebElement FcEmailTwo;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='FcContactTwo']")
	private WebElement FcContactTwo;

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='OcName']")
	private WebElement OcName;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='OcEmail']")
	private WebElement OcEmail;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='OcContact']")
	private WebElement OcContact;

	@FindBy(xpath = "//label[text()='Hub Name']/../..//input[@id='hubName']")
	private WebElement hubName;

	@FindBy(xpath = "//label[text()='Hub address']/../..//textarea[@id='address']")
	private WebElement hubAddress;

	@FindBy(xpath = "//label[text()='LT Regional office for the hub']/../..//option[text()='Select LT Regional office for the hub']")
	private WebElement clickHub;

	@FindBy(xpath = "//label[text()='City']/../..//option[text()='Select Hub City']")
	private WebElement clickCity;

	@FindBy(xpath = "//label[text()='Hub State']/../..//option[text()='Select State']")
	private WebElement clickState;

	public WebElement selectState(WebDriver driver, String state) {
		return driver.findElement(By.xpath("//label[text()='Hub State']/../..//option[text()='" + state + "']"));
	}

	public WebElement selectIndustry(WebDriver driver, String industry) {
		return driver.findElement(By.xpath("//label[text()='Industry Type']/../..//option[text()='" + industry + "']"));
	}

	public WebElement selectRegOffice(WebDriver driver, String state) {
		return driver.findElement(
				By.xpath("//label[text()='LT Regional office for the hub']/../..//option[text()='" + state + "']"));
	}

	public WebElement selectCity(WebDriver driver, String city) {
		return driver.findElement(By.xpath("//label[text()='City']/../..//option[text()='" + city + "']"));
	}

	@FindBy(xpath = "//label[text()='Name']/../..//input[@id='name']")
	private WebElement hubContact;

	@FindBy(xpath = "//label[text()='Email']/../..//input[@id='email']")
	private WebElement hubEmail;

	@FindBy(xpath = "//label[text()='Contact Number']/../..//input[@id='contact_number']")
	private WebElement hubCntctNumber;

	@FindBy(xpath = "//div[text()='Add New ']")
	private WebElement addNewCommercial;

	@FindBy(xpath = "//label[text()='Commercial Name']/../..//input[@id='name']")
	private WebElement commercialName;

	@FindBy(xpath = "//label[text()='Business Type']/../..//option[text()='Select Industry']")
	private WebElement businessType;

	@FindBy(xpath = "//label[text()='Contract End Date']/../..//input[@id='end']")
	private WebElement enddate;

	@FindBy(xpath = "//label[text()='Margin %']/../..//input[@id='margin']")
	private WebElement margin;
	
	@FindBy(xpath = "//label[text()='Credit Period']/../..//input[@id='credit_period']")
	private WebElement creditPeriod;
	
	@FindBy(xpath = "//label[text()='Remarks']/../..//textarea[@id='remarks']")
	private WebElement remarks;	

	@FindBy(xpath = "//label[text()='Attach Commercial']")
	private WebElement attachCommercial;

	@FindBy(xpath = "//label[text()='Attach Client Approval files']")
	private WebElement attachApproval;

	@FindBy(xpath = "//div[text()='Lock']")
	private WebElement lockButton;

	@FindBy(xpath = "//*[local-name()='svg']//*[name()='g']")
	private WebElement logOutButton;
	
	@FindBy(xpath = "//button[text()='Submit to OPM']")
	private WebElement submitToOPM;

	@FindBy(xpath = "//button[text()='Submit to finance']")
	private WebElement submitToFinance;
	
	@FindBy(xpath = "//div[text()='Pending List']")
	private WebElement pendingList;
	
	@FindBy(xpath = "//span[text()='Group']/../..//button[text()='Next']")
	private WebElement groupNext;
	
	@FindBy(xpath = "//span[text()='Legal Entity']/../..//button[text()='Next']")
	private WebElement legalEntityNext;
	
	@FindBy(xpath = "//span[text()='Vertical']/../..//button[text()='Next']")
	private WebElement verticalNext;
	
	@FindBy(xpath = "//span[text()='State/GSTIN No']/../..//button[text()='Next']")
	private WebElement stateGSTNext;
	
	@FindBy(xpath = "//span[text()='GSTIN Details']/../..//button[text()='Next']")
	private WebElement GSTNDetailsNext;
	
	@FindBy(xpath = "//span[text()='Hub']/../..//button[text()='Next']")
	private WebElement hubNext;
	
	@FindBy(xpath = "//div[text()='Commercial']/../..//button[text()='Submit to OPM']")
	private WebElement commercialToOPM;
	
	@FindBy(xpath = "//div[text()='Approval required']/../../..//div[text()='View']")
	private WebElement approvalReqView;
	
	@FindBy(xpath = "//div[text()='Is this commercial approved?']/..//div[text()='Approved']")
	private WebElement approvedButton;
	
	

	public WebElement selectBusinessType(WebDriver driver, String type) {
		return driver.findElement(By.xpath("//label[text()='Business Type']/../..//option[text()='" + type + "']"));
	}

	public WebElement getLockedClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='" + client
				+ "']/../..//div[text()='State/GSTIN No']/..//div[contains(@class,'fc2a1835 ')]/../../..//div[text()='Commercial']/..//div[contains(@class,'fc2a1835 ')]"));
	}

	public WebElement getApproveClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='" + client
				+ "']/../..//div[text()='State/GSTIN No']/..//div[contains(@class,'fc2a1835 ')]/../../..//div[text()='Commercial']/..//div[contains(@class,'fc2a1835 ')]"));
	}
	
	@FindBy(xpath = "//span[text()='Login']")
	private WebElement googgleLoginBtn;
	
	@FindBy(xpath = "//div[text()='Use another account']")
	private WebElement anotherAccount;

	@FindBy(xpath = "//input[@type='email']")
	private WebElement usrName;

	@FindBy(xpath = "//input[@type='password']")
	private WebElement pswrd;

	@FindBy(xpath = "//span[text()='Next']")
	private WebElement nxtBtn;
	
	@FindBy(xpath = "//div[text()='karthik.kr@letstransport.team']")
	private WebElement krBtn;
	
	@FindBy(xpath = "//div[text()='priya']")
	private WebElement priyaBtn;
	
	@FindBy(xpath = "//button[text()='Next']")
	private WebElement nextBtn;
	
	@FindBy(xpath = "//div[text()='OPM Approval']")
	private WebElement opmApproval;
	
	@FindBy(xpath = "//input[@placeholder='Search by Group Name']")
	private WebElement searchByGroupName;
	
	//div[text()='PLASMA']

	public void COB_Sales_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		driver.get(URL);
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(8000);
	}
	
	public void quick_COB_Sales_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		driver.get(URL);
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clickByJavaSriptExecutor(this.anotherAccount, driver, "priya Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='email']", 90, driver);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(8000);
	}

	public void addNewGroup(WebDriver driver,String filepath,String sheet, String testcase, String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.addGroup, driver, "Add group Button");
		WebActionUtil.waitTillCOBPageLoad("Add group button", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.groupName, client, "Group name", driver);
		WebActionUtil.clickElement(this.clickIndustry, driver, "Click industry");
		WebActionUtil.waitTillCOBPageLoad("Industry dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectIndustry(driver, sData[BaseLib.gv.cobitcount]), driver, "IT industry");
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewEntity(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.entityName,  client+" entity", "Entity name", driver);
		WebActionUtil.type(this.panNumber, sData[BaseLib.gv.cobpncount], "Pan number", driver);
		WebActionUtil.type(this.officeAddress, sData[BaseLib.gv.cobrocount], "Office Address", driver);
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewVertical(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Add New Entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.verticalName, client+" Vertical1", "vertica name", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewGSTNo(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.type(this.gstNumber, sData[BaseLib.gv.cobgstcount], "GST No", driver);
		WebActionUtil.waitTillCOBPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		// WebActionUtil.clickElement(this.checkBoxDummy,driver, "Dummy GST
		// No");
		// WebActionUtil.waitTillPageLoad("State dropdown", driver, "Client
		// Onboarding Page", 120);
		WebActionUtil.clickElement(this.clickState, driver, "Click State");
		WebActionUtil.waitTillCOBPageLoad("State dropdown", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectState(driver, sData[BaseLib.gv.cobhscount]), driver, "IT industry");
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.submitButton, driver, "Submit button");
		WebActionUtil.waitTillCOBPageLoad("Submit button", driver, "Client Onboarding Page", 120);
	}

	public void addNewGSTDetails(WebDriver driver,String filepath,String sheet, String testcase) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Submit button", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.billingAddress, sData[BaseLib.gv.cobbacount], "Billing Address", driver);
		WebActionUtil.type(this.FcNameOne, sData[BaseLib.gv.cobfn1count], "Finance contact name", driver);
		WebActionUtil.type(this.FcEmailOne, sData[BaseLib.gv.cobfe1count], "Finance contact email", driver);
		WebActionUtil.type(this.FcContactOne, sData[BaseLib.gv.cobfc1count], "Finance contact No", driver);
		WebActionUtil.scrollIntoView(driver, this.OcName);
		WebActionUtil.type(this.FcNameTwo, sData[BaseLib.gv.cobfn2count], "Finance contact name", driver);
		WebActionUtil.type(this.FcEmailTwo, sData[BaseLib.gv.cobfe2count], "Finance contact email", driver);
		WebActionUtil.type(this.FcContactTwo, sData[BaseLib.gv.cobfcn2count], "Finance contact No", driver);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.type(this.OcName, sData[BaseLib.gv.cobocncount], "Operation contact name", driver);
		WebActionUtil.type(this.OcEmail, sData[BaseLib.gv.cobocecount], "Operation contact email", driver);
		WebActionUtil.type(this.OcContact, sData[BaseLib.gv.cobocount], "Operation contact No", driver);
		WebActionUtil.waitTillCOBPageLoad("GST details", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewHUB(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.type(this.hubName, client+" HUB", "HUB NAME", driver);
		WebActionUtil.type(this.hubAddress, client+" HUB Address goes here", "HUB Address", driver);
		WebActionUtil.clickElement(this.clickHub, driver, "click HUB");
		WebActionUtil.waitTillCOBPageLoad("click HUB", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectRegOffice(driver, sData[BaseLib.gv.cobro2ount]), driver, "Regional office");
		WebActionUtil.waitTillCOBPageLoad("hub selected", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.clickCity, driver, "select City");
		WebActionUtil.waitTillCOBPageLoad("click city", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.selectCity(driver, sData[BaseLib.gv.cobcount]), driver, "City");
		WebActionUtil.waitTillCOBPageLoad("selected city", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.type(this.hubContact, sData[BaseLib.gv.cobhcncount], "HUB contact name", driver);
		WebActionUtil.type(this.hubEmail, sData[BaseLib.gv.cobhcecount], "HUB email name", driver);
		WebActionUtil.type(this.hubCntctNumber, sData[BaseLib.gv.cobhc1ount], "HUB contact number", driver);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("Save button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.nextButton, driver, "Next button");
		WebActionUtil.waitTillCOBPageLoad("Next button", driver, "Client Onboarding Page", 120);
	}

	public void addNewCommercial(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillCOBPageLoad("Commercial", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.addNewCommercial);
		WebActionUtil.waitTillCOBPageLoad("add new Commercial", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.addNewCommercial, driver, "Commercial");
		WebActionUtil.type(this.commercialName, client+" commercial", "Commercial "+client, driver);
		WebActionUtil.clickElement(this.businessType, driver, "Business type");
		WebActionUtil.clickElement(this.selectBusinessType(driver, sData[BaseLib.gv.cobbtcount]), driver, "Business type");
		WebActionUtil.waitTillCOBPageLoad("Business type Ent OD / Adhoc ", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.enddate, sData[BaseLib.gv.cobedcount], "Commercial Name", driver);
		WebActionUtil.type(this.margin, sData[BaseLib.gv.cobmcount], "Margin", driver);
		WebActionUtil.scrollIntoView(driver, this.attachApproval);
		WebActionUtil.type(this.creditPeriod, sData[BaseLib.gv.cobcpcount], "Credit period", driver);
		WebActionUtil.type(this.remarks, "could have been better", "Remarks", driver);
		WebActionUtil.clickElement(this.attachCommercial, driver, "attach commercial");
		WebActionUtil.uploadFile("D:\\finance_transactions_raised.csv");
		WebActionUtil.waitTillCOBPageLoad("attach commercial", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.attachApproval, driver, "atttach approval");
		WebActionUtil.uploadFile("D:\\finance_transactions_raised.csv");
		WebActionUtil.waitTillCOBPageLoad("attach approval", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.sveButton);
		WebActionUtil.clickElement(this.sveButton, driver, "save button");
		//WebActionUtil.clickElement(this.selectOtherCommercial, driver, "select Other Commercial");
		WebActionUtil.waitTillCOBPageLoad("select Other Commercial", driver, "select Other Commercial", 300);
		WebActionUtil.clickElement(this.commerceCheckBox, driver, "Commercial check box");
		WebActionUtil.waitTillCOBPageLoad("commerce Check Box", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.saveButton);
		WebActionUtil.clickElement(this.saveButton, driver, "save button");
		WebActionUtil.waitTillCOBPageLoad("save button", driver, "Client Onboarding Page", 300);
		WebActionUtil.clickElement(this.submitToFinance, driver, "submit to finance");
		WebActionUtil.waitTillCOBPageLoad("submit to finance", driver, "Client Onboarding Page", 300);
		Thread.sleep(5000);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list section");
		WebActionUtil.waitTillCOBPageLoad("pending List page", driver, "Client Onboarding Page", 300);
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client));
		WebActionUtil.assertElementDisplayed(this.getLockedClient(driver, client), driver, "client after submitting to finance");
	}
	
	public void COB_Finance_Login(String Uname, String Pass, String URL, WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 120);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clickByJavaSriptExecutor(this.anotherAccount, driver, "priya Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='email']", 90, driver);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		WebActionUtil.waitForVisiblityofElement("//div[text()='priya']", 90, driver);
		WebActionUtil.clickByJavaSriptExecutor(this.priyaBtn, driver, "priya Button");
		Thread.sleep(8000);
	}
	
	public void Approve_Client(WebDriver driver,String client) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("pending List", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.pendingList, driver, "pending list section");
		WebActionUtil.waitTillCOBPageLoad("pending List", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.getLockedClient(driver, client));
		WebActionUtil.clickElement(this.getLockedClient(driver, client), driver, "select the client");
		WebActionUtil.waitTillCOBPageLoad("Group Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.groupNext, driver, "group next button");
		WebActionUtil.waitTillCOBPageLoad("legal entity", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.legalEntityNext, driver, "legal entity next button");
		WebActionUtil.waitTillCOBPageLoad("Vertical Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.verticalNext, driver, "Vertical next button");
		WebActionUtil.waitTillCOBPageLoad("Lock Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.lockButton, driver, "lock button");
		WebActionUtil.waitTillCOBPageLoad("State GSTNext", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.stateGSTNext, driver, "GSTNo next button");
		WebActionUtil.waitTillCOBPageLoad("GSTN Details", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.GSTNDetailsNext);
		WebActionUtil.clickElement(this.GSTNDetailsNext, driver, "GSTNo next button");
		WebActionUtil.waitTillCOBPageLoad("Hub Next", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.hubNext);
		WebActionUtil.clickElement(this.hubNext, driver, "HUb next button");
		WebActionUtil.waitTillCOBPageLoad("Commercial next", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.lockButton, driver, "lock button");
		WebActionUtil.waitTillCOBPageLoad("Lock Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.approvalReqView, driver, "view button");
		WebActionUtil.waitTillCOBPageLoad("View Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.approvedButton);
		try {
			WebActionUtil.clickElement(this.approvedButton, driver, "Approve button");
		} catch (UnhandledAlertException f) {
		    try {
		        Alert alert = driver.switchTo().alert();
		        String alertText = alert.getText();
		        System.out.println("Alert data: " + alertText);
		        alert.accept();
		    } catch (NoAlertPresentException e) {
		        e.printStackTrace();
		    }
		}
		WebActionUtil.waitTillCOBPageLoad("approve Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.scrollIntoView(driver, this.closeButton);
		WebActionUtil.clickElement(this.closeButton, driver, "close button");
		WebActionUtil.waitTillCOBPageLoad("close Button", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.commercialToOPM, driver, "commercial OPM");
		WebActionUtil.waitTillCOBPageLoad("Commercial OPM", driver, "Client Onboarding Page", 230);
		Thread.sleep(5000);
		WebActionUtil.clickElement(this.opmApproval, driver, "OPM Approval");
		WebActionUtil.waitTillCOBPageLoad("OPM Approval", driver, "Client Onboarding Page", 180);
		WebActionUtil.scrollIntoView(driver, this.getApproveClient(driver, client));
		WebActionUtil.assertElementDisplayed(this.getApproveClient(driver, client), driver, "client after submitting to OPM");
	}
	public WebElement getClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='"+client+"']"));
	}
	public void verfiyAfterAddingHubs(WebDriver driver,String client) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.type(this.searchByGroupName, client, "Client Name", driver);
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.assertElementDisplayed(this.getClient(driver, client), driver, "client name");
	}
	
	public void clickLogOut(WebDriver driver) throws Exception{
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
		WebActionUtil.clickElement(this.logOutButton, driver, "Logout COB");
		WebActionUtil.waitTillCOBPageLoad("Client Onboarding", driver, "Client Onboarding Page", 120);
	}
}
