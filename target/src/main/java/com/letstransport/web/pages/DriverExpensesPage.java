package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class DriverExpensesPage {

		WebDriver driver;
		public DriverExpensesPage(WebDriver driver) {
	        // Allocate global driver reference to local driver
			this.driver = driver;
			// Initialize Ajax page initialisation
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(xpath = "//*[text()='Add Expense']")
		private WebElement addExpense;
		
		@FindBy(xpath = "//div[text()=' Add Expense ']/..//*[text()='Select Type...']")
		private WebElement expenseType;
		
		@FindBy(xpath = "//div[text()='Expense Date *']")
		private WebElement expenseDate;
		
		@FindBy(xpath = "//span[@class='number-span  selected-block  ']")
		private WebElement todaysDate;
		
		@FindBy(xpath = "(//*[@class='animated-input'])[1]")
		private WebElement vehicleNumber;
		
		@FindBy(xpath = "(//*[@class='animated-input'])[2]")
		private WebElement amount;
	
		@FindBy(xpath = "//*[text()='Upload Receipt']")
		private WebElement uploadReceipt;
				
		@FindBy(xpath = "(//*[@class='animated-input'])[4]")
		private WebElement clientName;
		
		@FindBy(xpath = "//*[text()='Save']")
		private WebElement saveBtn;
		
		@FindBy(xpath = "//*[@class='user-list-item']")
		private WebElement clientList;
		
		@FindBy(xpath = "//*[text()='Select From Date']")
		private WebElement selectFromDate;
		
		@FindBy(xpath = "//*[text()='Select To Date']")
		private WebElement selectToDate;
		
		@FindBy(xpath = "//*[@placeholder='Vehicle ( Optional )']")
		private WebElement vehiclePlaceHolder;
		
		@FindBy(xpath = "//div[@class='search-button']")
		private WebElement searchButton;
		
		@FindBy(xpath = "//div[text()='Verify']")
		private WebElement verifyBtn;
		
		@FindBy(xpath = "//span[text()='Advance']")
		private WebElement advanceTxt;
		
		@FindBy(xpath = "//span[text()='Toll and Parking']")
		private WebElement tollAndParking;
		
		public WebElement getExpense(WebDriver driver, String expense) {
			return driver.findElement(By.xpath("//div[text()=' Add Expense ']/..//*[text()='"+expense+"']"));
		}
		
		public WebElement getCheckBox(WebDriver driver, String date, String vehicle, String amount) {
			return driver.findElement(By.xpath("//div[text()='"+date+"']/..//div[text()='"+vehicle+"']/..//div[text()='"+amount+"']/..//*[@class='checkbox-div ']"));
		}
		
		public WebElement getCreatedExpense(WebDriver driver, String date, String vehicle, String amount) {
			return driver.findElement(By.xpath("//div[text()='"+date+"']/..//div[text()='"+vehicle+"']/..//div[text()='"+amount+"']"));
		}
		
		public void verifiedLink(WebDriver driver) throws Exception{
			WebActionUtil.clickElement(this.addExpense, driver, "add Expenses");
		}
	
		public void addExpense(WebDriver driver, String filepath, String sheet,String testcase,String imagePath,String expense) throws Exception{
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
			WebActionUtil.waitTillPageLoad("driver Expense", driver, "driver expense", 30);
			WebActionUtil.clickElement(this.addExpense, driver, "add Expenses");
			WebActionUtil.clickElement(this.expenseType, driver, "add Expenses");
			WebActionUtil.clickElement(this.getExpense(driver, expense), driver, "add Expenses");
			WebActionUtil.clickElement(this.expenseDate, driver, "Expense date");
			WebActionUtil.clickElement(this.todaysDate, driver, "Todays date");
			WebActionUtil.type(this.vehicleNumber, sData[BaseLib.gv.drvncount], "vehicle number", driver);
			WebActionUtil.waitTillPageLoad("vehicle number", driver, "vehicle number", 30);
			WebActionUtil.type(this.amount, "10", "vehicle number", driver);
			WebActionUtil.type(this.clientName, "Liberty", "vehicle number", driver);
			WebActionUtil.clickElement(this.clientList, driver, "Clinet name");
			WebActionUtil.clickElement(this.uploadReceipt, driver, "upload Receipt Btn");
			Thread.sleep(1000);
			WebActionUtil.uploadFile(imagePath);
			Thread.sleep(10000);
			WebActionUtil.waitTillPageLoad("save button", driver, "driver expense", 60);
			WebActionUtil.clickElement(this.saveBtn, driver, "save Btn");
		}
		
		public void verifyExpense(WebDriver driver, String filepath, String sheet,String testcase) throws Exception{
			String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
			WebActionUtil.waitTillPageLoad("driver Expense", driver, "driver expense", 30);
			WebActionUtil.clickElement(this.selectFromDate, driver, "from date");
			WebActionUtil.clickElement(this.todaysDate, driver, "today date");
			WebActionUtil.clickElement(this.selectToDate, driver, "to date");
			WebActionUtil.clickElement(this.todaysDate, driver, "today date");
			WebActionUtil.waitTillPageLoad("driver Expense", driver, "driver Expense", 30);
			WebActionUtil.type(this.vehiclePlaceHolder,sData[BaseLib.gv.drvncount],"vehicle textbox",driver);
			WebActionUtil.clickElement(this.searchButton, driver, "search button");
			WebActionUtil.waitTillPageLoad("driver Expense", driver, "driver Expense", 30);
			WebActionUtil.clickElement(this.advanceTxt, driver, "advance text");
			WebActionUtil.clickElement(this.getCheckBox(driver,WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),sData[BaseLib.gv.drvncount], "10"), driver, "click on checkbox");
			WebActionUtil.clickElement(this.verifyBtn, driver, "verify button");
			WebActionUtil.waitTillPageLoad("driver Expense", driver, "driver Expense", 30);
			WebActionUtil.clickElement(this.tollAndParking, driver, " tollAndParking");
			WebActionUtil.assertElementDisplayed(this.getCreatedExpense(driver,WebActionUtil.fetchSystemDateTime("d-MMM-yyyy"),sData[BaseLib.gv.drvncount], "10"), driver, "click on checkbox");
			//WebActionUtil.clickElement(this.verifyBtn, driver, "verify button");

		}
}
