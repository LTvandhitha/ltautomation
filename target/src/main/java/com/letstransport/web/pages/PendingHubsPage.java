package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class PendingHubsPage {
	WebDriver driver;

	public PendingHubsPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='PLASMA']")
	private WebElement hamburgerMenu;
	
	@FindBy(xpath = "//div[text()='Add Details']")
	private WebElement addDetails;
		
	@FindBy(xpath = "//div[text()='Add Hub Latitude']/..//input")
	private WebElement addHubLatitude;
	
	@FindBy(xpath = "//div[text()='Add Hub Longitude']/..//input")
	private WebElement addHubLongitude;
	
	@FindBy(xpath = "//div[text()='Add Hub Area']/..//div[@class='Select-placeholder']")
	private WebElement addHubArea;
	
	@FindBy(xpath = "//div[text()='Save']")
	private WebElement saveBtn;
	
	@FindBy(xpath = "//div[text()='Add Hub FO']/..//div[@class='Select-placeholder']")
	private WebElement addHubFO;
	
	@FindBy(xpath = "//div[@class='indicator-text']/../..//div[@class='user-notification-icon-wrapper']/following::div[1]")
	private WebElement logoutBtn;
	
	public WebElement getClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//div[text()='"+client+"']"));
	}
	
	public void addHubs(WebDriver driver,String filepath,String sheet, String testcase,String client) throws Exception{
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.getClient(driver,client), driver, "get client");
		WebActionUtil.waitTillPageLoad(client, driver, "pending hubs", 120);
		WebActionUtil.clickElement(this.addDetails, driver, "add details");
		WebActionUtil.type(this.addHubLatitude, sData[BaseLib.gv.cobhlcount], "add Hub Latitude", driver);
		WebActionUtil.type(this.addHubLongitude, sData[BaseLib.gv.cobhlocount], "add Hub Longitude", driver);
		WebActionUtil.scrollIntoView(driver, this.saveBtn);
		WebActionUtil.clickElement(this.addHubArea, driver, "add hub");
		WebActionUtil.scrollIntoView(driver, this.selectArea(driver, sData[BaseLib.gv.cobhacount]));
		WebActionUtil.clickElement(this.selectArea(driver, sData[BaseLib.gv.cobhacount]), driver, "Area");
		WebActionUtil.clickElement(this.addHubFO, driver, "add hub");
		WebActionUtil.scrollIntoView(driver, this.selectHub(driver, sData[BaseLib.gv.cobhfcount]));
		WebActionUtil.clickElement(this.selectHub(driver, sData[BaseLib.gv.cobhfcount]), driver, "hub");
		WebActionUtil.clickElement(this.saveBtn, driver, "Save button");
		WebActionUtil.waitTillPageLoad("Save button", driver, "pending hubs", 120);
		WebActionUtil.waitForVisiblityofElement("//div[@class='indicator-text']/../..//div[@class='user-notification-icon-wrapper']/following::div[1]", 100, driver);
		Thread.sleep(2000);
	}
	
	public WebElement selectArea(WebDriver driver, String area) {
		return driver.findElement(By.xpath("//div[text()='Add Hub Area']/..//*[text()='"+area+"']"));
	}
	
	public WebElement selectHub(WebDriver driver, String hub) {
		return driver.findElement(By.xpath("//div[text()='Add Hub FO']/..//*[text()='"+hub+"']"));
	}
}
