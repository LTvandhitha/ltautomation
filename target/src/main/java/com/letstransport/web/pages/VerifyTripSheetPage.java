package com.letstransport.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.WebActionUtil;

public class VerifyTripSheetPage {
	
	WebDriver driver;

	public VerifyTripSheetPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//*[@class='floating-button-icon-div']")
	private WebElement floatingButton;
	
	@FindBy(xpath = "//*[text()='Verify Tripsheet']")
	private WebElement verifyTripSheet;
	
	@FindBy(xpath = "//*[text()='Select Client']")
	private WebElement clientDropDown;

	@FindBy(xpath = "//*[text()='From Date']")
	private WebElement fromDate;

	@FindBy(xpath = "//*[text()='To Date']")
	private WebElement toDate;

	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(
				By.xpath("//*[@class='number-span  selected-block  ']/../..//*[@class='block numbers  ']//*[text()='"
						+ date + "']"));
	}
	
	@FindBy(xpath = "//*[@class='number-span  selected-block  ']")
	private WebElement selectToDate;
	
	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;
	
	public WebElement MapTripSheet(WebDriver driver, String client, String vehicle) {
		return driver.findElement(By
				.xpath("(//*[text()='Not Verified']/..//*[text()='" + vehicle + "']/../..//*[text()='" + client + "'])[1]"));
	}
	
	@FindBy(xpath = "//*[text()='Verification']")
	private WebElement verificationText;
	
	
	@FindBy(xpath = "//*[text()='Digital Tripsheets are?']/..//*[text()='Verified']")
	private WebElement verifiedBtn;
	
	@FindBy(xpath = "//*[text()='Old Remarks']/..//div[@class='close-icon']")
	private WebElement xBtn;
	
	@FindBy(xpath = "//*[text()='Select Hub']")
	private WebElement hubDropDown;
	
	@FindBy(xpath = "//*[text()='Select Status']")
	private WebElement statusDropDown;
	
	@FindBy(xpath = "//*[text()='Not Verified']")
	private WebElement notVerifiedStatus;
	
	@FindBy(xpath = "//*[text()='Not Verified']//*[text()='Verified']")
	private WebElement verifiedStatus;
	
	public WebElement getTripSheet(WebDriver driver, String client, String vehicle) {
		return driver.findElement(By
				.xpath("//*[text()='"+client+"']/../..//*[text()='"+vehicle+"']/../..//*[text()='Verified']"));
	}
	
	public WebElement getClient(WebDriver driver, String client) {
		return driver.findElement(By.xpath("//*[text()='Select Client']//*[text()='" + client.toUpperCase() + "']"));
	}

	public WebElement getClientHub(WebDriver driver, String hub) {
		return driver.findElement(By.xpath("//*[text()='Select Hub']//*[text()='" + hub.toUpperCase() + "']"));
	}

	public WebElement getStatus(WebDriver driver, String status) {
		return driver.findElement(By.xpath("//*[text()='Select Status']//*[text()='" + status + "']"));
	}
	
	public void verifyTripSheet(WebDriver driver, String filepath, String sheet,String testcase,String vehicle) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Agara area", driver, "Home Page", 30);
		WebActionUtil.clickElement(this.floatingButton, driver, "Floating button");
		WebActionUtil.clickElement(this.verifyTripSheet, driver, "verify tripsheet button");
		WebActionUtil.waitTillPageLoad("loading tripsheet", driver, "Gallery", 30);
		WebActionUtil.clickElement(this.fromDate, driver, "From date");
		WebActionUtil.clickElement(this.selectDate(driver, String.valueOf(Integer.parseInt(WebActionUtil.fetchSystemDateTime("dd")))), driver, "select From date");
		WebActionUtil.clickElement(this.toDate, driver, "To date");
		WebActionUtil.clickElement(this.selectToDate, driver, "select To date");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		WebActionUtil.waitTillPageLoad("Done", driver, "Tripsheet page", 30);
		WebActionUtil.clickElement(this.clientDropDown, driver, "client DropDown");
		WebActionUtil.waitTillPageLoad("clientDropDown", driver, "Tripsheet page", 30);
		WebActionUtil.clickElement(this.getClient(driver, sData[BaseLib.gv.cnCount]), driver, "client selected");
		WebActionUtil.clickElement(this.hubDropDown, driver, "hub DropDown");
		WebActionUtil.waitTillPageLoad("hubDropDown", driver, "Tripsheet page", 30);
		WebActionUtil.clickElement(this.getClientHub(driver, sData[BaseLib.gv.hCount]), driver, "hub selected");
		WebActionUtil.clickElement(this.statusDropDown, driver, "Status DropDown");
		WebActionUtil.waitTillPageLoad("Status DropDown", driver, "Tripsheet page", 30);
		WebActionUtil.clickElement(this.getStatus(driver, "Not Verified"), driver, " Status selected");
		WebActionUtil.waitTillPageLoad("Status selected", driver, "Tripsheet page", 30);
		WebActionUtil.assertElementDisplayed(this.MapTripSheet(driver, sData[BaseLib.gv.cnCount], vehicle), driver,
				"Client is displayed");
		WebActionUtil.clickElement(this.MapTripSheet(driver, sData[BaseLib.gv.cnCount], vehicle), driver, "click client");
		WebActionUtil.waitTillPageLoad("click client", driver, "Tripsheet page", 60);
		WebActionUtil.assertElementDisplayed(this.verificationText, driver, "Pop up");
		WebActionUtil.clickElement(this.verifiedBtn, driver, "verified button");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		WebActionUtil.waitTillPageLoad("done", driver, "Tripsheet page", 60);
		try{WebActionUtil.clickElement(this.xBtn, driver, "X button");}
		catch(Exception e){System.out.println("looks like even unmapped script has passed");}
		finally{
		WebActionUtil.clickElement(this.notVerifiedStatus, driver, "not Verified Status");
		WebActionUtil.waitTillPageLoad("Status DropDown", driver, "Tripsheet page", 60);
		WebActionUtil.clickElement(this.verifiedStatus, driver, " Verified Status selected");
		WebActionUtil.waitTillPageLoad("Status selected", driver, "Tripsheet page", 60);
		WebActionUtil.assertElementDisplayed(this.getTripSheet(driver, sData[BaseLib.gv.cnCount], vehicle), driver, "tripsheet verified");
		}
	}
}
