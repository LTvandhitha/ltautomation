package com.letstransport.web.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.init.InitializePages;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class WebAdvancePaymentPage {
	WebDriver driver;

	public WebAdvancePaymentPage(WebDriver driver) {
		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='options-footer']/div[text()='Fetch Logsheets']")
	private WebElement fetchLogSheet;
	@FindBy(xpath = "//div[@class='count-parent mapped']")
	private WebElement mapped;
	@FindBy(xpath = "//div[@class='booking-date']")
	private WebElement bookingDate;
	@FindBy(xpath = "//div[@class='vehicle-icon vehicle-info scheduled']")
	private WebElement vecno;
	@FindBy(xpath = "//div[@class='close-button']")
	private WebElement closeBtn;
	@FindBy(xpath = "//div[@class='back-home-button']/a[text()='Back To Home']")
	private WebElement backToHome;
	@FindBy(xpath = "//div[@class='link-with-submenu  ']/li[text()='Payments']")
	private WebElement payments;
	@FindBy(xpath = "//li[text()='Driver Extra Expenses']")
	private WebElement extraExpence;
	@FindBy(xpath = "//div[text()='Add Expense']")
	private WebElement addExpence;
	@FindBy(xpath = "//div[@class='Select-input']/input")
	private WebElement selectType;
	@FindBy(xpath = "//div[@class='Select-menu-outer']")
	private WebElement menuOuter;
	@FindBy(xpath = "//div[text()='Advance']")
	private WebElement advance;
	@FindBy(xpath = "//div[text()='Expense Date *']/../div/span")
	private WebElement cal;
	@FindBy(xpath = "//span[text()='Vehicle Number']/..//../input")
	private WebElement inputVecNo;
	@FindBy(xpath = "//span[text()='Amount(Rs)']/..//../input")
	private WebElement inputPrice;
	@FindBy(xpath = "//span[text()='Client Name']/..//../input")
	private WebElement client;
	@FindBy(xpath = "//div[text()='LONEE']")
	private WebElement clientName;
	@FindBy(xpath = "//div[text()='Save']")
	private WebElement save;
	@FindBy(xpath = "//div[text()='created']/../descendant::div[@class='expence-list-col select']/div")
	private WebElement checkbox;
	@FindBy(xpath = "//div[@class='expence-list-col status created']")
	private WebElement created;
	@FindBy(xpath = "//div[@class='expence-list-col status verified']")
	private WebElement verified;
	@FindBy(xpath = "//div[text()='Search']")
	private WebElement search;
	@FindBy(xpath = "//div[text()='Verify']")
	private WebElement verify;
	@FindBy(xpath = "//div[@class='expence-list-item']/div[2]")
	private WebElement vehiclemapped;
	@FindBy(xpath = "//div[@class='expence-list-item']/div[2]/../div[text()='verified']")
	private WebElement verifiedtext;

	public void _advancePaidForUploadPOD() throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "click on hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				" wait for searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				" click on searchtextfield", GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, " select and click NM");
		wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		WebActionUtil.clickElement(fetchLogSheet, driver, "FetchLogsheet");
		WebActionUtil.waitForElement(mapped, driver, "wait for Map", 4000);
		WebActionUtil.clickElement(mapped, driver, "click on Map");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String todayDate = dateFormat.format(date);
		String[] splitDate = todayDate.split("/");
		String part1 = splitDate[0];
		String bookindate = bookingDate.getText();
		String[] splitbookindate = bookindate.split(" ");
		String partbookin1 = splitbookindate[0];
		if (part1.equals(partbookin1)) {
			String vechileNo = vecno.getText();
			String am = GenericLib.amount;
			WebActionUtil.clickElement(closeBtn, driver, "click on close");
			WebActionUtil.clickElement(backToHome, driver, "click on Back To Home");
			WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, " click on hamberger");
			WebActionUtil.clickElement(payments, driver, "Click On Payment");
			WebActionUtil.clickElement(extraExpence, driver, "click on ExtraExpence");
			Thread.sleep(8000);
			WebActionUtil.clickElement(addExpence, driver, "click on AddExpence");
			WebActionUtil.actionClick(selectType, driver, "click on SelectType");
			WebActionUtil.actionClick(advance, driver, "click on Advance");
			WebActionUtil.clickElement(cal, driver, "click on calender");
			WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
			WebActionUtil.clickElement(inputVecNo, driver, "click on Input Vec No");
			WebActionUtil.clearAndType(inputVecNo, vechileNo, "type Vehicle No in textfield", driver);
			WebActionUtil.clickElement(inputPrice, driver, "click on Input Vec No");
			WebActionUtil.clearAndType(inputPrice, GenericLib.amount, "price", driver);
			WebActionUtil.clickElement(client, driver, "click on Input Vec No");
			WebActionUtil.clearAndType(client, "lonee", "name of client", driver);
			WebActionUtil.clickElement(clientName, driver, "name of client");
			WebActionUtil.clickElement(save, driver, "click on Save");
			Thread.sleep(10000);
			wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
			WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
			wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
			WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
			Thread.sleep(1000);
			WebActionUtil.actionClick(search, driver, "click on search");
			Thread.sleep(10000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", created);
			WebActionUtil.clickElement(checkbox, driver, "click on checkbox");
			WebActionUtil.clickElement(verify, driver, "click on verify");
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
			System.out.println(vehiclemapped.getText());
			System.out.println(vechileNo);
			if (vehiclemapped.getText().equals(vechileNo)) {
				WebActionUtil.verifyElementIsDisplayed(verifiedtext, driver, "verifiedText");
			}

		}
	}
}
