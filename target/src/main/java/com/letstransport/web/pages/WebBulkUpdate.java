package com.letstransport.web.pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.springframework.util.SystemPropertyUtils;
import org.testng.Assert;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;
import com.paulhammant.ngwebdriver.ByAngular;

//import ch.qos.logback.core.joran.action.ActionUtil;
import io.appium.java_client.android.AndroidDriver;

public class WebBulkUpdate {

	// Webdriver driver instance creation
	WebDriver driver;

	public WebBulkUpdate(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
		
		
		
	}

	/** Home Page **/
	
		
	
	@FindBy(xpath="//*[text()='Customer Relations']")
	private WebElement CustRelations;

	public WebElement clkCustRelations() {
		return CustRelations;
	}
	
	@FindBy(xpath="//*[text()='Miscellaneous Order']")
	private WebElement miscOrder;

	public WebElement clkmiscOrder() {
		return miscOrder;
	}

	
	@FindBy(xpath="//*[@id='react-select-2--value']/div[1]")
	private WebElement clntDrpdwn;

	public WebElement clkclntDrpdwn() {
		return clntDrpdwn;
	}
	
	@FindBy(xpath="//*[@id='mainContainer']/div/div[3]/div/div[2]/div/div/div[2]/div")
	private WebElement blkUpload;

	public WebElement clkblkUpload() {
		return blkUpload;
	}
	
	@FindBy(xpath="//*[text()='Choose a file']")
	private WebElement chooseFile;

	public WebElement clkchooseFile() {
		return chooseFile;
	}
	
	
	
	@FindBy(xpath="//*[@class='Select-menu-outer']")
	private WebElement searchResult;
	
	public WebElement clickResult() {
		return searchResult;
	}
	
	@FindBy(xpath="//*[contains(text(),'ownload')]")
	private WebElement dwnfile;
	
	public WebElement clickdwnfile() {
		return dwnfile;
	}
	
	
	@FindBy(xpath="//*[text()='Confirmation']")
	private WebElement confirmPage;
	
	public WebElement verifyconfirmPage() {
		return confirmPage;
	}
	
	
	@FindBy(xpath="//input[@value='Confirm Creation']")
	private WebElement confirmBtn;
	
	public WebElement clkconfirmBtn() {
		return confirmBtn;
	}
	
	@FindBy(xpath="//input[@value='Ok']")
	private WebElement ok;
	
	public WebElement clkok() {
		return ok;
	}
	
	@FindBy(xpath="//*[contains(text(),'Duplicate')]")
	private WebElement dup;
	
	public WebElement clkDup() {
		return dup;
	}
	
	
	@FindBy(xpath="//*[@value='Cancel']")
	private WebElement cancel;
	
	public WebElement clkcnl() {
		return cancel;
	}
	
	@FindBy(xpath="//*[text()='Failure']")
	private WebElement failure;
	
	public WebElement verifyFailure() {
		return failure;
	}
	
	
	@FindBy(xpath="//*[text()=' No. of trips not ']")
	private WebElement notCreatedTrips;
	
	public WebElement verifynotCreatedTrips() {
		return notCreatedTrips;
	}
	
	
	@FindBy(xpath="//*[text()='Finance']")
	private WebElement finance;
	
	public WebElement clkFinance() {
		return finance;
	}
	
	
	@FindBy(xpath="//*[text()='To']/..//*[text()='Select Date']")
	private WebElement dateTo;
	
	public WebElement clkdateTo() {
		return dateTo;
	}
	
	@FindBy(xpath="//*[text()='From']/..//*[text()='Select Date']")
	private WebElement dateFrom;
	
	public WebElement clkdateFrom() {
		return dateFrom;
	}
	
	
	@FindBy(xpath="//*[@class='from-date-div']")
	private WebElement frmDate;
	
	public WebElement clkfrmDate() {
		return frmDate;
	}
	
	@FindBy(xpath="//*[@class='to-date-div']")
	private WebElement toDate;
	
	public WebElement clktoDate() {
		return toDate;
	}
	
	
	

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : To verify Bulk update- defcome 
	 *
	 * @throws Exception
	 * 
	 * 
	 */


	public void CustomerRelations(WebDriver driver) throws Exception {
		
	WebActionUtil.scrollIntoViewAndClick(driver, CustRelations);
		
	}
	
	
	public void miscOrders(WebDriver driver) throws Exception {
		
		WebActionUtil.scrollIntoViewAndClick(driver, miscOrder);
			
		}
	
	public void bulk_upload( WebDriver driver, String client) throws Exception{
	
		WebActionUtil.actionClickAndType(clntDrpdwn, driver, "Client drop down", client);
		Thread.sleep(10000);
		
		WebActionUtil.actionClick(searchResult, driver, "Client");
		Thread.sleep(5000);
		
		WebActionUtil.clickElement(blkUpload, driver, "Bulk Upload");
		
		WebActionUtil.clickElement(dwnfile, driver, "Download file");
		
		

	}
	
	
	public void bulk_writeandupload( WebDriver driver, String client) throws Exception{
		
		WebActionUtil.actionClickAndType(clntDrpdwn, driver, "Client drop down", client);
		Thread.sleep(10000);
		
		WebActionUtil.actionClick(searchResult, driver, "Client");
		Thread.sleep(5000);
		
		WebActionUtil.clickElement(blkUpload, driver, "Bulk Upload");
		
		
		WebActionUtil.clickElement(chooseFile, driver, "Choose file");
	
	}
	
	@FindBy(xpath="//*[@class='logout-svg']")
	private WebElement logout;
	
	public WebElement clkLogout() {
		return logout;
	}
	
	
	public void logout( WebDriver driver) throws IOException{
		
		WebActionUtil.clickElement(logout, driver, "logout");
	}
	
	
	public void choosefile(WebDriver driver) throws IOException
	{
		
		WebActionUtil.clickElement(chooseFile, driver, "Choose file");
	}
	
	
	public void downloadFile_bulk(String fileLocation) throws Exception {
		StringSelection file = new StringSelection(fileLocation);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);

		
	
		Robot rb = new Robot();

		rb.setAutoDelay(1800); // Similar to thread.sleep
		
		
//		rb.keyPress(KeyEvent.VK_CONTROL);
//		rb.keyPress(KeyEvent.VK_C);
//		Thread.sleep(2000);
//		rb.keyRelease(KeyEvent.VK_CONTROL);
//		rb.keyRelease(KeyEvent.VK_C);

		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);

		rb.setAutoDelay(1800);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		rb.setAutoDelay(1800);
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		
		rb.setAutoDelay(1800);



		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		
	}
	
	
	public void confirm_page( WebDriver driver) throws IOException{
		
		WebActionUtil.verifyElementIsDisplayed(confirmPage, driver, "Confirm upload");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		WebActionUtil.clickElement(confirmBtn, driver, "Confirm creation");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		
		
	}
	
	
	
	public void ok( WebDriver driver) throws IOException {
		
	WebActionUtil.clickElement(ok, driver, "OK");
			
	}
	
	public void upload_cancel( WebDriver driver) throws IOException {
		
		WebActionUtil.clickElement(cancel, driver, "cancel");
		WebActionUtil.verifyElementIsDisplayed(chooseFile, driver, "Choose a file");
		
				
		}
	public void verify_duplicate( WebDriver driver) throws IOException{
	
		
		WebActionUtil.verifyElementIsDisplayed(dup, driver, "Duplicate");
	}
	
	public void verify_failure( WebDriver driver) throws IOException{
		
		
		WebActionUtil.verifyElementIsDisplayed(failure, driver, "Failure");
		
		
	}
	
	
	public void verifynotCreated_trips(WebDriver driver ) throws IOException{
		
		WebActionUtil.verifyElementIsDisplayed(notCreatedTrips, driver, "Trip not created");
	}
	
	
	public WebElement selectDate(WebDriver driver, String date) {
		return driver.findElement(By.xpath("//*[@class='content-calender']//*[@class='block numbers  ']//*[text()='"+date+"']"));
		
				}
	
	public void defcome_finance(WebDriver driver, String SysDate) throws Exception{
		
		WebActionUtil.scrollIntoViewAndClick(driver, finance);	
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
				
		WebActionUtil.clickElement(dateFrom, driver, "from date");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
//		System.out.println(selectDate(driver,SysDate));
		
		WebActionUtil.clickElement(selectDate(driver,SysDate), driver, "Select date");
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		WebActionUtil.clickElement(dateTo, driver, "To date");
		
		WebActionUtil.clickElement(selectDate(driver,SysDate), driver, "Select date");
		
		WebActionUtil.clickElement(dwnfile, driver, "DownloadFile");
		
			
		
	}
	
	
	public static String returnday(String SysDate ){
		String []finalDate= SysDate.split("/",3);
		
		String finDate=null;

           
		for (int i = 0;i<2;i++) 
				{
				  finDate= finalDate[1];
				}	
								
		return finDate;
		}

	
	
	public void downloadFile_finance(String fileLocation) throws Exception {
		StringSelection file = new StringSelection(fileLocation);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);

		
	
		Robot rb = new Robot();

		rb.setAutoDelay(1800); // Similar to thread.sleep
		
		
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_C);
		Thread.sleep(2000);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_C);

		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);

		rb.setAutoDelay(1800);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		rb.setAutoDelay(1800);
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		
		rb.setAutoDelay(1800);


		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		
	}
	
	
	
	@FindBy(xpath="//*[text()='Raised/Paid']")
	private WebElement raised;
	
	public WebElement clkraised() {
		return raised;
	}
	
	@FindBy(xpath="//*[@class='upload-button']")
	private WebElement uplbtn;
	
	public WebElement clkuplbtn() {
		return uplbtn;
	}

	@FindBy(xpath="//*[@class='file-upload-input-label']")
	private WebElement browselink;
	
	public WebElement clkbrowselink() {
		return browselink;
	}
	
	
	@FindBy(xpath="//*[@class='file-name-input']")
	private WebElement bankname;
	
	public WebElement enterBankName() {
		return bankname;
	}
	
	
	@FindBy(xpath = "//*[@class='general-popup ']//*[text()='DONE']")
	private WebElement done;
	
	public WebElement clkDone() {
		return done;
	}
	
	
	@FindBy(xpath = "//*[@class='done-button ']")
	private WebElement donebtn;
	
	public WebElement clkDonebtn() {
		return donebtn;
	}
	
	
	
	
	public void raisePayment ( WebDriver driver) throws IOException{
		
		
		WebActionUtil.clickElement(raised, driver, "Raised/Paid");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(uplbtn, driver, "Upload Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(browselink, driver, "browse link");
		
	
		
		
	}

	public WebElement filecreated(WebDriver driver, String filename) {
		return driver.findElement(By.xpath("//*[@class='file-name' and text()='"+filename+"']"));
			
			}
	
	
	
	public WebElement getCheckBox(WebDriver driver, String vehicle, String city) {
		return driver.findElement(By.xpath(
				"//*[text()='Status']/../..//*[text()='raised']/../..//*[text()='Vehicle']/../..//*[text()='" + vehicle
						+ "']/../..//*[text()='City']/../..//*[text()='" + city + "']/..//*[@class='checkbox-div ']"));
	}
	
	
	@FindBy(xpath="//*[text()='Paid']")
	private WebElement paid;
	
	public WebElement clkpaid() {
		return paid;
	}
	
	
	@FindBy(xpath = "//*[@class='general-popup ']//*[text()='Select Date']")
	private WebElement selectDate;
	
	
	@FindBy(xpath = "//*[text()='CONFIRM']")
	private WebElement confirm;
	public WebElement clkconfirm() {
		return confirm;
	}
	
	@FindBy(xpath = "//*[text()='1']")
	private WebElement okbtn;
	public WebElement clkokbtn() {
		return okbtn;
	}
	
	@FindBy(xpath = "//*[text()='OK']")
	private WebElement success;
	public WebElement verifysuccess() {
		return success;
	}
	
	public void upload_bankname(WebDriver driver, String banknamestr,String date) throws Exception{
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.type(bankname, banknamestr, "Bank name", driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(donebtn, driver, "Done");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.verifyElementContainsText(filecreated(driver,banknamestr), banknamestr, driver);		
		WebActionUtil.clickElement(filecreated(driver,banknamestr), driver, "file");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(getCheckBox(driver,"KA02EC1234","bang"), driver, "checkbox");		
		WebActionUtil.clickElement(paid, driver, "Paid");	
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(selectDate, driver, "SelectDate");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(selectDate(driver,date), driver, "date");		
		WebActionUtil.clickElement(done, driver, "done");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		WebActionUtil.clickElement(confirm, driver, "confirm");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
		
		
		
	}
	
	
	public void sucess( WebDriver driver) throws IOException{
		
		WebActionUtil.verifyElementIsDisplayed(success, driver, "Sucess upload");
		WebActionUtil.clickElement(okbtn, driver, "ok");
		
	}
	

	
	public static String removeZero(String str) 
    { 
        // Count leading zeros 
        int i = 0; 
        while (i < str.length() && str.charAt(i) == '0') 
            i++; 
  
        // Convert str into StringBuffer as Strings 
        // are immutable. 
        StringBuffer sb = new StringBuffer(str); 
  
        // The  StringBuffer replace function removes 
        // i characters from given index (0 here) 
        sb.replace(0, i, ""); 
  
        return sb.toString();  // return in String 
    } 
	
	
	@FindBy(xpath = "//*[text()='Search Client...']")
	private WebElement srchClnt;
	public WebElement clksrchClnt() {
		return srchClnt;
	}
	
	@FindBy(xpath = "//*[text()='Search Client...']/..//input")
	private WebElement searchClientInputfield;

	public WebElement clickSelectClient() {
		return searchClientInputfield;
	}
	
	
	@FindBy(xpath = "//*[@class='Select-menu-outer']")
	private WebElement menuOuter;

	public WebElement clickmenuOuter() {
		return menuOuter;
	}
	
	@FindBy(xpath = "//*[@class='search']")
	private WebElement srch;

	public WebElement clickmclksrch() {
		return srch;
	}
	
	
	
	public void paymentvisibilityMisc( WebDriver driver,String SysDate) throws Exception{
		
		
		WebActionUtil.clickElement(frmDate, driver, "From Date");
		WebActionUtil.clickElement(selectDate(driver,SysDate), driver, "Select date");
		WebActionUtil.clickElement(toDate, driver, "To Date");
		WebActionUtil.clickElement(selectDate(driver,SysDate), driver, "Select date");
		
		
		WebActionUtil.clickElement(srchClnt, driver, "search client");
		WebActionUtil.type(searchClientInputfield, GenericLib.client, "Client name", driver);
		WebActionUtil.waitTillLoaderFinishes("client name ",driver, "loader", 300);
		WebActionUtil.actionClick(menuOuter, driver, " Client name");
		
		WebActionUtil.clickElement(srch, driver, "Search");
		
				
		
			}
		

	
	
		 
	
	public void verifyTableContent( WebDriver driver, String data, String rowName1,String rowName2) throws  Exception{
		
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	
		
		WebElement firstRow= driver.findElement(By.xpath("//*[@class='row-class table-filter-row']"));
		
		
		String Rowdata= firstRow.getText();
		
		WebActionUtil.verifyContainsText(Rowdata,rowName1," contains  Actual : " + rowName1 + "As Expected : ");
		WebActionUtil.verifyContainsText(Rowdata,rowName2," contains  Actual : " + rowName2 + " Expected : ");
		
		
}
	}

		
	