package com.letstransport.web.pages;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.letstransport.init.InitializePages;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class WebHomePage {
	WebDriver driver;
	public static String vehicleNum;

	public WebHomePage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	@FindBy(xpath = "//div[@class='hamburger-menu']")
	private WebElement hamburgerMenu;

	public WebElement clickOnhamburgerMenu() {
		return hamburgerMenu;
	}

	@FindBy(xpath = "//li[text()='All List']")
	private WebElement allList;

	public void clickOnAllList(WebDriver driver) throws Exception {
		Thread.sleep(5000);
		WebActionUtil.actionClick(allList, driver, "All List");
		Thread.sleep(2000);
	}

	public WebElement getLGoingOption(String client, String hub, String time, String phone) {
		return driver.findElement(By.xpath(
				"//*[text()='" + client + "']/..//*[@class='ent-od-div']/..//*[@class='loadboard-div']/..//*[text()='"
						+ hub + "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone
						+ "']/../../../..//*[text()='Select ']/..//*[text()='(P) Going']"));
	}

	@FindBy(xpath = "//*[text()='Loadboard']")
	private WebElement loadboardLink;

	public WebElement clickLoadboard() {
		return loadboardLink;
	}

	/*
	 * <<<<<<< HEAD
	 * 
	 * @FindBy(xpath = "//*[@id='form']/button") =======
	 */
	@FindBy(xpath = "//button")
	private WebElement SignIn;

	public WebElement ClickSignIn() {
		return SignIn;
	}

	@FindBy(xpath = "//*[local-name()='svg']//*[name()='g']")
	// @FindBy(xpath ="//*[@class='logout-svg']")
	private WebElement logoutBtn;

	public WebElement getLogoutBtn() {
		return logoutBtn;
	}

	@FindBy(xpath = "//div[text()='OPM']")
	private WebElement opmLink;

	public WebElement SlectOPM() {
		return opmLink;
	}

	@FindBy(xpath = "//*[text()='FQLC']")
	private WebElement fqlcLink;

	public WebElement clickFQLC() {
		return fqlcLink;
	}
	
	@FindBy(xpath = "//*[text()='fieldops']")
	private WebElement loginid;

	public WebElement clickloginid() {
		return loginid;
	}
	
	@FindBy(xpath = "//*[text()='Enter Vehicle Number']/..//input")
	private WebElement enterVehicleNumber;

	@FindBy(xpath = "//*[text()='Next']")
	private WebElement nextBtn;

	@FindBy(xpath = "//*[text()='Select Vendor...']")
	private WebElement selectVendor;

	@FindBy(xpath = "//*[text()='Select Vendor...']/../..//*[@class='Select-menu-outer']")
	private WebElement selectVendorAgain;

	@FindBy(xpath = "//*[text()='FQLC Supervisor']")
	private WebElement fqlcSuperLink;

	@FindBy(xpath = "//*[text()='DONE']")
	private WebElement doneBtn;

	public WebElement clickFQLCSupervisor() {
		return fqlcSuperLink;
	}

	@FindBy(xpath = "//li[text()='Verification Dashboard']")
	private WebElement verifyDashBoardLink;

	public WebElement clickVerifyDashBoard() {
		return verifyDashBoardLink;
	}

	@FindBy(xpath = "//li[text()='Driver Verification']")
	private WebElement driverVerificationLink;

	@FindBy(xpath = "//*[text()='Costing Revenue Model']")
	private WebElement costingRevenueModelLink;

	@FindBy(xpath = "//*[text()='Scheduled Vehicle Costing']")
	private WebElement scheduledVehicleCostingLink;

	@FindBy(xpath = "//*[text()='Tripsheet Executive']")
	private WebElement tripSheetExecutiveLink;

	public WebElement clickOnTripSheetExecutive() {
		return tripSheetExecutiveLink;

	}

	@FindBy(xpath = "//div[@class='floating-button-icon-div']")
	private WebElement floatingicon;

	public WebElement _FloatingIcon() {
		return floatingicon;
	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws IOException
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	@FindBy(xpath = "//li[text()='OPM']")
	private WebElement opmFqlc;

	public void clickOpmFqlc(WebDriver driver) throws Exception {
		// WebActionUtil.scrollIntoViewAndClick(driver, opmFqlc);
		System.out.println("<<<<<<<<<<before Clicking on OPM link>>>>>>>>>>");
		WebActionUtil.clickElement(driver.findElement(By.xpath("//*[text()='OPM']")), driver, "OPM link");
		System.out.println("<<<<<<<<<<Clicked on OPM link here>>>>>>>>>>");

	}

	public void oPM(WebDriver driver) throws IOException, Exception {
		WebActionUtil.waitForElement(driver.findElement(By.xpath("//*[text()='OPM']")), driver, "OPM link", 1000);
		WebActionUtil.actionClick(driver.findElement(By.xpath("//*[text()='OPM']")), driver, "OPM link");
		Thread.sleep(1000);
	}

	public void driverVerificationlink(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		WebActionUtil.clickElement(driverVerificationLink, driver, "click on driver verification");
		Thread.sleep(3000);
	}

	public void verifyUserLogin(WebDriver driver) throws IOException {
		WebActionUtil.assertElementDisplayed(this.logoutBtn, driver, "User logged into FO-app");
	}

	public void clickLogout(WebDriver driver) throws IOException {
		WebActionUtil.actionClick(this.logoutBtn, driver, "logout Btn");
	}

	public void clickLogout1(WebDriver driver) throws IOException {
		WebActionUtil.scrollIntoViewAndClick(driver, logoutBtn);
	}

	public void fqlcSupervisor(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.fqlcSuperLink, driver, "FQLC Supervisor");
	}

	public void fqlc(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.fqlcLink, driver, "FQLC");
	}

	public void verifyDashBoard(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.verifyDashBoardLink, driver, "Verify Dashboard");
	}

	public void costingRevenueModel(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.costingRevenueModelLink, driver, "Verify Dashboard");
	}

	public void scheduledVehicleCosting(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.scheduledVehicleCostingLink, driver, "Verify Dashboard");
	}

	public void tripSheetExecutive(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(clickOnTripSheetExecutive(), driver, "tripSheetExecutive", 60);
		WebActionUtil.clickElement(clickOnTripSheetExecutive(), driver, "tripSheetExecutive");
	}

	public void navigateToVerifyDashBoardViaFQLC(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.hamburgerMenu, driver, "Left navigation Bar");
		WebActionUtil.clickElement(this.opmLink, driver, "Click on OPM");
		WebActionUtil.clickElement(this.fqlcLink, driver, "Click on FQLC");
		WebActionUtil.waitTillPageLoad("Click on FQLC", driver, "Home Page", 60);
		WebActionUtil.clickElement(this.hamburgerMenu, driver, "Left navigation Bar");
		WebActionUtil.clickElement(this.verifyDashBoardLink, driver, "Click on Verification DashBoard");
		WebActionUtil.waitTillPageLoad("Click on Verification DashBoard", driver, "Verification DashBoard Page", 60);
	}

	public void navigateToVerifyDashBoardViaFQLCSuper(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.hamburgerMenu, driver, "Left navigation Bar");
		WebActionUtil.clickElement(this.opmLink, driver, "Click on OPM");
		WebActionUtil.clickElement(this.fqlcSuperLink, driver, "Click on FQLC Supervisor");
		WebActionUtil.waitTillPageLoad("Click on FQLC", driver, "Home Page", 60);
		WebActionUtil.clickElement(this.hamburgerMenu, driver, "Left navigation Bar");
		WebActionUtil.clickElement(this.verifyDashBoardLink, driver, "Click on Verification DashBoard");
		WebActionUtil.waitTillPageLoad("Click on Verification DashBoard", driver, "Verification DashBoard Page", 60);
	}

	@FindBy(xpath = "//input[@type='email']")
	private WebElement loginUser;

	public WebElement enterLoginUser() {
		return loginUser;
	}

	@FindBy(xpath = "/html/body/div/div/div[3]/div[2]/div/div/div/div/div[2]/div[1]")
	private WebElement acc;

	public WebElement clkacc() {
		return acc;
	}

	@FindBy(xpath = "//input[@type='password']")
	private WebElement loginPass;

	public WebElement enterloginPass() {
		return loginPass;
	}

	@FindBy(xpath = "//div[@class='loader-backgroud-div']")
	private WebElement ldr;

	public WebElement bckldr() {
		return ldr;
	}

	public WebElement getAddVehicle(String client, String hub, String time, String phone) {
		return driver.findElement(By.xpath(
				"//*[text()='" + client + "']/..//*[@class='ent-od-div']/..//*[@class='loadboard-div']/..//*[text()='"
						+ hub + "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone
						+ "']/../../../..//*[text()='Add Vehicle Details']"));
	}

	@FindBy(xpath = "//*[text()='Verification Dashboard']")
	private WebElement verDash;

	public WebElement clkverDash() {
		return verDash;
	}

	@FindBy(xpath = "//*[@class='verification-tab-item ' and text()='Vehicle']")
	private WebElement vehTab;

	public WebElement clkVehTab() {
		return vehTab;
	}

	@FindBy(xpath = "//*[@class='verification-tab-item ' and text()='Created']")
	private WebElement CreatedTab;

	public WebElement ClkCreateTab() {
		return CreatedTab;
	}

	@FindBy(xpath = "//*[@class='verification-list']/div[@class='verification-card-data disabled'][1]")
	private WebElement fstVeh;

	public WebElement verifyfstVeh() {
		return fstVeh;
	}

	@FindBy(xpath = "//ul[@class='menu-panel visible-menu']/a/li[text()='Home']")
	private WebElement homeLink;

	public WebElement verifyhomelink() {
		return homeLink;
	}

	@FindBy(xpath = "//div[text()='Logsheets']")
	private WebElement logsheet;

	public WebElement _LogSheet() {
		return logsheet;
	}

	public WebElement getLBooking(String client, String hub, String time, String phone) {
		return driver.findElement(By.xpath(
				"//*[text()='" + client + "']/..//*[@class='ent-od-div']/..//*[@class='loadboard-div']/..//*[text()='"
						+ hub + "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone
						+ "']/../../../..//*[text()='Select ']"));
	}

	public WebElement getLVehicle(String client, String hub, String time, String phone) {
		return driver.findElement(By.xpath("//*[text()='" + client
				+ "']/..//*[@class='ent-od-div']/..//*[@class='loadboard-div']/..//*[text()='" + hub
				+ "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone + "']/../../..//div[3]"));
	}

	@FindBy(xpath = "//div[@id='mainContainer']/descendant::div[text()='Select Hub']")
	private WebElement dropdown;

	@FindBy(xpath = "//*[@class='dropdown-popup-div']")
	private WebElement popUp;
	@FindBy(xpath = "//*[@class='Select-menu-outer']")
	private WebElement scheduleUpDropdown;

	public WebElement _scheduleUpDropdown() {
		return scheduleUpDropdown;
	}

	@FindBy(xpath = "//li[text()='NM']")
	private WebElement nmList;

	public WebElement _NMText() {
		return nmList;
	}

	@FindBy(xpath = "//img[@class='bookmark-img'][1]")
	private WebElement RED;

	public void red() throws IOException, Exception {
		WebActionUtil.scrollIntoView(driver, RED);
		Thread.sleep(1000);
		String color = RED.getCssValue("color");
		String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");

		System.out.println(color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
	}

	@FindBy(xpath = "//li[text()='Busstop']")
	private WebElement busstpList;
	@FindBy(xpath = "//div[text()='Scheduled']")
	private WebElement scheduleList;
	@FindBy(xpath = "//div[text()='Enterprise OD']")
	private WebElement enterpriseOD;

	public WebElement _enterpriceOD() {
		return enterpriseOD;
	}

	public WebElement _dropdown() {
		return dropdown;
	}

	@FindBy(xpath = "//div[@class='Select options-child has-value Select--single']")
	private WebElement scheduled;

	public WebElement _scheduled() {
		return scheduled;
	}

	@FindBy(xpath = "//span[text()='�']")
	private WebElement close;

	public WebElement _close() {
		return close;
	}

	@FindBy(xpath = "//div[@class='Select-menu-outer']")
	private WebElement menu;
	@FindBy(xpath = "//div[@id='react-select-6--option-0']")
	private WebElement scheduleoption;
	@FindBy(xpath = "//div[@id='react-select-6--option-1']")
	private WebElement EnterpriseOD;
	@FindBy(xpath = "//div[@class='options-header']/div[4]/descendant::div/div/span[text()='Scheduled']")
	private WebElement scheduledDropdown;

	public WebElement _scheduledDropDown() {
		return scheduledDropdown;
	}

	@FindBy(xpath = "//div[@class='booking-incomplete-div'][1]/../div[7]/div/div[3]")
	private WebElement vecNo;
	@FindBy(xpath = "//div[@class='booking-incomplete-div'][1]")
	private WebElement redMark;
	@FindBy(xpath = "//div[@class='genric_dropdown valid-dropdown']/span[text()='Select ']")
	private List<WebElement> goingTextBox;
	@FindBy(xpath = "//div[@class='genric_dropdown valid-dropdown']/span[text()='Select ']")
	private WebElement goingTextBox1;
	@FindBy(xpath = "//div[@class='dropdown-elements-div']/div[3]/h3[text()='(P) Going']")
	private List<WebElement> going;
	@FindBy(xpath = "//img[@class='bookmark-img']/../../div[7]/div/div[3]")
	private List<WebElement> redmark1;
	@FindBy(xpath = "//div[@class='dropdownlist attendance-dropdown-custom-style'][1]")
	private WebElement dopdownValue;
	@FindBy(xpath = "//*[@class='bookmark-img']//../..//h3[text()='(P) Going']")
	private WebElement goingg;

	@FindBy(xpath = "//div[@class='Select-value']")
	private WebElement areaTxtField;
	@FindBy(xpath = "//div[@class='Select-menu-outer']")
	private WebElement allMenus;
	@FindBy(xpath = "//div[text()='Sarjapur']")
	private WebElement menuesText;

	public void hambergerMenu(WebDriver driver) throws Exception {
		WebActionUtil.actionClick(hamburgerMenu, driver, "click on hamberger menu");
	}

	public void leftNavigationalBar(WebDriver driver) throws IOException, InterruptedException {
		Thread.sleep(9000);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000000, driver);
		WebActionUtil.clickElement(this.hamburgerMenu, driver, "Left navigation Bar");
	}

	public void LoadBoard(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.loadboardLink, driver, "Loadboard");
	}

	public void _clickOnHomeLink(WebDriver driver) throws Exception {
		WebActionUtil.clickElement(verifyhomelink(), driver, "home");

	}

	public void _clickOnfloatingIcon(WebDriver driver) throws Exception {
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 50000, driver);
		WebActionUtil.clickElement(_FloatingIcon(), driver, "floatingicon");
	}

	public void _clickOnLogSheet(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(_LogSheet(), driver, "logsheet", 60);
		WebActionUtil.clickElement(_LogSheet(), driver, "logsheet");
	}

	public void _clickOnDropDown(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(_dropdown(), driver, "dropdown", 60);
		WebActionUtil.actionClick(_dropdown(), driver, "dropdown");

	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws Exception
	 * 
	 * 
	 */

	public void _login(WebDriver driver, String email, String pass) throws Exception {

		Thread.sleep(3000);
		WebActionUtil.clickElement(SignIn, driver, "Google Sign in");

		String parentWinHandle = driver.getWindowHandle();
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			System.out.println(handle);
			if (!handle.equals(parentWinHandle)) {
				driver.switchTo().window(handle);
				break;
			}
		}
		WebActionUtil.clearAndTypeEnter(loginUser, email, "Username", driver);
		Thread.sleep(5000);
		WebActionUtil.clearAndTypeEnter(loginPass, pass, "Password", driver);
		Thread.sleep(5000);

		driver.switchTo().window(parentWinHandle);
		Thread.sleep(9000);
	}

	
	public void labour_login(WebDriver driver) throws Exception {

		Thread.sleep(3000);
		WebActionUtil.clickElement(SignIn, driver, "Google Sign in");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 3000, driver);
		WebActionUtil.clickElement(loginid, driver, "Login to labour");
		
	}


	public void _dropdownInLogSheetPage() throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 3000, driver);
		_clickOnHomeLink(driver);
		_clickOnfloatingIcon(driver);
		_clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 100);
		_clickOnDropDown(driver);
		WebActionUtil.clickElement(this.nmList, driver, "NM");
		WebActionUtil.actionClick(_scheduledDropDown(), driver, "scheduleddropdown");

	}

	public void verify_dropdownInLogSheetPage() throws Exception {
		_dropdownInLogSheetPage();
		String dropdownoption1 = scheduleoption.getText();
		String dropdownoption2 = EnterpriseOD.getText();
		WebActionUtil.verifyContainsText(dropdownoption1, GenericLib.dropdownname1);
		WebActionUtil.verifyContainsText(dropdownoption2, GenericLib.dropdownname2);
	}

	@FindBy(xpath = "//span[text()='Select ']/../../../../preceding-sibling::div/img[@class='bookmark-img']/../following-sibling::div[6]/descendant::div/div[3]")
	private List<WebElement> vecNoOPM;
	@FindBy(xpath = "//div[@class='sc-gGBfsJ ezOsTr']/div[@class='sc-dNLxif eRlxVU']/input")
	private WebElement vecNumberTextField;

	// div[text()='OD89OD8954']/following-sibling::div[2][text()='verified']
	public WebElement getVechileNumberOPM(WebDriver driver, String vecno) {
		return driver
				.findElement(By.xpath("//div[text()=\'" + vecno + "\']/following-sibling::div[2][text()='verified']"));
	}

	/*
	 * @FindBy(
	 * xpath="//div[text()=' Created By ']/child::div/div[@class='table-filter-icon']"
	 * ) private WebElement createdFilterIcon;
	 * 
	 * @FindBy(
	 * xpath="//div[text()='Select All']/../div[@class='selected filter-check-box']"
	 * ) private WebElement checkBox;
	 * 
	 * @FindBy(xpath=
	 * "//div[text()='lonee.kashyap']/../div[@class='filter-check-box']")
	 * private WebElement filterCheckBox;
	 * 
	 * @FindBy(xpath="//div[@class='list-row']/div[4]") private List<WebElement>
	 * verifiedVechile; public WebElement getverifiedVechileOPM(WebDriver
	 * driver,String vecNo){ return
	 * driver.findElement(By.xpath("//span[text()=\'" +vecNo +
	 * "\']/../../../preceding-sibling::div[2]/div/div/div/span[text()='Select ']"
	 * )); }
	 * 
	 * @FindBy(xpath="//div[text()='Load More']") private WebElement loadMore;
	 * 
	 * @FindBy(xpath="//div[text()=' State ']") private WebElement state;
	 */
	@FindBy(xpath = "//div[@class='sc-jqCOkK lRIXQ']/input")
	private WebElement inputSearch;
	@FindBy(xpath = "//img[@class='bookmark-img']/../following-sibling::div[6]/div/child::div[3]")
	private List<WebElement> vechileNoOPM;

	public WebElement getVechileStatus(WebDriver driver, String vechileNumber) {
		return driver.findElement(
				By.xpath("//div[text()=\'" + vechileNumber + "\']/following-sibling::div[3][text()='discarded']"));
	}

	// span[text()='KA22DB1111']/../../../../div/following-sibling::div[4]/div/div/div/span[text()='Select
	// ']
	public WebElement getVechileStatusverified(WebDriver driver, String vechileNumber) {
		return driver.findElement(By.xpath("//div[text()=\'" + vechileNumber + "\']/following-sibling::div[3]"));
	}

	public WebElement getattendenceOfVerifiedVechile(WebDriver driver, String vechileNumber) {
		return driver.findElement(By.xpath("//span[text()=\'" + vechileNumber
				+ "\']/../../../../div/following-sibling::div[4]/div/div/div/span[text()='Select ']"));
	}

	// span[text()='KA34XC1111']/../../../../child::div[5]/descendant::div[8]/following-sibling::div/div[3]/h3[text()='(P)
	// Going']
	public WebElement getgoing(WebDriver driver, String vechileNumber) {
		return driver.findElement(By.xpath("//span[text()=\'" + vechileNumber
				+ "\']/../../../../child::div[5]/descendant::div[8]/following-sibling::div/div[3]/h3[text()='(P) Going']"));
	}

	public String _getvecNo1() throws Exception {
		String vecNo = "";
		int allVechile = vechileNoOPM.size();
		for (int i = 0; i < allVechile; i++) {

			WebActionUtil.scrollIntoView(driver, vechileNoOPM.get(i));
			vecNo = vechileNoOPM.get(i).getText();
			System.out.println(vecNo);
			InitializePages wdInit = new InitializePages(driver);
			WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
			Thread.sleep(3000);
			wdInit.o_HomePage.driverVerificationlink(driver);
			Thread.sleep(1000);
			wdInit.o_DriverVerificationPage.verifiedLink(driver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
			WebActionUtil.clickElement(inputSearch, driver, "input");
			WebActionUtil.clearAndTypeEnter(inputSearch, vecNo, "vechileNoSearchField", driver);

			Thread.sleep(9000);

			if (getVechileStatusverified(driver, vecNo).getText().equalsIgnoreCase("verified")) {
				Thread.sleep(3000);
				wdInit.o_HomePage.leftNavigationalBar(driver);
				wdInit.o_HomePage._clickOnHomeLink(driver);
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
				wdInit.o_HomePage.clickOnAllList(driver);
				// WebActionUtil.scrollIntoView(driver, vechileNoOPM.get(i));
				Thread.sleep(3000);
				WebActionUtil.scrollIntoViewAndClick(driver, getattendenceOfVerifiedVechile(driver, vecNo));

				Thread.sleep(3000);
				WebActionUtil.clickElement(getgoing(driver, vecNo), driver, "Going");

				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
				Thread.sleep(2000);

				break;
			} else {
				Thread.sleep(2000);
				wdInit.o_HomePage.leftNavigationalBar(driver);
				wdInit.o_HomePage._clickOnHomeLink(driver);
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
				wdInit.o_HomePage.clickOnAllList(driver);
				Thread.sleep(2000);
				continue;
			}

		}

		return vecNo;
	}

	public String _getVecNoFo() throws Exception {
		Thread.sleep(4000);
		String vecNoFO = "";
		for (WebElement we : redmark1) {
			vecNoFO = vecNo.getText();
			if (we.isDisplayed()) {
				for (WebElement we2 : goingTextBox) {
					WebActionUtil.actionClick(we2, driver, "TextBox");
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,250)");
					Thread.sleep(1000);
					WebActionUtil.actionClick(goingg, driver, "Going");
					Thread.sleep(5000);

					break;
				}
				break;
			}
			break;
		}
		return vecNoFO;
	}

	public void selectArea1(WebDriver driver, String filepath, String sheet, String testcase) throws Exception {

		Thread.sleep(1000);
		WebActionUtil.actionClick(this.areaDropdown, driver, "Area");
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		Thread.sleep(1000);
		WebActionUtil.clickElement(this.getArea(driver, sData[GenericLib.areaSelect]), driver, "Area");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
	}

	@FindBy(xpath = "//span[@class='Select-arrow-zone']")
	private WebElement arrowOpm;

	public void clickArrowOPM(WebDriver driver) throws Exception {
		WebActionUtil.scrollIntoViewAndClick(driver, arrowOpm);
	}

	@FindBy(xpath = "//div[@id='react-select-2--value']/div[2]/input")
	private WebElement area;

	public void selectAreainOPM() throws Exception {
		WebActionUtil.actionClick(area, driver, "Area");
		Thread.sleep(2000);
	}

	public String _getVecNoBeforeGoing() throws Exception {
		Thread.sleep(4000);
		String vecNoFO = "";
		for (WebElement we : redmark1) {
			vecNoFO = vecNo.getText();
			System.out.println(vecNoFO);
		}
		return vecNoFO;
	}

	// }

	/*
	 * public void leftNavigationalBar(WebDriver driver) throws IOException,
	 * InterruptedException { Thread.sleep(10000);
	 * WebActionUtil.waitForinvisiblityofElement(
	 * "//div[@class='loader-backgroud-div']", 500, driver);
	 * WebActionUtil.clickElement(this.hamburgerMenu, driver,
	 * "Left navigation Bar"); }
	 */

	public void leftNavigationalBar1(WebDriver driver) throws IOException, InterruptedException {
		Thread.sleep(10000);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 500, driver);
		WebActionUtil.scrollIntoViewAndClick(driver, hamburgerMenu);
	}

	/*
	 * public void LoadBoard(WebDriver driver) throws IOException {
	 * WebActionUtil.clickElement(this.loadboardLink, driver, "Loadboard"); }
	 */

	@FindBy(xpath = "//*[text()='Ops']")
	private WebElement opsLink;

	@FindBy(xpath = "//*[text()='View Areas']")
	private WebElement viewAreas;

	@FindBy(xpath = "//*[text()='Tripsheet Executive']")
	private WebElement tripSheetExecutive;

	@FindBy(xpath = "//*[text()='Driver Verification']")
	private WebElement driverVerification;

	@FindBy(xpath = "//*[text()='Payments']")
	private WebElement payments;

	@FindBy(xpath = "//*[text()='Detailed Vehicle Payments']")
	private WebElement detailedVehiclePayments;

	@FindBy(xpath = "//*[text()='Process Vehicle Payments']")
	private WebElement processVehiclePayments;

	@FindBy(xpath = "//*[text()='Finance']")
	private WebElement finance;

	@FindBy(xpath = "//*[text()='Field Ops']")
	private WebElement fieldOps;

	@FindBy(xpath = "//*[text()='Customer Relations']")
	private WebElement custRelations;

	@FindBy(xpath = "//*[text()='Supply']")
	private WebElement supply;

	@FindBy(xpath = "//li[text()='Driver Extra Expenses']")
	private WebElement driverExpenses;

	@FindBy(xpath = "//li[text()='Pending Hubs']")
	private WebElement pendingHub;

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws IOException
	 * 
	 * @throws Exception
	 * 
	 * 
	 */

	public void ops(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.opsLink, driver, "Ops");
	}

	public void pendingHubs(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.pendingHub, driver, "pending Hub");
		WebActionUtil.waitTillPageLoad("Veiw areas", driver, "Areas Page", 60);
	}

	public void viewAreas(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.viewAreas, driver, "Veiw area");
		WebActionUtil.waitTillPageLoad("Veiw areas", driver, "Areas Page", 60);

	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws Exception
	 * 
	 * 
	 */

	public void homeLink(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.homeLink, driver, "Home");
	}

	public void allList(WebDriver driver) throws IOException {
		WebActionUtil.waitTillPageLoad("All listing", driver, "Home Page", 120);
		WebActionUtil.clickElement(this.allList, driver, "Home");
		WebActionUtil.waitTillPageLoad("All listing", driver, "Home Page", 120);
	}

	public void selectArea(WebDriver driver, String filepath, String sheet, String testcase) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("select area", driver, "Home Page", 120);
		WebActionUtil.clickElement(this.areaDropdown, driver, "Area");
		WebActionUtil.clickElement(this.getArea(driver, sData[BaseLib.gv.arCount]), driver, "Mangalore");
	}

	public void verifyingBookingPublished(WebDriver driver, String filepath, String sheet, String testcase,
			String timeStamp, String vehicle) throws IOException, Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Mangalore area", driver, "Home Page", 120);
		WebActionUtil.scrollIntoView(driver, this.getBooking(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount],
				timeStamp, sData[BaseLib.gv.dcCount], vehicle));
		WebActionUtil.verifyElementIsDisplayed(this.getBooking(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount],
				timeStamp, sData[BaseLib.gv.dcCount], vehicle), driver, "Created Booking successfully");
		Thread.sleep(3000);
	}

	public WebElement getArea(WebDriver driver, String area) {
		return driver.findElement(By.xpath("//*[text()='" + area + "']"));
	}

	@FindBy(xpath = "//*[text()='Area']/..//*[@name='city-dd']/..//div[@class='Select-value']")
	private WebElement areaDropdown;

	@FindBy(xpath = "//div[@class='dropdown-elements-div']//h3[text()='(P) Going']")
	private WebElement goingOption;

	public WebElement getBooking(String client, String hub, String time, String phone, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/../..//*[text()='" + hub
				+ "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone + "']/../../..//*[text()='"
				+ vehicle + "']/../../../..//*[text()='Final']/..//span[text()='Select ']"));
	}

	public void searchBooking(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp,
			String vehicle) throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Mangalore area", driver, "Home Page", 120);
		WebActionUtil.scrollIntoView(driver, this.getBooking(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount],
				timeStamp, sData[BaseLib.gv.dcCount], vehicle));
	}

	public WebElement getGoingOption(String client, String hub, String time, String phone, String vehicle) {
		return driver.findElement(By.xpath("//*[text()='" + client + "']/../..//*[text()='" + hub
				+ "']/../..//*[text()='" + time + "']/../../..//*[text()='" + phone + "']/../../..//*[text()='"
				+ vehicle + "']/../../../..//*[text()='Final']/..//span[text()='Select ']/..//h3[text()='(P) Going']"));
	}

	public void selectGoing(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp,
			String vehicle) throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.getBooking(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount], timeStamp,
				sData[BaseLib.gv.dcCount], vehicle), driver, "Select dropdown");
		WebActionUtil.clickElement(this.getGoingOption(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hCount], timeStamp,
				sData[BaseLib.gv.dcCount], vehicle), driver, "Going");
		WebActionUtil.waitTillPageLoad("Select going", driver, "Home Page", 120);
	}

	/*
	 * public WebElement getSelectDropdown(String client, String hub, String
	 * time, String phone, String vehicle) { return
	 * driver.findElement(By.xpath("//*[text()='" + client +
	 * "']/../..//*[text()='" + hub + "']/../..//*[text()='" + time +
	 * "']/../../..//*[text()='" + phone + "']/../../..//*[text()='" + vehicle +
	 * "']/../../../..//*[text()='Final']/../div")); }
	 */
	public void tripsheetExecutive(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.tripSheetExecutive, driver, "TripSheet executive link");
	}

	public void driverVerification(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.driverVerification, driver, "Driver Verification");
	}

	public void payments(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.payments, driver, "payments");
	}

	public void detailedVehiclePayments(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.detailedVehiclePayments, driver, "Detailed Vehicle Payments");
	}

	public void driverExpenses(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.driverExpenses, driver, "Driver expenses");
	}

	public void processVehiclePayments(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.processVehiclePayments, driver, "Process Vehicle Payments");
	}

	public void financeLink(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.finance, driver, "Finance Role");
	}

	public void fieldOps(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.fieldOps, driver, "Field Ops");
		WebActionUtil.waitTillPageLoad("switch to FO", driver, "Home Page", 120);

	}

	public void supply(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		WebActionUtil.clickElement(this.supply, driver, "supply ");
		Thread.sleep(3000);
	}

	public void cR(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		WebActionUtil.clickElement(this.custRelations, driver, "customer Relations ");
		WebActionUtil.waitTillPageLoad("switch to FO", driver, "Home Page", 120);
	}

	public void searchLBooking(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.waitTillPageLoad("Mangalore area", driver, "Home Page", 120);
		WebActionUtil.scrollIntoView(driver, this.getLBooking(sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount],
				timeStamp, sData[BaseLib.gv.ldcCount]));
		vehicleNum = WebActionUtil.gettext(this.getLVehicle(sData[BaseLib.gv.cCount], sData[BaseLib.gv.hcount],
				timeStamp, sData[BaseLib.gv.ldcCount]), driver, "vehicle number");
	}

	public void selectLGoing(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp)
			throws IOException {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.getLBooking(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hcount], timeStamp,
				sData[BaseLib.gv.ldcCount]), driver, "Select dropdown");
		WebActionUtil.clickElement(this.getLGoingOption(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hcount], timeStamp,
				sData[BaseLib.gv.ldcCount]), driver, "Going");
		WebActionUtil.waitTillPageLoad("Select going", driver, "Home Page", 120);
	}

	public void addVehicleDetails(WebDriver driver, String filepath, String sheet, String testcase, String timeStamp)
			throws Exception {
		String[] sData = GenericLib.toReadExcelData(filepath, sheet, testcase);
		WebActionUtil.clickElement(this.getAddVehicle(sData[BaseLib.gv.cnCount], sData[BaseLib.gv.hcount], timeStamp,
				sData[BaseLib.gv.ldcCount]), driver, "Select dropdown");
		WebActionUtil.type(this.enterVehicleNumber, vehicleNum, "vehicle number", driver);
		WebActionUtil.clickElement(this.nextBtn, driver, "next button");
		WebActionUtil.clickElement(this.doneBtn, driver, "Done button");
		// WebActionUtil.clickElement(this.selectVendor, driver, "select
		// vendor");
		// WebActionUtil.getInnerHtml(this.selectVendorAgain, driver);
		WebActionUtil.waitTillPageLoad("Next Button", driver, "Home Page", 1200);
	}

	public void _clickOn_Exportfile(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(clkexportfile(), driver, "logsheet", 20);
		WebActionUtil.clickElement(clkexportfile(), driver, "logsheet");
	}

	@FindBy(xpath = "//div[text()='Export Files']")
	private WebElement exportfile;

	public WebElement clkexportfile() {
		return exportfile;
	}

	@FindBy(xpath = "//img[@src='/shared/img/files-icon.png']")
	private WebElement filesIcon;

	public WebElement clkfilesIcon() {
		return filesIcon;
	}

	@FindBy(xpath = "//*[@class='file-checkbox']//*[text()='Booking Report']")
	private WebElement bookingReportChkbox;

	public WebElement clkbookingReportChkbox() {
		return bookingReportChkbox;
	}

	@FindBy(xpath = "//*[@class='file-checkbox']//*[text()='Ent OD Report']")
	private WebElement EntODReportChkbox;

	public WebElement clkEntODReport() {
		return EntODReportChkbox;
	}

	@FindBy(xpath = "//*[@class='file-checkbox']//*[text()='Misc Order Report']")
	private WebElement MiscOrderReportChkbox;

	public WebElement clkMiscOrderReportChkbox() {
		return MiscOrderReportChkbox;
	}

	@FindBy(xpath = "//*[@class='footer']")
	private WebElement footerExport;

	public WebElement clkfooterExport() {
		return footerExport;
	}

	@FindBy(xpath = "//*[text()='Miscellaneous Order']")
	private WebElement miscOrder;

	public WebElement clkmiscOrder() {
		return miscOrder;
	}

	public void exportMiscFile(WebDriver driver) throws IOException, InterruptedException {

		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);
		WebActionUtil.clickElement(filesIcon, driver, "File icon");
		WebActionUtil.clickElement(bookingReportChkbox, driver, "bookingReportChkbox");
		WebActionUtil.clickElement(EntODReportChkbox, driver, "EntODReportChkbox");
		WebActionUtil.clickElement(MiscOrderReportChkbox, driver, "MiscOrderReportChkbox");
		WebActionUtil.clickElement(footerExport, driver, "Export");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 1000, driver);

	}

	@FindBy(xpath = "//*[text()='EntOdArranged']")
	private WebElement entOdArranged;

	public void floatingBtn(WebDriver driver) throws IOException {
		WebActionUtil.waitTillPageLoad("payment requested", driver, "Entod page", 90);
		WebActionUtil.clickElement(this.floatingicon, driver, "Floating icon");
		WebActionUtil.waitTillPageLoad("payment requested", driver, "Entod page", 90);
	}

	public void entOdArrnged(WebDriver driver) throws IOException {
		WebActionUtil.clickElement(this.entOdArranged, driver, "entOdArranged");
	}

	public void clkMiscOrder(WebDriver driver) throws IOException {
		WebActionUtil.scrollIntoViewAndClick(driver, miscOrder);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 100, driver);
	}
	
	public void closeBrowser(WebDriver driver) throws Exception {
		driver.quit();
	}

}