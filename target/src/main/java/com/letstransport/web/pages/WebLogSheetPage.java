package com.letstransport.web.pages;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.inject.PrivateBinder;
import com.letstransport.init.InitializePages;
import com.letstransport.library.GenericLib;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class WebLogSheetPage {

	WebDriver driver;

	public WebLogSheetPage(WebDriver driver) {
		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	static String data = WebActionUtil.generateRandomString();

	@FindBy(xpath = "//div[@class='hamburger-menu']")
	private WebElement hamMenu;

	public WebElement clickOnHambergerMenu() {
		return hamMenu;
	}

	@FindBy(xpath = "//ul[@class='menu-panel visible-menu']/a[2]")
	private WebElement logsheetLink;

	public WebElement clickOnLogSheet() {
		return logsheetLink;
	}

	@FindBy(xpath = "//div[text()='Search Client...']/..//input")
	private WebElement searchTxtInputField;

	public WebElement clickOnSearchTxtField() {
		return searchTxtInputField;
	}

	@FindBy(xpath = "//*[text()='Add New']")
	private WebElement addNewLink;

	public WebElement clickOnAddNewLink() {
		return addNewLink;
	}

	@FindBy(xpath = "//div[@class='editable-parent']/input")
	private WebElement enterTextField;

	public WebElement clickOnEnterTextField() {
		return enterTextField;
	}

	@FindBy(xpath = "//*[text()='Save']")
	private WebElement saveBtn;

	public WebElement clickOnSaveButton() {
		return saveBtn;
	}

	@FindBy(xpath = "//div[@id='mainContainer']/descendant::div[@class='invoice-button'][1]")
	private WebElement invoiceBtn;

	public WebElement clickOnInvoiceBtn() {
		return invoiceBtn;
	}

	@FindBy(xpath = "//div[@class='data-grid']/descendant::div[1]/descendant::div[5]")
	private WebElement closingKms;
	@FindBy(xpath = "//div[@class='cell selected editing']/input[1]")
	private WebElement inputClosingKms;

	@FindBy(xpath = "//div[@class='edit-button'][1]")
	private WebElement getinfo;
	@FindBy(xpath = "//div[@id='content_calender']/descendant::div/span[@class='number-span  selected-block  ']")
	private WebElement calenderdate;

	public WebElement _calenderDate() {
		return calenderdate;
	}

	@FindBy(xpath = "//div[text()='Enter Info']")
	private WebElement enterinfo;
	@FindBy(xpath = "//div[text()='No Info Entered']/../../descendant::div[3]/div[2][@class='edit-button']")
	private WebElement enterinotext;

	public void _clickOnEnterinfo() throws Exception {
		WebActionUtil.waitForElement(enterinfo, driver, "EnterInfo", 4000);
		Thread.sleep(2000);
		if (enterinfo.isDisplayed()) {
			WebActionUtil.actionClick(enterinfo, driver, "EnterInfo");
		} 
	}

	@FindBy(xpath = "//div[text()='Select From Date']")
	private WebElement selectdate;

	public void _ClickselectDateFrom(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(selectdate, driver, "Calender", 20);
		WebActionUtil.actionClick(selectdate, driver, "Calender");
	}

	@FindBy(xpath = "//div[@class='Select-menu-outer']")
	private WebElement menu;
	@FindBy(xpath = "//div[@id='react-select-3--option-0']")
	private WebElement lonee;
	@FindBy(xpath = "//div[text()='Edit Info']")
	private WebElement editinfo;
	@FindBy(xpath = "//label[@class='verification-label verified']")
	private WebElement verification;

	/*
	 * @FindBy(xpath = "//div[text()='Enter Info']") private WebElement
	 * EnterInfo;
	 */
	@FindBy(xpath = "//div[text()='Enter Info']")
	private List<WebElement> EnterInfo;
	@FindBy(xpath = "//div[text()='Select To Date']")
	private WebElement selectDateTo;
	@FindBy(xpath = "//div[text()='Edit Info']")
	private List<WebElement> EditInfo;

	public void _ClickselectDateTo(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(selectDateTo, driver, "SelectDateTo", 20);
		WebActionUtil.actionClick(selectDateTo, driver, "SelectDateTo");
	}

	@FindBy(xpath = "//div[@class='cell selected read-only']")
	private WebElement selectedcell;

	@FindBy(xpath = "//label[text()='Gallery']")
	private WebElement gallery;

	public void _clickOnGallery() throws Exception {
		WebActionUtil.waitForElement(gallery, driver, "Gallery", 400);
		WebActionUtil.actionClick(gallery, driver, "Gallery");
	}

	@FindBy(xpath = "//div[@class='template-name']")
	private WebElement header;

	public void headerText(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(header, driver, "header", 50);
		// .WebActionUtil.gettext(header, driver, "header");
	}

	@FindBy(xpath = "//div[@class='react-viewer-close react-viewer-btn']")
	private WebElement closebtn;

	public void _clickOnCloseButton(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(closebtn, driver, "close", 20);
		WebActionUtil.actionClick(closebtn, driver, "close");
	}

	@FindBy(xpath = "//img[@class='thumbnail-image']")
	private WebElement longpressImg;

	public void _clickOnImage(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(longpressImg, driver, "image", 20);
		WebActionUtil.clickElement(longpressImg, driver, "longpress");
	}

	@FindBy(xpath = "//div[text()='Save Images']")
	private WebElement saveImgLink;

	public void _clickOnSaveImgLink(WebDriver driver) throws Exception {
		WebActionUtil.waitForElement(saveImgLink, driver, "SaveImage", 1000);
		WebActionUtil.actionClick(saveImgLink, driver, "saveimage");
	}

	@FindBy(xpath = "//div[@class='action-buttons']/div[@class='edit-button']")
	private List<WebElement> infolink;

	@FindBy(xpath = "//div[@class='options-footer']/div[text()='Fetch Logsheets']")
	private WebElement fetchLogSheet;

	@FindBy(xpath = "//label[text()='Gallery']")
	private WebElement gall;

	@FindBy(xpath = "//div[text()='No Info Entered']")
	private List<WebElement> noinfo;

	@FindBy(xpath = "//div[@class='action-buttons']/div[@class='delete-button']")
	private WebElement deleteBtn1;
	/*
	 * @FindBy(xpath =
	 * "//div[@class='action-buttons']/div[@class='edit-button'][1]") private
	 * WebElement enterinfo1;
	 */
	@FindBy(xpath = "//div[@class='logsheet-image-group']/div/div/div[1]")
	private WebElement dateText;
	@FindBy(xpath = "//div[@class='logsheet-image-group']/div/div/div[1]")
	private WebElement headerText;
	@FindBy(xpath = "//div[@class='action-buttons']/div[1][@class='delete-button']")
	private WebElement deleteButton;
	@FindBy(xpath = "//div[@class='Select-value']/span[text()='Scheduled']")
	private WebElement scheduledTextBox;
	@FindBy(xpath = "//div[text()='Enter Info']/preceding-sibling::div[1]")
	private WebElement deleteButton1;
	@FindBy(xpath = "//li[text()='Tripsheet Executive']")
	private WebElement tripshitExecutive;
	@FindBy(xpath = "//div[text()='Edit Info']/preceding-sibling::div[1]")
	private WebElement deleteButtoneditInfo;

	public void _clickOnTripsheetexecutive() throws Exception {
		WebActionUtil.waitForElement(tripshitExecutive, driver, "TripsheetExecutive", 50);
		WebActionUtil.clickElement(tripshitExecutive, driver, "click on tripsheetexecutive");
	}

	@FindBy(xpath = "//div[text()='Verify Logsheets']")
	private WebElement verifyLogsheet;

	public void _clickOnVerifyLogsheet() throws Exception {
		WebActionUtil.waitForElement(verifyLogsheet, driver, "click on verify logsheet", 30);
		WebActionUtil.clickElement(verifyLogsheet, driver, "LogSheet");
	}

	public void LogSheetLink() throws Exception {
		WebActionUtil.waitForElement(clickOnLogSheet(), driver, "logsheet", 20);
		WebActionUtil.clickElement(logsheetLink, driver, "logsheet");
		Thread.sleep(2000);
		WebActionUtil.waitForElement(clickOnSearchTxtField(), driver, "searchclient", 20);
		WebActionUtil.actionClickAndType(clickOnSearchTxtField(), driver, "searchclient", GenericLib.client);
		Thread.sleep(5000);
		WebActionUtil.pressEnterKey();
		Thread.sleep(12000);
		WebActionUtil.waitForElement(clickOnAddNewLink(), driver, "AddNew", 5000);
		WebActionUtil.clickElement(clickOnAddNewLink(), driver, "addNew");
		WebActionUtil.isEleDisplayed(clickOnEnterTextField(), driver, "Enter", 1000);
		WebActionUtil.waitForElement(clickOnEnterTextField(), driver, "Enter", 20);
		WebActionUtil.clearAndType(clickOnEnterTextField(), data, "Enter", driver);
		Thread.sleep(1000);
		WebActionUtil.waitForElement(clickOnSaveButton(), driver, "save", 20);
		WebActionUtil.clickElement(clickOnSaveButton(), driver, "save");
		Thread.sleep(2000);
		WebActionUtil.waitForElement(clickOnInvoiceBtn(), driver, "Invoice", 50);
		WebActionUtil.clickElement(clickOnInvoiceBtn(), driver, "Invoice");
		WebActionUtil.waitTillPageLoad(driver, 50);

	}

	public void _toCreateLogSheetTemplate(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		wdInit.o_HomePage.oPM(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnTripSheetExecutive(), driver,
				"click on TripsheetExecutive");
		Thread.sleep(5000);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "click on hamberger");
		Thread.sleep(5000);
		LogSheetLink();

	}
	@FindBy(xpath="//div[text()='Back']")
	private WebElement backBtn;
	@FindBy(xpath="//div[text()='Tripsheet Executive']")
	private WebElement tripsheetExecutive;
	@FindBy(xpath="//li[text()='OPM']")
	private WebElement opm;
	/*@FindBy(xpath = "//li[text()='" + name + "']")
	private WebElement templateText;*/
	private WebElement getLogsheetTemplateName(String name){
		return driver.findElement(By.xpath("//li[text()='" + name + "']"));
	}

	/*public void _clickOnTemplateText() throws Exception {
		WebActionUtil.waitForElement(templateText, driver, "TemplateText", 50);
		Thread.sleep(1000);
		WebActionUtil.actionClick(templateText, driver, "TemplateText");
		Thread.sleep(2000);
	}*/

	public void Verify_toCreateLogSheetTemplate(WebDriver driver) throws Exception {
		Thread.sleep(2000);
		InitializePages wdInit=new InitializePages(driver);
		_toCreateLogSheetTemplate(driver);
		String newCreatedTemplateText = header.getText();
		WebActionUtil.verifyContainsText(data, newCreatedTemplateText);
		Thread.sleep(9000);
		WebActionUtil.clickElement(backBtn, driver, "click on Back");
		Thread.sleep(6000);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "click on hamberger menu");
		Thread.sleep(5000);
		WebActionUtil.clickElement(tripsheetExecutive, driver, "click on tripsheetExecutive");
		Thread.sleep(6000);
		WebActionUtil.clickElement(opm, driver, "click on opm ");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		wdInit.o_HomePage.clickOnAllList(driver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		Thread.sleep(6000);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 1000);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		wdInit.o_WebLogSheetPage._clickOnEnterinfo();
		wdInit.o_WebTemplatePage._clickOnSelectTemplate();
		Thread.sleep(5000);
	    String templateName = getLogsheetTemplateName(newCreatedTemplateText).getText();
		WebActionUtil.verifyContainsEqualText(newCreatedTemplateText, templateName);
		Thread.sleep(2000);
		
	}

	public void _UploadPOD_ENTOD(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		_ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_clickOnGallery();
		Thread.sleep(1000);
		WebActionUtil.uploadFile(GenericLib.enterpath);
		WebActionUtil.longPress(longpressImg, driver, "longpress");
		_clickOnCloseButton(driver);
		Thread.sleep(3000);
		_clickOnSaveImgLink(driver);
		Thread.sleep(5000);
	}

	public void Verify_UploadPOD_ENTOD(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		_ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		Thread.sleep(1000);
		WebActionUtil.clickElement(fetchLogSheet, driver, "FetchLogsheet");
		Thread.sleep(5000);
		if (!EnterInfo.isEmpty()) {
			for (WebElement we : noinfo) {
				WebActionUtil.actionClick(deleteButton1, driver, "deleteButton");
				driver.switchTo().alert().accept();
				Thread.sleep(5000);
			}
			_clickOnGallery();
			WebActionUtil.uploadFile(GenericLib.enterpath);
			Thread.sleep(1000);
			WebActionUtil.longPress(longpressImg, driver, "longpress");
			_clickOnCloseButton(driver);
			Thread.sleep(3000);
			_clickOnSaveImgLink(driver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
			String enterinfo = enterinotext.getText();
			WebActionUtil.verifyContainsText(enterinfo, "Enter Info");

		} else {
			_clickOnGallery();
			WebActionUtil.waitTillPageLoad(driver, 100);
			WebActionUtil.uploadFile(GenericLib.enterpath);
			WebActionUtil.longPress(longpressImg, driver, "longpress");
			_clickOnCloseButton(driver);
			Thread.sleep(3000);
			_clickOnSaveImgLink(driver);
			Thread.sleep(20000);
			String enterinfo = enterinotext.getText();
			WebActionUtil.verifyContainsText(enterinfo, "Enter Info");

		}
	}

	@FindBy(xpath = "//a[text()='Back To Home']")
	private WebElement backToHome;

	public void Verify_ToEdit_LogSheetInfo(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 1000);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 1000);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		_ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		Thread.sleep(3000);
		for (WebElement we : infolink) {
			if (we.getText().equals("Edit Info")) {
				WebActionUtil.actionClick(we, driver, "EditInfo");
				WebActionUtil.waitForElement(closingKms, driver, "ClosingKMS", 1000);
				WebActionUtil.actionClick(closingKms, driver, "closingKMS");
				WebActionUtil.pressEnterKey();
				WebActionUtil.verifyElementIsDisplayed(selectedcell, driver, "we can read only");
			}
		}

	}
@FindBy(xpath="//div[@class='no-image-group-label']")
private WebElement noLogsheet;
	public void Verify_Remove_LogsheetInfo(WebDriver driver) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitTillPageLoad(driver, 20);
		Thread.sleep(2000);
		wdInit.o_HomePage._clickOnDropDown(driver);
		Thread.sleep(1000);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		_ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, driver);
		if (!"No Info Entered".equals(headerText.getText())) {
			WebActionUtil.actionClick(deleteButton, driver, "Delete");
			Thread.sleep(3000);
			driver.switchTo().alert().accept();
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 17000, driver);
            Thread.sleep(3000);
			//WebActionUtil.verifyElementIsDisplayed(headerText, driver, "date in which logsheet created");
           String noLogsheetText = noLogsheet.getText();
           Thread.sleep(1000);
           WebActionUtil.verifyContainsText(noLogsheetText, "No Logsheet Images Uploaded for this Hub.");
			  Thread.sleep(1000);
		}
	}
	public void Verify_logSheetByTripsheet(WebDriver driver, String entervecno) throws Exception {
		InitializePages wdInit = new InitializePages(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "hamberger");
		WebActionUtil.waitTillPageLoad(driver, 100);
		wdInit.o_HomePage._clickOnHomeLink(driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		wdInit.o_HomePage._clickOnLogSheet(driver);
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, " searchtextfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver, "searchtextfield",
				GenericLib.searchfieldtext);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "NM");
		wdInit.o_WebLogSheetPage._ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		wdInit.o_WebLogSheetPage._ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(wdInit.o_WebLogSheetPage._calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		if(!EditInfo.isEmpty()){
			for (WebElement we : EditInfo) {
				WebActionUtil.actionClick(deleteButtoneditInfo, driver, "deleteButton");
				driver.switchTo().alert().accept();
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
			}
		}
		Thread.sleep(1000);
		if (!EnterInfo.isEmpty()) {
			for (WebElement we : noinfo) {
				WebActionUtil.actionClick(deleteButton1, driver, "deleteButton");
				driver.switchTo().alert().accept();
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
			}
			_clickOnGallery();
			WebActionUtil.uploadFile(GenericLib.enterpath);
			WebActionUtil.longPress(longpressImg, driver, "longpress");
			_clickOnCloseButton(driver);
			Thread.sleep(3000);
			_clickOnSaveImgLink(driver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		} else {
			_clickOnGallery();
			WebActionUtil.waitTillPageLoad(driver, 100);
			WebActionUtil.uploadFile(GenericLib.enterpath);
			WebActionUtil.longPress(longpressImg, driver, "longpress");
			_clickOnCloseButton(driver);
			Thread.sleep(3000);
			_clickOnSaveImgLink(driver);
			WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		}
		wdInit.o_WebLogSheetPage._clickOnEnterinfo();
		wdInit.o_WebTemplatePage._clickOnSelectTemplate();
		Thread.sleep(1000);
		wdInit.o_WebTemplatePage._clickOnTemplateText();
		Thread.sleep(1000);
		wdInit.o_WebTemplatePage._clickAndEnterVehicleNo(entervecno);
		wdInit.o_WebTemplatePage._clickAndEnterDate();
		wdInit.o_WebTemplatePage._clickOnOpeningKms();
		wdInit.o_WebTemplatePage._clickOnClosingKms();
		wdInit.o_WebTemplatePage._clickOnOpeningTime();
		wdInit.o_WebTemplatePage._clickOnClosingTime();
		wdInit.o_WebTemplatePage._clickOnBooking();
		wdInit.o_WebTemplatePage._ClickOnFreezeButton();
		Thread.sleep(1000);
		wdInit.o_WebTemplatePage._clickOnSaveButton();
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 9000, driver);
		WebActionUtil.clickElement(backToHome, driver, "click on back to home");
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnhamburgerMenu(), driver, "click on hamberger");
		wdInit.o_HomePage.oPM(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage.clickOnTripSheetExecutive(), driver,
				"click on tripsheetExecutive");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		wdInit.o_HomePage._clickOnfloatingIcon(driver);
		_clickOnVerifyLogsheet();
		WebActionUtil.waitForElement(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				" wait for search textfield", 20);
		WebActionUtil.actionClickAndType(wdInit.o_WebLogSheetPage.clickOnSearchTxtField(), driver,
				"click on search textfield", GenericLib.searchfieldtext);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		WebActionUtil.clickElement(lonee, driver, "click on search client");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 5000, driver);
		wdInit.o_HomePage._clickOnDropDown(driver);
		WebActionUtil.clickElement(wdInit.o_HomePage._NMText(), driver, "select and click on NM");
		_ClickselectDateFrom(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		_ClickselectDateTo(driver);
		WebActionUtil.clickOnCurrentDate(_calenderDate(), "/", 0);
		WebActionUtil.waitForElement(fetchLogSheet, driver, "wait for fetch logsheet", 200);
		WebActionUtil.clickElement(fetchLogSheet, driver, "click on FetchLogsheet");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		if (editinfo.isDisplayed()) {
			WebActionUtil.actionClick(editinfo, driver, "click on editinfo");
			Thread.sleep(1000);
		}
		String ver = verification.getText();
		WebActionUtil.verifyContainsText(ver, "Verified");

	}

}