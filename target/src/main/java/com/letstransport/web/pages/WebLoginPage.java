package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.WebActionUtil;

//import ch.qos.logback.core.joran.action.ActionUtil;

public class WebLoginPage {

	// Webdriver driver instance creation
	WebDriver driver;

	public WebLoginPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	/** Login Page **/

	@FindBy(xpath = "//input[@type='email']")
	private WebElement usrName;

	public WebElement enterUsername() {
		return usrName;
	}

	@FindBy(xpath = "//input[@type='password']")
	private WebElement pswrd;

	public WebElement enterPass() {
		return pswrd;
	}

	@FindBy(xpath = "//span[text()='Create account']/../../../../../..//span[text()='Next']")
	private WebElement nxtAccBtn;
	
	@FindBy(xpath = "//span[text()='Forgot password?']/../../../../../..//span[text()='Next']")
	private WebElement nxtPassBtn;
	
	
	@FindBy(xpath = "//*[text()='KR']")
	private WebElement krBtn;

	@FindBy(xpath = "//*[text()='All List']")
	private WebElement allListBtn;
	
	@FindBy(xpath = "//*[local-name()='svg']//*[name()='g']")
	private WebElement logoutBtn;
	
	public WebElement ClickSignIn() {
		return nxtAccBtn;
	}

	@FindBy(xpath = "//span[text()='Login']")
	private WebElement googgleLoginBtn;
	
	@FindBy(xpath = "//div[text()='Use another account']")
	private WebElement anotherAccount;
	
	
	public WebElement selectAccount(WebDriver driver,String Account) throws IOException {
		return driver.findElement(By.xpath("//*[text()='"+Account+"']"));
	}

	/**
	 * 
	 * Author : Vandhitha Rao
	 * 
	 * Description : Login to FOWeb
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public void fo_Login(String Uname, String Pass, WebDriver driver) throws Exception {
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 60);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		Thread.sleep(1000);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		Thread.sleep(5000);
		try{
		//WebActionUtil.clickElement(this.nxtAccBtn, driver, "Next Button");
		WebActionUtil.clickByJavaSriptExecutor(this.nxtAccBtn, driver, "Next Button");
		}catch(Exception e){WebActionUtil.clickByJavaSriptExecutor(this.nxtAccBtn, driver, "Next Button");}
		// WebActionUtil.waitForElement(this.pswrd, driver, "Password input
		// box", 15);
		Thread.sleep(5000);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtPassBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(33000);
		WebActionUtil.clickElement(this.allListBtn, driver, "All list Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(10000);
	}
	
	public void quick_Fo_Login(String Uname, String Pass,String url, WebDriver driver) throws Exception {
		driver.get(url);
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 60);
		WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		WebActionUtil.clickByJavaSriptExecutor(this.anotherAccount, driver, "priya Button");
		WebActionUtil.waitForVisiblityofElement("//input[@type='email']", 90, driver);
		WebActionUtil.clearAndType(this.usrName, Uname, "UserName", driver);
		WebActionUtil.waitForVisiblityofElement("//span[text()='Create account']/../../../../../..//span[text()='Next']", 30, driver);
		try{
		WebActionUtil.clickByJavaSriptExecutor(this.nxtAccBtn, driver, "Next Button");
		}catch(Exception e){WebActionUtil.clickByJavaSriptExecutor(this.nxtAccBtn, driver, "Next Button");}
		WebActionUtil.waitForVisiblityofElement("//input[@type='password']", 90, driver);
		try {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		} catch (Exception e) {
			WebActionUtil.clearAndType(this.pswrd, Pass, "Password", driver);
		}
		WebActionUtil.clickByJavaSriptExecutor(this.nxtPassBtn, driver, "Next Button");
		WebActionUtil.switchToWindow(driver, 1);
		Thread.sleep(33000);
		WebActionUtil.clickElement(this.allListBtn, driver, "All list Button");
		WebActionUtil.waitForVisiblityofElement("//*[local-name()='svg']//*[name()='g']", 600, driver);
	}
	
	public void ReLogin(WebDriver driver,String Uname)throws Exception{
		WebActionUtil.waitTillPageLoad("Google button", driver, "Login page", 30);
		try{WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");} 
		catch(Exception e){WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");}
		//WebActionUtil.clickElement(this.googgleLoginBtn, driver, "Google Login Button");
		WebActionUtil.switchToWindow(driver, 2);
		Thread.sleep(2000);
		try{
		WebActionUtil.clickElement(this.selectAccount(driver, Uname.toLowerCase()), driver, "FO login Account");
		}
		catch(Exception e){
			WebActionUtil.clickElement(this.selectAccount(driver, Uname.toLowerCase()), driver, "FO login Account");	
		}
		Thread.sleep(1000);
		try{
			WebActionUtil.switchToWindow(driver, 1);
		}catch (Exception e){
			WebActionUtil.switchToWindow(driver, 1);
		}
		Thread.sleep(33000);
		WebActionUtil.clickElement(this.allListBtn, driver, "All list Button");
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 12000, driver);
		Thread.sleep(10000);
	}
}