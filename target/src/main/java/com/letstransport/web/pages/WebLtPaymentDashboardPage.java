package com.letstransport.web.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.letstransport.util.WebActionUtil;

public class WebLtPaymentDashboardPage {
	WebDriver driver;

	public WebLtPaymentDashboardPage(WebDriver driver) {

		// Allocate global driver reference to local driver
		this.driver = driver;
		// Initialize Ajax page initialisation
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@class='username-box']/input")
	private WebElement userInputBox;

	@FindBy(xpath = "//*[@class='password-box']/input")
	private WebElement passInputBox;

	@FindBy(xpath = "//*[@class='sign-in-button']")
	private WebElement signInBtn;

	public void pricingPage_Login(String Uname, String Pass, WebDriver driver) throws Exception {
		WebActionUtil.waitTillPaymentPageLoad("Add Costing", driver, "Login page", 120);
		WebActionUtil.type(this.userInputBox, Uname, "UserName", driver);
		WebActionUtil.type(this.passInputBox, Pass, "Password", driver);
		WebActionUtil.clickElement(this.signInBtn, driver, "Next Button");
		WebActionUtil.waitTillPaymentPageLoad("Sign In button", driver, "pricing page", 120);
	}
		

}
