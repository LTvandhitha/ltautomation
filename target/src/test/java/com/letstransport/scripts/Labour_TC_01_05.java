package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class Labour_TC_01_05 extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To validate user should be able to navigate to Add new helper page & be able to add all labour types")

	public void Labour_TC_01_05() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("Labour_TC_01_05");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE :  To validate user should be able to navigate to Add new helper page & be able to add all labour types", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		//<<<<<<<<<<<<To navigate to labour module>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_labour.navigateLabourModule(gv.wDriver);
		
		wdInit.o_HomePage.labour_login(gv.wDriver);
		
		wdInit.o_labour.verify_addhelperpage(gv.wDriver);
		
		
		//<<<<<<<<<<<<< to Validate adding all labour types>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_labour.validate_addvendorhelper(gv.wDriver,"Vendor");
		wdInit.o_labour.addanotherHelper(gv.wDriver);
		wdInit.o_labour.validate_addmanagedhelper(gv.wDriver,"Managed Helper");
		wdInit.o_labour.addanotherHelper(gv.wDriver);
		wdInit.o_labour.validate_addadhochelper(gv.wDriver,"Adhoc Helper");
		wdInit.o_labour.addanotherHelper(gv.wDriver);
		wdInit.o_labour.validate_addadhochelper(gv.wDriver,"Direct");
	}
	
	
	
	
		@AfterMethod
	public void reset() {
		gv.wDriver.close();
		

	}

}
