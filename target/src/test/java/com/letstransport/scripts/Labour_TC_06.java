package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class Labour_TC_06 extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To validate user should not be able to add new labour with existing mobile number")

	public void Labour_TC_06() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("Labour_TC_06");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE :  To validate user should not be able to add new labour with existing mobile number", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		//<<<<<<<<<<<<To navigate to labour module>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_labour.navigateLabourModule(gv.wDriver);
		
		wdInit.o_HomePage.labour_login(gv.wDriver);
		
		wdInit.o_labour.verify_addhelperpage(gv.wDriver);
		
		
		//<<<<<<<<<<<<< to Validate adding all labour types>>>>>>>>>>>>>>>>>>>>>>
		
		
		wdInit.o_labour.validate_duplicateaddvendorhelper(gv.wDriver,"Managed Helper");
	
		
	}
	
	
	
	
		@AfterMethod
	public void reset() {
		gv.wDriver.close();
		

	}

}
