package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_001__002_DriverHelpline extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To verify  driverhelp line menu ")

	public void TC_001__002_DriverHelpline() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_001_DriverHelpline");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : To verify driverhelp line menu", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		wdInit.o_HomePage._login( gv.wDriver,gv.sPortal_Username, gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
	
		//<<<<<<<<<<<<Click on DriverHelpLine menu in FO portal & verify if homepage is displayed>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_DriverHelpline.scrollclickhelpLineMenu(gv.wDriver);
		wdInit.o_DriverHelpline.driverhelpmenu(gv.wDriver);
		
	
	}
	
		@AfterMethod
	public void reset() {
		gv.wDriver.close();
		

	}

}
