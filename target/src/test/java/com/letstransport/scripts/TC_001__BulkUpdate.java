package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_001__BulkUpdate extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : To verify  if user is able to download csv & to verify its content")

	public void TC_001__BulkUpdate() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_001__BulkUpdate");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE :  To verify  if user is able to  download csv & to verify its content", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>
			

		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		
		
		//<<<<<<<<<<<<To verify bulk booking upload>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		wdInit.o_DriverHelpline.downOPMclk(gv.wDriver);
		wdInit.o_BulkUpdate.CustomerRelations(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_BulkUpdate.miscOrders(gv.wDriver);
		wdInit.o_BulkUpdate.bulk_upload(gv.wDriver,GenericLib.client);
		
		
		
		
		//download and verify the csv contents
		
		wdInit.o_BulkUpdate.downloadFile_bulk(GenericLib.filelocation);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 0, GenericLib.ValidateDate);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 1, GenericLib.ValidateticNum);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 2, GenericLib.ValidateHQName);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 3,GenericLib.ValidateVehNum);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 4, GenericLib.ValidateVehType);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 5,GenericLib.ValidateAmount);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 6, GenericLib.ValidateName);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 7,GenericLib.ValidateAccNum);
		WebActionUtil.verifyCsvData(GenericLib.filelocation, 0, 8, GenericLib.ValidateIFSC);
		
	}
	
	
	
	
		@AfterMethod
	public void reset() {
//		gv.wDriver.close();
		

	}

}
