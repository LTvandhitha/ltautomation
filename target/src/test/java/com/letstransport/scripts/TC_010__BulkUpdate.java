package com.letstransport.scripts;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

/* this script is dependent on TC_09_BulkUpdate*/

public class TC_010__BulkUpdate extends BaseLib implements TestDataCoulmns {
	
	@Test(enabled = true, priority = 1, description = " Test case :To validate Miscellaneous Bookings tab should be available under hambuger menu  ")
	public void TC_010__BulkUpdate() throws Exception {
		
		
		// assign category
		MyExtentListners.test.assignCategory("TC_010__BulkUpdate");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : To validate Miscellaneous Bookings tab should be available under hambuger menu ", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		
		
		//<<<<<<<<<<<<Navigate to hub page in FO portal>>>>>>>>>>>>>>>>>>>>>>>>>>>

		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);		
		
		wdInit.o_HomePage.clkMiscOrder(gv.wDriver);
		
		String SysDate= WebActionUtil.fetchcurrentSystemDateTime();
		String day=wdInit.o_BulkUpdate.returnday(SysDate);
		String withoutzeroday = wdInit.o_BulkUpdate.removeZero(day);
		
		
			
		wdInit.o_BulkUpdate.paymentvisibilityMisc(gv.wDriver, withoutzeroday);

	}
		
		
					
		
		@AfterMethod
	public void reset() {
//			gv.wDriver.close();
			
		

	}

}
