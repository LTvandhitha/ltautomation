
package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_08_09_UploadDelDocuments extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Upload Vehicle documents in partner app & verify if it can be deleted from portal ")

	public void TC_08_09_UploadDelDocuments() throws Throwable {
		// assign category
		MyExtentListners.test.assignCategory("TC_08_09_UploadDelDocuments");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : Upload Vehicle documents in partner app & verify if it can be deleted from portal", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		
		//<<<<<<<<<<<<Upload vehicle documents of a vehicle in Partner App>>>>>>>>>>>>>>>>>>>>
		MobileActionUtil.waitForinvisiblityofElement("//*[name()='svg']//*[name()='g']//*[name()='circle']", 100, gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		//		MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		//		MobileActionUtil.waitTillProgressBarLoad("Jobs around you ", gv.aDriver, "Listings", 300);
		
		//	    adInit.a_addveh._handleReferral(gv.aDriver);
		
//		Thread.sleep(50000);
	    
	    String vehnum=MobileActionUtil.generateRandomString(2,4);
	       
	    //Add a vehicle	    
		adInit.a_addveh._addVehicles(gv.aDriver,vehnum);

		//upload RC documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"RC");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
		//upload FC documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"FC");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
		
		//Upload Insurance documents
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"Insurance");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		adInit.a_addveh.goback(gv.aDriver);
		
		//scroll down 
		Thread.sleep(7000);
		WebElement ele = gv.aDriver.findElement(By.xpath("//*[contains(text(),'Back Image')]"));
		MobileActionUtil.scrollIntoViewAndClick(gv.aDriver, ele);
		
		
		//Upload back image 
		adInit.a_addveh._uploadDocs(gv.aDriver, vehnum,"Vehicle Back Image");
		gv.aDriver.context("NATIVE_APP");
		adInit.a_addveh._takephoto(gv.aDriver);
		//adInit.a_addveh._takeohotovivo(gv.aDriver);
		MobileActionUtil.switchToView(gv.aDriver);
		
		
		//verify scratch card earn
		
		//adInit.a_addveh.verify_ScratchCard(gv.aDriver);
		

		//<<<<<<<<<<<<Verify if vehicle documents uploaded is visible in FO portal >>>>>>>>>>>>>>>>>>>>>>>>>>>>

		wdInit.o_HomePage._login(gv.wDriver,gv.sPortal_Username,gv.sPortal_Password);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addarea._clickOPMFQLC(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_addveh.Navigate_VerifyUploadDocs(gv.wDriver);
		wdInit.o_addveh.verifyVehicledocs_upload(gv.wDriver, vehnum);	
		
		//verify if uploaded vehicle can be deleted
		
		wdInit.o_addveh.verify_uploadVehdel(gv.wDriver);
			
		
	}
	
	
		

	@AfterMethod
	public void reset() {
		gv.wDriver.close();

	}

}
