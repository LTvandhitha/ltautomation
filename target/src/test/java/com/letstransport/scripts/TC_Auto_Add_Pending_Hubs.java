package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Add_Pending_Hubs extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Onboard Client")

	public void TC_Auto_Add_Pending_Hubs() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Adding_Hubs_FOWebApp");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Adding_Hubs_FOWebApp", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_ClientOnboardingPage.COB_Sales_Login(gv.sSALE_USER, gv.sSALE_PASS,gv.sCLIENT_URL, gv.wDriver);
		wdInit.o_ClientOnboardingPage.addNewGroup(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,gv.randomName2);
		wdInit.o_ClientOnboardingPage.addNewEntity(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,gv.randomName2);
		wdInit.o_ClientOnboardingPage.addNewVertical(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding,gv.randomName2);
		wdInit.o_ClientOnboardingPage.addNewGSTNo(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewGSTDetails(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding);
		wdInit.o_ClientOnboardingPage.addNewHUB(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, gv.randomName2);
		wdInit.o_ClientOnboardingPage.addNewCommercial(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, gv.randomName2);
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
		
		wdInit.o_ClientOnboardingPage.COB_Finance_Login(gv.sFINANCE_USER, gv.sFINANCE_PASS,gv.sCLIENT_URL, gv.wDriver);
		wdInit.o_ClientOnboardingPage.Approve_Client(gv.wDriver,gv.randomName2);
		wdInit.o_ClientOnboardingPage.clickLogOut(gv.wDriver);
		
		wdInit.o_login.quick_Fo_Login(gv.sPortal_Username, gv.sPortal_Password,gv.sPortal_Url, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.ops(gv.wDriver);
		wdInit.o_HomePage.pendingHubs(gv.wDriver);
		wdInit.o_PendingHubsPage.addHubs(gv.wDriver,GenericLib.sPortalTestDataPath, Client_OnBoarding_sheet,TC_Client_OnBoarding, gv.randomName2);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
