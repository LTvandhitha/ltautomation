package com.letstransport.scripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.ExcelLibrary;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

import io.appium.java_client.AppiumDriver;

public class TC_Auto_Create_Listing_Partner_Apply extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Create listing and partner apply ")

	public void TC_001_listingCreation() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Creating_Listing & Partner_Apply");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Creating_Listing & Partner_Apply", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		//<<<<<<<<<<<<creating Listing from FO-App>>>>>>>>>>>>>>>>>>>>	
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.LoadBoard(gv.wDriver);
		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
		wdInit.o_LoadBoardPage.createListing(GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.refreshListing();
		WebActionUtil.waitTillPageLoad("Loading Lisitngs", gv.wDriver, "Listings page", 300);
		wdInit.o_LoadBoardPage.searchListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.verifyCreatedListing(GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		
		//<<<<<<<<<<<<Applying the above Listing from partner-App>>>>>>>>>>>>>>>>>>>>
		
		//adInit.a_login.signUp(GenericLib.sAppTestDataPath, App_Listing);
		Thread.sleep(5000);
		MobileActionUtil.switchToWebView(gv.aDriver);
		MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		adInit.a_homepage.applyListing(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);	
		adInit.a_homepage.clickApply(gv.aDriver);
		adInit.a_homepage.selectVehicle(gv.aDriver,1);
		adInit.a_homepage.clickOkAndWait();
		adInit.a_homepage.verifyAfterApply(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);
		wdInit.o_LoadBoardPage.deleteListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
		}
}
