package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Create_Shchedule_Booking extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : Create a Scheduled booking ")
	public void TC_0028_Scheduled_Booking_Create_Publish() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Auto_Create_Shchedule_Booking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Auto_Create_Shchedule_Booking", ExtentColor.CYAN));
		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.ops(gv.wDriver);
		wdInit.o_HomePage.viewAreas(gv.wDriver);
		wdInit.o_addarea.getArea(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01).click();
		wdInit.o_addarea.clickAddNewBooking(gv.wDriver);
		wdInit.o_AddBookingPage.createBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.portalTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_addarea.verifyBookingPage(gv.wDriver);
		wdInit.o_addarea.verifyBookingSuccess(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.createdTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_addarea.getPublishButton(gv.wDriver, GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.createdTimeStamp,gv.sVEHICLE_NUMBER).click();
		//wdInit.o_addarea.verifyPublishedPopup(gv.wDriver);
		wdInit.o_addarea.deleteBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.createdTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.homeLink(gv.wDriver);
		wdInit.o_HomePage.allList(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01);
		wdInit.o_HomePage.verifyingBookingPublished(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01,gv.displayedTimeStamp,gv.sVEHICLE_NUMBER);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
