package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;

import io.appium.java_client.android.Activity;

public class TC_Auto_FQLC_Reject_Partner_Doc extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = " Test case : reject Uploaded profile ")

	public void TC_002_reject_Docuements() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_FQLC_Reject_Partner_Doc");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_FQLC_Reject_Partner_Doc", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.navigateToVerifyDashBoardViaFQLC(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.rejectUploadDocuements(gv.wDriver, gv.randomMobileNumber,GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.rejectUploadDocuements(gv.wDriver,
		//"9792400059", GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		 
		wdInit.o_VerifyDashBoardPage.clickOnRejectedTab(gv.wDriver);
		wdInit.o_VerifyDashBoardPage.verifyRejectedDocuements(gv.wDriver, gv.randomMobileNumber,
				GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.verifyRejectedDocuements(gv.wDriver,
		//"9792400059", GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		Runtime.getRuntime().exec("adb shell am force-stop in.letstransport.vendor_app.test");
		Thread.sleep(2000);
		Runtime.getRuntime().exec(
				"adb shell am start -n in.letstransport.vendor_app.test/in.letstransport.vendor_app.activity.SplashScreen");
		Thread.sleep(8000);	
		MobileActionUtil.switchToWebView(gv.aDriver);
		adInit.a_homepage.clickOnProfile(gv.aDriver);
		adInit.a_AppProfilePage.verifyPersonalDocuementRejected();
		adInit.a_AppProfilePage.clickOnPersonalDocuements();
		adInit.a_AppProfilePage.verifyDriverLicencetRejected(gv.aDriver);
		adInit.a_homepage.clickOnProfile(gv.aDriver);
		adInit.a_AppProfilePage.verifyBankDocuementRejected();
		adInit.a_AppProfilePage.clickOnBankDocuements();
		adInit.a_AppProfilePage.verifyPassbookPhotoRejected(gv.aDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}
}
