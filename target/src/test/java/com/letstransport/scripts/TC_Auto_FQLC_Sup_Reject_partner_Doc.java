package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_FQLC_Sup_Reject_partner_Doc extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "Test case : FQLC Sup reject Submitted profile ")

	public void TC_004_reject_Submitted_Docuements() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_FQLC_Sup_Reject_partner_Doc");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :TC_Verify_FQLC_Sup_Reject_partner_Doc", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.navigateToVerifyDashBoardViaFQLCSuper(gv.wDriver);
		
		wdInit.o_VerifyDashBoardPage.rejectSubmittedDocuements(gv.wDriver, gv.randomMobileNumber,
			GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		
		//wdInit.o_VerifyDashBoardPage.rejectSubmittedDocuements(gv.wDriver, "9450139514",
			//GenericLib.sAppTestDataPath, Onboarding, TC_Signup);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.fqlcSupervisor(gv.wDriver);
		wdInit.o_HomePage.fqlc(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.verifyDashBoard(gv.wDriver);
		
		wdInit.o_VerifyDashBoardPage.verifyUploadDocuements(gv.wDriver,gv.randomMobileNumber,
			GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		//wdInit.o_VerifyDashBoardPage.verifyUploadDocuements(gv.wDriver,"9450139514",
		//GenericLib.sAppTestDataPath, Onboarding, TC_Signup);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}

}
