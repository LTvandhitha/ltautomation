package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Listing_Filter_by_Client_and_HubName extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : filter by Client and Hub name(Active)")

	public void TC_001_listing_filter_by_Client_and_Hub_name_Active() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Auto_Listing_Filter_by_Client_and_HubName");
		// write testcase name in report
		MyExtentListners.test
				.info(MarkupHelper.createLabel("TESTCASE : TC_Auto_Listing_Filter_by_Client_and_HubName", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		//<<<<<<<<<<<<creating Listing from FO-App>>>>>>>>>>>>>>>>>>>>	
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.LoadBoard(gv.wDriver);
		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
		try{wdInit.o_LoadBoardPage.deleteDupListing(gv.wDriver, GenericLib.sPortalTestDataPath,Listing_Sheet);
		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);}
		catch(Exception e){System.out.println("looks like there is duplicate listing");}
		finally{
		wdInit.o_LoadBoardPage.createListing(GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.refreshListing();
		WebActionUtil.waitTillPageLoad("Loading Lisitngs", gv.wDriver, "Listings page", 300);
		wdInit.o_LoadBoardPage.searchListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.verifyCreatedListing(GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		
		//<<<<<<<<<<<<Applying the above Listing from partner-App>>>>>>>>>>>>>>>>>>>>
		wdInit.o_LoadBoardPage.selectFilterByHubAndAraNme(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet);
		wdInit.o_LoadBoardPage.searchListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.verifyCreatedListing(GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.deleteListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		}
	}
}
