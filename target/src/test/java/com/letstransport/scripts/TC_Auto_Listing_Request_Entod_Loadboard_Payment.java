package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Listing_Request_Entod_Loadboard_Payment extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "Request entod loadboard payment")

	public void TC_0068_Request_entod_loadboard_payment() throws Exception {
		// assign category
				MyExtentListners.test.assignCategory("TC_Verify_User_is_able_to_Request_Entod_Loadboard_Payment");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_User_is_able_to_Request_Entod_Loadboard_Payment", ExtentColor.CYAN));

				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
				
			    wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.LoadBoard(gv.wDriver);
				
				WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
				try{wdInit.o_LoadBoardPage.deleteDupListing(gv.wDriver, GenericLib.sPortalTestDataPath,Listing_Sheet);
				WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);}
				catch(Exception e){System.out.println("looks like there is duplicate listing");}
				finally{
				wdInit.o_LoadBoardPage.createListing(GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.refreshListing();
				WebActionUtil.waitTillPageLoad("Loading Lisitngs", gv.wDriver, "Listings page", 300);
				wdInit.o_LoadBoardPage.searchListing(gv.wDriver, GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.verifyCreatedListing(GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);

				Thread.sleep(2000);
				MobileActionUtil.switchToWebView(gv.aDriver);
				MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
				adInit.a_homepage.applyListing(GenericLib.sAppTestDataPath, App_Listing, TC_Listing,gv.appTimeStamp);
				adInit.a_homepage.clickApply(gv.aDriver);
				adInit.a_homepage.selectVehicle(gv.aDriver,3);
				adInit.a_homepage.clickOkAndWait();
				adInit.a_homepage.verifyAfterApply(GenericLib.sAppTestDataPath, App_Listing, TC_Listing,gv.appTimeStamp);
				
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_HomePage.supply(gv.wDriver);
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.LoadBoard(gv.wDriver);
				WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
				wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.ApproveQuoteStatus(gv.wDriver);
				wdInit.o_HomePage.clickLogout(gv.wDriver);
				wdInit.o_login.ReLogin(gv.wDriver,gv.sPortal_Username);
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.LoadBoard(gv.wDriver);
				WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
				wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.ApproveOPM(gv.wDriver);

				wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.verifyApprovedListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_HomePage.cR(gv.wDriver);
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.LoadBoard(gv.wDriver);
				WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
				wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.convertBooking(gv.wDriver);
				wdInit.o_LoadBoardPage.goToCrtdBookingList(gv.wDriver);
				wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
				wdInit.o_LoadBoardPage.verifyBookingCreated(gv.wDriver);
				
				Thread.sleep(2000);
				MobileActionUtil.switchToWebView(gv.aDriver);
				MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
				adInit.a_homepage.verifyAfterBooking(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);
				
				wdInit.o_HomePage.clickLogout(gv.wDriver);
				wdInit.o_login.ReLogin(gv.wDriver,gv.sPortal_Username);
			
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01);
				wdInit.o_HomePage.searchLBooking(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,TC_Listing,gv.displayedTimeStamp);
				wdInit.o_HomePage.selectLGoing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,TC_Listing,gv.displayedTimeStamp);
				wdInit.o_HomePage.addVehicleDetails(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,TC_Listing,gv.displayedTimeStamp);
				wdInit.o_HomePage.floatingBtn(gv.wDriver);
				wdInit.o_HomePage.entOdArrnged(gv.wDriver);
				wdInit.o_EntODArrangedPage.requestEntodPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,TC_Listing,gv.displayedTimeStamp);
				wdInit.o_EntODArrangedPage.navigateBack(gv.wDriver);
				
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_HomePage.financeLink(gv.wDriver);
				wdInit.o_FinancePage.downloadCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE);
				wdInit.o_FinancePage.verifyLData(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE,gv.sREQ_STATUS);
				//wdInit.o_FinancePage.deleteFile(gv.wDriver,gv.sREQUESTED_FILE);
				wdInit.o_HomePage.clickLogout(gv.wDriver);
		}		
	}
}
