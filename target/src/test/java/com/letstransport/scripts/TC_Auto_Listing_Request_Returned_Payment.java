package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Listing_Request_Returned_Payment extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "re-request entod loadboard payment")

	public void TC_0072_Raise_rerequest_entod_loadboard_payment() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_verify_User_is_able_to_Request_Returned_Payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_verify_User_is_able_to_Request_Returned_Payment", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.selectArea(gv.wDriver,GenericLib.sPortalTestDataPath,Booking_Sheet,TC_Booking_01);
		wdInit.o_HomePage.floatingBtn(gv.wDriver);
		wdInit.o_HomePage.entOdArrnged(gv.wDriver);
		wdInit.o_EntODArrangedPage.requestEntodPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,TC_Listing,gv.displayedTimeStamp);
		wdInit.o_EntODArrangedPage.navigateBack(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.verifyLCorrectedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCOR_STATUS);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		
	}
}
