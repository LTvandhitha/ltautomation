package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Re_request_Returned_Payment extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : re-request returned payment")
	public void TC_0037_request_Payment_Schedule_Vehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Re_request_of_Returned_Payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Re_request_of_Returned_Payment", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
       
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.payments(gv.wDriver);
		wdInit.o_HomePage.processVehiclePayments(gv.wDriver);
		wdInit.o_ProcessVehiclePaymentsPage.raiseReturnedPayment(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sRET_STATUS);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.verifyCorrectedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sCOR_STATUS);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
