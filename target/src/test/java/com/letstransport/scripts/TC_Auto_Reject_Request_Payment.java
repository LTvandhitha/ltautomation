package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_Reject_Request_Payment extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = "Test case : reject requested payment")
	public void TC_0053_request_Payment_Schedule_Vehicle() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Rejecting_Payment_Request");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Rejecting_Payment_Request", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.downloadCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE);
		wdInit.o_FinancePage.verifyData(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREQUESTED_FILE,gv.sREQ_STATUS);
		wdInit.o_FinancePage.raiseRejected(gv.wDriver,gv.sREQUESTED_FILE,gv.sREJECTED_FILE,gv.sREQ_STATUS,gv.sREJ_STATUS);
		wdInit.o_FinancePage.uploadCSV(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment,gv.sREJECTED_FILE,gv.sREJ_STATUS);
		//wdInit.o_FinancePage.deleteFile(gv.wDriver,gv.sREQUESTED_FILE);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
	}
}
