package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_UC_Listing_Dismiss_OPM_Approval extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "Dismiss OPM approval of UC listing")

	public void TC_001_UClisting_Dismiss_OPM_approval() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_User_Can_Dismiss_UC_Listing_OPM_Approval");
		// write testcase name in report
		MyExtentListners.test.info(
				MarkupHelper.createLabel("TestCase: TC_Verify_User_Can_Dismiss_UC_Listing_OPM_Approval", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		WebActionUtil.waitTillPageLoad("signing in", gv.wDriver, "HomePage", 300);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.LoadBoard(gv.wDriver);

		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
		try{wdInit.o_LoadBoardPage.deleteDupListing(gv.wDriver, GenericLib.sPortalTestDataPath,Listing_Sheet);
		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);}
		catch(Exception e){System.out.println("looks like there is duplicate listing");}
		finally{
		wdInit.o_LoadBoardPage.createListingUpCountry(GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.refreshListing();
		WebActionUtil.waitTillPageLoad("Loading Lisitngs", gv.wDriver, "Listings page", 300);
		wdInit.o_LoadBoardPage.searchListing(gv.wDriver, GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.verifyCreatedListing(GenericLib.sPortalTestDataPath, Listing_Sheet,gv.portalTimeStamp);

		Thread.sleep(2000);
		MobileActionUtil.switchToWebView(gv.aDriver);
		MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		adInit.a_homepage.switchToUpCountry(gv.aDriver);
		adInit.a_homepage.applyListingUpCountry(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);
		adInit.a_homepage.clickApply(gv.aDriver);
		adInit.a_homepage.selectVehicle(gv.aDriver,4);
		adInit.a_homepage.clickOkAndWait();
		adInit.a_homepage.verifyUpCntryLstngAfterApply(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.supply(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.LoadBoard(gv.wDriver);
		WebActionUtil.waitTillPageLoad("Loadboard", gv.wDriver, "Listing page", 300);
		wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.ApproveQuoteStatus(gv.wDriver);
		wdInit.o_HomePage.clickLogout(gv.wDriver);
		wdInit.o_login.ReLogin(gv.wDriver,gv.sPortal_Username);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.LoadBoard(gv.wDriver);
		
		wdInit.o_LoadBoardPage.selectListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		wdInit.o_LoadBoardPage.DismissOPMApproval(gv.wDriver);
		
		Thread.sleep(2000);
		MobileActionUtil.switchToWebView(gv.aDriver);
		MobileActionUtil.waitTillPageLoad("Loading screen ", gv.aDriver, "HomePage ", 300);
		adInit.a_homepage.verifyUCAfterDismiss(GenericLib.sAppTestDataPath, App_Listing,TC_Listing,gv.appTimeStamp);
		
		wdInit.o_LoadBoardPage.deleteListing(gv.wDriver,GenericLib.sPortalTestDataPath,Listing_Sheet,gv.portalTimeStamp);
		}
	}
}