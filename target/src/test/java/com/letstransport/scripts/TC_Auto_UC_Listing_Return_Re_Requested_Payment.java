package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_Auto_UC_Listing_Return_Re_Requested_Payment extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "return re-request UC entod loadboard payment")

	public void TC_0071_Return_rerequest_UCentod_loadboard_payment() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Auto_UC_Listing_Return_Re-Requested_payment");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Auto_UC_Listing_Return_Re-Requested_payment", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		wdInit.o_HomePage.financeLink(gv.wDriver);
		wdInit.o_FinancePage.returnLRaisedRequest(gv.wDriver,GenericLib.sPortalTestDataPath,Request_Payment_Sheet,TC_Request_Payment);
		wdInit.o_HomePage.clickLogout(gv.wDriver);	
	 }

}
