package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_Auto_Update_Driver_Pricing extends BaseLib implements TestDataCoulmns {

	@Test(enabled = true, priority = 1, description = " Test case : edit added Driver pricing ")
	public void TC_0026_Edit_Added_Driver_pricing() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_Verify_Driver_Pricing_Updation");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : TC_Verify_Driver_Pricing_Updation", ExtentColor.CYAN));

		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);

		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.costingRevenueModel(gv.wDriver);
		wdInit.o_HomePage.scheduledVehicleCosting(gv.wDriver);
		wdInit.o_WebAddCostingPage.scheduledVehicleCosting(gv.wDriver);
		WebActionUtil.switchToTab(gv.wDriver, 1);
		wdInit.o_WebLtPaymentDashboardPage.pricingPage_Login(gv.spricing_Username, gv.spricing_Password, gv.wDriver);
		try{wdInit.o_ClientAndDriverPricingPage.deleteDriverPricing(gv.wDriver,gv.randomName);}
		catch(Exception e){System.out.println("Looks like client Pricing is already deleted");}
		finally{
		wdInit.o_ClientAndDriverPricingPage.addDriverPricing(gv.wDriver,gv.randomName1);
		wdInit.o_ClientAndDriverPricingPage.verifyAddedDriverPricing(gv.wDriver,gv.randomName1);
		
		wdInit.o_ClientAndDriverPricingPage.updateDriverPricing(gv.wDriver,gv.randomName1);
		wdInit.o_ClientAndDriverPricingPage.verifyUpdatedDriverPricing(gv.wDriver,gv.randomName1);
		
		wdInit.o_ClientAndDriverPricingPage.deleteDriverPricing(gv.wDriver,gv.randomName1);
		wdInit.o_ClientAndDriverPricingPage.verifyDeletedDriverPricing(gv.wDriver,gv.randomName1);
		}
	}
}
