package com.letstransport.scripts;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_AddNotVerifiedVehcileDetails_TodayBooking extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "Test case :  To verify if we add not verified vehcile details in Today booking  it should reflect under Today booking of customer relation.")
	public void TC_AddNotVerifiedVehcileDetails_TodayBooking() throws Exception {
		// assign category
		MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_AddNotVerifiedVehcileDetails_TodayBooking");
		// write testcase name in report
		MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE :  To verify if we add not verified vehcile details in Today booking  it should reflect under Today booking of customer relation.",ExtentColor.CYAN));

		
		InitializePages wdInit = new InitializePages(gv.wDriver);
		InitializePages adInit = new InitializePages(gv.aDriver);
		
		wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
		wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
		
		wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_HomePage.oPM(gv.wDriver);
		
		wdInit.o_WebCustomerRelationsPage.EnTODBookingForTodayNotVerifiedVehcile(gv.wDriver);
		wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
		wdInit.o_WebCustomerRelationsPage.customerRelationsLink1(gv.wDriver);
		wdInit.o_WebCustomerRelationsPage.opm(gv.wDriver);
		WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 11000, gv.wDriver);
		wdInit.o_HomePage.clickOnAllList(gv.wDriver);
		wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
		
		wdInit.o_WebCustomerRelationsPage.EnTODAttendenceForTodayNotVerifiedVeh(gv.wDriver);
		Thread.sleep(3000);
		String vendorname = WebActionUtil.generateRandomString(4);
		String vehnum=MobileActionUtil.generateRandomString(2,4);
		wdInit.o_WebCustomerRelationsPage.EnTODAddnotVerifiedVechileDetailsForToday(gv.wDriver,vendorname,vehnum);
		wdInit.o_WebCustomerRelationsPage.verify_EnTODAddnotVerifiedVechileDetailsTodayBooking(gv.wDriver);
		
	}
}
