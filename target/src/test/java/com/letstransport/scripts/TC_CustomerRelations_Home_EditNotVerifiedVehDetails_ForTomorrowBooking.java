package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForTomorrowBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "To verify if we edit driver details of non verified vehicle in tomorrow booking ,edited driver details should reflect under Tomorrow Booking ")
	public void TC_ENTOD_EditNotVerifiedVehDetails_ForTomorrowBooking() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForTomorrowBooking");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if we edit driver details of non verified vehicle in tomorrow booking ,edited driver details should reflect under Tomorrow Booking ", ExtentColor.CYAN));

				
				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
			    wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				String vehnum=MobileActionUtil.generateRandomString(2,4);
				String vendorname = WebActionUtil.generateRandomString(4);
				wdInit.o_WebCustomerRelationsPage.tomorrowTabOPM(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.verify_toModifyDriverDetailsFor_TomorrowBooking(gv.wDriver, vendorname,vehnum);
	}
}
