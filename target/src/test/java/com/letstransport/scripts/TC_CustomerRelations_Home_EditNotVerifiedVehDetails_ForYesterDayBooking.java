package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.MobileActionUtil;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForYesterDayBooking extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "To verify if we edit driver details of non verified vehicle in Yesterday booking ,edited driver details should reflect under Yesterday Booking")
	public void TC_ENTOD_EditNotVerifiedVehDetails_ForYesterDayBooking() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_EditNotVerifiedVehDetails_ForYesterDayBooking");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify if we edit driver details of non verified vehicle in Yesterday booking ,edited driver details should reflect under Yesterday Booking ", ExtentColor.CYAN));

				
				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
			    wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				/*wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);*/
				String vehnum=MobileActionUtil.generateRandomString(2,4);
				String vendorname = WebActionUtil.generateRandomString(4);
				wdInit.o_WebCustomerRelationsPage.yesterdayTabOPM(gv.wDriver); 
				wdInit.o_WebCustomerRelationsPage.verify_toModifyDriverDetailsForYesterdayBooking(gv.wDriver, vendorname,vehnum);
	}
}
