package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;

public class TC_CustomerRelations_Home_RepeatBooking_ForTomorrowVerifedVeh extends BaseLib implements TestDataCoulmns{
	@Test(enabled = true, priority = 1, description = "Test case :To verify  Repeat booking for Tomorrow verified vehicle should reflect in TodayBooking")
	public void TC_RepeatBooking_ForTomorrowBooking() throws Exception{
		// assign category
				MyExtentListners.test
						.assignCategory("TC_CustomerRelations_Home_RepeatBooking_ForTomorrowVerifedVeh");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel(
						"TESTCASE :To verify  Repeat booking for Tomorrow verified vehicle should reflect in TodayBooking",
						ExtentColor.CYAN));

				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);

				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);

				wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations","TC_CustomerRelations_Home_001");
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_HomePage.oPM(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.verify_TomorrowRepeatBookingForToday(gv.wDriver);
				
	}
}
