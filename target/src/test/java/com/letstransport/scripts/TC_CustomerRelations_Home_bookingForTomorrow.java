
package com.letstransport.scripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.letstransport.init.InitializePages;
import com.letstransport.init.TestDataCoulmns;
import com.letstransport.library.BaseLib;
import com.letstransport.library.GenericLib;
import com.letstransport.listener.MyExtentListners;
import com.letstransport.util.WebActionUtil;

public class TC_CustomerRelations_Home_bookingForTomorrow extends BaseLib implements TestDataCoulmns {
	@Test(enabled = true, priority = 1, description = "To verify  Booking For Tomorrow")
	public void TC_003_ENTOD_Booking_ForTomorrow() throws Exception{
		// assign category
				MyExtentListners.test.assignCategory("TC_CustomerRelations_Home_Booking For Tomorrow");
				// write testcase name in report
				MyExtentListners.test.info(MarkupHelper.createLabel("TESTCASE : To verify  Booking For Tomorrow", ExtentColor.CYAN));

				InitializePages wdInit = new InitializePages(gv.wDriver);
				InitializePages adInit = new InitializePages(gv.aDriver);
				
				wdInit.o_login.fo_Login(gv.sPortal_Username, gv.sPortal_Password, gv.wDriver);
				wdInit.o_HomePage.verifyUserLogin(gv.wDriver);
				
				
				wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				Thread.sleep(2000);
				wdInit.o_HomePage.oPM(gv.wDriver); 
				wdInit.o_WebCustomerRelationsPage.EnTODBookingForTommorrow(gv.wDriver);
				wdInit.o_HomePage.leftNavigationalBar(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.customerRelationsLink1(gv.wDriver);
				wdInit.o_WebCustomerRelationsPage.opm(gv.wDriver);
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, gv.wDriver);
				wdInit.o_HomePage.clickOnAllList(gv.wDriver);
				WebActionUtil.waitForinvisiblityofElement("//div[@class='loader-backgroud-div']", 15000, gv.wDriver);
				wdInit.o_HomePage.selectArea1(gv.wDriver, GenericLib.sPortalTestDataPath, "CustomerRelations", "TC_CustomerRelations_Home_001");
				wdInit.o_WebCustomerRelationsPage.verify_EnTODBookingForTommorrow(gv.wDriver);
				
	}
}
